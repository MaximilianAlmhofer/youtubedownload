﻿using Lib.Common;
using Lib.Message.Messages;
using Lib.Model;
using Lib.Model.PlaylistEditor;

using Microsoft.Extensions.Options;

using MvvmCross.Plugin.Messenger;
using MvvmCross.WeakSubscription;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

using xtra_snd_tools.Infra.ViewModels;

namespace Modules.PlaylistEditor
{
	public class PlaylistTitleInputRequest
	{

		public Action<string> TitleCallback { get; set; }

		public string Information { get; set; }
	}

	public class AddPlaylistRequest : ViewModelBase, IAddPlaylistRequest, INotifyDataErrorInfo
	{
		private readonly ILocalPlaylist playlist;
		private readonly IDataErrorInfo errorInfo;
		internal IDictionary<string, List<string>> validationErrors = new Dictionary<string, List<string>>();

		public AddPlaylistRequest(IMvxMessenger messenger, IOptions<GlobalConfig> options) : base(messenger)
		{
			playlist = new LocalPlaylist(options);
			errorInfo = new DataErrorInfo(this);
		}

		public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged = delegate { };

		
		public string Title
		{
			get => playlist.Title;
			set
			{
				playlist.Title = value;
				RaisePropertyChanged();
			}
		}

		private ObservableCollection<PlaylistEntry> entries;
		public ObservableCollection<PlaylistEntry> Entries
		{
			get
			{
				return entries ??= new ObservableCollection<PlaylistEntry>();
			}
		}

		public bool HasErrors
		{
			get
			{
				return validationErrors.Count > 0;
			}
		}

		public IEnumerable GetErrors(string propertyName)
		{
			return validationErrors[propertyName];
		}

		object IAddPlaylistRequest.GetPlaylist()
		{
			return playlist;
		}

		IDataErrorInfo IAddPlaylistRequest.ErrorInfo
		{
			get
			{
				return errorInfo;
			}
		}




		internal class DataErrorInfo : IDataErrorInfo
		{

			private readonly AddPlaylistRequest request;

			public DataErrorInfo(AddPlaylistRequest request)
			{
				this.request = request;
			}

			public string this[string columnName]
			{
				get
				{
					return request.GetErrors(columnName)?.Cast<string>()?.LastOrDefault();
				}
			}

			public string Error
			{
				get
				{
					var sb = new StringBuilder();

					foreach (var item in request.validationErrors)
					{
						foreach (var error in request.GetErrors(item.Key).Cast<string>())
						{
							sb.AppendLine(error);
						}
					}

					return sb.ToString();
				}
			}
		}
	}
}
