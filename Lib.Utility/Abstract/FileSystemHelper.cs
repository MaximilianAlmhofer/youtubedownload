﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Text;

namespace Lib.Utility.Abstract
{
	internal static class FileSystemHelper
	{
		[return: NotNullIfNotNull("path")]
		public static string EnsureExtendedPrefixIfNeeded(string path)
		{
			if (path != null && (path.Length >= 260 || EndsWithPeriodOrSpace(path)))
			{
				return EnsureExtendedPrefix(path);
			}
			return path;
		}

		public static bool EndsWithPeriodOrSpace(string path)
		{
			if (string.IsNullOrEmpty(path))
			{
				return false;
			}
			char c = path[path.Length - 1];
			if (c != ' ')
			{
				return c == '.';
			}
			return true;
		}

		public static string EnsureExtendedPrefix(string path)
		{
			if (IsPartiallyQualified(path.AsSpan()) || IsDevice(path.AsSpan()))
			{
				return path;
			}
			if (path.StartsWith("\\\\", StringComparison.OrdinalIgnoreCase))
			{
				return path.Insert(2, "?\\UNC\\");
			}
			return "\\\\?\\" + path;
		}

		public static bool IsDevice(ReadOnlySpan<char> path)
		{
			if (!IsExtended(path))
			{
				if (path.Length >= 4 && IsDirectorySeparator(path[0]) && IsDirectorySeparator(path[1]) && (path[2] == '.' || path[2] == '?'))
				{
					return IsDirectorySeparator(path[3]);
				}
				return false;
			}
			return true;
		}

		public static bool IsExtended(ReadOnlySpan<char> path)
		{
			if (path.Length >= 4 && path[0] == '\\' && (path[1] == '\\' || path[1] == '?') && path[2] == '?')
			{
				return path[3] == '\\';
			}
			return false;
		}

		public static bool IsPartiallyQualified(ReadOnlySpan<char> path)
		{
			if (path.Length < 2)
			{
				return true;
			}
			if (IsDirectorySeparator(path[0]))
			{
				if (path[1] != '?')
				{
					return !IsDirectorySeparator(path[1]);
				}
				return false;
			}
			if (path.Length >= 3 && path[1] == ':' && IsDirectorySeparator(path[2]))
			{
				return !IsValidDriveChar(path[0]);
			}
			return true;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool IsDirectorySeparator(char c)
		{
			if (c != '\\')
			{
				return c == '/';
			}
			return true;
		}

		public static bool IsValidDriveChar(char value)
		{
			if (value < 'A' || value > 'Z')
			{
				if (value >= 'a')
				{
					return value <= 'z';
				}
				return false;
			}
			return true;
		}
	}
}
