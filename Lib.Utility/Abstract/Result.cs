﻿using System;

namespace Lib.Utility.Abstract
{
	public class Result<T> : Result
	{
		public T Value
		{
			get;
		}

		protected internal Result(T value, bool success, string error)
			: base(success, error)
		{
			Ensure.IsTrue(value != null || !success);
			Value = value;
		}
	}
	public class Result
	{
		public bool Success
		{
			get;
		}

		public string Error
		{
			get;
		}

		public Exception Exception
		{
			get;
		}

		public bool Failure => !Success;

		protected Result(bool success, string error = null, Exception exception = null)
		{
			Success = success;
			Error = (error ?? "");
			Exception = exception;
		}

		public static Result Fail(string message = null)
		{
			return new Result(success: false, message);
		}

		public static Result Fail(Exception exception)
		{
			return new Result(success: false, "", exception);
		}

		public static Result<T> Fail<T>(string message = "")
		{
			return new Result<T>(default, success: false, message);
		}

		public static Result Ok()
		{
			return new Result(success: true, "");
		}

		public static Result<T> Ok<T>(T value)
		{
			return new Result<T>(value, success: true, "");
		}
	}
}
