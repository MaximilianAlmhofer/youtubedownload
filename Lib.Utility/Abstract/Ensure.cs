﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Lib.Utility.Abstract
{
	public static class Ensure
	{
		public static void NotEmpty(string arg, string argName, string message = null)
		{
			if (!string.IsNullOrEmpty(arg))
			{
				return;
			}
			if (string.IsNullOrEmpty(message))
			{
				throw new ArgumentException("ArgumentException: " + argName + " string not valid.");
			}
			throw new ArgumentException(message ?? "");
		}

		public static void NotEmpty<T>(IEnumerable<T> arg, string argName)
		{
			if (arg == null || !arg.Any())
			{
				throw new ArgumentException("ArgumentException: sequence " + argName + " is empty or null");
			}
		}

		public static void NotNull<T>(T arg, [CallerMemberName] string argName = null) where T : class
		{
			if (arg == null)
			{
				throw new ArgumentNullException("ArgumentException: " + argName + " is null");
			}
		}

		public static void NotNullOrEmpty(string arg)
		{
			if (string.IsNullOrEmpty(arg))
			{
				throw new ArgumentNullException("ArgumentException: " + nameof(arg) + " is null");
			}
		}

		public static void IsTrue(bool condition, string message = "")
		{
			if (!condition)
			{
				throw new ArgumentException("Condition not satisfied: " + message);
			}
		}

		public static void IsFalse(bool condition, string message = "")
		{
			if (condition)
			{
				throw new ArgumentException("Condition not satisfied: " + message);
			}
		}
	}
}
