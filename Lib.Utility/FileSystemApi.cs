﻿using System;
using System.Runtime.InteropServices;

using Lib.Utility.Abstract;

namespace Lib.Utility
{
	internal class FileSystemApi
	{
		internal struct FILE_TIME
		{
			internal uint dwLowDateTime;

			internal uint dwHighDateTime;

			internal long ToTicks()
			{
				return (long)(((ulong)dwHighDateTime << 32) + dwLowDateTime);
			}

			internal DateTimeOffset ToDateTimeOffset()
			{
				return DateTimeOffset.FromFileTime(ToTicks());
			}
		}

		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
		internal struct WIN32_FIND_DATA
		{
			internal uint dwFileAttributes;

			internal FILE_TIME ftCreationTime;

			internal FILE_TIME ftLastAccessTime;

			internal FILE_TIME ftLastWriteTime;

			internal uint nFileSizeHigh;

			internal uint nFileSizeLow;

			internal uint dwReserved0;

			internal uint dwReserved1;

			private unsafe fixed char _cFileName[260];

			private unsafe fixed char _cAlternateFileName[14];

			internal unsafe ReadOnlySpan<char> cFileName
			{
				get
				{
					fixed (char* pointer = _cFileName)
					{
						return new ReadOnlySpan<char>(pointer, 260);
					}
				}
			}
		}

		internal enum FINDEX_INFO_LEVELS : uint
		{
			FindExInfoStandard = 0u,
			FindExInfoBasic = 1u,
			FindExInfoMaxInfoLevel = 2u
		}

		internal enum FINDEX_SEARCH_OPS : uint
		{
			FindExSearchNameMatch = 0u,
			FindExSearchLimitToDirectories = 1u,
			FindExSearchLimitToDevices = 2u,
			FindExSearchMaxSearchOp = 3u
		}

		[DllImport("kernel32.dll", CharSet = CharSet.Unicode, EntryPoint = "FindFirstFileExW", SetLastError = true)]
		private static extern SafeFindHandle FindFirstFileExPrivate(string lpFileName,
			FINDEX_INFO_LEVELS fInfoLevelId, ref WIN32_FIND_DATA lpFindFileData,
			FINDEX_SEARCH_OPS fSearchOp,
			IntPtr lpSearchFilter,
			int dwAdditionalFlags);

		internal static SafeFindHandle FindFirstFile(string fileName, ref WIN32_FIND_DATA data)
		{
			fileName = FileSystemHelper.EnsureExtendedPrefixIfNeeded(fileName);
			return FindFirstFileExPrivate(fileName, FINDEX_INFO_LEVELS.FindExInfoBasic, ref data, FINDEX_SEARCH_OPS.FindExSearchNameMatch, IntPtr.Zero, 0);
		}
	}

	internal sealed class SafeFindHandle : SafeHandle
	{
		public override bool IsInvalid
		{
			get
			{
				if (!(handle == IntPtr.Zero))
				{
					return handle == new IntPtr(-1);
				}
				return true;
			}
		}

		internal SafeFindHandle()
			: base(IntPtr.Zero, ownsHandle: true)
		{
		}


		internal IntPtr Handle => handle;

		protected override bool ReleaseHandle()
		{
			return FindClose(handle);
		}

		[DllImport("kernel32.dll", SetLastError = true)]
		internal static extern bool FindClose(IntPtr hFindFile);
	}
}
