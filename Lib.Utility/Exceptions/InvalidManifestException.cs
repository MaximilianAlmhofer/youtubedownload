﻿
using System;

namespace Lib.Utility
{
	public class InvalidManifestException : Exception
	{
		private readonly string manifestJson;

		protected InvalidManifestException()
		{
		}

		protected InvalidManifestException(string message) : base(message)
		{
		}

		protected InvalidManifestException(string message, Exception innerException) : base(message, innerException)
		{
		}

		public InvalidManifestException(string manifestJson, bool isPlaylist)
			: this(StringFormatter.Format("Fehlerhaftes Metadatenmanifest bei {0}\r\n", isPlaylist ? "Playlist" : "Track"))
		{
			this.manifestJson = manifestJson;
		}

		public InvalidManifestException(string manifestJson, bool isPlaylist, Exception innerException)
			: this(StringFormatter.Format("Fehlerhaftes Metadatenmanifest bei {0}\r\n", isPlaylist ? "Playlist" : "Track"), innerException)
		{
			this.manifestJson = manifestJson;
		}

		public string LongMessage => StringFormatter.Format("{0}\r\n{1}", Message, manifestJson);
	}
}
