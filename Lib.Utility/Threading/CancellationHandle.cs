﻿using System.Threading;

using Lib.Utility.Abstract;

namespace Lib.Utility.Threading
{
	public sealed class CancellationHandle
	{
		private volatile CancellationTokenSource _tokenSource = new CancellationTokenSource();

		public CancellationToken Token => _tokenSource.Token;

		public CancellationHandle()
		{

		}

		public CancellationHandle(CancellationHandle other, CancellationToken linkedToken)
		{
			_tokenSource = CancellationTokenSource.CreateLinkedTokenSource(linkedToken, other.Token);
		}

		public void Cancel()
		{
			CancellationTokenSource value = new CancellationTokenSource();
			CancellationTokenSource tokenSource;
			CancellationTokenSource cancellationTokenSource;
			do
			{
				tokenSource = _tokenSource;
				cancellationTokenSource = Interlocked.CompareExchange(ref _tokenSource, value, tokenSource);
			}
			while (cancellationTokenSource != tokenSource);
			cancellationTokenSource.Cancel();
			cancellationTokenSource.Dispose();
		}


		public static CancellationHandle CreateLinkedTokenSource(CancellationHandle handle, CancellationToken linkedToken)
		{
			return new CancellationHandle(handle, linkedToken);
		}
	}
}
