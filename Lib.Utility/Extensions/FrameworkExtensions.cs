﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;

using Lib.Common;

using MvvmCross.IoC;

namespace Lib.Utility
{
	public static partial class FrameworkExtensions
	{
		public static T FindVisualChild<T> (this DependencyObject parent, params Type[] skipTypes) where T : DependencyObject
		{
			return FindNextVisualChild<T>(parent, skipTypes);
		}

		public static T FindNextVisualChild<T> (this DependencyObject parent, params Type[] skipTypes) where T : DependencyObject
		{
			T foundChild = default;
			skipTypes = skipTypes ?? Array.Empty<Type>();

			// Die Null-Prüfung geschieht in GetChildrenCount
			for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
			{
				DependencyObject child = VisualTreeHelper.GetChild(parent, i);

				if (child != null)
				{
					if (child is T)
					{
						foundChild = (T)child;
						break;
					}
					if (!skipTypes.Contains(child.DependencyObjectType.SystemType))
					{
						foundChild = child.FindNextVisualChild<T>(skipTypes);
						if (foundChild is null)
							continue;
						break;
					}
				}
			}
			#region T ist kein IAddChild-Objekt
			if (parent is ContentControl)
			{
				if (parent.GetValue(ContentControl.ContentProperty) is DependencyObject content)
				{
					foundChild = content switch
					{
						T _ => (T)content,
						_ => content.FindNextVisualChild<T>(skipTypes)
					};
				}
			}
			#endregion
			return foundChild;
		}

		public static IEnumerable<T> FindVisualChildren<T> (this DependencyObject rootObject) where T : DependencyObject
		{
			if (rootObject is null)
				yield break;

			for (int i = 0; i < VisualTreeHelper.GetChildrenCount(rootObject); i++)
			{
				DependencyObject child = VisualTreeHelper.GetChild(rootObject, i);

				if (child != null && child is T)
				{
					yield return (T)child;
				}

				T childsChild = FindNextVisualChild<T>(child);
				if (childsChild != null)
				{
					yield return childsChild;
				}
			}
		}

		public static T FindParent<T> (this DependencyObject child) where T : DependencyObject
		{
			DependencyObject parent = VisualTreeHelper.GetParent(child);
			do
			{
				T matchedParent = parent as T;
				if (matchedParent != null)
					return matchedParent;
				parent = VisualTreeHelper.GetParent(parent);
			}
			while (parent != null);

			return null;
		}

		public static bool GetValidUniformResourceLocator (this System.Windows.IDataObject data, out Uri downloadSource)
		{
			downloadSource = default;
			if (data != null && data.GetDataPresent("UniformResourceLocator"))
			{
				MemoryStream mem = new MemoryStream();
				try
				{
					((MemoryStream)data.GetData("UniformResourceLocator", true))?.CopyTo(mem);
					string url = Encoding.UTF8.GetString(mem?.ToArray()).TrimEnd(new char[] { '\0' });
					downloadSource = new Uri(url);
				}
				catch (ArgumentNullException ex)
				{
					ex.Trace(ex.Message, "Die Zwischenablage war leer.");
				}
				finally
				{
					mem.Close();
					mem.Dispose();
				}
			}
			return downloadSource is null || Uri.IsWellFormedUriString(downloadSource.OriginalString, UriKind.Absolute);
		}
	}
}
