﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;

namespace Lib.Utility.Extensions
{
	public static class ExceptionExtensions
	{
		public static bool IsFileAccessExcetion(this Exception exception)
		{
			if (!(exception is IOException))
			{
				return exception is UnauthorizedAccessException;
			}
			return true;
		}

		public static string CombinedMessage(this Exception exception)
		{
			return string.Join(" ---> ", from ex in ThisAndInnerExceptions(exception)
										 select ex.Message);
		}

		public static bool IsRegistryAccessException(this Exception ex)
		{
			if (!(ex is SecurityException) && !(ex is UnauthorizedAccessException))
			{
				return ex is IOException;
			}
			return true;
		}

		private static IEnumerable<Exception> ThisAndInnerExceptions(Exception e)
		{
			while (e != null)
			{
				yield return e;
				e = e.InnerException;
			}
		}
	}
}
