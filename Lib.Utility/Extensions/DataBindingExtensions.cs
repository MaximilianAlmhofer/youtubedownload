﻿
using System;
using System.Windows;
using System.Windows.Data;

namespace Lib.Utility
{

	public static class DataBindingExtensions
	{
		/// <summary>
		/// Binds the specified data source.
		/// </summary>
		/// <param name="dataSource">The data source.</param>
		/// <param name="sourcePath">The source path.</param>
		/// <param name="destinationObject">The destination object.</param>
		/// <param name="dp">The dp.</param>
		public static void Bind(object dataSource, string sourcePath, FrameworkElement destinationObject, DependencyProperty dp)
		{
			Bind(dataSource, sourcePath, destinationObject, dp, null, BindingMode.Default, null);
		}

		/// <summary>
		/// Binds the specified data source.
		/// </summary>
		/// <param name="dataSource">The data source.</param>
		/// <param name="sourcePath">The source path.</param>
		/// <param name="destinationObject">The destination object.</param>
		/// <param name="dp">The dp.</param>
		/// <param name="bindingMode">The binding mode.</param>
		public static void Bind(object dataSource, string sourcePath, FrameworkElement destinationObject, DependencyProperty dp, BindingMode bindingMode)
		{
			Bind(dataSource, sourcePath, destinationObject, dp, null, bindingMode, null);
		}

		/// <summary>
		/// Binds the specified data source.
		/// </summary>
		/// <param name="dataSource">The data source.</param>
		/// <param name="sourcePath">The source path.</param>
		/// <param name="destinationObject">The destination object.</param>
		/// <param name="dp">The dp.</param>
		/// <param name="stringFormat">The string format.</param>
		public static void Bind(object dataSource, string sourcePath, FrameworkElement destinationObject, DependencyProperty dp, string stringFormat)
		{
			Bind(dataSource, sourcePath, destinationObject, dp, stringFormat, BindingMode.Default, null);
		}

		/// <summary>
		/// Binds the specified data source.
		/// </summary>
		/// <param name="dataSource">The data source.</param>
		/// <param name="sourcePath">The source path.</param>
		/// <param name="destinationObject">The destination object.</param>
		/// <param name="dp">The dp.</param>
		/// <param name="stringFormat">The string format.</param>
		/// <param name="bindingMode">The binding mode.</param>
		public static void Bind(object dataSource, string sourcePath, FrameworkElement destinationObject, DependencyProperty dp, string stringFormat, BindingMode bindingMode)
		{
			Bind(dataSource, sourcePath, destinationObject, dp, stringFormat, bindingMode, null);
		}

		/// <summary>
		/// Binds the specified data source.
		/// </summary>
		/// <param name="dataSource">The data source.</param>
		/// <param name="sourcePath">The source path.</param>
		/// <param name="destinationObject">The destination object.</param>
		/// <param name="dp">The dp.</param>
		/// <param name="stringFormat">The string format.</param>
		/// <param name="bindingMode">The binding mode.</param>
		/// <param name="converter">The converter.</param>
		/// <exception cref="ArgumentNullException">
		/// dataSource
		/// or
		/// destinationObject
		/// or
		/// dp
		/// </exception>
		/// <exception cref="ArgumentException">message - sourcePath</exception>
		public static void Bind(object dataSource, string sourcePath, FrameworkElement destinationObject, DependencyProperty dp, string stringFormat, BindingMode bindingMode, IValueConverter converter)
		{
			#region NULL Checks
			if (dataSource is null)
			{
				throw new ArgumentNullException(nameof(dataSource));
			}

			if (string.IsNullOrEmpty(sourcePath))
			{
				throw new ArgumentException("message", nameof(sourcePath));
			}

			if (destinationObject is null)
			{
				throw new ArgumentNullException(nameof(destinationObject));
			}

			if (dp is null)
			{
				throw new ArgumentNullException(nameof(dp));
			}
			#endregion

			Binding binding = new Binding
			{
				Source = dataSource,
				UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
				Mode = bindingMode,
				Path = new PropertyPath(sourcePath),
				StringFormat = stringFormat,
				Converter = converter
			};
			destinationObject.SetBinding(dp, binding);
		}

		/// <summary>
		/// Updates the binding.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="property">The property.</param>
		public static void UpdateBinding(this object source, DependencyProperty property)
		{
			if (source is null)
			{
				return;
			}

			if (source is FrameworkElement e)
			{
				BindingExpressionBase be = e.GetBindingExpression(property);
				if (be != null)
				{
					be.UpdateSource();
					be.UpdateTarget();
				}
			}
		}
	}
}
