﻿using System.IO;
using System.Runtime.InteropServices;

namespace Lib.Utility.Extensions
{
	public static class FileSystemExtensions
	{
		public static Abstract.Result Open(this FileInfo file, FileAccess fileAccess)
		{
			FileSystemApi.WIN32_FIND_DATA data = default(FileSystemApi.WIN32_FIND_DATA);
			SafeFindHandle hFile = FileSystemApi.FindFirstFile(file.FullName, ref data);
			if (hFile.IsInvalid)
			{
				return Abstract.Result.Fail($"HRESULT: {Marshal.GetLastWin32Error()}");
			}
			var fileHandle = new Microsoft.Win32.SafeHandles.SafeFileHandle(hFile.Handle, false);
			return Abstract.Result<FileStream>.Ok<FileStream>(new FileStream(fileHandle, fileAccess));
		}
	}
}
