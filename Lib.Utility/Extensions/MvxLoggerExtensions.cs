﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

using Lib.Utility.Extensions;
using Lib.Utility.Logging;

using MvvmCross.Logging;

namespace Lib.Utility
{
	public static class MvxLoggerExtensions
	{
		public static void Logged(this ILogger logger, Action action, string message)
		{
			logger.Logged(action, () => message);
		}

		public static void Logged(this ILogger logger, Action action, Func<string> message)
		{
			logger.Logged((Func<bool>)Function, message);
			bool Function()
			{
				action();
				return false;
			}
		}

		public static T Logged<T>(this ILogger logger, Func<T> function, string message)
		{
			return logger.Logged(function, () => message);
		}

		public static T Logged<T>(this ILogger logger, Func<T> function, Func<string> message)
		{
			try
			{
				return function();
			}
			catch (Exception exception)
			{
				logger.Error(message() + ": " + exception.CombinedMessage());
				throw;
			}
		}

		private static void _LogCaller(this ILogger logger, MvxLogLevel logLevel, Type typeToLogFor)
		{
			MethodBase caller = GetCaller(typeToLogFor);
			logger.Debug(StringFormatter.Format("[LOGGER CALL LOCATION]:\r\n" +
					  "Declaring type:\t{0}\r\n" +
					  "Logging member:\t{1}",
				null, caller.DeclaringType.Name, caller.Name));
		}

		private static MethodBase GetCaller(Type declaringType)
		{
			return new StackTrace(true).GetFrames()
				.LastOrDefault(x => x.GetMethod()
				?.DeclaringType == declaringType)
				?.GetMethod();
		}
	}
}
