﻿using System;
using System.Globalization;

namespace Lib.Utility
{
	public class StringFormatter
	{

		private readonly CultureInfo formatCulture;
		private readonly IFormatProvider formatProvider;

		public StringFormatter(IFormatProvider formatProvider, CultureInfo culture)
		{
			formatCulture = culture ?? CultureInfo.CurrentUICulture;
			this.formatProvider = formatProvider ?? throw null;
		}


		public static string Format(string value, params object[] args)
		{
			return string.Format(CultureInfo.InvariantCulture, value, args);
		}

		public string CultureFormat(string value, params object[] args)
		{
			return string.Format(formatCulture, value, args);
		}

		public string FormatValue(string format, params object[] args)
		{
			return string.Format(formatProvider, format, args);
		}


		public static string Constant(string constant)
		{
			return Format(constant);
		}
	}
}
