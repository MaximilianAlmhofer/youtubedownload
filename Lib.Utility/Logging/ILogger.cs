﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lib.Utility.Logging
{
	public interface ILogger
	{
		void Info(string message);

		void Error(string message);

		void Fatal(string message);

		void Debug(string message);

		void Trace(string message);

		void Warn(string message);

		void Error(Exception exception);

		bool IsTraceEnabled();
	}
}
