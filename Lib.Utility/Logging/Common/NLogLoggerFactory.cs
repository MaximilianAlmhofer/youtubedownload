﻿using Lib.Utility.Logging.Configuration;

using NLog;

namespace Lib.Utility.Logging.Common
{
	public class NLogLoggerFactory : ILoggerFactory
	{

		public NLogLoggerFactory(ILoggerConfiguration config)
		{
			config.Setup();
		}

		public ILogger Logger()
		{
			return new NLogLogger(LogManager.GetLogger("logger"));
		}
	}
}
