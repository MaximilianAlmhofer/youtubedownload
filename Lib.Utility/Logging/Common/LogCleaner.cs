﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

using Lib.Utility.Extensions;

using Microsoft.Win32;

namespace Lib.Utility.Logging.Common
{
	internal class LogCleaner
	{
		private readonly ILogger _logger;

		public LogCleaner(ILogger logger)
		{
			_logger = logger;
		}

		public void Cleanup(string directory, int maxFiles)
		{
			var files = GetFiles(directory);
		}

		private IEnumerable<FileInfo> GetFiles(string directory)
		{
			try
			{
				return new DirectoryInfo(directory).GetFiles();
			}
			catch (Exception ex) when (ex.IsFileAccessExcetion())
			{
				return new FileInfo[0];
			}
		}

		private DateTime CreationTime(FileInfo file)
		{
			try
			{
				return file.CreationTimeUtc;
			}
			catch (Exception ex) when (ex.IsFileAccessExcetion() || ex is ArgumentOutOfRangeException)
			{

				_logger.Error("In " + nameof(CreationTime) + " :" + Environment.NewLine + ex.Message);
				return DateTime.MinValue;
			}
		}

		private void DeleteFiles(IEnumerable<FileInfo> files)
		{
			foreach (var file in files)
			{
				DeleteFile(file.FullName);
			}
		}

		private void DeleteFile(string fileName)
		{
			try
			{
				File.Delete(fileName);
			}
			catch (Exception)
			{

				throw;
			}
		}
	}
}
