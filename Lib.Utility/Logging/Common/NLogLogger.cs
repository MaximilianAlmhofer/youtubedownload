﻿using System;

namespace Lib.Utility.Logging.Common
{
	internal class NLogLogger : ILogger
	{
		private readonly NLog.Logger _logger;

		public NLogLogger(NLog.Logger logger)
		{
			_logger = logger;
		}

		public void Info(string message)
		{
			_logger.Info(message);
		}

		public void Error(string message)
		{
			_logger.Error(message);
		}

		public void Fatal(string message)
		{
			_logger.Fatal(message);
		}

		public void Debug(string message)
		{
			_logger.Debug(message);
		}

		public void Warn(string message)
		{
			_logger.Warn(message);
		}

		public void Error(Exception exception)
		{
			_logger.Error(exception);
		}

		public void Trace(string message)
		{
			_logger.Trace(message);
		}

		public bool IsTraceEnabled()
		{
			return _logger.IsTraceEnabled;
		}
	}
}
