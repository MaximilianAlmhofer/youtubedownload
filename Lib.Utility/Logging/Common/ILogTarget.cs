﻿using NLog.Targets;

namespace Lib.Utility.Logging.Common
{
	public interface ILogTarget
	{
		T Target<T>(string targetName) where T : Target;
	}
}