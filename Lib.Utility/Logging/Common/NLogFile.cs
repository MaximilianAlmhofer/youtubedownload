﻿using System;
using System.IO;
using System.IO.Compression;

using Lib.Utility.Abstract;
using Lib.Utility.Extensions;

using NLog;
using NLog.Layouts;
using NLog.Targets;
using NLog.Targets.Wrappers;

namespace Lib.Utility.Logging.Common
{
	public sealed class NLogFile : ILogTarget
	{
		private readonly ILogger _logger;

		private readonly FileTarget _target;

		public NLogFile(ILogger logger, string targetName)
			: this(logger, _Target<FileTarget>(targetName))
		{
		}


		internal NLogFile(ILogger logger, FileTarget target)
		{
			Abstract.Ensure.NotNull<ILogger>(logger, nameof(logger));
			Abstract.Ensure.NotNull<FileTarget>(target, nameof(target));
			_logger = logger;
			_target = target;
		}

		public string PlainContent()
		{
			return SafeContent((string f) => PlainFileContent(f));
		}

		public byte[] ZippedContent(string targetName)
		{
			return SafeContent((string f) => ZippedFileContent(f));
		}

		private T SafeContent<T>(Func<string, T> func)
		{
			try
			{
				return LogContent(func);
			}
			catch (IOException exception)
			{
				_logger.Error(exception);
			}
			catch (UnauthorizedAccessException exception2)
			{
				_logger.Error(exception2);
			}
			catch (NotSupportedException exception3)
			{
				_logger.Error(exception3);
			}
			return default(T);
		}

		private T LogContent<T>(Func<string, T> func)
		{
			string text = LogFileName(_target);
			if (string.IsNullOrEmpty(text))
			{
				throw new NotSupportedException("Unable to get latest log file name");
			}
			T result = default(T);
			if (text != null && File.Exists(text))
			{
				return func(text);
			}
			return result;
		}

		private byte[] ZippedFileContent(string fileName)
		{
			Ensure.NotNullOrEmpty(fileName);
			using (var memStream = new MemoryStream())
			{
				using (ZipArchive archive = new ZipArchive(memStream))
				{
					using (Stream dest = archive.CreateEntry(fileName, CompressionLevel.Optimal).Open())
					{
						using (FileStream fs = ((Result<FileStream>)new FileInfo(fileName).Open(FileAccess.Read)).Value)
						{
							try
							{
								fs.CopyTo(dest);
							}
							catch (Exception)
							{
								fs.Close();
								using (var fileStream = File.OpenRead(fileName))
								{
									fileStream.CopyTo(dest);
								}
							}
						}
					}
				}
				return memStream.ToArray();
			}
		}

		private string PlainFileContent(string fileName)
		{
			Ensure.NotNullOrEmpty(fileName);
			return File.ReadAllText(fileName);
		}


		private string LogFileName(string targetName)
		{
			return LogFileName(_Target<FileTarget>(targetName));
		}


		private string LogFileName(FileTarget fileTarget)
		{
			Abstract.Ensure.NotNull<FileTarget>(fileTarget, nameof(fileTarget));
			if (!(fileTarget.FileName is SimpleLayout))
				return null;
			LogEventInfo logEvent = new LogEventInfo
			{
				TimeStamp = DateTime.UtcNow.ToLocalTime()
			};
			return fileTarget.FileName.Render(logEvent);
		}

		T ILogTarget.Target<T>(string targetName)
		{
			return _Target<T>(targetName);
		}

		private static T _Target<T>(string targetName) where T : Target
		{
			Abstract.Ensure.NotNullOrEmpty(targetName);
			Target target = LogManager.Configuration?.FindTargetByName(targetName);
			while (target != null)
			{
				if (target is WrapperTargetBase wrapper)
				{
					target = wrapper.WrappedTarget;
				}
			}
			return target as T;
		}
	}
}
