﻿namespace Lib.Utility.Logging
{
	public interface ILoggerFactory
	{
		ILogger Logger();
	}
}
