﻿using System.Runtime.CompilerServices;
using System.Text;

using Lib.Utility.Abstract;
using Lib.Utility.Logging.Common;

using NLog;
using NLog.Config;
using NLog.Targets;
using NLog.Targets.Wrappers;

namespace Lib.Utility.Logging.Configuration
{

	internal class NLogLoggingConfiguration : ILoggerConfiguration
	{
		private FileTarget _fileTarget;

		private readonly string _logFolderPath;

		private readonly string _prefix;

		public NLogLoggingConfiguration(string logFolderPath, string prefix)
		{
			_logFolderPath = logFolderPath;
			_prefix = prefix;
		}

		public ILoggerConfiguration Setup()
		{
			Ensure.NotEmpty(_logFolderPath, "_logFolderPath");
			Ensure.NotEmpty(_prefix, "_prefix");
			LoggingConfiguration loggingConfiguration = new LoggingConfiguration();
			FileTarget obj = new FileTarget
			{
				FileName = _logFolderPath + "/" + _prefix + ".txt",
				Layout = "${longdate} ${level:uppercase=true} ${message} ${exception:format=tostring}",
				Encoding = Encoding.UTF8,
				KeepFileOpen = true,
				ConcurrentWrites = false,
				OptimizeBufferReuse = true,
				OpenFileCacheTimeout = 30,
				ArchiveEvery = FileArchivePeriod.Day,
				ArchiveAboveSize = 490000L,
				ArchiveFileName = _logFolderPath + "/" + _prefix + ".{#}.txt",
				ArchiveNumbering = ArchiveNumberingMode.DateAndSequence,
				MaxArchiveFiles = 30,
				ArchiveDateFormat = "yyyy-MM-dd"
			};
			FileTarget wrappedTarget = obj;
			_fileTarget = obj;
			AsyncTargetWrapper target = new AsyncTargetWrapper("file", wrappedTarget);
			loggingConfiguration.AddTarget(target);
			loggingConfiguration.LoggingRules.Add(new LoggingRule("*", LogLevel.Debug, target));
			LogManager.Configuration = loggingConfiguration;
			return this;
		}

		public ILogTarget LogFile(ILogger logger)
		{
			return new NLogFile(logger, _fileTarget);
		}
	}
}
