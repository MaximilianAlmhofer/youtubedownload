﻿using System;

namespace Lib.Utility.Logging.Configuration
{
	public static class LoggerConfigurationBuilderExtensions
	{
		public static LoggerConfigurationBuilder WithDirectory(this LoggerConfigurationBuilder builder, Func<string> directoryConfigurator)
		{
			builder.logFolderPath = directoryConfigurator();
			return builder;
		}

		public static LoggerConfigurationBuilder WithPrefix(this LoggerConfigurationBuilder builder, Func<string> directoryConfigurator)
		{
			builder.prefix = directoryConfigurator();
			return builder;
		}
	}
}
