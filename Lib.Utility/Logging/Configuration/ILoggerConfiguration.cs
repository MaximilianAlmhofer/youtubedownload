﻿using Lib.Utility.Logging.Common;

namespace Lib.Utility.Logging.Configuration
{
	public interface ILoggerConfiguration
	{
		ILoggerConfiguration Setup();

		ILogTarget LogFile(ILogger logger);
	}
}