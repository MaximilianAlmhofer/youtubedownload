﻿
using Lib.Utility.Abstract;

namespace Lib.Utility.Logging.Configuration
{
	public class LoggerConfigurationBuilder
	{
		internal string logFolderPath;
		internal string prefix;

		public ILoggerConfiguration Build()
		{
			Ensure.NotNullOrEmpty(logFolderPath);
			Ensure.NotNullOrEmpty(prefix);
			return new NLogLoggingConfiguration(logFolderPath, prefix);
		}
	}
}
