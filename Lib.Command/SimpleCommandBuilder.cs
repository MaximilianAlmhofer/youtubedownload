﻿using Lib.Common;
using Lib.Process;

using System.Diagnostics;
using System.Globalization;
using System.Text;

namespace Lib.Command
{

	public class SimpleCommandBuilder : CommandBuilder
	{
		private readonly DownloadOptions userPrefs;
		private readonly string scripts;

		public SimpleCommandBuilder(DownloadOptions userPreferences, string scriptsPath, string itemId)
			: base(itemId)
		{
			userPrefs = userPreferences;
			scripts = scriptsPath.ToLinux();
		}

		public override IInstruction Build(IDownloadSource model)
		{
			var sb = new StringBuilder("youtube-dl -o '$OUTTEMPL'");
			int formatId = (int)model.GetFormatIdentifier(userPrefs.ExtractAudio);
			if (userPrefs.Archive)
			{
				sb.AppendFormat(CultureInfo.InvariantCulture, " --download-archive {0}", userPrefs.ArchiveFile.ToLinux());
			}
			if (userPrefs.ExtractAudio)
			{
				sb.AppendFormat(CultureInfo.InvariantCulture, " -x --audio-format {0}", userPrefs.AudioFormat);
			}
			sb.AppendFormat(CultureInfo.InvariantCulture, " --format {0}", formatId);
			sb.AppendFormat(CultureInfo.InvariantCulture, " --audio-quality {0} {1} --restrict-filenames --add-metadata", userPrefs.AudioQuality, model.GetWebpageUri().Query.Substring(3));
			Trace.WriteLine(sb.ToString() + "\r\n" + queryMetadataProcessArgs.WorkingDirectory);

			return new ProcessArgs(ProcessBehavior.DefaulBehavior,
				"bash.exe", string.Format(CultureInfo.InvariantCulture, " -ic \"{0}\"", sb.ToString()), queryMetadataProcessArgs.WorkingDirectory);
		}


		protected override void PrepareMetadataCommand(string itemId, string workingDirectory)
		{
			if (!string.IsNullOrEmpty(itemId))
			{
				queryMetadataProcessArgs = new ProcessArgs(
				ProcessBehavior.DefaulBehavior,
				"bash.exe", string.Format(CultureInfo.InvariantCulture, " -ic \"/bin/bash {0}/mddump.sh {1}\"", scripts, itemId), workingDirectory);
			}
		}
	}
}
