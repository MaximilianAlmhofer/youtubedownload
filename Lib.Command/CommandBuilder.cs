﻿using Lib.Common;
using Lib.Process;

using System;
using System.Drawing;
using System.IO;
using System.Text;

using Lib.Utility;

namespace Lib.Command
{
	public abstract class CommandBuilder
	{
		protected string id;
		protected ProcessArgs queryMetadataProcessArgs;

		public abstract IInstruction Build(IDownloadSource model);

		protected abstract void PrepareMetadataCommand(string itemId, string workingDirectory);

		protected CommandBuilder(string itemId)
		{
			id = itemId;
		}

		public IInstruction GetMetadataCommand(string workingDirectory)
		{
			PrepareMetadataCommand(id, workingDirectory);

			return queryMetadataProcessArgs;
		}

		public static CommandBuilder CreateFromUrl(Uri source, GlobalConfig config)
		{
			if (source == null)
			{
				throw new ArgumentNullException(nameof(source));
			}
			if (source.Query.Split('&').Length > 1)
			{
				if (source.Query.Split('&')[1].Split('=')[0] == "list")
				{
					return new PlaylistCommandBuilder(config.DownloadOptions, config.AppFolder.ScriptsFolderPath, source.Query.Split('&')[1].Split('=')[1]);
				}
				// Hier ist der Playlist teil der Url ungülig. Schmeißen, oder 1. Track nehmen ...?
				throw new UriFormatException(StringFormatter.Format(
					"Informationen zur Playlist konnten dem Query-Segment nicht entnommen werden!\r\nQuerysegment: {0}\r\nAbsolute Url: {1}",
					source.Query.Split('&')[1], source.AbsoluteUri));
			}
			return new SimpleCommandBuilder(config.DownloadOptions, config.AppFolder.ScriptsFolderPath, source.OriginalString);
		}

		public static bool GenerateScript(string fileName, string content)
		{
			if (!File.Exists(fileName))
			{
				try
				{
					File.WriteAllText(fileName, new StringBuilder("#!/bin/bash")
						.AppendLine()
						.AppendLine(content)
						.ToString());
					return true;
				}
				catch (Exception ex)
				{
					ex.Trace();
				}
			}
			return false;
		}


		public static IInstruction GetYoutubeDlVersionCommand()
		{
			return GetCommand("wsl.exe", string.Format(" -- youtube-dl --version"));
		}


		public static IInstruction GetInstallYoutubeDlCommand()
		{
			return GetCommand("wsl.exe", string.Format(" -u root -- sudo apt -y install youtube-dl"));
		}


		public static IInstruction GetAllFeaturesCommand()
		{
			return GetCommand("dism.exe", string.Format(" /Online /Get-Features /Format:List"), "C:\\Windows\\System32");
		}



		public static IInstruction InvokeScript(string verb, string arg)
		{
			if (string.IsNullOrEmpty(verb))
				throw new ArgumentNullException(nameof(verb));

			string powerShellargument = $"{verb}-WindowsOptionalFeature -Online -FeatureName {arg}";
			return GetPowershellCommand(powerShellargument);
		}



		private static IInstruction GetPowershellCommand(string argument, string workingDirectory = null, ProcessBehavior behaviour = null)
		{
			return GetCommand("pwsh.exe", " -Command " + argument, workingDirectory, behaviour);
		}



		private static IInstruction GetCommand(string executable, string args, string workingDirectory = null, ProcessBehavior behaviour = null)
		{
			var cmd = new ProcessArgs(behaviour ?? ProcessBehavior.DefaulBehavior, executable, args, workingDirectory ?? Directory.GetCurrentDirectory());
			System.Diagnostics.Debug.WriteLine(cmd.Command);
			return cmd;
		}
	}
}
