﻿/* Hier wird das Befehlszeilenkommando für den Process.Start()-Aufruf gekapselt.
 * 
 * Diese Klasse fungiert als Message zwischen dem UIThread und dem der den IPC-Aufruf durchführt.
 * Über die Result-Eigenschaft wird zuerst während des IPC-Vorganges der Status/Fortschritt/stdout des IPC-Prozesses per Callback 
 * an den UI-Thread zurück-überliefert, sowie am Ende das Ergebnis und bei Fehlern entsprechende Information dazu.
 */

using Lib.Common;

using System.Threading;

namespace Lib.Command
{
	
	public class ManagedProcessCommand
	{
		/// <summary>
		/// Kommando fürm die Befehlszeile
		/// </summary>
		public IInstruction Instruction { get; set; }

		/// <summary>
		/// Zugriff auf Status, Ergebnis, Fehler
		/// </summary>
		public IResult Result { get; set; }

		/// <summary>
		/// Zur Steuerung des Vorgangs (Unterbrechung, Fortsetzung, Koordination d. Callbacks)
		/// </summary>
		public ManualResetEventSlim SetEvent { get; }

		/// <summary>
		/// Zur Steuerung des Vorgangs (Unterbrechung, Fortsetzung, Koordination d. Callbacks)
		/// </summary>
		public ManualResetEvent ResetEvent { get; }

		/// <summary>
		/// Eine Instanz, die als Message zwischen dem UIThread und dem der den IPC-Aufruf durchführt, fungiert.
		/// </summary>
		/// <param name="instruction">Der Befehl an den IPC-Prozess</param>
		/// <param name="waitHandle">Übergibt eine Referenz auf ein Waithandle im ViewModel.</param>
		public ManagedProcessCommand(IInstruction instruction, ManualResetEventSlim waitHandle)
		{
			Instruction = instruction;
			SetEvent = waitHandle;
			ResetEvent = new ManualResetEvent(false);
			Result = new StateResult("");
		}
	}
}
