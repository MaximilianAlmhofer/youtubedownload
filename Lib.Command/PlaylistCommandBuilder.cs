﻿using Lib.Common;
using Lib.Process;

using System.Diagnostics;
using System.IO;
using System.Text;

namespace Lib.Command
{
	public class PlaylistCommandBuilder : CommandBuilder
	{
		private readonly DownloadOptions userPrefs;
		private readonly string scripts;
		private readonly string script;

		public PlaylistCommandBuilder(DownloadOptions userSettings, string scriptsPath, string itemId) : base(itemId)
		{
			userPrefs = userSettings;

			script = Path.Combine(scriptsPath, userPrefs.ExtractAudio ? "execalistdl.sh" : "execvlistdl.sh").ToLinux();
			scripts = scriptsPath.ToLinux();
		}

		public override IInstruction Build(IDownloadSource model)
		{
			StringBuilder sb = new StringBuilder($" /bin/bash {script}");
			string formatId = (string)model.GetFormatIdentifier(userPrefs.ExtractAudio);
			if (userPrefs.ExtractAudio)
			{
				sb.AppendFormat(" {0} {1} {2} {3} {4} {5}", userPrefs.PlaylistStart, userPrefs.PlaylistEnd,
				userPrefs.AudioFormat, formatId, userPrefs.AudioQuality, model.GetWebpageUri().Query.Split('&')[1].Split('=')[1]);
			}
			else
			{
				sb.AppendFormat(" {0} {1} {2} {3} {4}", userPrefs.PlaylistStart, userPrefs.PlaylistEnd,
				userPrefs.VideoFormat, userPrefs.AudioQuality, model.GetWebpageUri().Query.Split('&')[1].Split('=')[1]);
			}

			Trace.WriteLine(sb.ToString() + "\r\n" + queryMetadataProcessArgs.WorkingDirectory);
			return new ProcessArgs(ProcessBehavior.DefaulBehavior, "bash.exe", string.Format(" -ic \"{0}\"", sb.ToString(), queryMetadataProcessArgs.WorkingDirectory));
		}

		protected override void PrepareMetadataCommand(string itemId, string workingDirectory)
		{

			if (!string.IsNullOrEmpty(itemId))
			{
				queryMetadataProcessArgs = new ProcessArgs(
				ProcessBehavior.DefaulBehavior,
				"bash.exe", string.Format(" -ic \"/bin/bash {0}/mdlistdump.sh {1} {2} {3}\"", scripts,
				userPrefs.PlaylistStart, userPrefs.PlaylistEnd, itemId), workingDirectory);
			}
		}
	}
}
