#pragma once

#include <iostream>
#include <Windows.h>

#include <shobjidl.h>
#include <propvarutil.h>
#include <propsys.h>
#include <propkey.h>
#include <cassert>

void ShowQuality(WAVEFORMATEX* pWave)
{
    // Store the average bytes per second in a local variable
    // with a more manageable name.
    DWORD dwBps = pWave->nAvgBytesPerSec;

    // Verify that the value is a VBR quality level by using 
    // a bitmask to check for the bit pattern 0x7FFFFFXX. 
    if (dwBps & (0x7FFFFF00 == 0x7FFFFF00))
        printf("VBR Quality: %d%%\n", (dwBps & 0x000000FF));
    else // Not a valid VBR quality value.
        printf("Not a valid one-pass VBR audio format.\n");
}


errno_t* GetFileProperties(LPCWSTR const lpfileName, IPropertyStore* pStore)
{
    errno_t* perror = nullptr;
    
    if (strlen((const char*)lpfileName) <= 0)
    {
        perror = (errno_t*)ERROR_BAD_LENGTH;
        std::cout << perror;
    }
    else
    {
        HRESULT result;
        if ((result = SHGetPropertyStoreFromParsingName(*(&lpfileName), NULL, GPS_READWRITE, __uuidof(IPropertyStore), (void**)&pStore)) >= 0)
        {
            perror = (errno_t*)(int)result;
        }
        IPropertyStore* ppStore = (IPropertyStore*)(void**)pStore;
    }
    return perror;
}

BOOL GetPropertyValue(IPropertyStore** pStore, const PROPERTYKEY key, PROPVARIANT& variant)
{
    if ((*pStore)->GetValue(key, &variant) == 0)
    {
        return TRUE;
    }

    return FALSE;
}
