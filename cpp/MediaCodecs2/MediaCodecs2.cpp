// MediaCodecs2.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include <iostream>
#include "MediaCodecs2.h"
#include "MediaAttributes.h"

extern void ShowQuality(WAVEFORMATEX* pWave);

int main()
{

    CoInitialize(NULL);

    std::cout << "ZEASSS !!!\n";
    std::cout << "Media codecs2...!\n";

    WAVEFORMATEX wfmtEx = WAVEFORMATEX();
    wfmtEx.wFormatTag = (WORD)0x163;
    wfmtEx.nBlockAlign = 0x8;
    wfmtEx.nChannels = 0x2;
    wfmtEx.nSamplesPerSec = 0x5A0;
    wfmtEx.wBitsPerSample = 0x20;
    
    wfmtEx.nAvgBytesPerSec = wfmtEx.cbSize * wfmtEx.nBlockAlign * wfmtEx.nChannels;

    WAVEFORMATEX* p_wFmtEx = &wfmtEx;
    ShowQuality(p_wFmtEx);

    std::cout << "Samples per second: \t " << p_wFmtEx->nSamplesPerSec << "\n";
    std::cout << "Avg bytes per second:\t " << p_wFmtEx->nAvgBytesPerSec << "\n";
    std::cout << "Bits per sample:\t " << p_wFmtEx->wBitsPerSample << "\n";
    std::cout << "Num channels:\t " << p_wFmtEx->nChannels << "\n";

    char buf[4] = { ' ',' ',' ',' ' };
    _itoa_s(p_wFmtEx->wFormatTag, buf, 10);
    buf[3] = '\0';
    printf("Format tag: \t %s\n", buf);

    IPropertyStore* pStore = (IPropertyStore*)malloc(sizeof(IPropertyStore*));
    errno_t *err = GetFileProperties(LPCWSTR(L"E:\\wsl\\home\\max\\youtube-dl\\dl\\Liquid_Drum_Bass_Mix_2020_18-igiiMi2ZIMo.mp3"), pStore);
    
    if (*err > 0)
    {
        size_t size = 25;
        char buffer[25];
        snprintf(buffer, size, "Fehler bei GetFileProperties: %d", *err);
        buffer[24] = '\0';
        perror(buffer);
    }

    PROPVARIANT variant;
    if (!GetPropertyValue(&pStore, PKEY_Media_EncodingSettings, variant))
    {
        std::cout << "Fehler beim Lesen der Properties!!" << std::endl;
        pStore->Release();
    }
    else 
    {
        std::cout << variant.bstrVal << std::endl;
        std::cout << "\n" << std::endl;
        std::cout << variant.date << std::endl;
        std::cout << variant.pdecVal << std::endl;

        variant = *((PROPVARIANT*)NULL);
        pStore->Release();
    }

    HRESULT hr = ShowGetAttributes();

    if (!SUCCEEDED(hr))
    {
        printf("HRESULT: %d\n", hr);
    }


    std::cin;
}

// Programm ausführen: STRG+F5 oder Menüeintrag "Debuggen" > "Starten ohne Debuggen starten"
// Programm debuggen: F5 oder "Debuggen" > Menü "Debuggen starten"

// Tipps für den Einstieg: 
//   1. Verwenden Sie das Projektmappen-Explorer-Fenster zum Hinzufügen/Verwalten von Dateien.
//   2. Verwenden Sie das Team Explorer-Fenster zum Herstellen einer Verbindung mit der Quellcodeverwaltung.
//   3. Verwenden Sie das Ausgabefenster, um die Buildausgabe und andere Nachrichten anzuzeigen.
//   4. Verwenden Sie das Fenster "Fehlerliste", um Fehler anzuzeigen.
//   5. Wechseln Sie zu "Projekt" > "Neues Element hinzufügen", um neue Codedateien zu erstellen, bzw. zu "Projekt" > "Vorhandenes Element hinzufügen", um dem Projekt vorhandene Codedateien hinzuzufügen.
//   6. Um dieses Projekt später erneut zu öffnen, wechseln Sie zu "Datei" > "Öffnen" > "Projekt", und wählen Sie die SLN-Datei aus.
