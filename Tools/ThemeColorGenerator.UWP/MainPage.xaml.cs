﻿
using System;
using System.IO;
using System.Linq;

using ToolsLib.UWP;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;

namespace ThemeColorGenerator.UWP
{
	/// <summary>
	/// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
	/// </summary>
	public sealed partial class MainPage : Windows.UI.Xaml.Controls.Page
	{
		static readonly char[] HEX = new char[17] 
		{ 
			'#', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
			'A', 'B', 'C', 'D', 'E', 'F' 
		};

		public MainPage()
		{
			InitializeComponent();
			DataContext = new MainWindowViewModel();
			Loaded += MainWindow_Loaded;
		}

		private void MainWindow_Loaded(object sender, RoutedEventArgs e)
		{
			MainWindowViewModel vm = (MainWindowViewModel)DataContext;
			vm.XamlCodeGenerated += ViewModel_XamlCodeGenerated;

			((Frame)Window.Current.Content).Navigate(typeof(Page));
		}

		private void ViewModel_XamlCodeGenerated(object sender, XamlCodeGeneratedEventArgs e)
		{
			e.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None)
				.Select(line => new Run { Text = line })
				.ToList().ForEach(run =>
				{
					if (e.CodeType == XamlCodeType.Color)
					{
						textboxContent.Inlines.Add(run);
						textboxContent.Inlines.Add(new LineBreak());
					}
					else
					{
						textboxContent2.Inlines.Add(run);
						textboxContent2.Inlines.Add(new LineBreak());
					}
				});
		}

		private void ButtonReset_Click(object sender, RoutedEventArgs e)
		{
			textboxContent.Inlines.Clear();
			textboxContent2.Inlines.Clear();
			var viewModel = (MainWindowViewModel)DataContext;
			viewModel.Reset();
			txtThemeKey.SelectAll();
			tabControl.SelectedIndex = 0;
			return;
		}

		private void ThemeKey_LosingFocus(UIElement sender, LosingFocusEventArgs e)
		{
			e.Cancel = (from c in (sender as TextBox).Text?.ToCharArray() 
						let b = Path.GetInvalidFileNameChars().Concat(Path.GetInvalidPathChars())
							.Contains(c) select b)
							.Any(_ => _ == true);
		}

		private void HexColorInput_BeforeTextChanging(TextBox sender, TextBoxBeforeTextChangingEventArgs e)
		{
			GetTextChange(sender.Text, e.NewText, out int offset, out int addedLength, out int removedLength);

			if (addedLength > 0)
			{
				string newChars = e.NewText.Substring(offset, addedLength).ToUpper();
				e.Cancel = (from c in newChars let b = !HEX.Contains(c) || (c >= 95 && !HEX.Contains((char)(c - 32))) select b).Any(_ => _ == true);
			}
		}

		private void GetTextChange(string oldText, string newText, out int offset, out int addedLength, out int removedLength)
		{
			offset = -1;
			addedLength = 0;
			removedLength = 0;
			char[] chOld = null, chNew = null;

			if (!string.IsNullOrEmpty(oldText))
				chOld = oldText.ToCharArray();
			if (!string.IsNullOrEmpty(newText))
				chNew = newText.ToCharArray();
			if (chOld is null && chNew != null)
			{
				offset = 0;
				removedLength = 0;
				addedLength = chNew.Length;
			}
			else
			if (chOld != null && chNew is null)
			{
				offset = 0;
				addedLength = 0;
				removedLength = chOld.Length;
			}
			else
			if (chOld is null && chNew is null)
			{
				throw new Exception("Old Textbox value not tracked!");
			}
			else
			{
				removedLength = Math.Max(0, chOld.Length - chNew.Length);
				addedLength = removedLength > 0 ? 0 : chNew.Length - chOld.Length;
				for (int i = 0; i < Math.Max(chOld.Length, chNew.Length); i++)
				{
					if (chOld.Length <= i || chNew.Length <= i || chNew[i] != chOld[i])
					{
						offset = i;
						break;
					}
				}
			}
		}
	}
}
