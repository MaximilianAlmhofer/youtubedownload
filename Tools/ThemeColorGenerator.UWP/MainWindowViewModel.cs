﻿using MvvmCross.Commands;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;

using Windows.UI;
using ToolsLib.UWP;
using System.Reflection;

namespace ThemeColorGenerator.UWP
{
	public class MainWindowViewModel : INotifyPropertyChanged
	{
		
		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			var handler = PropertyChanged;
			handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public bool AreColorsGenerated { get; private set; }

		private string themeKey;

		public string ThemeKey
		{
			get => themeKey;
			set
			{
				themeKey = value;
				OnPropertyChanged();
			}
		}

		private string brushKey;

		public string BrushKey
		{
			get => brushKey;
			set
			{
				brushKey = value;
				OnPropertyChanged();
			}
		}

		private double brushOpacity = 1.0d;

		public double BrushOpacity
		{
			get => brushOpacity;
			set
			{
				brushOpacity = value;
				OnPropertyChanged();
			}
		}

		private string baseColorHexString;

		public string BaseColorHexString
		{
			get => baseColorHexString;
			set
			{
				baseColorHexString = value.ToUpper();
				SetColorFromHexString(ref baseColor, baseColorHexString);
				OnPropertyChanged();
			}
		}

		private Color baseColor = Colors.Transparent;

		private string altColorHexString;

		public string AltColorHexString
		{
			get => altColorHexString;
			set
			{
				altColorHexString = value.ToUpper();
				SetColorFromHexString(ref altColor, altColorHexString);
				OnPropertyChanged();
			}
		}

		private string output;

		public string Output
		{
			get { return output; }
			set 
			{
				output = value;
				OnPropertyChanged();
			}
		}


		public void Reset()
		{
			ramps.Clear();
			sb.Clear();
			Output = string.Empty;
			BaseColorHexString = string.Empty;
			AltColorHexString = string.Empty;
			BrushKey = string.Empty;
			baseColor = Colors.Transparent;
			altColor = Colors.Transparent;
			BrushOpacity = 1D;
			AreColorsGenerated = false;
			foreach (var pInfo in GetType().GetTypeInfo().GetProperties(BindingFlags.Instance|BindingFlags.Public))
				OnPropertyChanged(pInfo.Name);
		}

		private Color altColor = Colors.Transparent;

		private void SetColorFromHexString(ref Color color, string hex)
		{
			if (hex.Length == 6 && HexStringConverter.FromHexString(hex, out var bytes))
			{
				color = bytes.Length == 4 ? Color.FromArgb(bytes[0], bytes[1], bytes[2], bytes[3]) : Color.FromArgb(0, bytes[0], bytes[1], bytes[2]);
			}
		}

		private readonly List<IReadOnlyDictionary<string, Color>> ramps = new List<IReadOnlyDictionary<string, Color>>(2);

		private readonly StringBuilder sb = new StringBuilder();

		public event EventHandler<XamlCodeGeneratedEventArgs> XamlCodeGenerated = delegate { };

		protected virtual void OnXamlCodeGenerated(string xamlCode, XamlCodeType codeType)
		{
			var handler = XamlCodeGenerated;
			handler?.Invoke(this, new XamlCodeGeneratedEventArgs(xamlCode, codeType));
		}

		private MvxCommand<int> generateXamlCodeCommand;

		public ICommand GenerateXamlCodeCommand => generateXamlCodeCommand ??= new MvxCommand<int>(parameter =>
		{
			sb.Clear();
			XamlCodeType codeType = (XamlCodeType)parameter;
			WriteXamlCode(baseColor, ColorRampType.Base, codeType);
			WriteXamlCode(altColor, ColorRampType.Alt, codeType);
			AreColorsGenerated = codeType == XamlCodeType.Color;
			OnPropertyChanged(nameof(AreColorsGenerated));
			//Output += Environment.NewLine + sb.ToString();
			OnXamlCodeGenerated(sb.ToString(), codeType);
		});

		private void WriteXamlCode(Color color, ColorRampType rampCategory, XamlCodeType codeType)
		{
			switch (codeType)
			{
				case XamlCodeType.Color:
					var ccg = new ColorXamlGenerator();
					sb.Append($"*****   Theme Resource - {themeKey}  ***** \r\n");
					sb.Append($"*****   {rampCategory} Colors   ***** \r\n");
					sb.Append(ccg.GenerateXaml(themeKey, rampCategory, color));
					sb.AppendLine();
					ramps.Add(ccg.ColorRamp);
					break;

				case XamlCodeType.SolidColorBrush:
					if (ramps.Count > 0)
					{
						string colorHexString = rampCategory == ColorRampType.Base ? baseColorHexString : altColorHexString;
						var scg = new SolidColorBrushXamlGenerator(ramps[0], ramps[1], brushOpacity);
						sb.Append($"*****   Theme Resource - {themeKey}  ***** \r\n");
						sb.Append($"*****   SolidColorBrush.Color #{colorHexString}  ***** \r\n");
						sb.Append(scg.GenerateXaml(brushKey, rampCategory, color));
						sb.AppendLine();
					}
					break;
			}
		}
	}
}
