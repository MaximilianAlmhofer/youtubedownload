﻿using System;

namespace ToolsLib.UWP
{
	public sealed class XamlCodeGeneratedEventArgs : EventArgs
	{
		public string Text { get; }

		public XamlCodeType CodeType { get; }

		public XamlCodeGeneratedEventArgs(string text, XamlCodeType codeType)
		{
			Text = text;
			CodeType = codeType;
		}
	}
}
