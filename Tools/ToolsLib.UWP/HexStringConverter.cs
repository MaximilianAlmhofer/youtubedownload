﻿using System;

namespace ToolsLib.UWP
{
	public static class HexStringConverter
	{
		public static bool FromHexString(string hex, out byte[] bytes)
		{
			bytes = string.IsNullOrEmpty(hex) ? Array.Empty<byte>() : new byte[hex.Length / 2];
			for (int i = 0; i < bytes.Length; i++)
				bytes[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
			return bytes.Length > 0;
		}
	}
}
