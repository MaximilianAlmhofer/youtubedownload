﻿using System;

using Windows.UI;

namespace ToolsLib.UWP
{
	public sealed class RelativeLuminance
	{

		private Color darkColor;
		private Color lightColor;

		public static double GetContrastRatio(Color foreGround, Color backGround)
		{
			var luminance = new RelativeLuminance(foreGround, backGround);
			return luminance.CalculateContrastRatio();
		}

		public static double Measure(Color color)
		{
			return Measure(color);
		}

		public RelativeLuminance(Color foreGround, Color backGround)
		{
			double lumFG = MeasureLuminance(foreGround);
			double lumBG = MeasureLuminance(backGround);
			if (Math.Max(lumFG, lumBG) == lumBG)
			{
				lightColor = backGround;
				darkColor = foreGround;
			}
			if (Math.Min(lumFG, lumBG) == lumBG)
			{
				lightColor = foreGround;
				darkColor = backGround;
			}
			ContrastRatio = CalculateContrastRatio();
		}

		public double ContrastRatio { get; private set; }

		private double CalculateContrastRatio()
		{
			double y1lum = MeasureLuminance(lightColor);
			double y2lum = MeasureLuminance(darkColor);
			return (y1lum + 0.05) / (y2lum + 0.05);
		}

		private double MeasureLuminance(Color color)
		{
			double r = MeasureRelativeLuminanceOfByteChannel(color.R);
			double g = MeasureRelativeLuminanceOfByteChannel(color.G);
			double b = MeasureRelativeLuminanceOfByteChannel(color.B);
			return 0.2126f * r + 0.7152f * g + 0.0722f * b;
		}

		private static double MeasureRelativeLuminanceOfByteChannel(byte bytev)
		{
			float scRgbChannel = bytev / 255f;
			return MeasureRelativeLuminanceOfByteChannel(scRgbChannel);
		}

		private static double MeasureRelativeLuminanceOfByteChannel(float scRgb_byteChannel)
		{
			float sRgb_byteChannel = ScRgbTosRgb(scRgb_byteChannel);
			return sRgb_byteChannel <= 0.03928 ? sRgb_byteChannel / 12.92 : Math.Pow((sRgb_byteChannel + 0.055f) / 1.055f, 2.4);
		}

		private static byte ScRgbTosRgb(float val)
		{
			if (!(val > 0.0))
			{
				return 0;
			}
			if (val <= 0.0031308)
			{
				return (byte)(255f * val * 12.92f + 0.5f);
			}
			if (val < 1.0)
			{
				return (byte)(255f * (1.055f * (float)Math.Pow(val, 5.0 / 12.0) - 0.055f) + 0.5f);
			}
			return byte.MaxValue;
		}
	}
}
