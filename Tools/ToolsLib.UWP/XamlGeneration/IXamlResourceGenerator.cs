﻿namespace ToolsLib.UWP
{
	internal interface IXamlResourceGenerator<in T>
	{
		string GenerateXaml(string resourceKeyPrefix, ColorRampType rampCategory, T resource);
	}
}
