﻿namespace ToolsLib.UWP
{

	public abstract class XamlGenerator<T> : IXamlGenerator<T>
	{
		public abstract string GenerateXamlCode(string resourceKeyPrefix, T element);
	}
}
