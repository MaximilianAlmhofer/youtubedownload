﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Windows.UI;

namespace ThemeGenerator.UI.Xaml.Data
{
	public class ContrastRatioInformation : IFormattable, INotifyPropertyChanged
	{
		Color color;
		public Color Color
		{
			get => color;
			set
			{
				color = value;
				OnPropertyChanged();
			}
		}

		public string Text { get; set; }

		double ratio;
		public double Ratio 
		{
			get => ratio;
			set
			{
				ratio = value;
				OnPropertyChanged();
			}
		}

		public override string ToString()
		{
			return ConvertToString(null, null);
		}

		public string ToString(IFormatProvider provider)
		{
			return ConvertToString(null, provider);
		}

		string IFormattable.ToString(string format, IFormatProvider formatProvider)
		{
			return ConvertToString(format, formatProvider);
		}

		internal string ConvertToString(string format, IFormatProvider provider)
		{
			return string.Format("{0}:{1},Ratio:{2}", Text, ((IFormattable)Color).ToString(format, provider), Ratio);
		}

		public override bool Equals(object obj)
		{
			if (obj is ContrastRatioInformation information)
			{
				ContrastRatioInformation info;
				info = information;
				return this == info;
			}
			return false;
		}

		public bool Equals(ContrastRatioInformation info)
		{
			return this == info;
		}

		public override int GetHashCode()
		{
			return Color.GetHashCode() ^ Ratio.GetHashCode() ^ Text.GetHashCode();
		}

		public static bool operator ==(ContrastRatioInformation info1, ContrastRatioInformation info2)
		{
			return info1.Color == info2.Color
				&& info1.Ratio == info2.Ratio
				&& info1.Text == info2.Text;
		}

		public static bool operator !=(ContrastRatioInformation info1, ContrastRatioInformation info2)
		{
			return !(info1 == info2);
		}

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			var handler = PropertyChanged;
			handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
