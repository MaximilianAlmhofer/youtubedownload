﻿using Microsoft.Toolkit.Collections;
using Microsoft.Toolkit.Uwp.Helpers;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Windows.Foundation.Metadata;
using Windows.UI;

namespace ThemeGenerator.UI.Xaml.Data
{
	public abstract class ColorPresetInfo : INotifyPropertyChanged
	{
		protected ColorPresetInfo()
		{
			Initialize();
		}

		protected abstract void Initialize();

		public Color PresetColor { get; set; }

		public string PresetColorHex => PresetColor.ToHex();

		public string ThemeName { get; set; }

		public string TargetArea { get; set; }

		public string FlyoutHeadline1 => string.Join(" ", ThemeName.Split(' ')[0], TargetArea.ToLower());

		public IList<ContrastRatioInformation> ContrastRatiosInfo { get; protected set; }

		protected virtual event PropertyChangedEventHandler _PropertyChanged = delegate { };

		event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
		{
			add => _PropertyChanged += value;
			remove => _PropertyChanged -= value;
		}

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			var handler = _PropertyChanged;
			handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}

	public class RegionColorPresetInfo : ColorPresetInfo
	{
		protected override void Initialize()
		{
			ContrastRatiosInfo = new List<ContrastRatioInformation>(3)
			{
				new ContrastRatioInformation
				{
					Text = "Black Text",
					Color = Colors.Black
				},
				new ContrastRatioInformation
				{
					Text = "Light Base",
					Color = Colors.White
				},
				new ContrastRatioInformation
				{
					Text = "Light Primary",
					Color = PresetColor
				}
			};

			foreach (var item in ContrastRatiosInfo)
			{
				item.PropertyChanged += Item_PropertyChanged;
			}
		}

		private void Item_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == nameof(ContrastRatioInformation.Color))
			{

			}
		}
	}
}
