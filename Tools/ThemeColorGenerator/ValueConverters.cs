﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using ToolsLib;

namespace ToolsLib
{
	public class Hex2ColorConverter : IValueConverter
	{
		const string HEX = "0123456789ABCDEF";

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			string hex = value?.ToString();
			if (hex is null || hex.Length != 6)
				return Colors.Transparent;
			for (int i = 0; i < hex.Length; i++)
				if (!HEX.Contains(hex[i], StringComparison.OrdinalIgnoreCase))
					return Colors.Transparent;
			Color color;
			byte alpha = parameter is null ? (byte)0 : System.Convert.ToByte(parameter.ToString(), 16);
			if (HexStringConverter.FromHexString(hex, out var bytes))
				color = Color.FromArgb(alpha, bytes[0], bytes[1], bytes[2]);
			return color;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			Color color = (Color)value;
			string rbyte = System.Convert.ToString(color.R, 16);
			string gbyte = System.Convert.ToString(color.G, 16);
			string bbyte = System.Convert.ToString(color.B, 16);
			return string.Format("#{0}{1}{2}", rbyte, gbyte, bbyte);
		}
	}

	public class ErrorState2BrushConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			bool hasErrors = System.Convert.ToBoolean(value);
			return hasErrors ? Brushes.IndianRed : Brushes.LightGreen;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
