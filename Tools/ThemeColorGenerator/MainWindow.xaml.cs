﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using ToolsLib;

namespace ToolsLib
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		const string HEX = "#0123456789ABCDEF";

		public MainWindow()
		{
			InitializeComponent();
			DataContext = new MainWindowViewModel();
			Loaded += MainWindow_Loaded;
		}

		private void MainWindow_Loaded(object sender, RoutedEventArgs e)
		{
			MainWindowViewModel vm = (MainWindowViewModel)DataContext;
			vm.XamlCodeGenerated += ViewModel_XamlCodeGenerated;
		}

		private void ViewModel_XamlCodeGenerated(object sender, XamlCodeGeneratedEventArgs e)
		{
			if (e.CodeType == XamlCodeType.Color && !string.IsNullOrEmpty(txtOut.Text))
				txtOut.Clear();
			txtOut.AppendText(e.Text);
		}

		private void ButtonReset_Click(object sender, RoutedEventArgs e)
		{
			txtOut.Clear();
			txtBASEColor.Clear();
			txtALTColor.Clear();
			DataContext = new MainWindowViewModel();
			txtThemeKey.SelectAll();
			return;
		}

		private void HexColorInput_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (sender is TextBox tb && !string.IsNullOrEmpty(tb.Text))
			{
				for (int i = 0; i < e.Changes.Count; i++)
				{
					var change = e.Changes.ElementAtOrDefault(i);
					if (change.RemovedLength > 0)
						e.Handled = true;

					if (!e.Handled && change.AddedLength > 0)
					{
						tb.BeginChange();
						string newChars = tb.Text.Substring(change.Offset, change.AddedLength);

						for (int j = 0; j < newChars.Length; j++)
						{
							if (char.IsLetter(newChars[j]) && newChars[j] > 96)
								tb.Text = tb.Text.Replace(newChars[j], (char)(newChars[j] - 32));

							if (!HEX.Contains(newChars[j], StringComparison.OrdinalIgnoreCase))
							{
								tb.Text = tb.Text.Remove(change.Offset, change.AddedLength);
								tb.CaretIndex = Math.Max(0, tb.Text.Length - 1);
							}
						}
						tb.EndChange();
					}
				}
			}
		}

		private void HexColorText_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			e.Handled = (!HEX.Contains(e.Text, StringComparison.OrdinalIgnoreCase)) || e.Text == " ";
		}

		private void TxtThemeKey_TextInput(object sender, TextCompositionEventArgs e)
		{
			e.Handled = (from c in e.Text.ToCharArray() let b = Path.GetInvalidFileNameChars().Concat(Path.GetInvalidPathChars()).Contains(c) select b).Any(_ => _ == true);
		}
	}
}
