﻿using MvvmCross.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using System.Windows.Media;

using ToolsLib;

namespace ToolsLib
{
	public class MainWindowViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			var handler = PropertyChanged;
			handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
		
		public bool AreColorsGenerated { get; private set; }

		private string themeKey;

		public string ThemeKey
		{
			get { return themeKey; }
			set 
			{ 
				themeKey = value;
				OnPropertyChanged();
			}
		}

		private string brushKey;

		public string BrushKey
		{
			get { return brushKey; }
			set 
			{
				brushKey = value;
				OnPropertyChanged();
			}
		}

		private double brushOpacity;

		public double BrushOpacity
		{
			get { return brushOpacity; }
			set 
			{
				brushOpacity = value;
				OnPropertyChanged();
			}
		}

		private string baseColorHexString;

		public string BaseColorHexString
		{
			get { return baseColorHexString; }
			set 
			{
				baseColorHexString = value;
				SetColorFromHexString(ref baseColor, baseColorHexString);
				OnPropertyChanged();
			}
		}

		private Color baseColor = Colors.Transparent;
		
		private string altColorHexString;

		public string AltColorHexString
		{
			get { return altColorHexString; }
			set
			{
				altColorHexString = value;
				SetColorFromHexString(ref altColor, altColorHexString);
				OnPropertyChanged();
			}
		}

		private Color altColor = Colors.Transparent;

		private void SetColorFromHexString(ref Color color, string hex)
		{
			if (hex.Length == 6 && HexStringConverter.FromHexString(hex, out var bytes))
			{
				color = bytes.Length == 4 ? Color.FromArgb(bytes[0], bytes[1], bytes[2], bytes[3]) : Color.FromRgb(bytes[0], bytes[1], bytes[2]);
			}
		}

		private readonly List<IReadOnlyDictionary<string, Color>> ramps = new List<IReadOnlyDictionary<string, Color>>(2);

		private readonly StringBuilder sb = new StringBuilder();

		public event EventHandler<XamlCodeGeneratedEventArgs> XamlCodeGenerated = delegate { };

		protected virtual void OnXamlCodeGenerated(string xamlCode, XamlCodeType codeType)
		{
			var handler = XamlCodeGenerated;
			handler?.Invoke(this, new XamlCodeGeneratedEventArgs(xamlCode, codeType));
		}

		private MvxCommand<int> generateXamlCodeCommand;

		public ICommand GenerateXamlCodeCommand
		{
			get
			{
				return generateXamlCodeCommand ??= new MvxCommand<int>(parameter =>
				{
					sb.Clear();
					XamlCodeType codeType = (XamlCodeType)parameter;
					WriteXamlCode(baseColor, ColorRampType.Base, codeType);
					WriteXamlCode(altColor, ColorRampType.Alt, codeType);
					AreColorsGenerated = codeType == XamlCodeType.Color;
					OnPropertyChanged(nameof(AreColorsGenerated));
					OnXamlCodeGenerated(sb.ToString(), codeType);
				});
			}
		}

		private void WriteXamlCode(Color color, ColorRampType rampCategory, XamlCodeType codeType)
		{
			switch (codeType)
			{
				case XamlCodeType.Color:
					var ccg = new ColorXamlGenerator();
					sb.AppendLine($"*****   Theme Resource - {themeKey}  *****");
					sb.AppendLine($"*****   {rampCategory} Colors   *****");
					sb.AppendLine(ccg.GenerateXaml(themeKey, rampCategory, color));
					sb.AppendLine();
					ramps.Add(ccg.ColorRamp);
					break;

				case XamlCodeType.SolidColorBrush:
					string colorHexString = rampCategory == ColorRampType.Base ? baseColorHexString : altColorHexString;
					var scg = new SolidColorBrushXamlGenerator(ramps[0], ramps[1], brushOpacity);
					sb.AppendLine($"*****   Theme Resource - {themeKey}  *****");
					sb.AppendLine($"*****   SolidColorBrush.Color #{colorHexString}  *****");
					sb.AppendLine(scg.GenerateXaml(brushKey, rampCategory, color));
					sb.AppendLine();
					break;
			}	
		}
	}
}
