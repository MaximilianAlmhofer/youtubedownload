﻿namespace ToolsLib
{
	public enum ColorRamp : byte
	{
		Low = 0x33,
		MediumLow = 0x66,
		Medium = 0x99,
		MediumHigh = 0xCC,
		High = 0xFF
	}

	public enum ColorRampType
	{
		Base,
		Alt
	}

	public enum DefaultColorTheme
	{
		Dark,
		Light
		//HighContrast
	}

	public enum XamlCodeType
	{
		Color,
		SolidColorBrush
	}
}
