﻿namespace ToolsLib
{

	public abstract class XamlGenerator<T> : IXamlGenerator<T>
	{
		public abstract string GenerateXamlCode(string resourceKeyPrefix, T element);
	}
}
