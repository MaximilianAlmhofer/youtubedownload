﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Media;

namespace ToolsLib
{
	public sealed class ColorXamlGenerator : IXamlResourceGenerator<Color>
	{
		private IDictionary<string, Color> _generatedColorRamp = new Dictionary<string, Color>();

		public IReadOnlyDictionary<string, Color> ColorRamp => new ReadOnlyDictionary<string, Color>(_generatedColorRamp);

		public string GenerateXaml(string resourceKeyPrefix, ColorRampType rampCategory, Color resource)
		{
			var sb = new StringBuilder();

			_generatedColorRamp = GetAccentColorRampForColor(string.Concat(resourceKeyPrefix, rampCategory.ToString()), resource);

			foreach (var item in _generatedColorRamp)
			{
				var line = string.Format("<Color x:Key=\"{0}\">{1}</Color>", item.Key, item.Value.ToString());
				sb.AppendLine(line);
			}

			return sb.ToString();
		}

		static IDictionary<string, Color> GetAccentColorRampForColor(string resourceKey, Color color)
		{
			var dict = new Dictionary<string, Color>();
			foreach (var name in Enum.GetNames(typeof(ColorRamp)))
			{
				ColorRamp ramp = (ColorRamp)Enum.Parse(typeof(ColorRamp), name);
				Color rampColor = Color.FromArgb((byte)ramp, color.R, color.G, color.B);
				string rampKey = string.Concat(resourceKey, name, "Color");
				dict.Add(rampKey, rampColor);
			}
			return dict;
		}
	}
}
