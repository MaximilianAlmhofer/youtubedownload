﻿using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Media;

namespace ToolsLib
{
	public class SolidColorBrushXamlGenerator : IXamlResourceGenerator<Color>
	{
		private readonly IReadOnlyDictionary<string, Color> baseColorRamp;
		private readonly IReadOnlyDictionary<string, Color> altColorRamp;
		private readonly double opacity = 1.0d;

		public SolidColorBrushXamlGenerator(IReadOnlyDictionary<string, Color> baseColorRamp, IReadOnlyDictionary<string, Color> altColorRamp, double opacity)
		{
			this.baseColorRamp = baseColorRamp;
			this.altColorRamp = altColorRamp;
			this.opacity = opacity;
		}

		public string GenerateXaml(string resourceKeyPrefix, ColorRampType rampCategory, Color resource)
		{
			var sb = new StringBuilder();
			var requestedRamp = rampCategory == ColorRampType.Base ? baseColorRamp : altColorRamp;

			foreach (var colorKey in requestedRamp)
			{
				sb.AppendFormat("<SolidColorBrush x:Key=\"{0}\" Opacity=\"{1}\" Color=\"{{StaticResource {2}}}\"/>", string.Concat(resourceKeyPrefix, colorKey.Key, "Brush"), string.Format(NumberFormatInfo.InvariantInfo, "{0}", opacity), colorKey.Key);
				sb.AppendLine();
			}
			return sb.ToString();
		}
	}
}
