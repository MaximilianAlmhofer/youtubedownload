﻿namespace ToolsLib
{
	internal interface IXamlGenerator<in T>
	{
		string GenerateXamlCode(string resourceKeyPrefix, T element);
	}
}
