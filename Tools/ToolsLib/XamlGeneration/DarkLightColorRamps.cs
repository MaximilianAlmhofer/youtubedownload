﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;

namespace ToolsLib
{
	internal static class DarkLightColorRamps
	{
		public static IEnumerable GetRamp(string resourceKey, DefaultColorTheme defaultRamps)
		{
			resourceKey = resourceKey ?? throw new ArgumentNullException(nameof(resourceKey));
			baseColor = defaultRamps == DefaultColorTheme.Dark ? Colors.Black : Colors.White;
			altColor = defaultRamps == DefaultColorTheme.Dark ? Colors.White : Colors.Black;
			IEnumerable baseRamp = GetBaseRamp(resourceKey);
			IEnumerable altRamp = GetAltRamp(resourceKey);
			return ((IEnumerable<object>)baseRamp).Concat((IEnumerable<object>)altRamp);
		}

		private static IEnumerable GetBaseRamp(string resourceKey)
		{
			var dict = new Dictionary<string, Color>();
			yield return from item in BaseRamp select new KeyValuePair<string, Color>(string.Concat(resourceKey, item.Key, "Color"), item.Value);
		}

		private static IEnumerable GetAltRamp(string resourceKey)
		{
			var dict = new Dictionary<string, Color>();
			yield return from item in AltRamp select new KeyValuePair<string, Color>(string.Concat(resourceKey, item.Key, "Color"), item.Value);
		}

		static IDictionary<string, Color> BaseRamp
		{
			get
			{
				ref Color colorRef = ref baseColor;
				return new Dictionary<string, Color>
				{
					{ "BaseHigh", Color.FromArgb(0xFF, colorRef.R, colorRef.G, colorRef.B) },
					{ "BaseLow", Color.FromArgb(0x33, colorRef.R, colorRef.G, colorRef.B) },
					{ "BaseMedium", Color.FromArgb(0x99, colorRef.R, colorRef.G, colorRef.B) },
					{ "BaseMediumHigh", Color.FromArgb(0xCC, colorRef.R, colorRef.G, colorRef.B) },
					{ "BaseMediumLow", Color.FromArgb(0x66, colorRef.R, colorRef.G, colorRef.B) },
				};
			}
		}

		static IDictionary<string, Color> AltRamp
		{
			get
			{
				ref Color colorRef = ref altColor;
				return new Dictionary<string, Color>
				{
					{ "AltHigh", Color.FromArgb(0xFF, colorRef.R, colorRef.G, colorRef.B) },
					{ "AltLow", Color.FromArgb(0x33, colorRef.R, colorRef.G, colorRef.B) },
					{ "AltMedium", Color.FromArgb(0x99, colorRef.R, colorRef.G, colorRef.B) },
					{ "AltMediumHigh", Color.FromArgb(0xCC, colorRef.R, colorRef.G, colorRef.B) },
					{ "AltMediumLow", Color.FromArgb(0x66, colorRef.R, colorRef.G, colorRef.B) },
				};
			}
		}

		static Color baseColor = Colors.Black;
		static ref Color baseColorRef { get => ref baseColor; }
		
		static Color altColor = Colors.White;
		static ref Color altColorRef { get => ref altColor; }
	}
}
