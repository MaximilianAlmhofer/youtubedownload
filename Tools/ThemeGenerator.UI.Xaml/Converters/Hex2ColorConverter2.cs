﻿using Microsoft.Toolkit.Uwp.Helpers;

using System;

using Windows.UI;
using Windows.UI.Xaml.Media;

namespace ThemeGenerator.UI.Xaml.Converters
{
	public class Hex2ColorConverter2 : Hex2ColorConverter
	{
		public override object Convert(object value, Type targetType, object parameter, string language)
		{
			object _value = base.Convert(value, targetType, parameter, language);
			if (_value is SolidColorBrush brush && brush.Color.ToHex().EndsWith("000000"))
			{
				brush.Color = Color.FromArgb(99, brush.Color.R, brush.Color.G, brush.Color.B);
			}
			return _value;
		}
	}
}
