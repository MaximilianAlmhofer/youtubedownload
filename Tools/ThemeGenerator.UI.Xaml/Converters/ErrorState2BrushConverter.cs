﻿using System;

using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace ThemeGenerator.UI.Xaml.Converters
{
	public class ErrorState2BrushConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, string language)
		{
			bool hasErrors = System.Convert.ToBoolean(value);
			return hasErrors ? new SolidColorBrush(Colors.IndianRed) : new SolidColorBrush(Colors.LightGreen);
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			throw new NotImplementedException();
		}
	}
}
