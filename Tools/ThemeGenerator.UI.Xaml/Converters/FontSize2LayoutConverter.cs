﻿using System;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace ThemeGenerator.UI.Xaml.Converters
{
	public class FontSize2LayoutConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, string language)
		{
			if (targetType == typeof(Thickness))
			{
				value = new Thickness(20, 0, 0, System.Convert.ToDouble(value) - 10);
			}
			if (targetType == typeof(double))
			{
				value = System.Convert.ToDouble(value) + 10;
			}
			return value;
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			throw new NotImplementedException();
		}
	}
}
