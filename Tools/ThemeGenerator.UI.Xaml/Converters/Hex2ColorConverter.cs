﻿
using System;

using ToolsLib.UWP;

using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace ThemeGenerator.UI.Xaml.Converters
{
	public class Hex2ColorConverter : IValueConverter
	{
		const string HEX = "0123456789ABCDEF";

		public virtual object Convert(object value, Type targetType, object parameter, string language)
		{
			string hex = value?.ToString();
			if (hex != null && hex.StartsWith('#'))
				hex = hex.Remove(0, 1);
			if (hex is null || hex.Length < 6)
				return Colors.Transparent;
			int start = hex.Length - 6;
			for (int i = start; i < hex.Length; i++)
				if (!HEX.Contains(hex[i].ToString(), StringComparison.OrdinalIgnoreCase))
					return Colors.Transparent;
			Color color;
			byte alpha = parameter is null ? hex.Length == 8
					? System.Convert.ToByte(hex.Substring(0, 2), 16)
					: (byte)0 : System.Convert.ToByte(parameter.ToString(), 16);
			if (HexStringConverter.FromHexString(hex.Substring(start), out var bytes) && bytes.Length >= 3)
				color = Color.FromArgb(alpha, bytes[0], bytes[1], bytes[2]);
			return targetType == typeof(Brush) ? new SolidColorBrush(color) : (object)color;
		}

		public virtual object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			Color color = targetType == typeof(Brush) ? ((SolidColorBrush)value).Color : (Color)value;
			string rbyte = System.Convert.ToString(color.R, 16);
			string gbyte = System.Convert.ToString(color.G, 16);
			string bbyte = System.Convert.ToString(color.B, 16);
			return string.Format("#{0}{1}{2}", rbyte, gbyte, bbyte);
		}
	}
}
