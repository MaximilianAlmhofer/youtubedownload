﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace ThemeGenerator.UI.Xaml.Controls
{
	public sealed class ColorPickerPreviewButton : Windows.UI.Xaml.Controls.Button
	{
		public ColorPickerPreviewButton()
		{
			this.DefaultStyleKey = typeof(ColorPickerPreviewButton);
		}
	}
}
