﻿
using MvvmCross.Plugin.Messenger;

using System;
using System.Collections.ObjectModel;
using System.Windows.Media;

using xtra_snd_tools.Infra.ViewModels;

namespace PlaybackControlTest
{
	public class EditingImages : ViewModelBase
	{
		private Color foreground, background;
		private ObservableCollection<Uri> imageFiles;
		public EditingImages() : base(MvvmCross.Mvx.IoCProvider.Resolve<IMvxMessenger>())
		{
			Foreground = Colors.White;
			Background = Colors.RoyalBlue;
			imageFiles = new ObservableCollection<Uri>();
			imageFiles.Add(new Uri(@"C:\Users\almho\Pictures\couch_selfie_mongolook.jpg"));
			imageFiles.Add(new Uri(@"C:\Users\almho\Pictures\couch_selfie_weirdlook.jpg"));
			RaisePropertyChanged("ImageFiles");
		}

		public Color Foreground
		{
			get => foreground;
			set
			{
				foreground = value;
				RaisePropertyChanged();
			}

		}

		public Color Background
		{
			get => background;
			set
			{
				background = value;
				RaisePropertyChanged();
			}

		}

		public ObservableCollection<Uri> ImageFiles 
		{ 
			get
			{
				return imageFiles ??= new ObservableCollection<Uri>();
			}
		}
	}
}
