﻿using MvvmCross.IoC;
using MvvmCross.Platforms.Wpf.Views;
using MvvmCross.Plugin.Messenger;

using System.Windows;

namespace PlaybackControlTest
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : MvxApplication
	{
		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);
			MvxIoCProvider.Initialize();
			MvvmCross.Mvx.IoCProvider.RegisterType<IMvxMessenger, MvxMessengerHub>();
		}
	}
}
