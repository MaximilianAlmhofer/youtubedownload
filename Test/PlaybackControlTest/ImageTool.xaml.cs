﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;

using xtra_snd_tools.Logging;

namespace PlaybackControlTest
{
	/// <summary>
	/// Interaktionslogik für ImageTool.xaml
	/// </summary>
	public partial class ImageTool : Page
	{
		public ImageTool()
		{
			InitializeComponent();
			Loaded += ImageTool_Loaded;
		}

		private void ImageTool_Loaded(object sender, RoutedEventArgs e)
		{
			DataContext = new EditingImages();
		}
	}
}
