﻿namespace Settings.WinForms.Components
{
	public enum SegoeMDL2Assets : uint
	{
		OpenFolder = 0xe838,
		Settings = 0xe115,
		Clear = 0xe194,
		Save = 0xe105,
		Exit = 0xe7e8,
		Ok = 0xe10b
	};
}
