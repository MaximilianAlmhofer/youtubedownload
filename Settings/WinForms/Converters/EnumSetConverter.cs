﻿/* @Source: https://stackoverflow.com/questions/831612/expose-a-collection-of-enums-flags-in-visual-studio-designer */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Settings.WinForms.Converters
{
	internal abstract class EnumSetConverter<T> : TypeConverter where T : struct
	{
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return destinationType == typeof(string) || base.CanConvertTo(context, destinationType);
		}

		public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == typeof(string))
			{
				HashSet<T> set = (HashSet<T>)value;
				if (set == null) return "(null)";

				StringBuilder sb = new StringBuilder();
				foreach (T item in Enum.GetValues(typeof(T)))
				{
					if (set.Contains(item))
					{
						if (sb.Length > 0) sb.Append(", ");
						sb.Append(item);
					}
				}
				return sb.ToString();
			}
			return base.ConvertTo(context, culture, value, destinationType);
		}
	}
}
