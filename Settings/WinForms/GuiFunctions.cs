﻿using Lib.Common;

using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace Settings.WinForms
{
	public static class GuiFunctions
	{
		public static void UpdateText(this Control c, string value)
		{
			if (c.InvokeRequired)
			{
				if (c is RichTextBox || c is TextBoxBase && ((TextBoxBase)c).Lines.Length > 1)
					c.Invoke((MethodInvoker)delegate
					{
						((TextBoxBase)c).AppendText(!value.StartsWith("\r\n") ? string.Concat("\r\n", value) : value);
					});
				else
					c.Invoke((MethodInvoker)delegate { c.Text = value; });
			}
			else
			{
				if (c is RichTextBox || c is TextBoxBase && ((TextBoxBase)c).Lines.Length > 1)
					((TextBoxBase)c).AppendText(!value.StartsWith("\r\n") ? string.Concat("\r\n", value) : value);
				else
					c.Text = value;
			}
		}


		public static void UpdateProperty<T>(this Control control, string propertyName, T value)
		{
			if (propertyName is null) return;
			if (control is null) return;

			if (control.InvokeRequired)
			{
				control.Invoke((MethodInvoker)delegate { control.SetProperty(propertyName, value); });
			}
			else
			{
				control.Invoke((MethodInvoker)delegate { control.SetProperty(propertyName, value); });
			}
		}

		public static void InvokeDelegate<T>(this Control control, MethodInfo method, params object[] args)
		{
			if (control is null) return;
			if (method is null) return;

			Delegate @delegate = method.CreateDelegate(typeof(T), control);
			control.Invoke(@delegate, args);
		}


		public static void InvokeMember(this Control control, object target, MethodInfo method, params object[] args)
		{
			if (control is null) return;
			if (target is null) return;
			if (method is null) return;

			if (control.InvokeRequired)
			{
				control.Invoke((MethodInvoker)delegate
				{
					method.Invoke(target, args);
				});
			}
			else
			{
				method.Invoke(target, args);
			}
		}

		public static bool GetValidUniformResourceLocator(this System.Windows.Forms.IDataObject data, out string contentAsString)
		{
			contentAsString = "";
			if (data.GetDataPresent("UniformResourceLocator"))
			{
				MemoryStream mem = new MemoryStream();
				((MemoryStream)data.GetData("UniformResourceLocator", true)).CopyTo(mem);
				contentAsString = Encoding.UTF8.GetString(mem.ToArray()).TrimEnd(new char[] { '\0' });
				mem.Close();
				mem.Dispose();
				return Uri.IsWellFormedUriString(contentAsString, UriKind.Absolute);
			}
			object d = data.GetData(typeof(object));
			contentAsString = d.ToString();
			return false;
		}

	}
}
