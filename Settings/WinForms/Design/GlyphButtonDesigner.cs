﻿using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms.Design;

namespace Settings.WinForms.Design
{
	[ToolboxItemFilter("Settings.WinForms.Components.SegoeMDL2Assets", ToolboxItemFilterType.Require)]
	[System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
	public sealed class GlyphButtonDesigner : ControlDesigner
	{
		public GlyphButtonDesigner() : base()
		{
			Trace.WriteLine("GlyphButtonDesigner ctor");
		}
	}
}
