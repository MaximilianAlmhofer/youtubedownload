﻿using Settings.WinForms.Components;

using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Settings.WinForms.Controls
{
	[Designer(typeof(Design.GlyphButtonDesigner), typeof(IRootDesigner))]
	public partial class GlyphButton : Button
	{
		private readonly PrivateFontCollection pvc;
		private readonly byte[] fontResource;
		private Point glyphLocation;

		public GlyphButton() : base()
		{
			glyphLocation = new Point(1, 1);
			fontResource = Properties.Resources.segmdl2 ?? throw new ArgumentException("SegoeMDL2Assets ttf-file missing");
			pvc = LoadSegoeMdl2AssetsFromEmbeddedResources();
			InitializeComponent();
		}


		private SolidBrush InternalColorBrush => new SolidBrush(ForeColor);

		protected override Size DefaultSize => new Size(35, 20);

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		[TypeConverter(typeof(EnumConverter))]
		public SegoeMDL2Assets Glyph { get; set; }

		[EditorBrowsable(EditorBrowsableState.Never)]
		public override Font Font
		{
			get
			{
				if (base.Font is null)
					base.Font = new Font(pvc.Families[0],
						Math.Min(Size.Height, Size.Width), FontStyle.Regular, GraphicsUnit.Pixel, 0);
				return base.Font;
			}
			set => base.Font = value;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public override ContentAlignment TextAlign
		{
			get => base.TextAlign;
			set
			{
				int x = 0, y = 0;
				switch (value)
				{
					case ContentAlignment.TopLeft:
					case ContentAlignment.BottomLeft:
					case ContentAlignment.MiddleLeft:
						x = Left + 1;
						y = (int)((Size.Height - (int)Font.Size) * 0.5);
						break;

					case ContentAlignment.TopCenter:
					case ContentAlignment.BottomCenter:
					case ContentAlignment.MiddleCenter:
						x = (int)((Size.Width - CreateGraphics().MeasureString(Text, Font, (int)Font.Size).ToSize().Width) * 0.5);
						y = (int)((Size.Height - (int)Font.Size) * 0.5);
						break;

					case ContentAlignment.TopRight:
					case ContentAlignment.BottomRight:
					case ContentAlignment.MiddleRight:
						x = Width - CreateGraphics().MeasureString(Text, Font, (int)Font.Size).ToSize().Width;
						y = (int)((Size.Height - (int)Font.Size) * 0.5);
						break;
				}

				base.TextAlign = value;
				glyphLocation = new Point(x, y);
			}
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);

			e.Graphics.DrawString(
				char.ConvertFromUtf32((int)Glyph),
				Font, InternalColorBrush, new Point(glyphLocation.X, glyphLocation.Y));
		}

		private PrivateFontCollection LoadSegoeMdl2AssetsFromEmbeddedResources()
		{
			byte[] fontBytes = fontResource;
			IntPtr ptr = Marshal.AllocHGlobal(fontBytes.Length * Marshal.SizeOf<byte>());
			Marshal.Copy(fontBytes, 0, ptr, fontBytes.Length);

			var fc = new PrivateFontCollection();
			fc.AddMemoryFont(ptr, fontBytes.Length);
			return fc;
		}
	}
}
