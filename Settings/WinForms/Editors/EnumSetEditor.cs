﻿/* @Source: https://stackoverflow.com/questions/831612/expose-a-collection-of-enums-flags-in-visual-studio-designer */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace Settings.WinForms.Editors
{

	internal abstract class EnumSetEditor<T> : UITypeEditor where T : struct
	{
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			return UITypeEditorEditStyle.DropDown;
		}

		public override bool IsDropDownResizable => true;

		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			IWindowsFormsEditorService svc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
			if (svc != null && value is HashSet<T> set)
			{
				UserControl ctrl = new UserControl();
				CheckedListBox clb = new CheckedListBox
				{
					Dock = DockStyle.Fill
				};
				Button btn = new Button
				{
					Dock = DockStyle.Bottom
				};
				foreach (T item in Enum.GetValues(typeof(T)))
				{
					clb.Items.Add(item, set.Contains(item));
				}
				ctrl.Controls.Add(clb);
				ctrl.Controls.Add(btn);
				btn.Text = "OK";
				btn.Click += delegate
				{
					set.Clear();
					foreach (T item in clb.CheckedItems)
					{
						set.Add(item);
					}
					svc.CloseDropDown();
				};
				svc.DropDownControl(ctrl);
			}

			return value;
		}
	}
}
