﻿using Lib.Command;
using Lib.Common;
using Lib.Modules.Downloader;

using Newtonsoft.Json;

using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ycraft.Settings
{

	public delegate Task ProgressReporter(
		int progress,
		string percentage,
		string dataMB,
		string rateKBs,
		string remainingTime);

	internal class Downloader
	{
		private delegate string CreateMediaFileName(
			GlobalConfig config,
			string destFileName,
			INode downloadDest,
			ref IStorageItem local,
			ref IDownloadSource model,
			bool isPlayList);

		private readonly Func<string, Task> consolelog_Delegate = async delegate { await Task.CompletedTask; };
		private readonly Func<Delegate, object[], Task> storetodisk_Delegate =
			async delegate { await Task.CompletedTask; lock (new object()) { Task.Delay(0); } };

		private readonly GlobalConfig config;

		public Downloader(
			Func<string, Task> consolelog_callback,
			Func<Delegate, object[], Task> storetodisk_callback,
			GlobalConfig config)
		{
			this.config = config;
			consolelog_Delegate = consolelog_callback;
			storetodisk_Delegate = storetodisk_callback;
		}

		public async Task ExecuteAsync(string url, ProgressReporter reportProgress_Delegate, CancellationToken cancellation)
		{

			Task delayedTask = Task.Delay(150000, cancellation);

			#region Download - IPC Aktion

			Uri droppedSource = new Uri(url);
			var builder = CommandBuilder.CreateFromUri(droppedSource, config);
			var processCmd = builder.GetMetadataCommand(config.AppFolder.DownloadFolderPath);

			IPCRunner.Instance.ExecuteThread(new CommandObject(processCmd, null),
			ipcResponse =>
			{
				var downloadDest = config.AppFolder.DownloadFolder;
				IStorageItem local = null;
				IDownloadSource model = null;
				string json = ipcResponse.Value.ToString();
				if (json != null)
				{
					Folder dlFolder = downloadDest;

					VorbereitenDownloadZielVomMetadata(builder, json, ref dlFolder, out model);
					IPCRunner.Instance.ExecuteThread(new CommandObject(builder.Build(model), null),
					async state =>
					{
						#region Update GUI								
						if (state is IResult res && res.Value != null)
						{
							#region Download Zeilen
							if (res.Value.ToString().StartsWith("[download]"))
							{
								string downloadInfoText = res.Value.ToString().Replace("[download]", "").TrimStart().TrimEnd();
								string perc, data, speed, time;
								double value;

								#region FileName aussafischen
								if (downloadInfoText.StartsWith("Destination:"))
								{
									Delegate @delegate = new CreateMediaFileName(ErzeugeDateiname);
									object[] args = new object[]
									{
										config,
										downloadInfoText.Replace("Destination:", "").TrimStart().TrimEnd(),
										dlFolder,
										local,
										model,
										builder is PlaylistCommandBuilder
									};
									await storetodisk_Delegate(@delegate, args);

									local = (IStorageItem)args[3];
								}
								#endregion
								else if (char.IsDigit(downloadInfoText[0]))
								{
									var numericParts = downloadInfoText
										.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
										.Where(p => char.IsDigit(p[0]));

									// Für den Fall das das 1. Zeichen im Dateinamen numerisch ist
									if (numericParts.Count() > 1)
									{
										perc = numericParts.ElementAtOrDefault(0);
										data = numericParts.ElementAtOrDefault(1);
										speed = numericParts.ElementAtOrDefault(2);
										time = numericParts.ElementAtOrDefault(3);

										value = double.Parse(perc.TrimEnd(new char[] { '%' }),
												NumberStyles.Float, new NumberFormatInfo());

										await reportProgress_Delegate((int)value, perc, data, speed, time);

										if (delayedTask.Status == TaskStatus.RanToCompletion)
										{
											await await delayedTask.ContinueWith(task =>
											{
												if (task.IsCanceled || cancellation.IsCancellationRequested)
												{
													task.ConfigureAwait(true);
													return Task.FromCanceled(cancellation);
												}
												return Task.Delay(1000, cancellation);
											}, cancellation);
										}
									}
								}
								else if (res is IErrorResult)
								{
									await consolelog_Delegate(new StringBuilder(res.Value.ToString()).AppendLine(config.DownloadOptions.ToString()).ToString());
								}
							}
							#endregion

							#region Restlicher Output
							else
							{
								await consolelog_Delegate(res.Value.ToString());
							}
							#endregion
						}
						#endregion
					},
					async result =>
					{
						#region Reset GUI
						try
						{
							await reportProgress_Delegate(0, "0.0 %", "00.0", "0.0 MiB", "00:00:00.000");
						}
						catch (Exception ex)
						{
							ex.Trace();
							await consolelog_Delegate(ex.ToLongString());
						}
						finally
						{
							if (local is IStorageItem)
							{
								Uri file = new Uri(local.FileLocation);

								await consolelog_Delegate(string.Format("{0}\r\n\r\n{1}",
								yCraft.Settings.Properties.Resources.StartDownloadHint, file?.AbsoluteUri));

							}
							FormMain.Cancel();
						}
						#endregion
					}, cancellation);
				}
			});

			#endregion

			await delayedTask;
		}

		private static string ErzeugeDateiname(GlobalConfig config, string destFileName, INode downloadDest, ref IStorageItem local, ref IDownloadSource model, bool isPlaylist)
		{
			string saveFilePath;

			if (isPlaylist)
			{
				IPlaylistInfo info = (IPlaylistInfo)model;
				destFileName = Path.ChangeExtension(destFileName, config.DownloadOptions.AudioFormat ?? ((IStorageItem)info).GetExtension());
				saveFilePath = Path.Combine(((Folder)downloadDest.GetChild("pl")).FullPath, destFileName);
				local = info.ToLocal(saveFilePath);
			}
			else
			{
				ITrackInfo info = (ITrackInfo)model;
				if (string.IsNullOrEmpty(config.DownloadOptions.AudioFormat))
				{
					destFileName = Path.ChangeExtension(destFileName, (string)info.GetType().GetTypeInfo().GetRuntimeProperty("Ext").GetValue(info)?.ToString());
				}
				else
				{
					destFileName = Path.ChangeExtension(destFileName, config.DownloadOptions.AudioFormat);
				}
				saveFilePath = Path.Combine(downloadDest.FullPath, destFileName);
				local = model.ToLocal(saveFilePath);
			}

			return saveFilePath;
		}

		private static void VorbereitenDownloadZielVomMetadata(CommandBuilder builder, string json, ref Folder downloadDest, out IDownloadSource model)
		{
			model = default;

			if (builder is PlaylistCommandBuilder)
			{
				PlaylistInfo info = JsonConvert.DeserializeObject<PlaylistInfo>(json);
				model = info;
				INode tmpdownloadDest = downloadDest;
				downloadDest = downloadDest.GetChild("pl") as Folder;
				var dirInfo = new DirectoryInfo(downloadDest.FullPath);
				if (!dirInfo.Exists)
				{
					downloadDest = (Folder)tmpdownloadDest;
				}
			}
			else if (builder is SimpleCommandBuilder)
			{
				TrackInfo info = JsonConvert.DeserializeObject<TrackInfo>(json);
				model = info;
			}
		}
	}
}
