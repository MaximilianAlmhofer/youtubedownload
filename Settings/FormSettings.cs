﻿using Lib.Command;
using Lib.Common;

using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Settings
{

	public partial class FormSettings : Form
	{

		public static DialogResult ModalDialog(GlobalConfig config, IConfigurationRoot appSettings)
		{
			using (var frm = new FormSettings(config, appSettings))
			{
				return frm.ShowDialog();
			}
		}


		readonly struct ResetValues
		{
			readonly IDictionary<string, string> values;
			internal ResetValues(string appFolder, string dldest, string mdloc, string confbak, string scripts)
			{
				values = new Dictionary<string, string>
				{
					[nameof(FormSettings.dfAppFolder)] = appFolder,
					[nameof(FormSettings.dfDlDest)] = dldest,
					[nameof(FormSettings.dfMdLoc)] = mdloc,
					[nameof(FormSettings.dfConfBak)] = confbak,
					[nameof(FormSettings.dfScripts)] = scripts
				};
			}
			internal string GetValue(string key)
			{
				return values[key];
			}
		}

		private readonly BindingSource bindingSrc = new BindingSource();
		private readonly IConfigurationRoot appSettings;
		private readonly GlobalConfig config;

		private ResetValues resetValues;
		private DownloadOptions Settings
		{
			get => config.DownloadOptions;
			set => config.DownloadOptions = value;
		}


		private bool AppFolderIsSetAndValid => !string.IsNullOrEmpty(config.AppFolder.FullPath) && Directory.Exists(config.AppFolder.FullPath);

		private string DefaultApplicationRoot => Path.Combine(AppFolderIsSetAndValid ? config.AppFolder.FullPath : "%userprofile%");


		public FormSettings(GlobalConfig config, IConfigurationRoot appSettings)
		{
			this.config = config ?? throw new ArgumentNullException(nameof(config));
			this.appSettings = appSettings ?? throw new ArgumentNullException(nameof(appSettings));
			bindingSrc.DataSource = config.AppFolder;
			InitializeComponent();
			SetDataBinding();
			CreateConfigFiles(config.AppFolder?.FullPath);
			tabControl1.SelectedTab = tabPage1;
			cbIndexDatUeberschreiben.Enabled = !string.IsNullOrEmpty(config.IndexDat) && File.Exists(config.IndexDat);
			FormBorderStyle = FormBorderStyle.FixedToolWindow;
			StartPosition = FormStartPosition.CenterScreen;
		}

		#region TabPage1 Controls EventHandlers
		private void tbAudQ_ValueChanged(object sender, EventArgs e)
		{
			Settings.AudioQuality = tbAudQ.Value;
		}

		private void cbArchive_CheckedChanged(object sender, EventArgs e)
		{
			Settings.ArchiveFile = (sender as CheckBox).Checked ? config.ArchiveFile : "";
		}

		private void cbIndexDatUeberschreiben_CheckedChanged(object sender, EventArgs e)
		{
			if (cbIndexDatUeberschreiben.Checked)
			{
				if (MessageBox.Show("Der bereits heruntergeladene Inhalt wird aus dem Programm entfernt. Die Dateien bleiben erhalten.\r\nSind Sie sicher?",
									"", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
				{
					File.Create(config.IndexDat).Dispose();
				}
				else
				{
					cbIndexDatUeberschreiben.CheckState = CheckState.Unchecked;
				}
			}
		}

		private void rbAudioVideo_CheckedChanged(object sender, EventArgs e)
		{
			groupBox2.Visible = rbAud.Checked;
			groupBox4.Visible = rbVid.Checked;
			UpdateFormatValues();
		}

		private void rbFormat_CheckedChanged(object sender, EventArgs e)
		{
			UpdateFormatValues();
		}
		#endregion

		#region TabPage2 Controls EventHandlers
		private void pbAppFolder_Click(object sender, EventArgs e)
		{
			using (var folderBrowser = new FolderBrowserDialog())
			{
				folderBrowser.SelectedPath = DefaultApplicationRoot;

				if (folderBrowser.ShowDialog(this) == DialogResult.OK)
				{
					string appFolder = folderBrowser.SelectedPath;

					CreateDirectoryStructure(appFolder);
					CreateScripts(appFolder);
					CreateConfigFiles(appFolder);
					UpdateConfig(appFolder);
					EnableDisableButtons();
					bindingSrc.ResetBindings(false);
				}
			}
		}

		private void pbSelectFolder_Click(object sender, EventArgs e)
		{
			string dfValueName = "df" + (sender as Control).Name.Substring(2);

			using (var folderBrowser = new FolderBrowserDialog())
			{
				folderBrowser.SelectedPath = ((Control)GetType().GetField(dfValueName, Globals.InstanceNonPublicGetField).GetValue(this)).Text;
				if (folderBrowser.ShowDialog(this) == DialogResult.OK)
				{
					((Control)GetType().GetField(dfValueName, Globals.InstanceNonPublicGetSetF).GetValue(this)).Text = folderBrowser.SelectedPath;
				}
			}

			(sender as Control).Focus();
		}
		#endregion

		#region TabControl Controls EventHandlers
		private void pbOk_Click(object sender, EventArgs e)
		{
			DialogResult dlgResult = MessageBox.Show(Settings.ToString(),
				"Sollen die Einstellungen übernommen werden?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

			switch (dlgResult)
			{
				case DialogResult.Cancel:
					DialogResult = DialogResult.Cancel;
					ResetFolderPaths();
					Settings = new DownloadOptions();
					break;

				case DialogResult.Yes:
					DialogResult = DialogResult.OK;
					SaveLocationChanges();
					Close();
					break;

				default:
					DialogResult = DialogResult == DialogResult.Cancel ? DialogResult.Retry : DialogResult;
					Close();
					break;
			}
		}

		private void pbReset_Click(object sender, EventArgs e)
		{
			ResetFolderPaths();
		}
		#endregion

		#region Methods
		private void SetDataBinding()
		{
			dfAppFolder.DataBindings.Add("Text", bindingSrc, nameof(AppFolder.FullPath));
			dfDlDest.DataBindings.Add("Text", bindingSrc, nameof(AppFolder.DownloadFolderPath));
			dfMdLoc.DataBindings.Add("Text", bindingSrc, nameof(AppFolder.MetadataFolderPath));
			dfConfBak.DataBindings.Add("Text", bindingSrc, nameof(AppFolder.ConfigBackupFolderPath));
			dfScripts.DataBindings.Add("Text", bindingSrc, nameof(AppFolder.ScriptsFolderPath));
			resetValues = new ResetValues(config.AppFolder?.FullPath, config.AppFolder?.DownloadFolderPath, config.AppFolder?.MetadataFolderPath, config.AppFolder?.ConfigBackupFolderPath, config.AppFolder?.ScriptsFolderPath);

			rbAud.Checked = ((ISettings)Settings).ExtractAudio;
			rbVid.Checked = !rbAud.Checked;

			if (rbAud.Checked)
			{
				rbAud0.Checked = Settings.AudioFormat == "mp3";
				if (!rbAud0.Checked)
					rbAud1.Checked = Settings.AudioFormat == "flac";
				if (!rbAud1.Checked)
					rbAud2.Checked = Settings.AudioFormat == "wave";
			}
			else
			{
				if (string.IsNullOrEmpty(Settings.VideoFormat))
					Settings.SetEnumField<Format>("Format", "mp4");
				rbVid0.Checked = Settings.VideoFormat == "mp4";
				if (!rbVid0.Checked)
					rbVid1.Checked = Settings.VideoFormat == "mkv";
				if (!rbVid1.Checked)
					rbVid2.Checked = Settings.VideoFormat == "webm";
			}

			tbAudQ.Value = Settings.AudioQuality;
			cbArchive.Checked = Settings.Archive;
		}

		private void UpdateFormatValues()
		{
			if (rbAud.Checked)
			{
				Settings.SetEnumField<FileType>("Type", "audio");
				if (rbAud0.Checked)
					Settings.SetEnumField<Format>("Format", "mp3");
				if (rbAud1.Checked)
					Settings.SetEnumField<Format>("Format", "flac");
				if (rbAud2.Checked)
					Settings.SetEnumField<Format>("Format", "wave");
			}
			else
			{
				Settings.SetEnumField<FileType>("Type", "video");
				if (rbVid0.Checked)
					Settings.SetEnumField<Format>("Format", "mp4");
				if (rbVid1.Checked)
					Settings.SetEnumField<Format>("Format", "mkv");
				if (rbVid2.Checked)
					Settings.SetEnumField<Format>("Format", "webm");
			}
		}

		private void SaveLocationChanges()
		{
			MDSerializer.SerializeB(config.IndexDat);

			if (!string.IsNullOrEmpty(config.AppFolder.FullPath))
			{
				if (appSettings != null)
					File.WriteAllText(appSettings.GetSection("Configuration").Value, JsonConvert.SerializeObject(config));
			}
		}

		private void ResetFolderPaths()
		{
			if (MessageBox.Show("Sicher dass die Ordnerpfade zurückgesetzt werden sollen?",
				"Ordnerpfade leeren", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				foreach (var pInfo in GetType().GetFields(Globals.InstanceNonPublicGetSetF)
				.Where(x => x.FieldType == typeof(TextBox)))
				{
					var tb = (TextBox)pInfo.GetValue(this);

					tb.Text = resetValues.GetValue(pInfo.Name);

					tb.Enabled = false;
					var btnInfo = GetType().GetFields(Globals.InstanceNonPublicGetSetF)
					.SingleOrDefault(x => x.Name.Substring(2).Equals(tb.Name.Substring(2)) && x.Name.StartsWith("pb"));
					if (btnInfo != null && btnInfo.Name != pbAppFolder.Name)
					{
						((WinForms.Controls.GlyphButton)btnInfo.GetValue(this)).Enabled = false;
					}
				}
				pbAppFolder.Focus();
			}
		}

		private void EnableDisableButtons()
		{
			#region Enablen
			if (AppFolderIsSetAndValid)
			{
				pbDlDest.Enabled = true;
				pbMdLoc.Enabled = true;
				pbConfBak.Enabled = true;
				pbScripts.Enabled = true;
			}
			#endregion
			#region Disablen
			else
			{
				pbDlDest.Enabled = false;
				pbMdLoc.Enabled = false;
				pbConfBak.Enabled = false;
				pbScripts.Enabled = false;
			}
			#endregion
			pbAppFolder.Focus();
		}

		private void CreateConfigFiles(string rootFolder)
		{

			if (!string.IsNullOrEmpty(rootFolder))
			{
				if (string.IsNullOrEmpty(config.ArchiveFile))
				{
					config.ArchiveFile = Path.Combine(rootFolder, "archive.txt");
				}
				if (!File.Exists(config.ArchiveFile))
				{
					File.Create(config.ArchiveFile);
				}
				if (string.IsNullOrEmpty(config.IndexDat))
				{
					config.IndexDat = Path.Combine(rootFolder, "index.dat");
				}
				if (!File.Exists(config.IndexDat))
				{
					File.Create(config.IndexDat).Dispose();
				}
			}
		}

		private void CreateDirectoryStructure(string rootFolder)
		{
			if (rootFolder is null)
			{
				throw new ArgumentNullException(nameof(rootFolder));
			}

			DirectoryInfo dirInfo = new DirectoryInfo(rootFolder);

			if (!dirInfo.Exists)
			{
				dirInfo.Create();
			}
			if (!Directory.Exists(Path.Combine(dirInfo.FullName, "dl")))
			{
				dirInfo.CreateSubdirectory("dl").CreateSubdirectory("pl");
			}
			if (!Directory.Exists(Path.Combine(dirInfo.FullName, "md")))
			{
				dirInfo.CreateSubdirectory("md").CreateSubdirectory("pl");
			}
			if (!Directory.Exists(Path.Combine(dirInfo.FullName, "conf")))
			{
				dirInfo.CreateSubdirectory("conf");
			}
			if (!Directory.Exists(Path.Combine(dirInfo.FullName, "scripts")))
			{
				dirInfo.CreateSubdirectory("scripts");
			}
		}

		private void CreateScripts(string rootFolder)
		{
			// TODO: Skript anpassen!
			string scripts = Path.Combine(rootFolder, "scripts");
			// HACK: youtube-dl
			CommandBuilder.GenerateScript(Path.Combine(scripts, "mddump.sh"), "youtube-dl --skip-download -J $1");
			CommandBuilder.GenerateScript(Path.Combine(scripts, "mdlistdump.sh"), "youtube-dl -J --playlist-start \"$1\" --playlist-end \"$2\" --skip-download --yes-playlist \"$3\"");
			CommandBuilder.GenerateScript(Path.Combine(scripts, "execalistdl.sh"), "youtube-dl -o $LISTOUTTEMPL --yes-playlist --playlist-start $1 --playlist-end $2 -x --audio-format $3 --format $4 --audio-quality $5 $6 --restrict-filenames --add-metadata");
			CommandBuilder.GenerateScript(Path.Combine(scripts, "execvlistdl.sh"), "youtube-dl -o $LISTOUTTEMPL --yes-playlist --playlist-start $1 --playlist-end $2 --format $3 --audio-quality $4 $5 --restrict-filenames --add-metadata");
		}

		private void UpdateConfig(string rootFolder)
		{
			config.AppFolder.FullPath = rootFolder;
			config.AppFolder.DownloadFolderPath = Path.Combine(rootFolder, "dl");
			config.AppFolder.MetadataFolderPath = Path.Combine(rootFolder, "md");
			config.AppFolder.ConfigBackupFolderPath = Path.Combine(rootFolder, "conf");
			config.AppFolder.ScriptsFolderPath = Path.Combine(rootFolder, "scripts");
		}
		#endregion
	}
}
