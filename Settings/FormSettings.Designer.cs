﻿using System;
using System.Windows.Forms;

namespace Settings
{
	partial class FormSettings
	{
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		/// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Vom Windows Form-Designer generierter Code

		/// <summary>
		/// Erforderliche Methode für die Designerunterstützung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.tbAudQ = new System.Windows.Forms.TrackBar();
			this.cbArchive = new System.Windows.Forms.CheckBox();
			this.cbIndexDatUeberschreiben = new System.Windows.Forms.CheckBox();
			this.pbArchivNeuAnlegen = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.rbVid2 = new System.Windows.Forms.RadioButton();
			this.rbVid1 = new System.Windows.Forms.RadioButton();
			this.rbVid0 = new System.Windows.Forms.RadioButton();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.rbAud = new System.Windows.Forms.RadioButton();
			this.rbVid = new System.Windows.Forms.RadioButton();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.rbAud2 = new System.Windows.Forms.RadioButton();
			this.rbAud1 = new System.Windows.Forms.RadioButton();
			this.rbAud0 = new System.Windows.Forms.RadioButton();
			this.lblAppFolder = new System.Windows.Forms.Label();
			this.lblDlDest = new System.Windows.Forms.Label();
			this.lblMdLoc = new System.Windows.Forms.Label();
			this.lblConfBak = new System.Windows.Forms.Label();
			this.lblScripts = new System.Windows.Forms.Label();
			this.dfAppFolder = new System.Windows.Forms.TextBox();
			this.dfDlDest = new System.Windows.Forms.TextBox();
			this.dfMdLoc = new System.Windows.Forms.TextBox();
			this.dfConfBak = new System.Windows.Forms.TextBox();
			this.dfScripts = new System.Windows.Forms.TextBox();
			this.pbAppFolder = new Settings.WinForms.Controls.GlyphButton();
			this.pbDlDest = new Settings.WinForms.Controls.GlyphButton();
			this.pbMdLoc = new Settings.WinForms.Controls.GlyphButton();
			this.pbConfBak = new Settings.WinForms.Controls.GlyphButton();
			this.pbScripts = new Settings.WinForms.Controls.GlyphButton();
			this.pbOk = new Settings.WinForms.Controls.GlyphButton();
			this.pbReset = new Settings.WinForms.Controls.GlyphButton();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			tabPage1 = new System.Windows.Forms.TabPage();
			this.panel1.SuspendLayout();
			this.groupBox5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tbAudQ)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.tabControl1.SuspendLayout();
			tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.groupBox5);
			this.panel1.Controls.Add(this.groupBox1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(2, 2);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(623, 567);
			this.panel1.TabIndex = 1;
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.tbAudQ);
			this.groupBox5.Controls.Add(this.cbArchive);
			this.groupBox5.Controls.Add(this.cbIndexDatUeberschreiben);
			this.groupBox5.Location = new System.Drawing.Point(39, 261);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(563, 204);
			this.groupBox5.TabIndex = 1;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Diverses";
			// 
			// tbAudQ
			// 
			this.tbAudQ.LargeChange = 2;
			this.tbAudQ.Location = new System.Drawing.Point(6, 41);
			this.tbAudQ.Maximum = 9;
			this.tbAudQ.Name = "tbAudQ";
			this.tbAudQ.Size = new System.Drawing.Size(316, 82);
			this.tbAudQ.TabIndex = 1;
			this.tbAudQ.TickStyle = System.Windows.Forms.TickStyle.None;
			this.tbAudQ.ValueChanged += new System.EventHandler(this.tbAudQ_ValueChanged);
			// 
			// cbArchive
			// 
			this.cbArchive.AutoSize = true;
			this.cbArchive.Location = new System.Drawing.Point(366, 41);
			this.cbArchive.Name = "cbArchive";
			this.cbArchive.Size = new System.Drawing.Size(175, 29);
			this.cbArchive.TabIndex = 16;
			this.cbArchive.Text = "Titel archivieren";
			this.cbArchive.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.cbArchive.ThreeState = true;
			this.cbArchive.UseVisualStyleBackColor = true;
			this.cbArchive.CheckedChanged += new System.EventHandler(this.cbArchive_CheckedChanged);
			// 
			// cbIndexDatUeberschreiben
			// 
			this.cbIndexDatUeberschreiben.AutoSize = true;
			this.cbIndexDatUeberschreiben.Enabled = false;
			this.cbIndexDatUeberschreiben.Checked = false;
			this.cbIndexDatUeberschreiben.Location = new System.Drawing.Point(366, 80);
			this.cbIndexDatUeberschreiben.Name = "cbIndexDatUeberschreiben";
			this.cbIndexDatUeberschreiben.Size = new System.Drawing.Size(175, 29);
			this.cbIndexDatUeberschreiben.TabIndex = 17;
			this.cbIndexDatUeberschreiben.Text = "Index zurücksetzen";
			this.cbIndexDatUeberschreiben.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.cbIndexDatUeberschreiben.ThreeState = false;
			this.cbIndexDatUeberschreiben.UseVisualStyleBackColor = true;
			this.cbIndexDatUeberschreiben.CheckedChanged += new System.EventHandler(this.cbIndexDatUeberschreiben_CheckedChanged);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.groupBox4);
			this.groupBox1.Controls.Add(this.groupBox3);
			this.groupBox1.Controls.Add(this.groupBox2);
			this.groupBox1.Location = new System.Drawing.Point(39, 26);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(563, 215);
			this.groupBox1.TabIndex = 18;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Medienziel";
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.rbVid2);
			this.groupBox4.Controls.Add(this.rbVid1);
			this.groupBox4.Controls.Add(this.rbVid0);
			this.groupBox4.Location = new System.Drawing.Point(294, 48);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(227, 139);
			this.groupBox4.TabIndex = 6;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = " &Videoformat";
			this.groupBox4.Visible = false;
			// 
			// rbVid2
			// 
			this.rbVid2.AutoSize = true;
			this.rbVid2.Location = new System.Drawing.Point(117, 98);
			this.rbVid2.Name = "rbVid2";
			this.rbVid2.Size = new System.Drawing.Size(100, 29);
			this.rbVid2.TabIndex = 3;
			this.rbVid2.TabStop = true;
			this.rbVid2.Text = "WEBM";
			this.rbVid2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.rbVid2.UseVisualStyleBackColor = true;
			this.rbVid2.CheckedChanged += new System.EventHandler(this.rbFormat_CheckedChanged);
			// 
			// rbVid1
			// 
			this.rbVid1.AutoSize = true;
			this.rbVid1.Location = new System.Drawing.Point(117, 63);
			this.rbVid1.Name = "rbVid1";
			this.rbVid1.Size = new System.Drawing.Size(82, 29);
			this.rbVid1.TabIndex = 4;
			this.rbVid1.TabStop = true;
			this.rbVid1.Text = "MKV";
			this.rbVid1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.rbVid1.UseVisualStyleBackColor = true;
			this.rbVid1.CheckedChanged += new System.EventHandler(this.rbFormat_CheckedChanged);
			// 
			// rbVid0
			// 
			this.rbVid0.AutoSize = true;
			this.rbVid0.Location = new System.Drawing.Point(117, 28);
			this.rbVid0.Name = "rbVid0";
			this.rbVid0.Size = new System.Drawing.Size(78, 29);
			this.rbVid0.TabIndex = 2;
			this.rbVid0.TabStop = true;
			this.rbVid0.Text = "MP4";
			this.rbVid0.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.rbVid0.UseVisualStyleBackColor = true;
			this.rbVid0.CheckedChanged += new System.EventHandler(this.rbFormat_CheckedChanged);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.rbAud);
			this.groupBox3.Controls.Add(this.rbVid);
			this.groupBox3.Location = new System.Drawing.Point(36, 48);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(216, 135);
			this.groupBox3.TabIndex = 6;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Typ";
			// 
			// rbAud
			// 
			this.rbAud.AutoSize = true;
			this.rbAud.Checked = true;
			this.rbAud.Location = new System.Drawing.Point(37, 28);
			this.rbAud.Name = "rbAud";
			this.rbAud.Size = new System.Drawing.Size(93, 29);
			this.rbAud.TabIndex = 1;
			this.rbAud.TabStop = true;
			this.rbAud.Text = " &Audio";
			this.rbAud.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.rbAud.UseVisualStyleBackColor = true;
			this.rbAud.CheckedChanged += new System.EventHandler(this.rbAudioVideo_CheckedChanged);
			// 
			// rbVid
			// 
			this.rbVid.AutoSize = true;
			this.rbVid.Location = new System.Drawing.Point(37, 78);
			this.rbVid.Name = "rbVid";
			this.rbVid.Size = new System.Drawing.Size(93, 29);
			this.rbVid.TabIndex = 0;
			this.rbVid.Text = " &Video";
			this.rbVid.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.rbVid.UseVisualStyleBackColor = true;
			this.rbVid.CheckedChanged += new System.EventHandler(this.rbAudioVideo_CheckedChanged);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.rbAud2);
			this.groupBox2.Controls.Add(this.rbAud1);
			this.groupBox2.Controls.Add(this.rbAud0);
			this.groupBox2.Location = new System.Drawing.Point(294, 48);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(227, 139);
			this.groupBox2.TabIndex = 5;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = " &Audioformat";
			// 
			// rbAud2
			// 
			this.rbAud2.AutoSize = true;
			this.rbAud2.Location = new System.Drawing.Point(118, 98);
			this.rbAud2.Name = "rbAud2";
			this.rbAud2.Size = new System.Drawing.Size(98, 29);
			this.rbAud2.TabIndex = 3;
			this.rbAud2.TabStop = true;
			this.rbAud2.Text = "WAVE";
			this.rbAud2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.rbAud2.UseVisualStyleBackColor = true;
			this.rbAud2.CheckedChanged += new System.EventHandler(this.rbFormat_CheckedChanged);
			// 
			// rbAud1
			// 
			this.rbAud1.AutoSize = true;
			this.rbAud1.Location = new System.Drawing.Point(118, 63);
			this.rbAud1.Name = "rbAud1";
			this.rbAud1.Size = new System.Drawing.Size(89, 29);
			this.rbAud1.TabIndex = 4;
			this.rbAud1.TabStop = true;
			this.rbAud1.Text = "FLAC";
			this.rbAud1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.rbAud1.UseVisualStyleBackColor = true;
			this.rbAud1.CheckedChanged += new System.EventHandler(this.rbFormat_CheckedChanged);
			// 
			// rbAud0
			// 
			this.rbAud0.AutoSize = true;
			this.rbAud0.Location = new System.Drawing.Point(118, 28);
			this.rbAud0.Name = "rbAud0";
			this.rbAud0.Size = new System.Drawing.Size(78, 29);
			this.rbAud0.TabIndex = 2;
			this.rbAud0.TabStop = true;
			this.rbAud0.Text = "MP3";
			this.rbAud0.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.rbAud0.UseVisualStyleBackColor = true;
			this.rbAud0.CheckedChanged += new System.EventHandler(this.rbFormat_CheckedChanged);
			// 
			// lblAppFolder
			// 
			this.lblAppFolder.Location = new System.Drawing.Point(9, 12);
			this.lblAppFolder.Name = "lblAppFolder";
			this.lblAppFolder.Size = new System.Drawing.Size(138, 30);
			this.lblAppFolder.TabIndex = 1;
			this.lblAppFolder.Text = "Application Folder:";
			this.lblAppFolder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDlDest
			// 
			this.lblDlDest.Location = new System.Drawing.Point(9, 48);
			this.lblDlDest.Name = "lblDlDest";
			this.lblDlDest.Size = new System.Drawing.Size(174, 30);
			this.lblDlDest.TabIndex = 3;
			this.lblDlDest.Text = "Download Destination:";
			this.lblDlDest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMdLoc
			// 
			this.lblMdLoc.Location = new System.Drawing.Point(9, 84);
			this.lblMdLoc.Name = "lblMdLoc";
			this.lblMdLoc.Size = new System.Drawing.Size(138, 30);
			this.lblMdLoc.TabIndex = 6;
			this.lblMdLoc.Text = "Metadata Location:";
			this.lblMdLoc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblConfBak
			// 
			this.lblConfBak.Location = new System.Drawing.Point(9, 120);
			this.lblConfBak.Name = "lblConfBak";
			this.lblConfBak.Size = new System.Drawing.Size(174, 30);
			this.lblConfBak.TabIndex = 8;
			this.lblConfBak.Text = "Configuration Backups:";
			this.lblConfBak.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblScripts
			// 
			this.lblScripts.Location = new System.Drawing.Point(9, 156);
			this.lblScripts.Name = "lblScripts";
			this.lblScripts.Size = new System.Drawing.Size(174, 30);
			this.lblScripts.TabIndex = 10;
			this.lblScripts.Text = "Scripts:";
			this.lblScripts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dfAppFolder
			// 
			this.dfAppFolder.Enabled = false;
			this.dfAppFolder.Location = new System.Drawing.Point(183, 12);
			this.dfAppFolder.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.dfAppFolder.Name = "dfAppFolder";
			this.dfAppFolder.ReadOnly = true;
			this.dfAppFolder.Size = new System.Drawing.Size(385, 29);
			this.dfAppFolder.TabIndex = 11;
			// 
			// dfDlDest
			// 
			this.dfDlDest.Enabled = false;
			this.dfDlDest.Location = new System.Drawing.Point(183, 48);
			this.dfDlDest.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.dfDlDest.Name = "dfDlDest";
			this.dfDlDest.ReadOnly = true;
			this.dfDlDest.Size = new System.Drawing.Size(385, 29);
			this.dfDlDest.TabIndex = 12;
			// 
			// dfMdLoc
			// 
			this.dfMdLoc.Enabled = false;
			this.dfMdLoc.Location = new System.Drawing.Point(183, 84);
			this.dfMdLoc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.dfMdLoc.Name = "dfMdLoc";
			this.dfMdLoc.ReadOnly = true;
			this.dfMdLoc.Size = new System.Drawing.Size(385, 29);
			this.dfMdLoc.TabIndex = 13;
			// 
			// dfConfBak
			// 
			this.dfConfBak.Enabled = false;
			this.dfConfBak.Location = new System.Drawing.Point(183, 120);
			this.dfConfBak.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.dfConfBak.Name = "dfConfBak";
			this.dfConfBak.ReadOnly = true;
			this.dfConfBak.Size = new System.Drawing.Size(385, 29);
			this.dfConfBak.TabIndex = 14;
			// 
			// dfScripts
			// 
			this.dfScripts.Enabled = false;
			this.dfScripts.Location = new System.Drawing.Point(183, 156);
			this.dfScripts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.dfScripts.Name = "dfScripts";
			this.dfScripts.ReadOnly = true;
			this.dfScripts.Size = new System.Drawing.Size(385, 29);
			this.dfScripts.TabIndex = 15;
			// 
			// pbAppFolder
			// 
			this.pbAppFolder.Font = new System.Drawing.Font("Segoe MDL2 Assets", 14.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(0)));
			this.pbAppFolder.Location = new System.Drawing.Point(573, 12);
			this.pbAppFolder.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.pbAppFolder.Name = "pbAppFolder";
			this.pbAppFolder.Size = new System.Drawing.Size(34, 30);
			this.pbAppFolder.TabIndex = 2;
			this.pbAppFolder.UseVisualStyleBackColor = true;
			this.pbAppFolder.Click += new System.EventHandler(this.pbAppFolder_Click);
			// 
			// pbDlDest
			// 
			this.pbDlDest.BackColor = System.Drawing.Color.AliceBlue;
			this.pbDlDest.Enabled = false;
			this.pbDlDest.Font = new System.Drawing.Font("Segoe MDL2 Assets", 14.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(0)));
			this.pbDlDest.Location = new System.Drawing.Point(573, 48);
			this.pbDlDest.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.pbDlDest.Name = "pbDlDest";
			this.pbDlDest.Size = new System.Drawing.Size(34, 30);
			this.pbDlDest.TabIndex = 5;
			this.pbDlDest.UseVisualStyleBackColor = true;
			this.pbDlDest.Click += new System.EventHandler(this.pbSelectFolder_Click);
			// 
			// pbMdLoc
			// 
			this.pbMdLoc.BackColor = System.Drawing.Color.AliceBlue;
			this.pbMdLoc.Enabled = false;
			this.pbMdLoc.Font = new System.Drawing.Font("Segoe MDL2 Assets", 14.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(0)));
			this.pbMdLoc.Location = new System.Drawing.Point(573, 84);
			this.pbMdLoc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.pbMdLoc.Name = "pbMdLoc";
			this.pbMdLoc.Size = new System.Drawing.Size(34, 30);
			this.pbMdLoc.TabIndex = 7;
			this.pbMdLoc.UseVisualStyleBackColor = true;
			this.pbMdLoc.Click += new System.EventHandler(this.pbSelectFolder_Click);
			// 
			// pbConfBak
			// 
			this.pbConfBak.BackColor = System.Drawing.Color.AliceBlue;
			this.pbConfBak.Enabled = false;
			this.pbConfBak.Font = new System.Drawing.Font("Segoe MDL2 Assets", 14.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(0)));
			this.pbConfBak.Location = new System.Drawing.Point(573, 120);
			this.pbConfBak.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.pbConfBak.Name = "pbConfBak";
			this.pbConfBak.Size = new System.Drawing.Size(34, 30);
			this.pbConfBak.TabIndex = 9;
			this.pbConfBak.UseVisualStyleBackColor = true;
			this.pbConfBak.Click += new System.EventHandler(this.pbSelectFolder_Click);
			// 
			// pbScripts
			// 
			this.pbScripts.BackColor = System.Drawing.Color.AliceBlue;
			this.pbScripts.Enabled = false;
			this.pbScripts.Font = new System.Drawing.Font("Segoe MDL2 Assets", 14.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(0)));
			this.pbScripts.Location = new System.Drawing.Point(573, 156);
			this.pbScripts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.pbScripts.Name = "pbScripts";
			this.pbScripts.Size = new System.Drawing.Size(34, 30);
			this.pbScripts.TabIndex = 11;
			this.pbScripts.UseVisualStyleBackColor = true;
			this.pbScripts.Click += new System.EventHandler(this.pbSelectFolder_Click);
			// 
			// pbOk
			// 
			this.pbOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(254)));
			this.pbOk.ForeColor = System.Drawing.Color.Green;
			this.pbOk.Glyph = WinForms.Components.SegoeMDL2Assets.Ok;
			this.pbOk.Location = new System.Drawing.Point(421, 624);
			this.pbOk.Name = "pbOk";
			this.pbOk.Size = new System.Drawing.Size(201, 42);
			this.pbOk.TabIndex = 2;
			this.pbOk.UseVisualStyleBackColor = true;
			this.pbOk.Click += new System.EventHandler(this.pbOk_Click);
			// 
			// pbReset
			// 
			this.pbReset.Font = new System.Drawing.Font("Segoe MDL2 Assets", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
			this.pbReset.ForeColor = System.Drawing.Color.Blue;
			this.pbReset.Glyph = WinForms.Components.SegoeMDL2Assets.Clear;
			this.pbReset.Location = new System.Drawing.Point(207, 625);
			this.pbReset.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.pbReset.Name = "pbReset";
			this.pbReset.Size = new System.Drawing.Size(193, 42);
			this.pbReset.TabIndex = 12;
			this.pbReset.UseVisualStyleBackColor = true;
			this.pbReset.Click += new System.EventHandler(this.pbReset_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl1.Controls.Add(tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Location = new System.Drawing.Point(0, -2);
			this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 1;
			this.tabControl1.Size = new System.Drawing.Size(635, 608);
			this.tabControl1.TabIndex = 2;
			// 
			// tabPage1
			// 
			tabPage1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			tabPage1.Controls.Add(this.panel1);
			tabPage1.Location = new System.Drawing.Point(4, 33);
			tabPage1.Margin = new System.Windows.Forms.Padding(5, 2, 5, 2);
			tabPage1.Name = "tabPage1";
			tabPage1.Padding = new System.Windows.Forms.Padding(2);
			tabPage1.Size = new System.Drawing.Size(627, 571);
			tabPage1.TabIndex = 0;
			tabPage1.Text = "Medienkonfiguration";
			// 
			// tabPage2
			// 
			this.tabPage2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.tabPage2.Controls.Add(this.dfScripts);
			this.tabPage2.Controls.Add(this.dfConfBak);
			this.tabPage2.Controls.Add(this.dfMdLoc);
			this.tabPage2.Controls.Add(this.dfDlDest);
			this.tabPage2.Controls.Add(this.dfAppFolder);
			this.tabPage2.Controls.Add(this.pbScripts);
			this.tabPage2.Controls.Add(this.pbConfBak);
			this.tabPage2.Controls.Add(this.pbMdLoc);
			this.tabPage2.Controls.Add(this.pbDlDest);
			this.tabPage2.Controls.Add(this.pbAppFolder);
			this.tabPage2.Controls.Add(this.lblScripts);
			this.tabPage2.Controls.Add(this.lblConfBak);
			this.tabPage2.Controls.Add(this.lblMdLoc);
			this.tabPage2.Controls.Add(this.lblDlDest);
			this.tabPage2.Controls.Add(this.lblAppFolder);
			this.tabPage2.Location = new System.Drawing.Point(4, 33);
			this.tabPage2.Margin = new System.Windows.Forms.Padding(5, 2, 5, 2);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(2);
			this.tabPage2.Size = new System.Drawing.Size(627, 571);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Verzeichnisstruktur";
			// 
			// FormSettings
			// 
			this.AcceptButton = this.pbOk;
			this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(634, 678);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.pbOk);
			this.Controls.Add(this.pbReset);
			this.Name = "FormSettings";
			this.panel1.ResumeLayout(false);
			this.groupBox5.ResumeLayout(false);
			this.groupBox5.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.tbAudQ)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.tabControl1.ResumeLayout(false);
			tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion
		private global::Settings.WinForms.Controls.GlyphButton pbOk;
		private global::Settings.WinForms.Controls.GlyphButton pbReset;
		private global::Settings.WinForms.Controls.GlyphButton pbAppFolder;
		private global::Settings.WinForms.Controls.GlyphButton pbDlDest;
		private global::Settings.WinForms.Controls.GlyphButton pbMdLoc;
		private global::Settings.WinForms.Controls.GlyphButton pbConfBak;
		private global::Settings.WinForms.Controls.GlyphButton pbScripts;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.Label lblAppFolder;
		private System.Windows.Forms.Label lblDlDest;
		private System.Windows.Forms.Label lblMdLoc;
		private System.Windows.Forms.Label lblConfBak;
		private System.Windows.Forms.Label lblScripts;
		private System.Windows.Forms.TextBox dfAppFolder;
		private System.Windows.Forms.TextBox dfDlDest;
		private System.Windows.Forms.TextBox dfMdLoc;
		private System.Windows.Forms.TextBox dfConfBak;
		private System.Windows.Forms.TextBox dfScripts;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RadioButton rbAud;
		private System.Windows.Forms.RadioButton rbVid;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RadioButton rbAud1;
		private System.Windows.Forms.RadioButton rbAud2;
		private System.Windows.Forms.RadioButton rbAud0;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.RadioButton rbVid2;
		private System.Windows.Forms.RadioButton rbVid1;
		private System.Windows.Forms.RadioButton rbVid0;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.CheckBox cbArchive;
		private System.Windows.Forms.Button pbArchivNeuAnlegen;
		private System.Windows.Forms.CheckBox cbIndexDatUeberschreiben;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TrackBar tbAudQ;
	}
}

