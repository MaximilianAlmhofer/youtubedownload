﻿using Microsoft.Extensions.Configuration;

using System;
using System.Drawing;
using System.IO;
using System.Reflection;

namespace yCraft.Settings
{
	internal sealed class Globals
	{
		public static IConfigurationRoot AppSettings { get; internal set; }

		internal static Font ItalicFont
		{
			get
			{
				return new Font("Microsoft Sans Serif", 10.625F, FontStyle.Italic, GraphicsUnit.World, 0);
			}
		}

		public static Font LightFont
		{
			get
			{
				return new Font("Microsoft Sans Serif", 10.625F, FontStyle.Regular, GraphicsUnit.World, 0);
			}
		}

		public static Font ButtonFont
		{
			get
			{
				return new Font("Segoe MDL2 Assets", 14.125F, FontStyle.Regular, GraphicsUnit.World, 0);
			}
		}

		internal static string FriendlyApplicationName
		{
			get => Path.GetFileNameWithoutExtension(AppDomain.CurrentDomain.FriendlyName);
		}

		internal static BindingFlags InstanceNonPublicGetFieldSetField
		{
			get
			{
				return (BindingFlags)3108;
			}
		}

		internal static BindingFlags InstanceNonPublicGetField
		{
			get
			{
				return (BindingFlags)1060;
			}
		}

		internal static BindingFlags InstanceNonPublicMethod
		{
			get
			{
				return (BindingFlags)292;
			}
		}

		internal static BindingFlags InstancePublicMethod
		{
			get
			{
				return (BindingFlags)276;
			}
		}

		internal static BindingFlags StaticNonPublicMethod
		{
			get
			{
				return (BindingFlags)296;
			}
		}

		internal static BindingFlags InstancePublicGetSetP
		{
			get
			{
				return (BindingFlags)12308;
			}
		}
	}
}
