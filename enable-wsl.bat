@echo off
REM Virtualization Platform feature base for Hyper-V
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
REM Hyper-V Virtual Machine Environment form WSL
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Hyper-V /all /norestart
REM WSL Subsystem for (-WSL2: native Linux Kernel, -WSL1: transformations syscalls from linux to ntos)
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart