﻿using System;

namespace FlacBox
{
	/// <summary>
	/// Base FlacBox exception class.
	/// </summary>
	public class FlacException : Exception
	{
		private static string flacStreamMarkerBytes => "0x66, 0x4C, 0x61, 0x43";

		public FlacException(string message)
			: base(message)
		{

		}

		public FlacException(string message, byte[] streamMarker) 
			: this(message)
		{
			string streamMarkerBytes = string.Empty;
			if (streamMarker != null && streamMarker.Length > 0)
				streamMarkerBytes = string.Join(", ", streamMarker);

			message = string.Join("\r\n", string.Format("Expected: {0} Actual are: {1}", flacStreamMarkerBytes, streamMarkerBytes));
		}
	}
}
