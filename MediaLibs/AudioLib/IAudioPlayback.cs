﻿using MvvmCross.Base;

using System.ComponentModel;
using System.Threading.Tasks;

namespace Lib.Modules.Playback
{
	public interface IAudioPlayback : INotifyPropertyChanged
	{

		/// <summary>
		/// Asynchronber Dispatcher für Dispatcher Objekkt Viewmodels 
		/// </summary>
		IMvxMainThreadAsyncDispatcher AsyncDispatcher { get; }


		/// <summary>
		/// Load media source and initialize playback
		/// </summary>
		/// <param name="fileName">Media source file</param>
		void Load(string fileName);


		/// <summary>
		/// Entladen der aktuellen Datei.
		/// </summary>
		void UnLoad();


		/// <summary>
		/// Schließt die gerade geöffnete Datei.
		/// </summary>
		void CloseFile();


		/// <summary>
		/// Erzeugt das Ausgabegerät. 
		/// <code>
		///		NAudio.Wave.WaveOut, NAudio.Wave.DirectSoundOut, NAudio.Wave.WasapiOut, NAudio.Wave.WaveOutEvent, etc...
		/// </code>
		/// </summary>
		void EnsureDeviceCreated();
	}
}
