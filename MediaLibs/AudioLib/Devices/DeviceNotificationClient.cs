﻿using MediaCore;

using NAudio.CoreAudioApi;

using System;

namespace AudioLib.Devices
{
	public class DeviceNotificationClient : NAudio.CoreAudioApi.Interfaces.IMMNotificationClient
	{
		private IDeviceNotificationTarget notificationTarget;

		public DeviceNotificationClient(IDeviceNotificationTarget notificationTarget)
		{
			this.notificationTarget = notificationTarget;
		}

		public void OnDefaultDeviceChanged(DataFlow flow, Role role, string defaultDeviceId)
		{
			try
			{
				MMDevice device = notificationTarget.DeviceEnumerator.GetDevice(defaultDeviceId);

				notificationTarget.UnregisterDeviceNotificationCallback();

				device.AudioSessionManager.RefreshSessions();

				notificationTarget.RegisterDeviceNotificationCallback();
			}
			catch
			{
				/* ~ */
			}

			//string guidString = defaultDeviceId.Split('{')[2].TrimEnd('}');
			//if (Guid.TryParse(guidString, out Guid guid))
			//{
			//	notificationTarget.ApplyDeviceChange(guid);
			//}
		}

		public void OnDeviceAdded(string pwstrDeviceId)
		{
			OnDeviceStateChanged(pwstrDeviceId, DeviceState.Active);
		}

		public void OnDeviceRemoved(string deviceId)
		{
			OnDeviceStateChanged(deviceId, DeviceState.NotPresent);
		}

		public void OnDeviceStateChanged(string deviceId, DeviceState newState)
		{
			string guidString = deviceId.Split('{')[2].TrimEnd('}');
			if (Guid.TryParse(guidString, out Guid guid))
			{
				switch (newState)
				{
					case DeviceState.Disabled:
					case DeviceState.NotPresent:
					case DeviceState.Unplugged:
						notificationTarget.ApplyDeviceChange(Guid.Empty);
						break;

					default:
						notificationTarget.ApplyDeviceChange(guid);
						break;
				}
			}
		}

		public void OnPropertyValueChanged(string pwstrDeviceId, PropertyKey key)
		{
			//Do some Work
			//fmtid & pid are changed to formatId and propertyId in the latest version NAudio
		}
	}
}
