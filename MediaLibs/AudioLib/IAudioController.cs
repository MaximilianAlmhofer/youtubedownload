﻿using MediaCore;

using MvvmCross.Commands;

namespace AudioLib
{
	public interface IAudioController : IMediaController
	{

		/// <summary>
		/// Command to mute the volume.
		/// </summary>
		IMvxCommand MuteCommand { get; }

		/// <summary>
		/// Determines wheter in shuffle mode or not.
		/// </summary>
		bool IsShuffle { get; set; }
	}
}
