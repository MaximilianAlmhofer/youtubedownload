﻿using MvvmCross.Logging;

using NAudio.Wave;

using xtra_snd_tools.Logging;

using System;
using System.Collections.Generic;

namespace AudioLib
{
	public sealed class DeviceInformation
	{
		private static readonly IMvxLogProvider logProvider;


		static DeviceInformation()
		{
			logProvider = MvvmCross.Mvx.IoCProvider.GetSingleton<IMvxLogProvider>();
		}


		public static IEnumerable<WaveOutCapabilities> GetWaveOutCapabilities()
		{

			WaveOutCapabilities[] caps = new WaveOutCapabilities[WaveOut.DeviceCount];

			for (int i = 0; i < WaveOut.DeviceCount;)
			{
				try
				{
					caps[i] = WaveOut.GetCapabilities(i++);
				}
				catch (Exception ex)
				{
					logProvider.GetExceptionLoggerAndLogFormat<DeviceInformation>(string.Format(
						Properties.Resources.WaveOut_DeviceCapabilities_Query_Failed, WaveOut.DeviceCount), ex);
				}
			}
			return caps;
		}


		public static IEnumerable<WaveInCapabilities> GetWaveInCapabilities()
		{

			WaveInCapabilities[] caps = new WaveInCapabilities[WaveIn.DeviceCount];

			for (int i = 0; i < WaveOut.DeviceCount;)
			{
				try
				{
					caps[i] = WaveIn.GetCapabilities(i++);
				}
				catch (Exception ex)
				{
					logProvider.GetExceptionLoggerAndLogFormat<DeviceInformation>(string.Format(
						Properties.Resources.WaveIn_DeviceCapabilities_Query_Failed, WaveOut.DeviceCount), ex);
				}
			}
			return caps;
		}
	}
}
