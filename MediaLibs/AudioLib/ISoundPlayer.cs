﻿using System.ComponentModel;

namespace AudioLib.Spectrum
{
	/// <summary>
	/// Provides access to functionality that is common
	/// across all sound players.
	/// </summary>
	/// <seealso cref="IWaveformPlayer"/>
	/// <seealso cref="ISpectrumPlayer"/>
	public interface ISoundPlayer : INotifyPropertyChanged
	{
		/// <summary>
		/// Gets whether the sound player is currently playing audio.
		/// </summary>
		bool IsPlaying { get; }
	}
}
