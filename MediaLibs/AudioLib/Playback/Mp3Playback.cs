﻿using AudioLib.Sampling;

using NAudio.Wave;

using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;

namespace AudioLib.Playback
{
	public sealed class Mp3Playback : NAudioEngine
	{

		private EventHandler<NAudio.Wave.StoppedEventArgs> playbackStopped;
		
		public Mp3Playback(EventHandler<StoppedEventArgs> PlaybackStopped, string key) : base(key)
		{
			playbackStopped = PlaybackStopped;
		}

	
		public override double PixelWidth
		{
			get => pixelWidth;
			set
			{
				pixelWidth = value;
				if (sampleAggregator != null)
				{
					sampleAggregator.SecondsPerPixel = (int)((ActiveStream as Mp3FileReader)?.TotalSeconds() / Math.Max(1, value));
				}
			}
		}
		
		protected override void CreateDevice()
		{
			waveOutDevice = new NAudio.Wave.WaveOut { Volume = 0.5F, DeviceNumber = 0, DesiredLatency = 100 };
			waveOutDevice.PlaybackStopped += playbackStopped;
		}

		protected override void OpenFile(string fileName)
		{
			base.OpenFile(fileName);

			if (System.IO.File.Exists(fileName))
			{
				try
				{
					FileTag = TagLib.File.Create(fileName);
					ActiveStream = new Mp3FileReader(fileName);
					InitSampleAggregator();
					ChannelLength = WaveOutChannel.TotalTime.TotalSeconds;
					WaveOutChannel.Sample += WaveStream_Sample;
					waveOutDevice.Init(WaveOutChannel);
				}
				catch
				{
					ActiveStream = null;
				}
			}
		}

		
		protected override void WaveStream_Sample(object sender, SampleEventArgs e)
		{
			sampleAggregator.Add(e.Left, e.Right);
			long repeatStartPosition = (long)((SelectionBegin.TotalSeconds / ActiveStream.TotalTime.TotalSeconds) * ActiveStream.Length);
			long repeatStopPosition = (long)((SelectionEnd.TotalSeconds / ActiveStream.TotalTime.TotalSeconds) * ActiveStream.Length);
			if (((SelectionEnd - SelectionBegin) >= TimeSpan.FromMilliseconds(repeatThreshold)) && ActiveStream.Position >= repeatStopPosition)
			{
				sampleAggregator.Clear();
				ActiveStream.Position = repeatStartPosition;
			}
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}
	}
}
