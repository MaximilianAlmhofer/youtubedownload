﻿using NAudio.Dsp;

using System;
using System.Diagnostics;

namespace MediaCore
{
	public class FftEventArgs : EventArgs
	{
		private readonly Complex[] fastFourierTransformData;

		[DebuggerStepThrough]
		public FftEventArgs(Complex[] result)
		{
			this.fastFourierTransformData = result;
		}
		public Complex[] Result()
		{
			return fastFourierTransformData;
		}
	}
}
