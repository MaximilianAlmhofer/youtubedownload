﻿using NAudio.CoreAudioApi;

using System;

namespace MediaCore
{
	public interface IDeviceNotificationTarget
	{
		MMDeviceEnumerator DeviceEnumerator { get; }

		void ApplyDeviceChange(Guid deviceId);

		void RegisterDeviceNotificationCallback();

		void UnregisterDeviceNotificationCallback();
	}
}
