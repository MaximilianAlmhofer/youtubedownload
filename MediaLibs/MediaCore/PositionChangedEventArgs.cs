﻿
using System;
using System.Diagnostics;

namespace MediaCore
{
	public class PositionChangedEventArgs : EventArgs
	{

		[DebuggerStepThrough]
		public PositionChangedEventArgs(double position) => ChannelPosition = position;

		public double ChannelPosition { get; }
	}
}
