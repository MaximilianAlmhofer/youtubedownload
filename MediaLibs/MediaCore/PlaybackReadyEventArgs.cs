﻿
using System;
using System.Diagnostics;

namespace MediaCore
{
	public class PlaybackReadyEventArgs : EventArgs
	{
		[DebuggerStepThrough]
		public PlaybackReadyEventArgs(long position, long length)
		{
			Position = position;
			Length = length;
		}

		public long Position { get; }

		public long Length { get; }
	}
}
