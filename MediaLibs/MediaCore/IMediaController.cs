﻿using MvvmCross.Commands;

namespace MediaCore
{
	public interface IMediaController
	{
		/// <summary>
		/// Command to start the playback.
		/// </summary>
		IMvxCommand PlayPauseCommand { get; }

		/// <summary>
		/// Command to stop the playback.
		/// </summary>
		IMvxCommand StopCommand { get; }

		/// <summary>
		/// Command to skip to the previous item.
		/// </summary>
		IMvxCommand SkipBackCommand { get; }

		/// <summary>
		/// Command to skip to the next item.
		/// </summary>
		IMvxCommand SkipNextCommand { get; }
	}
}
