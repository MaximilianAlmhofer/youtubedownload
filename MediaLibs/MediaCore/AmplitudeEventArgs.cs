﻿using NAudio.Wave;

using System;
using System.Diagnostics;

namespace MediaCore
{
	public class AmplitudeEventArgs : EventArgs
	{
		[DebuggerStepThrough]
		public AmplitudeEventArgs(float minValue, float maxValue)
		{
			this.AmplitudePeakValue = maxValue;
			this.AmplitudeThroughValue = minValue;
		}

		/// <summary>
		/// Maxima Extrempunkt des Wavegraphen, Amplitudenhöchtpunkt
		/// </summary>
		public float AmplitudePeakValue { get; private set; }

		/// <summary>
		/// Minima Extrempunkt des Wavegraphen, Amplitudentiefstpunkt
		/// </summary>
		public float AmplitudeThroughValue { get; private set; }
	}
}
