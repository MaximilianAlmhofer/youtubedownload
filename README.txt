#########################################


1)  Activate WSL2 windows feature by running:

    powershell
    Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux

2)  Install Ubuntu Terminal:

   cmd.exe: bash   OR
   Windows Store: Ubuntu 18.04

3)  Install youtube-dl

    user@ubuntu: sudo apt -y install youtube-dl

4)  Add following vars to the local .bashrc:

    OUTTEMPL=%(title)s.%(ext)s                                          - Naming template for single files
    LISTOUTTEMPL=%(playlist)s/%(playlist_index)s-%(title)s.%(ext)s      - Naming template for playlists

5)  Take the AppDataTemplate folder from the project and place it at your recommended location.

6)  In appsettings.json adjust the path to config.json by the location set one step above.

########################################

File folder structure:

-[BASE FOLDER NAME]
    - DL
        - pl
    - MD
        - pl
    - SCRIPTS
        
    - archive.txt
    - config.json
    - index.dat
    - list.conf
    - std.conf
    - std_fallback.conf

########################################

Entity FrameworkCore existing schema scaffolding

dotnet-ef dbcontext scaffold "Server=TURM;Database=d.server_x;User=devel;Password=dev" Microsoft.EntityFrameworkCore.SqlServer 
-o Model -c "TracksContext" --schema dbo --json --data-annotations --force