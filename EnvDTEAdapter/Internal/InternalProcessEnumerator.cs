﻿using System.Collections;
using System.Linq;

namespace EnvDTEAdapter.Internal
{


	internal static class InternalProcessEnumerator
	{
		internal static IEnumerable GetLocalProcessList(EnvDTE.DTE dte)
		{
			var processes = dte.Debugger.LocalProcesses;
			yield return processes.Cast<EnvDTE.Process>()?.Select(p => new
			{
				p.Name,
				p.ProcessID,
				p.Parent,
				p.Programs.DTE.DisplayMode,
			});
		}
	}
}
