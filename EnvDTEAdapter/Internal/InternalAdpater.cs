﻿
using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;

namespace EnvDTEAdapter
{


	internal static class InternalAdapter
	{

		internal static void AttachDebugger(EnvDTE.DTE dte)
		{
			if (!Debugger.IsAttached)
			{
				var processes = dte.Debugger.LocalProcesses;
				var dProc = Process.GetCurrentProcess();
				var envDteProc = processes.Cast<EnvDTE.Process>().SingleOrDefault(p => dProc.Id == p.ProcessID);
				if (envDteProc != null)
				{
					try
					{
						envDteProc.Attach();
					}
					catch (COMException ex)
					{
						int hResult = Marshal.GetHRForException(ex);
						Environment.FailFast(string.Format(CultureInfo.InvariantCulture, "Exception: {0}\r\nHRESULT: {1}", ex.Message, hResult));
					}
					catch (Exception e)
					{
						Debug.WriteLine(e.ToString());
					}
				}
			}
		}

		internal static void DetachDebugger(EnvDTE.DTE dte)
		{
			if (System.Diagnostics.Debugger.IsAttached && dte != null)
			{
				try
				{
					dte.Debugger.DetachAll();
				}
				catch (COMException ex)
				{
					int hResult = Marshal.GetHRForException(ex);
					Environment.FailFast(string.Format(CultureInfo.InvariantCulture, "Exception: {0}\r\nHRESULT: {1}", ex.Message, hResult));
				}
				catch (Exception e)
				{
					Debug.WriteLine(e.ToString());
				}
			}
		}
	}
}
