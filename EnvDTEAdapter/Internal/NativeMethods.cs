﻿
using System;
using System.Runtime.InteropServices;
using System.Security;

namespace EnvDTEAdapter
{
	[SecuritySafeCritical]
	internal static class NativeMethods
	{
		[DllImport("ole32.dll", PreserveSig = false)]
		internal static extern void CLSIDFromProgIDEx([MarshalAs(UnmanagedType.LPWStr)] string progId, out Guid clsid);

		[DllImport("oleaut32.dll", PreserveSig = false)]
		internal static extern void GetActiveObject(ref Guid rclsid, IntPtr reserved, [MarshalAs(UnmanagedType.Interface)] out object ppunk);

		[DllImport("ole32.dll", PreserveSig = false)]
		internal static extern void CLSIDFromProgID([MarshalAs(UnmanagedType.LPWStr)] string progId, out Guid clsid);
	}
}
