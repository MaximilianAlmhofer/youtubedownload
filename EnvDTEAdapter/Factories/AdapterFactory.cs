﻿using System;

namespace EnvDTEAdapter
{
	public static class AdapterFactory
	{
		public static IEnvDTEAdapter CreateWrapper()
		{
			return (IEnvDTEAdapter)Activator.CreateInstance(typeof(EnvDTEAdapter));
		}
	}
}
