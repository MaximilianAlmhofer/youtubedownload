﻿namespace EnvDTEAdapter
{
	public interface IEnvDTEAdapter
	{
		void AttachDebugger(string objectName);

		void DetachDebugger(string objectName);
	}
}
