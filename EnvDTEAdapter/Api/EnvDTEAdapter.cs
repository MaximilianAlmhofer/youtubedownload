﻿using EnvDTE;

using System;

namespace EnvDTEAdapter
{
	internal class EnvDTEAdapter : IEnvDTEAdapter
	{
		public void AttachDebugger(string objectName)
		{
			InternalAdapter.AttachDebugger(DTEObject.GetDTE(objectName));
		}

		public void DetachDebugger(string objectName)
		{
			InternalAdapter.DetachDebugger(DTEObject.GetDTE(objectName));
		}
	}

	internal static class DTEObject
	{
		public static DTE GetDTE(string objectName)
		{
			return (DTE)GetActiveObject(objectName); // VisualStudio.DTE.16.0
		}

		public static object GetActiveObject(string progID)
		{
			object ppunk = null;
			Guid clsid;
			try
			{
				NativeMethods.CLSIDFromProgIDEx(progID, out clsid);
			}
			catch (Exception)
			{
				NativeMethods.CLSIDFromProgID(progID, out clsid);
			}

			NativeMethods.GetActiveObject(ref clsid, IntPtr.Zero, out ppunk);
			return ppunk;
		}
	}
}
