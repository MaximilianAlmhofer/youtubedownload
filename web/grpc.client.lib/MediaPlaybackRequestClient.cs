﻿using System;
using System.IO;
using System.Threading.Tasks;

using grpc.client.lib.ApiHandlers;
using grpc.client.lib.Receivers;
using grpc.protobuf.Protos.SampleAggregation;

using Lib.Utility.Threading;

using NAudio.Dsp;

namespace grpc.client.lib
{
	public class MediaPlaybackRequestClient : IDisposable
	{
		private readonly IStreamedReceiver<Complex> receiver;

		public MediaPlaybackRequestClient(IStreamedReceiver<Complex> receiver)
		{
			this.receiver = receiver;
		}

		public async Task RequestAsync(string storageItemId, string filePath, CancellationHandle cancellationHandle)
		{
			
		}

		public void Dispose()
		{
			receiver.Dispose();
		}
	}
}
