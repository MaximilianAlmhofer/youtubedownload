﻿
using Newtonsoft.Json;

using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace grpc.client.lib.ApiHandlers
{
	public sealed class HttpClientSingleton : IDisposable, IAsyncDisposable
	{
		private readonly Func<CancellationToken, Task<bool>> refreshToken;
		private readonly Action<LoginResponse> setup;
		private HttpClient http;
		private LoginResponse login;
		private bool disposedValue;

		private HttpClientSingleton()
		{
			http = new HttpClient();
			ServicePointManager.ServerCertificateValidationCallback += (server, cert, chain, ssl) => true;
			setup = (_login) =>
			{
				var httpd = new HttpClientHandler
				{
					PreAuthenticate = true
				};
				http = new HttpClient(httpd, false);
				http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _login.JwtToken);
			};
			refreshToken = async token =>
			{
				bool refresh = false;
				bool first = login is null;
				if (first || (login != null && login.ExpiresAt.Value.Subtract(DateTime.Now) < TimeSpan.Zero))
				{
					using (var content = new StringContent("{ \"email\" : \"almhofer.m@hotmail.com\", \"password\" : \"yGQ.998%%\" }", Encoding.UTF8, "application/vnd+server-x.login+json"))
					{
						using (var response = await http.PostAsync("https://localhost:55443/account/login", content, token))
						{
							if (response.IsSuccessStatusCode)
							{
								login = JsonConvert.DeserializeObject<LoginResponse>(await response.Content.ReadAsStringAsync());
								if (first)
								{
									setup(login);
								}

								refresh = true;
							}
						}
					}
				}
				return refresh;
			};
			
			ServicePoint sp = ServicePointManager.FindServicePoint(new Uri("https://localhost:55443"));
			sp.ConnectionLeaseTimeout = 120 * 60;
			sp.SetTcpKeepAlive(true, 100, 3000);
		}
		private class Holder
		{
			internal static HttpClientSingleton instance;
			static Holder()
			{
				instance = new HttpClientSingleton();
			}
		}

		public static HttpClient Instance(CancellationToken? token = null)
		{
			Holder.instance.refreshToken(token.GetValueOrDefault());
			return Holder.instance.http;
		}
		public Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			return http.SendAsync(request, cancellationToken);
		}
		private void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					http.Dispose();
				}
				http = null;
				disposedValue = true;
			}
		}
		public void Dispose()
		{
			Dispose(true);
		}
		public ValueTask DisposeAsync()
		{
			return new ValueTask(Task.Run(() => Dispose()));
		}
	}
}
