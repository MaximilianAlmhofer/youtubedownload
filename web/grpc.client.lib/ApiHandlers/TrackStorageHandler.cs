﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using grpc.protobuf.Protos.Messages;
using grpc.protobuf.Protos.TrackStorage;

using Grpc.Core;
using Grpc.Net.Client;

namespace grpc.client.lib.ApiHandlers
{
	public class LoginResponse
	{
		public string JwtToken { get; set; }

		public DateTime? ExpiresAt { get; set; }
	}

	public class TrackStorageHandler : GrpcHandler
	{
		public TrackStorageHandler(string url)
		{
			Channel = GrpcChannel.ForAddress(url, new GrpcChannelOptions
			{
				Credentials = ChannelCredentials.Create(new SslCredentials(), CallCredentials.FromInterceptor(async (context, metadata) =>
				{
					var auth = HttpClientSingleton.Instance().DefaultRequestHeaders.Authorization;
					if (auth != null)
					{
						metadata.Add("Authorization", $"{auth.Scheme} {auth.Parameter}");
						await Task.CompletedTask;
					}
				}))
			});
		}

		public async Task<TrackInfo> GetByIdAsync(string id)
		{
			var client = new protobuf.Protos.TrackStorage.TrackStorage.TrackStorageClient(Channel);
			TrackReply reply = await client.GetByIdAsync(new protobuf.Protos.TrackStorage.TrackRequest { Id = id });
			return reply.Item;
		}

		public async Task<int> AddAsync(TrackInfo item)
		{
			var client = new protobuf.Protos.TrackStorage.TrackStorage.TrackStorageClient(Channel);
			AddReply reply = await client.AddAsync(new protobuf.Protos.TrackStorage.AddRequest { Item = item });
			return reply.Status;
		}

		public async Task<IEnumerable<TrackInfo>> QueryAsync(string property, string value)
		{
			var client = new protobuf.Protos.TrackStorage.TrackStorage.TrackStorageClient(Channel);
			TracksReply reply = await client.QueryAsync(new protobuf.Protos.TrackStorage.TracksRequest { Property = property, Value = value });
			return reply.Items;
		}
	}
}
