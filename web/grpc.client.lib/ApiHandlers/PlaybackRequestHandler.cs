﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

using grpc.client.lib.Receivers;
using grpc.protobuf.Protos.SampleAggregation;

using Grpc.Core;
using Grpc.Net.Client;

using Lib.Utility.Threading;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;

using MvvmCross.Core;

using NAudio.Dsp;

namespace grpc.client.lib.ApiHandlers
{
	public class PlaybackRequestHandler : GrpcHandler
	{
		public PlaybackRequestHandler(string url)
		{

			Channel = GrpcChannel.ForAddress(url, new GrpcChannelOptions
			{
				Credentials = ChannelCredentials.Create(new SslCredentials(), CallCredentials.FromInterceptor(async (context, metadata) =>
				{
					var auth = HttpClientSingleton.Instance().DefaultRequestHeaders.Authorization;
					if (auth != null)
					{
						metadata.Add("Authorization", $"{auth.Scheme} {auth.Parameter}");
						await Task.CompletedTask;
					}
				}))
			});
		}


		[HttpPost]
		public async IAsyncEnumerable<Task> StreamSamplesAsync(IStreamedReceiver<Complex> receiver, MediaItemInfoRequest request, CancellationHandle cancellationHandle)
		{
			var client = new SampleAggregation.SampleAggregationClient(Channel);

			using (var response = client.StreamSamples(request))
			{
				await Task.Delay(2000);
				await foreach (var item in response.ResponseStream.ReadAllAsync(cancellationHandle.Token))
				{
					yield return receiver.ReceiveAsync(new Complex { X = item.X, Y = item.Y });
				}
			}
		}
	}
}
