﻿using Grpc.Net.Client;

using System;
using System.Threading.Tasks;

namespace grpc.client.lib.ApiHandlers
{
	public class GrpcHandler : IDisposable, IAsyncDisposable
	{
		protected GrpcChannel Channel;

		private bool disposedValue;

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					Channel.Dispose();
				}
				Channel = null;
				disposedValue = true;
			}
		}

		public void Dispose()
		{
			Dispose(disposing: true);
			GC.SuppressFinalize(this);
		}

		public async ValueTask DisposeAsync()
		{
			await Channel.ShutdownAsync();
		}
	}
}
