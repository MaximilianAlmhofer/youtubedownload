﻿using System;

using Lib.Utility.Abstract;

namespace grpc.client.lib.Receivers
{
	public class StreamResponseEventArgs<T> : EventArgs
	{

		public StreamResponseEventArgs(T data)
		{
			Result = Result<T>.Ok(data);
		}

		public Result<T> Result { get; }
	}
}
