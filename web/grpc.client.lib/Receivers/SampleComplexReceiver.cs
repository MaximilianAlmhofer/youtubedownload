﻿using System;
using System.Threading.Tasks;

using MvvmCross.Base;

using NAudio.Dsp;

namespace grpc.client.lib.Receivers
{
	public class SampleComplexReceiver : IStreamedReceiver<Complex>
	{
		public event EventHandler<StreamResponseEventArgs<Complex>> ReceiveStreamPart = delegate { };

		private IMvxMainThreadAsyncDispatcher dispatcher;

		protected virtual void OnReceiveStreamPart(Complex complex)
		{
			var args = new StreamResponseEventArgs<Complex>(complex);
			var handler = ReceiveStreamPart;
			handler?.Invoke(this, args);
		}

		public async Task ReceiveAsync(Complex reply)
		{
			await dispatcher.ExecuteOnMainThreadAsync(() => OnReceiveStreamPart(reply));
		}

		public static IStreamedReceiver<Complex> Create()
		{
			return new SampleComplexReceiver(MvvmCross.Mvx.IoCProvider.Resolve<IMvxMainThreadAsyncDispatcher>());
		}

		public SampleComplexReceiver(IMvxMainThreadAsyncDispatcher dispatcher)
		{
			this.dispatcher = dispatcher;
		}

		public void Dispose()
		{
			dispatcher = null;
		}
	}
}
