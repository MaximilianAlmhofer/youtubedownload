﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace grpc.client.lib.Receivers
{
	public interface IReceiver
	{
		Action OnError(Exception ex, object data);
	}
}
