﻿using System;
using System.Threading.Tasks;

using grpc.protobuf.Protos.SampleAggregation;

namespace grpc.client.lib.Receivers
{
	public interface IStreamedReceiver<TResult> : IDisposable
	{
		Task ReceiveAsync(TResult reply);
		event EventHandler<StreamResponseEventArgs<TResult>> ReceiveStreamPart;
	}
}
