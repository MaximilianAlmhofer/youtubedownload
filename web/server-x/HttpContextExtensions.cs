﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;

namespace serverx
{
	public static class HttpContextExtensions
	{
		public static Task FailAsync(this HttpResponse response, HttpStatusCode statusCode, string reason)
		{
			response.StatusCode = (int)statusCode;
			return response.WriteAsync(reason);
		}
	}
}
