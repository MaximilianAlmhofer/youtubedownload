﻿
using System.Collections.Generic;

namespace serverx.Configuration
{
	public class AppSettings
	{
		public string TokenIssuer { get; set; }

		public string TokenHash { get; set; }

		public string Audience { get; set; }

		public List<string> AllowedHosts { get; set; }

		public List<KnownUser> KnownUsers { get; set; }

		public List<IdMap> IdMap { get; set; }
	}
}
