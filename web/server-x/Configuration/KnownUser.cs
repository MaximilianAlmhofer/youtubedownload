﻿using System;

namespace serverx.Configuration
{
	public class KnownUser
	{
		public string Name { get; set; }

		public string Email { get; set; }

		public Guid? Id { get; set; }
	}
}
