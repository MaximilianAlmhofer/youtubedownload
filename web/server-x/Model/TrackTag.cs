﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace serverx.Model
{
	[Table("M_TRACK_TAG")]
	public partial class TrackTag
	{
		[Key]
		[Column("ID")]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }
		[Column("TAG_ID")]
		public int TagId { get; set; }
		[Column("TRACK_ID")]
		[StringLength(30)]
		public string TrackId { get; set; }

		[ForeignKey(nameof(TagId))]
		[InverseProperty(nameof(Model.Tag.TracksTags))]
		public virtual Tag Tag { get; set; }

		[ForeignKey(nameof(TrackId))]
		[InverseProperty(nameof(Trackitem.TracksTags))]
		public virtual Trackitem Track { get; set; }
	}
}
