﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace serverx.Model
{
	[Table("D_TRACKITEM")]
	public partial class Trackitem
	{
		public Trackitem()
		{
			TracksCategories = new HashSet<TrackCategory>();
			TracksFormats = new HashSet<TrackFormat>();
			TracksTags = new HashSet<TrackTag>();
		}


		[Key]
		[Column("ID")]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		[Column("USR_ID")]
		public Guid UsrId { get; set; }

		[Column("TRACK_ID")]
		[StringLength(30)]
		public string TrackId { get; set; }
		[Column("TITLE")]
		[StringLength(256)]
		public string Title { get; set; }
		[Column("THUMBNAIL")]
		[StringLength(256)]
		public string Thumbnail { get; set; }
		[Column("VCODEC")]
		[StringLength(20)]
		public string Vcodec { get; set; }
		[Column("ACODEC")]
		[StringLength(20)]
		public string Acodec { get; set; }
		[Column("CREATOR")]
		[StringLength(256)]
		public string Creator { get; set; }
		[Column("ALBUM")]
		[StringLength(256)]
		public string Album { get; set; }
		[Column("DT_UPLOAD")]
		public DateTime? DtUpload { get; set; }
		[Column("DURATION")]
		public int? Duration { get; set; }
		[Column("FILENAME")]
		[StringLength(256)]
		public string Filename { get; set; }
		[Column("UPLOADER_URL")]
		[StringLength(500)]
		public string UploaderUrl { get; set; }
		[Column("DESCRIPTION")]
		[StringLength(4000)]
		public string Description { get; set; }
		[Column("FORMAT_NAME")]
		[StringLength(100)]
		public string FormatName { get; set; }
		[Column("FORMAT_ID")]
		[StringLength(10)]
		public string FormatId { get; set; }
		[Column("ABR", TypeName = "float")]
		public float? Abr { get; set; }
		[Column("UPLOADER_ID")]
		[StringLength(50)]
		public string UploaderId { get; set; }
		[Column("CHANNEL_NR")]
		[StringLength(50)]
		public string ChannelNr { get; set; }
		[Column("CHANNEL_URL")]
		[StringLength(128)]
		public string ChannelUrl { get; set; }
		[Column("WIDTH", TypeName = "float")]
		public float? Width { get; set; }
		[Column("HEIGHT", TypeName = "float")]
		public float? Height { get; set; }
		[Column("LIKE_COUNT")]
		public int? LikeCount { get; set; }
		[Column("DISLIKE_COUNT")]
		public int? DislikeCount { get; set; }
		[Column("AVG_RATING", TypeName = "float")]
		public float? AvgRating { get; set; }

		[InverseProperty("Track")]
		public virtual ICollection<TrackCategory> TracksCategories { get; set; }

		[InverseProperty("Track")]
		public virtual ICollection<TrackFormat> TracksFormats { get; set; }

		[InverseProperty("Track")]
		public virtual ICollection<TrackTag> TracksTags { get; set; }
	}
}
