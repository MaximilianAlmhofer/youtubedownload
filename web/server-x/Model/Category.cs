﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace serverx.Model
{
	[Table("D_CATEGORY")]
	public partial class Category
	{
		public Category()
		{
			TracksCategories = new HashSet<TrackCategory>();
		}

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Column("ID")]
		public int Id { get; set; }
		[Column("NAME", TypeName = "nvarchar(450)")]
		public string Name { get; set; }

		[InverseProperty("Category")]
		public virtual ICollection<TrackCategory> TracksCategories { get; set; }
	}
}
