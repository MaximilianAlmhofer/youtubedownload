﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace serverx.Model
{

	public partial class MusictrackContext : DbContext
	{
		private readonly IConfiguration configuration;

		public MusictrackContext(IConfiguration configuration)
		{
			this.configuration = configuration;
		}

		public MusictrackContext(IConfiguration configuration, DbContextOptions<MusictrackContext> options)
			: base(options)
		{
			this.configuration = configuration;
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			if (!optionsBuilder.IsConfigured)
			{
				optionsBuilder.UseSqlServer(configuration.GetConnectionString("serverx.devel.db"));
			}
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{


			modelBuilder.Entity<Format>(entity =>
			{
				entity.HasKey(e => e.FmtId)
					.IsClustered(false);
			});


			modelBuilder.Entity<Trackitem>(entity =>
			{

				entity.HasKey(e => new { e.UsrId, e.TrackId })
					.HasName("PK_D_TRACK");

				entity.HasIndex(e => e.TrackId)
					.HasName("AK_D_TRACK_TRACK_ID")
					.IsUnique();
			});


			modelBuilder.Entity<Category>(entity =>
			{
				entity.HasIndex(e => e.Name)
					.HasName("AK_D_CATEGORY_NAME")
					.IsUnique()
					.HasFilter("[NAME] IS NOT NULL");
			});


			modelBuilder.Entity<Tag>(entity =>
			{
				entity.HasIndex(e => e.Name)
					.HasName("AK_D_TAG_NAME")
					.IsUnique()
					.HasFilter("[NAME] IS NOT NULL");

				entity.HasAlternateKey(x => x.Name)
					.IsClustered(false);
			});


			modelBuilder.Entity<TrackCategory>(entity =>
			{
				entity.HasKey(e => e.Id)
					.HasName("PK_MTrack_Category")
					.IsClustered(false);

				entity.HasIndex(e => e.CategoryId)
					.HasName("IX_MTrackCateg_CATEG_ID");

				entity.HasIndex(e => e.TrackId)
					.HasName("FK_DTrack_MTrackCategs");

				entity.HasOne(d => d.Category)
					.WithMany(p => p.TracksCategories)
					.HasForeignKey(d => d.CategoryId)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_Category_TracksCategories");

				entity.HasOne(d => d.Track)
					.WithMany(p => p.TracksCategories)
					.HasPrincipalKey(p => p.TrackId)
					.HasForeignKey(d => d.TrackId)
					.HasConstraintName("FK_Track_TracksCategories");
			});

			modelBuilder.Entity<TrackFormat>(entity =>
			{

				entity.HasOne(d => d.Format)
					.WithMany(p => p.TracksFormats)
					.HasForeignKey(d => d.FormatId)
					.HasConstraintName("FK_Format_TracksFormats");

				entity.HasOne(d => d.Track)
					.WithMany(p => p.TracksFormats)
					.HasPrincipalKey(p => p.TrackId)
					.HasForeignKey(d => d.TrackId)
					.HasConstraintName("FK_Track_TracksFormats");
			});

			modelBuilder.Entity<TrackTag>(entity =>
			{
				entity.HasKey(e => e.Id)
					.HasName("PK_MTrack_Tag")
					.IsClustered(false);

				entity.HasIndex(e => e.TagId)
					.HasName("IX_MTrackTags_TAG_ID")
					.IsClustered();

				entity.HasOne(d => d.Tag)
					.WithMany(p => p.TracksTags)
					.HasForeignKey(d => d.TagId)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_Tag_TracksTags");

				entity.HasOne(d => d.Track)
					.WithMany(p => p.TracksTags)
					.HasPrincipalKey(p => p.TrackId)
					.HasForeignKey(d => d.TrackId)
					.HasConstraintName("FK_Track_TracksTags");
			});

			OnModelCreatingPartial(modelBuilder);
		}

		partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

		public virtual DbSet<Category> DCategory { get; set; }
		public virtual DbSet<Format> DFormat { get; set; }
		public virtual DbSet<Tag> DTag { get; set; }
		public virtual DbSet<Trackitem> DTrackitem { get; set; }
		public virtual DbSet<TrackCategory> MTrackCategory { get; set; }
		public virtual DbSet<TrackFormat> MTrackFormat { get; set; }
		public virtual DbSet<TrackTag> MTrackTag { get; set; }
	}
}
