﻿
using grpc.protobuf.Protos.Messages;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

using serverx.Model.Relational;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace serverx.Model
{
	internal static class QueryExtensions
	{
		internal static IQueryable<T> In<T>(this IEnumerable<T> source, MusictrackContext context,
			Expression<Func<IEnumerable<object>, IEnumerable>> inExpression, object[] args)
		{
			var func = Expression.Lambda<Func<IEnumerable<object>, IEnumerable>>(inExpression).Compile()(args);
			return null;
		}
	}

	internal static class Helpers
	{
		internal static class InitHelper
		{
			public static void OnDbContextConfigure(IHostEnvironment environment, DbContextOptionsBuilder optionsBuilder)
			{
				if (!optionsBuilder.IsConfigured)
				{
					optionsBuilder.UseSqlServer("Server=TURM;Database=D.SERVERX.MUSICMGMT;User=devel;Password=dev",
							options =>
							{
								options.EnableRetryOnFailure(2);
								options.MigrationsAssembly(typeof(Program).Assembly.FullName);
								options.UseRelationalNulls(true);
							})
						.EnableServiceProviderCaching()
						.EnableDetailedErrors(environment.IsDevelopment())
						.EnableSensitiveDataLogging(environment.IsDevelopment());
				}
			}
		}

		internal static class ModelTransformer
		{
			public static Format ConvertDTO(FormatInfo value)
			{
				Format fmt = new Format
				{
					FmtId = value.FormatId,
					Url = value.Url,
					Ext = value.Ext,
					Note = value.FormatNote,
					Name = value.FormatName,
					Filesize = value.Filesize,
					Abr = value.Abr > 0 ? value.Abr : 0,
					Fps = value.Fps > 0 ? (int)value.Fps : 0,
					Acodec = value.Acodec,
					Vcodec = value.Vcodec,
					Height = value.Height,
					Width = value.Width,
					Tbr = value.Tbr,
					Protocol = value.Protocol,
				};
				return fmt;
			}

			public static Trackitem ConvertDTO(TrackInfo value, PK_D_TRACK key)
			{
				Trackitem fmt = new Trackitem
				{
					TrackId = key.TRACK_ID,
					FormatId = value.FormatId,
					FormatName = value.Format,
					UsrId = key.USR_ID,
					Album = value.Album,
					Filename = value.Filename,
					ChannelNr = value.ChannelId,
					UploaderId = value.UploaderId,
					UploaderUrl = value.UploaderUrl,
					DtUpload = System.DateTime.Now,
					Acodec = value.Acodec,
					Thumbnail = value.Thumbnail,
					Vcodec = value.Vcodec,
					Height = value.Height,
					Width = value.Width,
					LikeCount = value.LikeCount > 0 ? (int)value.LikeCount : 0,
					DislikeCount = value.DislikeCount > 0 ? (int)value.DislikeCount : 0,
					Creator = value.Creator,
					Duration = value.Duration > 0 ? (int)value.Duration : 0,
					Title = value.Title,
					Abr = value.Abr,
					AvgRating = value.AverageRating
				};
				return fmt;
			}

			public static TrackInfo ConvertModel(Trackitem trackModel)
			{
				var trackInfo = new TrackInfo
				{
					Id = trackModel.TrackId,
					Width = trackModel.Width ?? 0F,
					Height = trackModel.Height ?? 0F,
					Title = trackModel.Title,
					Thumbnail = trackModel.Thumbnail,
					Description = trackModel.Description,
					Acodec = trackModel.Acodec,
					Vcodec = trackModel.Vcodec,
					ChannelId = trackModel.ChannelNr,
					ChannelUrl = trackModel.ChannelUrl,
					Format = trackModel.FormatName,
					FormatId = trackModel.FormatId,
					Filename = trackModel.Filename,
					Album = trackModel.Album,
					Creator = trackModel.Creator,
					UploaderId = trackModel.UploaderId,
					UploaderUrl = trackModel.UploaderUrl,
					LikeCount = (uint?)trackModel.LikeCount ?? 0,
					DislikeCount = (uint?)trackModel.DislikeCount ?? 0
				};
				return trackInfo;
			}

			public static List<Category> ConvertModel(IEnumerable<grpc.protobuf.Protos.Messages.Category> categories)
			{
				return new List<Category>(categories?.Select(n => new Category { Name = n.Name }));
			}

			public static List<Tag> ConvertModel(IEnumerable<grpc.protobuf.Protos.Messages.Tag> tags)
			{
				return new List<Tag>(tags?.Select(n => new Tag { Name = n.Name }));
			}
		}
	}
}
