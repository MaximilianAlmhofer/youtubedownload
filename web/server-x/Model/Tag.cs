﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace serverx.Model
{
	[Table("D_TAG")]
	public partial class Tag : IEquatable<Tag>
	{
		public Tag()
		{
			TracksTags = new HashSet<TrackTag>();
		}

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Column("ID")]
		public int Id { get; set; }

		[Column("NAME")]
		public string Name { get; set; }

		[InverseProperty("Tag")]
		public virtual ICollection<TrackTag> TracksTags { get; set; }

		public bool Equals([AllowNull] Tag other)
		{
			return Name.ToLower() == other.Name.ToLower();
		}
	}
}
