﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace serverx.Model
{
	[Table("M_TRACK_CATEGORY")]
	public partial class TrackCategory
	{
		[Key]
		[Column("ID")]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }
		[Column("CATEGORY_ID")]
		public int CategoryId { get; set; }
		[Column("TRACK_ID")]
		[StringLength(30)]
		public string TrackId { get; set; }

		[ForeignKey(nameof(CategoryId))]
		[InverseProperty(nameof(Model.Category.TracksCategories))]
		public virtual Category Category { get; set; }

		[ForeignKey(nameof(TrackId))]
		[InverseProperty(nameof(Model.Trackitem.TracksCategories))]
		public virtual Trackitem Track { get; set; }
	}
}
