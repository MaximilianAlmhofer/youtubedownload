﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace serverx.Model
{
	[Table("D_FORMAT")]
	public partial class Format
	{
		public Format()
		{
			TracksFormats = new HashSet<TrackFormat>();
		}

		[Key]
		[Column("FMT_ID")]
		[StringLength(30)]
		public string FmtId { get; set; }
		[Column("NAME")]
		[StringLength(500)]
		public string Name { get; set; }
		[Column("URL")]
		[StringLength(1000)]
		public string Url { get; set; }
		[Column("VCODEC")]
		[StringLength(20)]
		public string Vcodec { get; set; }
		[Column("ACODEC")]
		[StringLength(20)]
		public string Acodec { get; set; }
		[Column("NOTE")]
		[StringLength(500)]
		public string Note { get; set; }
		[Column("WIDTH", TypeName = "float")]
		public float? Width { get; set; }
		[Column("HEIGHT", TypeName = "float")]
		public float? Height { get; set; }
		[Column("ABR", TypeName = "float")]
		public float? Abr { get; set; }
		[Column("TBR", TypeName = "float")]
		public float? Tbr { get; set; }
		[Column("FSIZE")]
		public long? Filesize { get; set; }
		[Column("EXT")]
		[StringLength(30)]
		public string Ext { get; set; }
		[Column("FPS")]
		public int? Fps { get; set; }
		[Column("PROTOCOL")]
		[StringLength(30)]
		public string Protocol { get; set; }

		[InverseProperty("Format")]
		public virtual ICollection<TrackFormat> TracksFormats { get; set; }
	}
}
