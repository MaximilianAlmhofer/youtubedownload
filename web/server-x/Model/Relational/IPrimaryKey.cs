﻿using System;

namespace serverx.Model.Relational
{
	public interface IPrimaryKey
	{
		Guid USR_ID { get; set; }

		object[] Values { get; }
	}
}
