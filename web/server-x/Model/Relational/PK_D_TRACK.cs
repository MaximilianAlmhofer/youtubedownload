﻿
using System;

namespace serverx.Model.Relational
{
	public class PK_D_TRACK : IPrimaryKey
	{
		public PK_D_TRACK(Guid usr_id, string track_id)
		{
			if (usr_id == Guid.Empty)
			{
				throw new ArgumentNullException(nameof(usr_id));
			}

			TRACK_ID = track_id ?? throw new ArgumentNullException(nameof(track_id));
			USR_ID = usr_id;
		}
		public Guid USR_ID { get; set; }

		public string TRACK_ID { get; set; }

		public object[] Values => new object[] { USR_ID, TRACK_ID };
	}
}
