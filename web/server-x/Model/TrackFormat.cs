﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace serverx.Model
{
	[Table("M_TRACK_FORMAT")]
	public partial class TrackFormat
	{
		[Key]
		[Column("ID")]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }
		[Column("FORMAT_ID")]
		[StringLength(3)]
		public string FormatId { get; set; }
		[Column("TRACK_ID")]
		[StringLength(30)]
		public string TrackId { get; set; }
		[Column("USR_ID")]
		public Guid? UsrId { get; set; }

		[ForeignKey(nameof(FormatId))]
		[InverseProperty(nameof(Model.Format.TracksFormats))]
		public virtual Format Format { get; set; }

		[ForeignKey(nameof(TrackId))]
		[InverseProperty(nameof(Model.Trackitem.TracksFormats))]
		public virtual Trackitem Track { get; set; }
	}
}
