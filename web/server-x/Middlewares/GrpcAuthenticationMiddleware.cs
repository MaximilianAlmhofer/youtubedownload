﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace serverx.Middelwares
{
	internal class GrpcAuthenticationMiddleware
	{

		private readonly RequestDelegate next;
		private readonly ILoggerFactory loggerFactory;

		public GrpcAuthenticationMiddleware(ILoggerFactory loggerFactory, RequestDelegate next)
		{
			this.next = next;
			this.loggerFactory = loggerFactory;
		}

		public async Task InvokeAsync(HttpContext context)
		{
			string bearer = string.Empty;

			if (context.Request.Headers.TryGetValue("Authorization", out var headerValue))
			{
				try
				{
					bearer = headerValue.ToString().Split(" ").Last()?.TrimStart().TrimEnd();
					JwtSecurityToken token = new JwtSecurityToken(bearer);
					context.User.AddIdentity(new ClaimsIdentity(token.Claims));
				}
				catch (System.Exception ex)
				{
					loggerFactory.CreateLogger<GrpcAuthenticationMiddleware>()
						.LogError(ex, "Encoding the token failed!\r\nToken:\r\n{0}\r\nparam: bearer\r\nauth header: {1}", bearer, headerValue.ToString());
					context.Response.StatusCode = 500;
					return;
				}
			}
			await next.Invoke(context);
		}
	}
}
