﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using serverx.Model;

namespace serverx.Middelwares
{
	internal class DatabaseMigrationMiddleware
	{
		private readonly RequestDelegate next;

		public DatabaseMigrationMiddleware(RequestDelegate next)
		{
			this.next = next;
		}

		public async Task InvokeAsync(HttpContext context)
		{
			await context.RequestServices.GetRequiredService<MusictrackContext>()?.Database?.MigrateAsync();
			await next.Invoke(context);
		}
	}
}
