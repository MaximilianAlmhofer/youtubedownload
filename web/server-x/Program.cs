
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Hosting;

using System.Net;

namespace serverx
{
	public class Program
	{
		public static void Main(string[] args)
		{
			CreateHostBuilder(args).Build().Run();
		}

		public static IHostBuilder CreateHostBuilder(string[] args)
		{
			return Host.CreateDefaultBuilder(args)
.ConfigureWebHostDefaults(webBuilder =>
{
	webBuilder.UseKestrel(options =>
	{
		options.Listen(IPAddress.Any, 55443, listenOptions =>
	{
		listenOptions.Protocols = HttpProtocols.Http1AndHttp2;
		listenOptions.UseHttps();
		//listenOptions.UseHttps("Properties\\server-x.pfx", "yGQ.998%%");
		listenOptions.UseConnectionLogging();
	});
		//options.ListenAnyIP(55443, listenOptions =>
		//{
		//	listenOptions.Protocols = HttpProtocols.Http1AndHttp2;
		//	//listenOptions.UseHttps("Properties\\server-x.pfx", "yGQ.998%%");
		//	//listenOptions.UseConnectionLogging();
		//});
	});
	webBuilder.UseStartup<Startup>();
});
		}
	}
}
