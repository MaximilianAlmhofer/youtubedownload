﻿
using System;
using System.IdentityModel.Tokens.Jwt;

namespace serverx.Models
{

	public sealed class LoginResponse : StatusResponse
	{
		internal LoginResponse(ITokenResult authenticationResult)
		{
			try
			{
				var tok = new JwtSecurityToken(authenticationResult.Token);
				ExpiresAt = tok.ValidTo.ToLocalTime();
				JwtToken = authenticationResult.Token;
			}
			catch
			{
				Error = authenticationResult.Error;
			}
		}

		public override bool Success()
		{
			return !string.IsNullOrEmpty(JwtToken);
		}

		public string JwtToken { get; set; }

		public DateTime? ExpiresAt { get; set; }
	}
}
