﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

using AudioLib.Playback.v2;

using MediaCore;

namespace serverx.ServiceModels
{
	public class MediaItemInfo
	{
		public string FilePath { get; set; }

		public string StorageItemId { get; set; }

		public string Extension => Path.GetExtension(FilePath);
	}

	public interface IServerPlayback
	{
		Task RequestPlay(MediaItemInfo mediaItemInfo);


		event EventHandler<AmplitudeEventArgs> MaxCalculated;

		Task<bool> IsPlayingAsync();
	}

	public class ServerPlayback : IServerPlayback
	{

		private NAudioEngine_v2 nAudioEngine;

		public Task RequestPlay(MediaItemInfo mediaItemInfo)
		{
			return Task.WhenAll(Task.Run(() =>
			{
				if (nAudioEngine is null)
				{
					nAudioEngine = NAudioEngine_v2.Create(mediaItemInfo.StorageItemId, mediaItemInfo.Extension);
				}
				else
				{
					nAudioEngine.SetPlayback(mediaItemInfo.StorageItemId, mediaItemInfo.Extension);
				}

				nAudioEngine.MaximumCalculated += MaxCalculated;
				nAudioEngine.Load(mediaItemInfo.FilePath);
				nAudioEngine.Play();
			}).ContinueWith(completedTask => completedTask.GetAwaiter().GetResult(), TaskContinuationOptions.LongRunning));
		}

		public event EventHandler<AmplitudeEventArgs> MaxCalculated = delegate { };

		public Task<bool> IsPlayingAsync()
		{
			return Task.Run(() => nAudioEngine.IsPlaying);
		}
	}
}
