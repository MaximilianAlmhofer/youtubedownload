﻿namespace serverx.Models
{
	public abstract class StatusResponse
	{
		public abstract bool Success();

		public object Error { get; protected set; }
	}
}
