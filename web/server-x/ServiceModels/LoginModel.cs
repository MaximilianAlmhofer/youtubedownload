﻿using System.ComponentModel.DataAnnotations;

namespace serverx.Models
{
	public sealed class LoginModel
	{
		[Required(AllowEmptyStrings = false, ErrorMessage = "Email is required.")]
		public string Email { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Password is required.")]
		public string Password { get; set; }
	}
}
