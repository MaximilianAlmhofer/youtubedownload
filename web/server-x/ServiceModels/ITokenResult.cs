﻿namespace serverx.Models
{
	internal interface ITokenResult
	{
		string Token { get; set; }
		object Error { get; set; }
	}
}