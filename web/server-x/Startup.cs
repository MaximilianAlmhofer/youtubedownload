
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

using serverx.Configuration;
using serverx.Middelwares;
using serverx.Model;
using serverx.ServiceModels;
using serverx.Services;

using System.Text;

namespace serverx
{
	public class Startup
	{

		public Startup(IHostEnvironment environment)
		{
			Environment = environment;
			Configuration = new ConfigurationBuilder()
				.SetBasePath(environment.ContentRootPath)
				.AddJsonFile($"appsettings.{environment.EnvironmentName}.json", optional: false, reloadOnChange: true)
				.Build();
		}

		internal IHostEnvironment Environment { get; }
		internal IConfiguration Configuration { get; }


		public void ConfigureServices(IServiceCollection services)
		{
			services.AddCors();
			services.AddControllers();
			services.AddMvc(options =>
			{
				options.EnableEndpointRouting = false;
				options.SuppressAsyncSuffixInActionNames = true;
			}).AddJsonOptions(options =>
			{
				options.JsonSerializerOptions.IgnoreNullValues = true;
				options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
				options.JsonSerializerOptions.WriteIndented = true;
			});

			var appSettings = Configuration.GetSection("AppSettings");

			services.AddOptions();
			services.Configure<AppSettings>(appSettings);

			services.AddAuthentication(x =>
			{
				x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

			}).AddJwtBearer(x =>
			{
				x.RequireHttpsMetadata = true;
				x.SaveToken = true;
				x.TokenValidationParameters = new TokenValidationParameters
				{
					ValidateIssuerSigningKey = false,
					ValidateIssuer = true,
					ValidateAudience = true,
					ValidAudience = appSettings.GetValue<string>("Audience"),
					ValidIssuer = appSettings.GetValue<string>("TokenIssuer"),
					IssuerSigningKey = new SymmetricSecurityKey(Encoding.Unicode.GetBytes(appSettings.GetValue<string>("TokenHash")))
				};
				x.Validate("Bearer");
			});

			services.AddDbContext<MusictrackContext>();
			services.AddGrpc();
			services.AddTransient<ILoginService, LoginService>();
			services.AddSingleton<IServerPlayback, ServerPlayback>();
		}

		public void Configure(IApplicationBuilder app)
		{
			if (Environment.IsProduction())
			{
				app.UseHsts();
			}
			app.UseDeveloperExceptionPage();
			app.UseMiddleware<DatabaseMigrationMiddleware>();
			app.UseHttpsRedirection();
			app.UseRouting();
			app.UseAuthorization();
			app.UseMiddleware<GrpcAuthenticationMiddleware>();
			app.UseEndpoints(endpoint =>
			{
				endpoint.MapGrpcService<TrackStorageService>();
				endpoint.MapGrpcService<SampleAggregationService>();
			});
			app.UseMvc(routes =>
					routes.MapRoute("default", "{controller}/{action}"));
		}
	}
}
