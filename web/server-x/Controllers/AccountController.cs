﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

using serverx.Models;
using serverx.Services;

using System.Threading.Tasks;

namespace serverx.Controllers
{
	[Route("Account/Login"), AllowAnonymous, Consumes("application/vnd+server-x.login+json"), Produces("application/json")]
	public class AccountController : ControllerBase
	{

		private readonly ILoginService loginService;

		public AccountController(ILoginService loginService)
		{
			this.loginService = loginService;
		}

		[HttpPost]
		public async Task<IActionResult> LoginAsync([FromBody] LoginModel loginModel)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var result = await loginService.LoginAsync(loginModel.Email, loginModel.Password);

			if (result.Success())
			{
				return Ok(result);
			}

			if (result.Error is IdentityError)
			{
				return Unauthorized(result);
			}

			return BadRequest(result.Error);
		}
	}
}
