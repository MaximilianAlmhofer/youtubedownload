﻿using System;
using System.Threading.Tasks;

namespace serverx.Services
{
	internal class AsyncLock : IDisposable
	{
		private readonly TaskCompletionSource<object> _taskCompletion;

		public AsyncLock(TaskCompletionSource<object> taskCompletion)
		{
			_taskCompletion = taskCompletion;
		}

		public void Dispose()
		{
			_taskCompletion.TrySetResult(null);
		}
	}
}
