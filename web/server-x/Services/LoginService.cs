﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

using serverx.Configuration;
using serverx.Models;

using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace serverx.Services
{
	public class LoginService : ILoginService
	{
		private readonly IOptions<AppSettings> options;

		public LoginService(IOptions<AppSettings> options)
		{
			this.options = options;
		}

		public Task<LoginResponse> LoginAsync(string email, string password)
		{
			Guid? id = options.Value.IdMap.Find(x => x.Password == password)?.Id;
			LoginResponse response = new LoginResponse(GenerateJwtToken(email, id.GetValueOrDefault()));
			return Task.FromResult(response);
		}

		private TokenResult GenerateJwtToken(string email, Guid id)
		{
			if (id == Guid.Empty)
			{
				return new TokenResult(new IdentityErrorDescriber().PasswordMismatch());
			}

			DateTime expiration = DateTime.Now.AddHours(2);
			KnownUser user = options.Value.KnownUsers.Find(x => string.Equals(x.Email, email) && Guid.Equals(x.Id, id));

			if (user is null)
			{
				return new TokenResult(new IdentityErrorDescriber().InvalidEmail(email));
			}

			var claims = new List<Claim>
			{
				new Claim(JwtRegisteredClaimNames.Email, email),
				new Claim(JwtRegisteredClaimNames.Sub, user.Name),
				new Claim(JwtRegisteredClaimNames.Jti, id.ToString()),
				new Claim(JwtRegisteredClaimNames.Exp, expiration.ToString()),
			};
			var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(options.Value.TokenHash));
			var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
			var token = new JwtSecurityToken(
				options.Value.TokenIssuer,
				options.Value.Audience,
				claims,
				DateTime.Now,
				expiration,
				credentials);

			return new TokenResult(new JwtSecurityTokenHandler().WriteToken(token));
		}

		private class TokenResult : ITokenResult
		{
			public TokenResult(string token)
			{
				Token = token;
			}

			public TokenResult(object error)
			{
				Error = error;
			}

			public string Token { get; set; }

			public object Error { get; set; }
		}
	}
}