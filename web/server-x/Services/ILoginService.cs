﻿
using serverx.Models;

using System.Threading.Tasks;

namespace serverx.Services
{
	public interface ILoginService
	{
		Task<LoginResponse> LoginAsync(string email, string password);
	}
}
