﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Markup;
using System.Windows.Threading;

using Google.Protobuf;
using Google.Protobuf.WellKnownTypes;

using grpc.protobuf.Protos.SampleAggregation;

using Grpc.Core;

using Microsoft.Extensions.Hosting;

using serverx.ServiceModels;

namespace serverx.Services
{
	public class SampleAggregationService : SampleAggregation.SampleAggregationBase
	{
		private readonly IServerPlayback serverPlayback;

		public SampleAggregationService(IServerPlayback serverPlayback)
		{
			this.serverPlayback = serverPlayback;
		}

		public override Task StreamSamples(MediaItemInfoRequest request, IServerStreamWriter<SampleComplexReply> responseStream, ServerCallContext context)
		{

			var info = new MediaItemInfo()
			{
				FilePath = request.FilePath,
				StorageItemId = request.StorageItemId,
			};

			bool isFileTypeSupported = true;
			if (!string.Equals(request.Extension, info.Extension))
			{
				isFileTypeSupported = request.Extension.StartsWith(".") || info.Extension.StartsWith(".")
					&& request.Extension.EndsWith(info.Extension);
			}
			if (!isFileTypeSupported)
			{
				var response = context.GetHttpContext().Response;
				return response.FailAsync(HttpStatusCode.BadRequest, "ResponseFailReason_FileExtension_NonMatch")
					.ContinueWith(async _ => await response.CompleteAsync());
			}

			
			return Task.Run( async () => 
			{
				serverPlayback.MaxCalculated += (sender, e) =>
				{
					responseStream.WriteAsync(new SampleComplexReply
					{
						X = e.AmplitudePeakValue,
						Y = e.AmplitudeThroughValue,
						DateTimeStamp = Timestamp.FromDateTimeOffset(DateTimeOffset.UtcNow),
					}).ConfigureAwait(false);
				};

				await serverPlayback.RequestPlay(info);
				while (await serverPlayback.IsPlayingAsync()) ;
			});
		}

		private static readonly object lockToken = new LockCookie();
	}
}
