﻿using grpc.protobuf.Protos.Messages;
using grpc.protobuf.Protos.TrackStorage;

using Grpc.Core;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.IdentityModel.JsonWebTokens;

using serverx.Model;
using serverx.Model.Relational;

using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace serverx.Services
{
	public class TrackStorageService : TrackStorage.TrackStorageBase
	{
		private readonly MusictrackContext dbContext;

		public TrackStorageService(MusictrackContext context)
		{
			dbContext = context;
		}


		public override async Task<AddReply> Add(AddRequest request, ServerCallContext context)
		{
			var http = context.GetHttpContext();
			string userId = http.User.FindFirstValue(JwtRegisteredClaimNames.Jti);

			PK_D_TRACK pk = new PK_D_TRACK(Guid.Parse(userId), request.Item.Id);
			Trackitem track = Helpers.ModelTransformer.ConvertDTO(request.Item, pk);

			dbContext.DTrackitem.Attach(track).State = EntityState.Added;

			foreach (var _format in request.Item.Formats)
			{
				EntityEntry<Format> _entry = null;
				if (!dbContext.DFormat.Any(x => x.FmtId == _format.FormatId))
				{
					_entry = dbContext.Attach(Helpers.ModelTransformer.ConvertDTO(_format));
					_entry.State = EntityState.Added;
				}
				else
				{
					var _entity = dbContext.Set<Format>().Include(x => x.TracksFormats).ToList()
						.SingleOrDefault(x => x.FmtId == _format.FormatId);

					if (_entity != null)
					{
						_entry = dbContext.Attach(_entity);
						_entry.State = EntityState.Unchanged;
					}
				}
				if (_entry != null)
				{
					var trackLink = new TrackFormat
					{
						Format = _entry.Entity,
						FormatId = _entry.Entity.FmtId,
						UsrId = Guid.Parse(userId),
						Track = track,
						TrackId = track.TrackId
					};

					track.TracksFormats.Add(trackLink);
					_entry.Entity.TracksFormats.Add(trackLink);
				}
			}
			AddReply reply = new AddReply
			{
				Status = await dbContext.SaveChangesAsync()
			};

			#region  Tags
			var tags = Helpers.ModelTransformer.ConvertModel(request.Item.Tags);
			if (tags != null)
			{
				dbContext.DTag.UpdateRange(tags);
				foreach (var item in dbContext.DTag.Local.ToArray())
				{
					var tracksTags = new TrackTag { Tag = item, Track = track };
					track.TracksTags.Add(tracksTags);
					item.TracksTags.Add(tracksTags);
				}
			}
			#endregion
			#region Categories
			var categories = Helpers.ModelTransformer.ConvertModel(request.Item.Categories);
			if (categories != null)
			{
				dbContext.DCategory.UpdateRange(categories);
				dbContext.DCategory.UpdateRange(categories);
				foreach (var item in dbContext.DCategory.Local.ToArray())
				{
					var tracksCategories = new TrackCategory { Category = item, Track = track };
					track.TracksCategories.Add(tracksCategories);
					item.TracksCategories.Add(tracksCategories);
				}
			}
			#endregion

			reply.Status += dbContext.SaveChanges();
			return reply;
		}



		/// <summary>
		/// 
		/// </summary>
		/// <param name="request"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public override async Task<TrackReply> GetById(TrackRequest request, ServerCallContext context)
		{
			TrackReply reply = new TrackReply();
			var http = context.GetHttpContext();
			string userId = http.User.FindFirstValue(JwtRegisteredClaimNames.Jti);
			var track = await dbContext.FindAsync<Trackitem>(userId, request.Id, null);
			reply.Item = Helpers.ModelTransformer.ConvertModel(track);
			return reply;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="request"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public override Task<TracksReply> Query(TracksRequest request, ServerCallContext context)
		{
			TracksReply reply = new TracksReply();
			var http = context.GetHttpContext();
			string userId = http.User.FindFirstValue(JwtRegisteredClaimNames.Jti);

			foreach (var item in dbContext.DTrackitem.Include(x => x.TracksFormats).ThenInclude(x => x.Format).ToList())
			{
				reply.Items.Add(new TrackInfo
				{
					Id = item.TrackId,
					Title = item.Title,
					Format = item.FormatName
				});
			}
			return Task.FromResult(reply);
		}
	}
}
