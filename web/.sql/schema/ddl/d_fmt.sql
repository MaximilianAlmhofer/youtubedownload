use [d.server_x]
go


create table D_FMT
(
	USR_ID uniqueidentifier not null,
	FMT_ID nvarchar(30) not null,

	[NAME] nvarchar(20) null,
	[URL] nvarchar(128) null,
	VCODEC nvarchar(20) null,
	ACODEC nvarchar(20) null,
	FMT_NOTE nvarchar(50) null,
	WIDTH decimal(18,2) null,
	HEIGHT decimal(18,2) null,
	ABR decimal(18,2) null,
	ASR decimal(18,2) null,
	TBR decimal(18,2) null,
	FSIZE bigint null,
	EXT nvarchar(10) null,
	FPS int null,
	PROTOCOL nvarchar(10) null,
	PLAYER_URL nvarchar(MAX) null,
	CONSTRAINT PK_D_FMT PRIMARY KEY ([USR_ID], [FMT_ID])
);
go
