use [d.server_x]
go

create table D_TRACK
(
	USR_ID uniqueidentifier not null,
	TRACK_ID nvarchar(30) not null,
	DTCREAT datetime2 not null,

	TITLE nvarchar(256) null,
	THUMBN nvarchar(256) null,
	VCODEC nvarchar(20) null,
	ACODEC nvarchar(20) null,
	CREATOR nvarchar(256) null,
	ALBUM nvarchar(256) null,
	DTUPLOAD datetime2 null,
	DURATION int null,
	FNAME nvarchar(256) null,
	UPLDR_URL nvarchar(128) null,
	DESCR nvarchar(256) null,
	FMT_NAME nvarchar(20) null,
	FMT_ID nvarchar(30) null,
	TRK_NAME nvarchar(256) null,
	ABR decimal(18,2) null,
	UPLDR_ID nvarchar(50) null,
	CHANNEL_ID nvarchar(50) null,
	CHANNEL_URL nvarchar(128) null,
	WIDTH decimal(18,2) null,
	HEIGHT decimal(18,2) null,
	LIKE_COUNT int null,
	DISLIKE_COUNT int null,
	CATEG nvarchar(MAX) null,
	
	CONSTRAINT PK_D_TRACK PRIMARY KEY ([USR_ID], [TRACK_ID], [DTCREAT])
);
go