﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;

namespace Lib.WindowsLogin
{
	internal static class ThrowHelper
	{
		public static void ThrowCredentialsInvalid(Exception ex)
		{
			if (ex != null)
			{
				throw new InvalidCredentialsException(ex);
			}
		}

		internal static void ThrowBundled(Type exceptionType, List<Exception> innerExceptions)
		{
			if (innerExceptions.Count > 0)
			{
				System.ComponentModel.Design.ExceptionCollection exceptions = new System.ComponentModel.Design.ExceptionCollection(new System.Collections.ArrayList(innerExceptions));
				AggregateException aggregats = new AggregateException(string.Format(CultureInfo.InvariantCulture, "Aufgetretene Fehler:"), exceptions);
				var throwExpression = Expression.Throw(Expression.Constant(aggregats), exceptionType);
				Expression.Lambda(throwExpression).Compile();
			}
		}
	}
}
