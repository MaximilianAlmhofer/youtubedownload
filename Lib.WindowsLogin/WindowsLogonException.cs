﻿using System;

namespace Lib.WindowsLogin
{
#pragma warning disable CA1058 // Typen dürfen bestimmte Basistypen nicht erweitern
#pragma warning disable CA1032 // Standardausnahmekonstruktoren implementieren
	public class WindowsLogonException : ApplicationException
#pragma warning restore CA1032 // Standardausnahmekonstruktoren implementieren
#pragma warning restore CA1058 // Typen dürfen bestimmte Basistypen nicht erweitern
	{
		internal WindowsLogonException(Exception exception) : base(exception?.Message, exception)
		{

		}
	}
}
