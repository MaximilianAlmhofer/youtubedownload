﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Security.Principal;

namespace Lib.WindowsLogin
{

	public static class WinLogon
	{
		const int LOGON32_PROVIDER_DEFAULT = 0;
		const int LOGON32_LOGON_INTERACTIVE = 2;


		/// <summary>
		/// Erzeugt eine Meldung aus dem übergebenen P/Invoke Fehlercode.
		/// </summary>
		/// <param name="errorCode">der Fehlercode</param>
		/// <returns>Die erzeugte Nachricht.</returns>
		public static string GetErrorMessage(int errorCode)
		{
			int FORMAT_MESSAGE_ALLOCATE_BUFFER = 0x100;
			int FORMAT_MESSAGE_IGNORE_INSERTS = 0x200;
			int FORMAT_MESSAGE_FROM_SYSTEM = 0x1000;

			int msgSize = 255;
			string lpMsgBuf = null;
			int dwFlags = FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS;

			IntPtr lpSource = IntPtr.Zero;
			IntPtr lpArguments = IntPtr.Zero;
			int returnVal = SafeNativeMethods.FormatMessage(dwFlags, ref lpSource, errorCode, 0, ref lpMsgBuf, msgSize, ref lpArguments);

			if (returnVal == 0)
			{
				throw new Exception(string.Format(CultureInfo.InvariantCulture, "Failed to format message for error code {0}. ", errorCode));
			}
			return lpMsgBuf;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="userName"></param>
		/// <param name="passwd"></param>
		/// <returns></returns>
		public static bool LogonUser(string userName, string passwd, out WindowsPrincipal principal)
		{
			bool success = false;
			principal = (WindowsPrincipal)WindowsPrincipal.Current;

			try
			{
				if (SafeNativeMethods.LogonUser(userName, Environment.MachineName, passwd, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, out SafeTokenHandle tokenHandle))
				{
					try
					{
						using (WindowsIdentity newId = new WindowsIdentity(tokenHandle.DangerousGetHandle()))
						{
							principal = new WindowsPrincipal(newId);
#if DEBUG
							if (principal.IsInRole(WindowsBuiltInRole.Administrator))
							{
								Debug.WriteLine("User " + newId.Name + " is Administrator");
							}
#endif
							success = true;
							Debug.WriteLine("Zugriff erteilt");
						}
					}
					finally
					{
						tokenHandle.Dispose();
					}
				}
				else
				{
					int ret = Marshal.GetLastWin32Error();
					string errmsg = GetErrorMessage(ret);
					// Hier kann die WindowsBase nicht eingebunden werden weil es sonst Versionskonflikte mit der MainShell gibt wegen Referenz Flow.
					// Bis auf weiteres...
					System.Windows.Forms.MessageBox.Show(errmsg);
				}
			}
			catch (Exception ex)
			{
				System.Windows.Forms.MessageBox.Show("Fehler beim Login. \r\n" + ex.Message);


			}
			return success;
		}
	}
}
