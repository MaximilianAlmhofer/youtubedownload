﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;

namespace Lib.WindowsLogin
{
	[System.Security.SuppressUnmanagedCodeSecurity]
	internal static class SafeNativeMethods
	{

		[DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
		internal static extern bool LogonUser(string lpszUsername,
			string lpszDomain,
			string lpszPassword,
			int dwLogonType,
			int dwLogonProvider,
			out SafeTokenHandle phToken);


		[DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
		internal static extern bool LogonUser(string lpszUsername,
			string lpszDomain,
			IntPtr phPassword,
			int dwLogonType,
			int dwLogonProvider,
			out SafeTokenHandle phToken);


		[DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
		internal static extern int FormatMessage(int dwFlags,
			ref IntPtr dwSource,
			int dwMessageId,
			int dwLanguageId,
			ref String lpBuffer,
			int nSize,
			ref IntPtr Arguments);


		[DllImport("kernel32.dll", SetLastError = true)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern bool CloseHandle(IntPtr hObject);
	}
}
