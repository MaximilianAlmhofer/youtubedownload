﻿using Microsoft.Win32.SafeHandles;

using System;
using System.Security.Principal;

namespace Lib.WindowsLogin
{
	public static class Impersonation
	{

		/// <summary>
		/// Runs impersonated Function without return value.
		/// </summary>
		/// <param name="credentials"></param>
		/// <param name="logonType"></param>
		/// <param name="action"></param>
		public static void RunAsUser(UserCredentials credentials, LogonType logonType, Action action)
		{
			using (var tokenHandle = credentials?.Impersonate(logonType))
			{
				RunImpersonated(tokenHandle, _ => action());
			}
		}

		/// <summary>
		/// Runs impersonated Function with return value.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="credentials"></param>
		/// <param name="logonType"></param>
		/// <param name="function"></param>
		/// <returns>Returns the return value of the delegated function.</returns>
		public static T RunAsUser<T>(UserCredentials credentials, LogonType logonType, Func<T> function)
		{
			using (var tokenHandle = credentials?.Impersonate(logonType))
			{
				return RunImpersonated<T>(tokenHandle, _ => function());
			}
		}


		/// <summary>
		/// Runs impersonated Function passed a handle return value.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="credentials"></param>
		/// <param name="logonType"></param>
		/// <param name="function"></param>
		/// <returns></returns>
		public static T RunAsUser<T>(UserCredentials credentials, LogonType logonType, Func<SafeAccessTokenHandle, T> function)
		{
			using (var tokenHandle = credentials?.Impersonate(logonType))
			{
				return RunImpersonated<T>(tokenHandle, function);
			}
		}


		private static void RunImpersonated(SafeAccessTokenHandle tokenHandle, Action<SafeAccessTokenHandle> action)
		{
			WindowsIdentity.RunImpersonated(tokenHandle, () => action(tokenHandle));
		}


		private static T RunImpersonated<T>(SafeAccessTokenHandle tokenHandle, Func<SafeAccessTokenHandle, T> function)
		{
			return WindowsIdentity.RunImpersonated<T>(tokenHandle, () => function(tokenHandle));
		}
	}
}
