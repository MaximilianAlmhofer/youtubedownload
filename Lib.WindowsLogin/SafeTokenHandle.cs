﻿using Microsoft.Win32.SafeHandles;

using System;

namespace Lib.WindowsLogin
{
	internal sealed class SafeTokenHandle : SafeHandleZeroOrMinusOneIsInvalid, IDisposable
	{

		private SafeTokenHandle()
			: base(true)
		{
		}

		protected override bool ReleaseHandle()
		{
			if (!IsClosed && !IsInvalid)
			{
				try
				{
					SafeNativeMethods.CloseHandle(handle);
				}
				catch
				{
				}
			}
			return IsClosed;
		}


		internal SafeAccessTokenHandle ToSafeAccessTokenHandle()
		{
			return new SafeAccessTokenHandle(handle);
		}


		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}

		void IDisposable.Dispose()
		{
			Dispose(true);
			ReleaseHandle();
			GC.SuppressFinalize(this);
		}

		~SafeTokenHandle()
		{
			Dispose(false);
		}
	}
}
