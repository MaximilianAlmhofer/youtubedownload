﻿using System;

namespace Lib.WindowsLogin
{
	/// <summary>
	/// Exception thrown when user credentials contain errors.
	/// </summary>
	/// <remarks>
	/// Inherits from <see cref="WindowsLogonException"/> for backwards compatibility reasons.
	/// </remarks>
#pragma warning disable CA1058 // Typen dürfen bestimmte Basistypen nicht erweitern
#pragma warning disable CA1032 // Standardausnahmekonstruktoren implementieren
	public sealed class InvalidCredentialsException : WindowsLogonException
#pragma warning restore CA1032 // Standardausnahmekonstruktoren implementieren
#pragma warning restore CA1058 // Typen dürfen bestimmte Basistypen nicht erweitern
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="InvalidCredentialsException"/> class from a specific <see cref="Exception"/>.
		/// </summary>
		/// <param name="Exception">The exception to base this exception on.</param>
		public InvalidCredentialsException(Exception exception)
			: base(exception)
		{
		}
	}
}
