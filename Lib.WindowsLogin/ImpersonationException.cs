﻿using System.ComponentModel;

namespace Lib.WindowsLogin
{
	/// <summary>
	/// Exception thrown when impersonation fails.
	/// </summary>
	/// <remarks>
	/// Inherits from <see cref="WindowsLogonException"/> for backwards compatibility reasons.
	/// </remarks>
#pragma warning disable CA1032 // Standardausnahmekonstruktoren implementieren
#pragma warning disable CA1058 // Typen dürfen bestimmte Basistypen nicht erweitern
	public sealed class ImpersonationException : WindowsLogonException
#pragma warning restore CA1058 // Typen dürfen bestimmte Basistypen nicht erweitern
#pragma warning restore CA1032 // Standardausnahmekonstruktoren implementieren
	{
		private readonly Win32Exception _win32Exception;

		/// <summary>
		/// Initializes a new instance of the <see cref="ImpersonationException"/> class from a specific <see cref="Win32Exception"/>.
		/// </summary>
		/// <param name="win32Exception">The exception to base this exception on.</param>
		public ImpersonationException(Win32Exception win32Exception)
			: base(win32Exception)
		{

			_win32Exception = win32Exception;
		}

		/// <summary>
		/// Returns the Win32 error code handle for the exception.
		/// </summary>
		public int ErrorCode => _win32Exception.ErrorCode;

		/// <summary>
		/// Returns the Win32 native error code for the exception.
		/// </summary>
		public int NativeErrorCode => _win32Exception.NativeErrorCode;
	}
}
