﻿using Microsoft.Win32.SafeHandles;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Security;

namespace Lib.WindowsLogin
{
	/// <summary>
	/// Represents the credentials to be used for impersonation.
	/// </summary>
	public sealed class UserCredentials
	{
		private readonly string _domain;
		private readonly string _username;
		private readonly string _password;
		private readonly SecureString _securePassword;

		/// <summary>
		/// Returns the username;
		/// </summary>
		/// <returns></returns>
		public string GetName() => _username;

		/// <summary>
		/// Creates a <see cref="UserCredentials"/> class based on a username and plaintext password.
		/// The username can contain a domain name if specified in <c>domain\user</c> or <c>user@domain</c> form.
		/// If no domain is provided, a local computer user account is assumed.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <param name="password">The password.</param>
		public UserCredentials(string username, string password)
		{
			ValidateUserWithoutDomain(username);
			ValidatePassword(password);

			SplitDomainFromUsername(ref username, out var domain);

			_domain = domain;
			_username = username;
			_password = password;
		}


		/// <summary>
		/// Creates a <see cref="UserCredentials"/> class based on a domain, username, and plaintext password.
		/// </summary>
		/// <param name="domain">The domain.</param>
		/// <param name="username">The username.</param>
		/// <param name="password">The password.</param>
		public UserCredentials(string domain, string username, string password)
		{
			ValidateDomainAndUser(domain, username);
			ValidatePassword(password);

			_domain = domain;
			_username = username;
			_password = password;
		}


		/// <summary>
		/// Creates a <see cref="UserCredentials"/> class based on a username and password, where the password is provided as a <see cref="SecureString"/>.
		/// The username can contain a domain name if specified in <c>domain\user</c> or <c>user@domain</c> form.
		/// If no domain is provided, a local computer user account is assumed.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <param name="password">The password.</param>
		public UserCredentials(string username, SecureString password)
		{
			ValidateUserWithoutDomain(username);
			ValidatePassword(password);

			SplitDomainFromUsername(ref username, out var domain);

			_domain = domain;
			_username = username;
			_securePassword = password;
		}


		/// <summary>
		/// Creates a <see cref="UserCredentials"/> class based on a domain, username, and password, where the password is provided as a <see cref="SecureString"/>.
		/// </summary>
		/// <param name="domain">The domain.</param>
		/// <param name="username">The username.</param>
		/// <param name="password">The password.</param>
		public UserCredentials(string domain, string username, SecureString password)
		{
			ValidateDomainAndUser(domain, username);
			ValidatePassword(password);

			_domain = domain;
			_username = username;
			_securePassword = password;
		}


		internal SafeAccessTokenHandle Impersonate(LogonType logonType)
		{
			SafeTokenHandle tokenHandle = default;

			if (_securePassword == null)
			{
				if (!SafeNativeMethods.LogonUser(_username, _domain, _password, (int)logonType, 0, out tokenHandle))
				{
					HandleError(tokenHandle);
				}

				return tokenHandle.ToSafeAccessTokenHandle();
			}

			var passPtr = Marshal.SecureStringToGlobalAllocUnicode(_securePassword);

			try
			{
				if (!SafeNativeMethods.LogonUser(_username, _domain, passPtr, (int)logonType, 0, out tokenHandle))
				{
					HandleError(tokenHandle);
				}

				return tokenHandle.ToSafeAccessTokenHandle();
			}
			finally
			{
				Marshal.ZeroFreeGlobalAllocUnicode(passPtr);
			}
		}


		private static void HandleError(SafeTokenHandle tokenHandle)
		{
			var errorCode = Marshal.GetLastWin32Error();

			if (!tokenHandle.IsInvalid)
			{
				tokenHandle.Dispose();
			}

			throw new ImpersonationException(new Win32Exception(errorCode));
		}


		private static void ValidateDomainAndUser(string domain, string username)
		{
			List<Exception> innerExceptions = new List<Exception>();

			if (domain == null)
			{
				innerExceptions.Add(new ArgumentNullException(nameof(domain), string.Format(CultureInfo.InvariantCulture, "Domain cannot be null.")));
			}

			if (username == null)
			{
				innerExceptions.Add(new ArgumentNullException(nameof(username), string.Format(CultureInfo.InvariantCulture, "Username cannot be null.")));
			}

			if (string.IsNullOrEmpty(domain.Trim()))
			{
				innerExceptions.Add(new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Username cannot be empty or consist solely of whitespace characters."), nameof(domain)));
			}

			if (string.IsNullOrEmpty(username.Trim()))
			{
				innerExceptions.Add(new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Username cannot be empty or consist solely of whitespace characters."), nameof(username)));
			}

			if (domain.IndexOfAny(new[] { '\\', '@' }) != -1)
			{
				innerExceptions.Add(new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Domain cannot contain \\ or @ characters."), nameof(domain)));
			}

			if (username.IndexOfAny(new[] { '\\', '@' }) != -1)
			{
				innerExceptions.Add(new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Username cannot contain \\ or @ characters when domain is provided separately."), nameof(username)));
			}

			ThrowHelper.ThrowBundled(typeof(InvalidCredentialsException), innerExceptions);
		}


		private static void ValidateUserWithoutDomain(string username)
		{
			Exception innerException = default;

			if (username == null)
			{
				innerException = new ArgumentNullException(nameof(username), string.Format(CultureInfo.InvariantCulture, "Username cannot be null."));
			}

			else if (string.IsNullOrEmpty(username.Trim()))
			{
				innerException = new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Username cannot be empty or consist solely of whitespace characters."), nameof(username));
			}

			if (innerException is null)
			{
				int separatorCount = 0;
				foreach (var c in username)
				{
					if (c == '\\' || c == '@')
					{
						separatorCount++;
					}
				}

				if (separatorCount == 0)
				{
					return;
				}

				if (separatorCount > 1)
				{
					innerException = new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Username cannot contain more than one \\ or @ character."), nameof(username));
				}
			}

			if (innerException is null)
			{
				var firstChar = username[0];
				var lastChar = username[username.Length - 1];
				if (firstChar == '\\' || firstChar == '@' || lastChar == '\\' || lastChar == '@')
				{
					innerException = new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Username cannot start or end with a \\ or @ character."));
				}
			}

			ThrowHelper.ThrowCredentialsInvalid(innerException);
		}


		private static void ValidatePassword(string password)
		{
			Exception innerException = default;

			if (password == null)
			{
				innerException = new ArgumentNullException(nameof(password), string.Format(CultureInfo.InvariantCulture, "Password cannot be null."));
			}

			else if (string.IsNullOrEmpty(password.Trim()))
			{
				innerException = new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Password cannot be empty or consist solely of whitespace characters."), nameof(password));
			}

			ThrowHelper.ThrowCredentialsInvalid(innerException);
		}


		private static void ValidatePassword(SecureString password)
		{
			Exception innerException = default;

			if (password == null)
			{
				innerException = new ArgumentNullException(nameof(password), string.Format(CultureInfo.InvariantCulture, "Password cannot be null."));
			}

			if (password.Length == 0)
			{
				innerException = new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Password cannot be empty."), nameof(password));
			}

			ThrowHelper.ThrowCredentialsInvalid(innerException);
		}


		private static void SplitDomainFromUsername(ref string username, out string domain)
		{
			// Note: Only split for domain\user form, because user@domain form is accepted by LogonUser.

			var parts = username.Split('\\');
			if (parts.Length == 2)
			{
				domain = parts[0];
				username = parts[1];
			}
			else
			{
				domain = null;
			}
		}


		/// <inheritdoc />
		public override string ToString()
		{
			return _domain == null ? _username : _username + "@" + _domain;
		}
	}
}
