﻿using System;
using System.Windows.Threading;

namespace Lib.Common
{
	public sealed class Debouncer
	{
		private readonly DispatcherPriority priority;
		private readonly Dispatcher dispatcher;
		private DispatcherTimer timer;

		private DateTime TimerStarted { get; set; } = DateTime.UtcNow.AddYears(-1);

		public Debouncer(DispatcherPriority priority = DispatcherPriority.ApplicationIdle, Dispatcher dispatcher = null)
		{
			this.dispatcher = dispatcher ?? Dispatcher.CurrentDispatcher;
			this.priority = priority;
		}

		public void Debounce<T>(TimeSpan interval, Action<T> action, T parameter)
		{
			timer?.Stop();
			timer = null;

			timer = new DispatcherTimer(
				interval,
				priority,
				(s, e) => Execute(action, parameter),
				dispatcher);

			timer.Start();
		}

		public TResult Debounce<T, TResult>(TimeSpan interval, Func<T, TResult> func, T parameter)
		{
			timer?.Stop();
			timer = null;

			TResult ret = default;

			timer = new DispatcherTimer(
				interval,
				priority,
				(s, e) => ret = Execute(func, parameter),
				dispatcher);

			timer.Start();
			return ret;
		}

		public void Debounce(TimeSpan interval, Action<object> action, object parameter)
		{
			timer?.Stop();
			timer = null;

			timer = new DispatcherTimer(
				interval,
				priority,
				(s, e) => Execute(action, parameter),
				dispatcher);

			timer.Start();
		}


		public void Throttle(int interval, Action<object> action, object parameter = null,
			DispatcherPriority priority = DispatcherPriority.ApplicationIdle, Dispatcher disp = null)
		{
			priority = DispatcherPriority.Send;
			// Stop - Reset
			timer?.Stop();
			timer = null;

			// Initiate erste Mal
			if (disp == null)
			{
				disp = Dispatcher.CurrentDispatcher;
			}

			// interval noch nicht ganz abgelaufen?
			var currentTime = DateTime.UtcNow;
			if (currentTime.Subtract(TimerStarted).TotalMilliseconds < interval)
			{
				interval -= (int)currentTime.Subtract(TimerStarted).TotalMilliseconds;
			}

			// timer neu überschreiben
			timer = new DispatcherTimer(TimeSpan.FromMilliseconds(interval),
				priority,
				(s, e) => Execute(action, parameter),
				disp);

			// starten
			timer.Start();
			TimerStarted = currentTime;
		}



		/// <summary>
		/// Gleiche Logik für Debounce und Throtteling
		/// </summary>
		private void Execute<T>(Action<T> action, T parameter)
		{
			if (timer == null)
			{
				return;
			}

			timer?.Stop();
			timer = null;
			action.Invoke(parameter);
		}


		/// <summary>
		/// Gleiches mit einem return Wert
		/// </summary>
		private TResult Execute<T, TResult>(Func<T, TResult> func, T parameter)
		{
			if (timer == null)
			{
				return default(TResult);
			}

			timer?.Stop();
			timer = null;
			return func.Invoke(parameter);
		}
	}
}
