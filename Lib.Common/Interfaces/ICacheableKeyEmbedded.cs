﻿namespace Lib.Common
{
	/// <summary>
	/// Object that provides a unique key for use in keyed collections and/or Dictionaries  (e.g. for caching)
	/// </summary>
	/// <typeparam name="T">Type of the key</typeparam>
	public interface ICacheableKeyEmbedded<out T>
	{
		T Key { get; }
	}
}