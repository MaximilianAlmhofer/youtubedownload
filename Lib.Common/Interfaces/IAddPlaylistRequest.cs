﻿namespace Lib.Common
{
	public interface IAddPlaylistRequest
	{
		string Title { get; set; }
	}
}