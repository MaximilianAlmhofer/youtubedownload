﻿using System;
using System.Collections.Generic;

namespace Lib.Common
{
	public interface IPlaylist
	{
		/// <summary>
		/// Liefert ein <see cref="ITrackInfo"/> Element am angegebenen Index
		/// </summary>
		/// <param name="index">0-basierter Index des Elements</param>
		/// <returns></returns>
		ITrackInfo GetTrack(int index);

		/// <summary>
		/// Get Entries
		/// </summary>
		/// <returns></returns>
		IEnumerable<IPlaylistTrackEntry> GetEntries();

		/// <summary>
		/// Manifestdatei
		/// </summary>
		string ManifestFile { get; set; }

		/// <summary>
		/// FileLocation
		/// </summary>
		string StorageFolder { get; set; }

		/// <summary>
		/// Playlist cover
		/// </summary>
		Uri Thumbnail { get; set; }

	}
}
