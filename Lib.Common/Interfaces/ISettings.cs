﻿
namespace Lib.Common
{
	public interface ISettings : IMediaFormat, IPlaylistRange
	{
		bool Archive { get; }
		bool ExtractAudio { get; }
		string ArchiveFile { get; set; }
		int AudioQuality { get; set; }
	}
}
