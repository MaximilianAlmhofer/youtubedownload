﻿using System.Runtime.CompilerServices;

using TagLib;

namespace Lib.Common
{
	public interface IStorageFile
	{
		/// <summary>
		/// Stream access für die Playlist aber auch für Sonstige.
		/// </summary>
		File.IFileAbstraction File { get; }

		/// <summary>
		/// Liefert ein bomba File Objekt für jedes bestimmte File.
		/// </summary>
		/// <param name="fileName">optionaler Dateiname, kann auch weggelassen werden, 
		/// weil eh im StorageItem vorhanden.</param>
		/// <returns></returns>
		File GetTagLibFile([CallerMemberName] string fileName = null);
	}
}
