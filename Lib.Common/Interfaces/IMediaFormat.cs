﻿namespace Lib.Common
{
	public interface IMediaFormat
	{
		/// <summary>
		/// mpe, flac, wave
		/// </summary>
		string AudioFormat { get; }
		/// <summary>
		/// mp4, mkv, webm
		/// </summary>
		string VideoFormat { get; }
	}
}
