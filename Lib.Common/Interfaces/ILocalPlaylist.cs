﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Lib.Common
{
	public interface ILocalPlaylist : IPlaylist
	{
		/// <summary>
		/// Playlist-
		/// </summary>
		string Title { get; set; }

		/// <summary>
		/// Filter tags
		/// </summary>
		IEnumerable<string> FilterTags => new List<string>();


		EventHandler<PropertyChangedEventArgs> PropertyChangedHandler { get; }
	}
}
