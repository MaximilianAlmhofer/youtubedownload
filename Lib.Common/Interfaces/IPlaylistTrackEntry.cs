﻿namespace Lib.Common
{
	public interface IPlaylistTrackEntry : IStorageItem
	{
		int PlaylistIndex { get; }

		string Playlist { get; }

		int NEntries { get; }

		string PlaylistId { get; }
	}
}
