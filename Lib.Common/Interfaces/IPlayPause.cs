﻿using System;

namespace Lib.Common
{
	public interface IPlayPause
	{
		Uri PlayPauseImageSource { get; }
	}
}
