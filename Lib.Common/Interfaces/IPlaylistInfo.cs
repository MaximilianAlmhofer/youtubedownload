﻿using System.ComponentModel;

namespace Lib.Common
{
	public interface IPlaylistInfo : IPlaylist, IDownloadSource, INotifyPropertyChanged
	{

	}
}
