﻿
using System.Collections;

namespace Lib.Common
{
	public interface IPlaybackList : IList, IEnumerable
	{
		/// <summary>
		/// Advances the playback position to the next item of the scheduled playback.
		/// </summary>
		/// <returns></returns>
		IMediaItem MoveNext();

		/// <summary>
		/// Moves the playback position to the item that was scheduled before the current one.
		/// </summary>
		/// <returns></returns>
		IMediaItem MovePrevious();

		/// <summary>
		/// Advances the playback position to a specifically selected index in range of all scheduled items.
		/// </summary>
		/// <param name="index">The index to which position should be advanced.</param>
		/// <returns></returns>
		IMediaItem MoveToIndex(int index);

		/// <summary>
		/// Advances the playback position to the one of a specificially selected item in range of all scheduled items.
		/// </summary>
		/// <param name="item">The item to select.</param>
		/// <returns></returns>
		IMediaItem SelectItem(IMediaItem item);

		/// <summary>
		/// Current
		/// </summary>
		IMediaItem Current { get; }
	}
}
