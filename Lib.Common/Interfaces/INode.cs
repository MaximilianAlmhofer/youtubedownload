﻿namespace Lib.Common
{
	public interface INode : IChild, IParent
	{
		string FullPath { get; set; }
	}
}
