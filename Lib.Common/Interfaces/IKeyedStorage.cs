﻿namespace Lib.Common.Interfaces
{
	/// <summary>
	/// Storage for objects that implement <see cref="ICacheableKeyEmbedded{T}"/>
	/// </summary>
	/// <typeparam name="TKey"></typeparam>
	public interface IKeyedStorage<TKey>
	{
		/// <summary>
		/// Get the self-containing-key-item item if the key exists.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		bool TryGet(TKey key, out ICacheableKeyEmbedded<TKey> value);

		/// <summary>
		/// Adds a new entry, if the key does not yet exists
		/// otherwise overwrites the existing entry found at the key.
		/// </summary>
		/// <param name="value"></param>
		void Set(ICacheableKeyEmbedded<TKey> value);
	}
}
