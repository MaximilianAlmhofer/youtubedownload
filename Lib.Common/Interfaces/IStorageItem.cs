﻿using System;

namespace Lib.Common
{
	public interface IStorageItem : IStorageFile, IDownloadSource, IEquatable<IStorageItem>
	{
		/// <summary>
		/// Liefert die Dateiendung der Mediendatei.
		/// </summary>
		/// <returns></returns>
		string GetExtension();

		/// <summary>
		/// Liefert den Pfad ohne Dateiendung zur Mediendatei.
		/// </summary>
		/// <returns></returns>
		string GetFilename();

		/// <summary>
		/// Setzt den vollen Pfad zur Mediadatei.
		/// </summary>
		/// <param name="path">Voll qualifizierter Windows-Dateisystempfad</param>
		void SetFileLocation(string path);

		/// <summary>
		/// Setzt den vollen Pfad zur Manifestdatei.
		/// </summary>
		/// <param name="metadataFolder">Voll qualifizierter Windows-Dateisystempfad</param>
		void SetManifestLocation(string metadataFolder);

		/// <summary>
		/// Manifestdatei
		/// </summary>
		string ManifestFile { get; }

		/// <summary>
		/// FileLocation
		/// </summary>
		string FileLocation { get; set; }

		/// <summary>
		/// Title
		/// </summary>
		string Title { get; set; }

		/// <summary>
		/// Is Backup Manifest
		/// </summary>
		bool IsBackupManifest { get; set; }
	}
}
