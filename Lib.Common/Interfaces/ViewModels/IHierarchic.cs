﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Lib.Common
{
	public interface IHierarchic : INotifyPropertyChanged, System.ICloneable
	{
		IHierarchic Parent { get; set; }

		IHierarchic FirstChild { get; }

		int Level { get; }

		bool HasChildren { get; }

		bool IsExpanded { get; set; }

		bool IsVisible { get; set; }

		object Data { get; set; }

		IHierarchic DeepClone(object item);
	}

	public interface IHierarchic<T> : IHierarchic
	{
		new T Data { get; set; }

		IEnumerable<IHierarchic> GetVisibleDescendants();

		IEnumerable<IHierarchic> GetChildren();
	}
}
