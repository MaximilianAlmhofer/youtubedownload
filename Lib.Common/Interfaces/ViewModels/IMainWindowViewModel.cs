﻿using Lib.Common.Interfaces;

using MvvmCross.ViewModels;

namespace Lib.Common
{
	public interface IMainWindowViewModel : IMvxViewModel, IAppStartMarker
	{
		IHomeViewModel HomeViewModel { get; }

		IPlayerViewModel PlayerViewModel { get; }

		IPlaylistEditorViewModel PlaylistEditorViewModel { get; }

		ITrackListViewModel TrackListViewModel { get; }

		IDownloadViewModel DownloadViewModel { get; }

		IPlaybackControlPanelViewModel PlaybackControlPanelViewModel { get; }
	}
}
