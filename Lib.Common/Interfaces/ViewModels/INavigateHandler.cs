﻿namespace Lib.Common
{
	public interface INavigateHandler
	{
		void Navigate(object context);
	}
}
