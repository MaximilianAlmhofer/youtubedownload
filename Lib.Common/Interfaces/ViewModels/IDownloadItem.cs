﻿
using System.Collections.ObjectModel;

namespace Lib.Common
{
	public interface IDownloadItem
	{
		ObservableCollection<IDownloadViewModel> ActiveDownloads { get; }
	}
}
