﻿using System;
using System.Windows.Automation;

namespace Lib.Common
{
	public interface IAudioSettingsViewModel : INotifyPropertyChangedViewModel
	{
		event EventHandler<AutomationEventArgs> DeviceAutomation;
	}
}
