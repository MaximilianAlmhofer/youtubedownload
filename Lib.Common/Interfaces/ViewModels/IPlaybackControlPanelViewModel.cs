﻿using AudioLib.Playback;

namespace Lib.Common.Interfaces
{
	public interface IPlaybackControlPanelViewModel : IPlayerViewModel
	{
		bool PanelVisible { get; set; }

		bool MuteWhenHidden { get; set; }
	}
}
