﻿using Lib.WindowsLogin;
using MvvmCross.ViewModels;

using System;
using System.Threading.Tasks;

namespace Lib.Common
{
	public interface ILoginViewModel : IMvxViewModel<LoginParameters>
	{
		/// <summary>
		/// Email address nanonanet
		/// </summary>
		string EmailAddress { get; set; }

		/// <summary>
		/// Plain text password !! HAHA
		/// </summary>
		string Password { get; set; }

		/// <summary>
		/// Load the Profile picture of the currently logged in User if exist
		/// </summary>
		Uri WindowsUserProfilePicture { get; }

		/// <summary>
		/// Logs the login time
		/// </summary>
		DateTime LoginTime { get; }

		/// <summary>
		/// To bind to the XAML in a DataTrigger 
		/// </summary>
		bool IsGuestLogin { get; }

		/// <summary>
		/// To bind to the XAML in a DataTrigger 
		/// </summary>
		bool UserDelegation { get; }

		/// <summary>
		/// Obtains credentials frim the OS
		/// </summary>
		/// <param name="userName"></param>
		/// <returns></returns>
		UserCredentials GetCredentials(string userName);

		/// <summary>
		/// Performs login
		/// async so that it can be a remote procedure.
		/// </summary>
		/// <returns></returns>
		Task<bool> LoginAsync();
	}
}
