﻿using Lib.Common;

namespace Syn3X.Mvx.ViewModels.Interfaces
{
	public interface IPlaybackList
	{
		IMediaItem MoveNext();

		IMediaItem MovePrevious();

		IMediaItem MoveToIndex(int index);

		IMediaItem SelectItem(IMediaItem index);
	}
}
