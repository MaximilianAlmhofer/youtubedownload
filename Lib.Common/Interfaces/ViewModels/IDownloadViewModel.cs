﻿using Lib.Common.Message;

using MvvmCross.ViewModels;

using System;

namespace Lib.Common
{
	public interface IDownloadViewModel : IMvxViewModel<IAddSelf>, IMessagePublisher<NewDownloadMessage>, IAddSelf
	{
		/// <summary>
		/// Download Progress (numeric)
		/// </summary>
		int Progress { get; set; }

		/// <summary>
		/// Percentage as string (#.## %)
		/// </summary>
		string PercentProgress { get; set; }

		/// <summary>
		/// Data rate as string (#.## MiB)
		/// </summary>
		string DataRate { get; set; }

		/// <summary>
		/// Speed as string (#.## kB/s)
		/// </summary>
		string DownloadSpeed { get; set; }

		/// <summary>
		/// Time remaining as string (##:##:##)
		/// </summary>
		string DownloadTime { get; set; }

		/// <summary>
		/// The downloaded item with file reference
		/// </summary>
		IStorageItem StorageItem { get; set; }

		/// <summary>
		/// Owning viewmodel
		/// </summary>
		IDownloadItem Owner { get; }

		/// <summary>
		/// Shorthand to the stroage file path
		/// </summary>
		Uri StorageFilePath { get; }
	}
}
