﻿using System.Collections;
using System.Collections.Generic;

namespace Lib.Common
{
	public interface IHierarchicData : IList
	{
		IList<IHierarchic> RawData { get; }

		void AddChildrenToItems(IHierarchic d);

		void RemoveChildrenFromItems(IHierarchic d);

	}
}
