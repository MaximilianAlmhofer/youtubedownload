﻿
using MvvmCross.Plugin.Messenger;

namespace Lib.Common
{
	public interface IMessagePublisher<TMessage> where TMessage : MvxMessage
	{
		void Publish(TMessage message = null);
	}
}