﻿using System;
using System.Threading;

namespace Lib.Common
{
	public interface IAddSelf
	{
		/// <summary>
		/// Adds the download item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="raisePropertyChanged">if set to <c>true</c> [raise property changed].</param>
		/// <returns></returns>
		string AddSelf(object item, bool raisePropertyChanged);

		/// <summary>
		/// Starts downloading content from the specified URI.
		/// </summary>
		/// <param name="uri">The URI.</param>
		void Start(Uri uri);

		/// <summary>
		/// Gets the cancellation token.
		/// </summary>
		/// <value>
		/// The cancellation token.
		/// </value>
		CancellationToken CancellationToken { get; }
	}
}
