﻿using MvvmCross.Commands;
using MvvmCross.ViewModels;

using System.Collections.ObjectModel;

namespace Lib.Common
{
	public interface IPlaylistEditorViewModel : IMvxViewModel
	{

		/// <summary>
		/// Leert die Bearbeitungsliste
		/// </summary>
		IMvxCommand ClearListCommand { get; }

		/// <summary>
		/// Schickt die Liste an die View mit dem Player und PlayerViewmodel.
		/// </summary>
		IMvxCommand AddToPlaylistCommand { get; }

		/// <summary>
		/// 
		/// </summary>
		ObservableCollection<string> GetFormatTypeItemsSource();

		/// <summary>
		/// Datagrid Source
		/// </summary>
		IHierarchicData DataGridSource { get; set; }

		/// <summary>
		/// Ein neues zum Bearbeiten markieren.
		/// </summary>
		/// <param name="item"></param>
		void AddItem(object item);
	}
}
