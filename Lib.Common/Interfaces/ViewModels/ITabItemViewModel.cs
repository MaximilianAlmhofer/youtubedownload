﻿namespace Syn3X.Mvx.ViewModels.Interfaces
{

	public interface ITabItemViewModel
	{
		object TabData { get; set; }

		string TabHeaderName { get; set; }

		int DisplayIndex { get; set; }
	}
}
