﻿using Lib.Common.Message;

using MvvmCross.ViewModels;

using System.Threading.Tasks;

namespace Lib.Common
{
	public interface ITrackListViewModel : IMessagePublisher<MediaItemChangedMessage>, IMvxViewModel
	{
		/// <summary>
		/// Tracks
		/// </summary>
		IPlaybackList Tracks { get; }

		/// <summary>
		/// Current from Tracks.Current
		/// </summary>
		IMediaItem Current { get; }

		/// <summary>
		/// Anforderung für Löschen 
		/// </summary>
		/// <param name="selectedItem"></param>
		Task<StorageActionResult> RequestDeleteAsync(bool keepManifest = false, params IMediaItem[] itemsToDelete);
	}
}