﻿using MvvmCross.ViewModels;

namespace Lib.Common
{
	public interface IHomeViewModel : IMvxViewModel
	{
		/// <summary>
		/// Source Für die Liste
		/// </summary>
		/// <returns></returns>
		ITrackListViewModel TrackListViewModel { get; }

		/// <summary>
		/// Zum Editieren von Listen
		/// </summary>
		IPlaylistEditorViewModel PlaylistEditorViewModel { get; }
	}
}
