﻿
using MvvmCross.ViewModels;

using NAudio.Wave;

using System;
using System.Threading;

namespace Lib.Common
{

	public interface IPlayerViewModel : IMvxViewModel<PlayerParameters>, IPlayPause, IMvxNotifyPropertyChanged
	{

		/// <summary>
		/// For multithread access to mutable properties
		/// </summary>
		object LockToken { get; }

		/// <summary>
		/// PlaybackManager
		/// </summary>
		IPlaybackManager Manager { get; }

		/// <summary>
		/// Gets or sets the media item.
		/// </summary>
		/// <value>
		/// The media item.
		/// </value>
		IMediaItem MediaItem { get; }

		/// <summary>
		/// Gets the state of the playback.
		/// </summary>
		/// <value>
		/// The state of the playback.
		/// </value>
		PlaybackState PlaybackState { get; }

		/// <summary>
		/// Aktueller Playback Titel
		/// </summary>
		string Title { get; }

		/// <summary>
		/// Gets the current time.
		/// </summary>
		/// <value>
		/// The current time.
		/// </value>
		TimeSpan CurrentTime { get; }

		/// <summary>
		/// Gets the duration of the natural.
		/// </summary>
		/// <value>
		/// The duration of the natural.
		/// </value>
		TimeSpan NaturalDuration { get; }

		/// <summary>
		/// Gets or sets the volume.
		/// </summary>
		/// <value>
		/// The volume.
		/// </value>
		Single Volume { get; set; }

		/// <summary>
		/// Gets or sets the speed ratio.
		/// </summary>
		/// <value>
		/// The speed ratio.
		/// </value>
		float SpeedRatio { get; set; }

		/// <summary>
		/// Gets or sets the balance.
		/// </summary>
		/// <value>
		/// The balance.
		/// </value>
		double Balance { get; set; }

		/// <summary>
		/// Gets or sets the position.
		/// </summary>
		/// <value>
		/// The position of the playback.
		/// </value>
		double Position { get; set; }

		/// <summary>
		/// Gets or sets the length 
		/// of the playback in seconds.
		/// </summary>
		/// <value>
		/// The length of the playback.
		/// </value>
		double LengthInSeconds { get; set; }

		/// <summary>
		/// Mutes or unmutes the volume
		/// </summary>
		void Mute ();
	}
}
