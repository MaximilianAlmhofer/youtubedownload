﻿using System.Collections.Generic;

namespace Lib.Common
{
	public interface IParent
	{
		INode AddChild(INode child);
		INode GetChild(string name);
		IEnumerable<INode> GetChildren();
	}
}