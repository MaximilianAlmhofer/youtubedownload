﻿using System.Collections.Generic;
using System.Security.Cryptography;

namespace Lib.Common
{
	public interface IPlaybackManager
	{

		/// <summary>
		/// Interrupts the playback.
		/// </summary>
		void Pause();

		/// <summary>
		/// Starts/resumes the playback.
		/// </summary>
		void Play();

		/// <summary>
		/// Stops the playback and resets state.
		/// </summary>
		void Stop();

		/// <summary>
		/// Skips forward to the next track in the list
		/// and if the current is the list's last item, 
		/// returns to the first one.
		/// </summary>
		void NextTrack(bool isShuffle);

		/// <summary>
		/// Skips backward to the previous track in the list
		/// and if the current is the list's first item, 
		/// goes to the last one.
		/// </summary>
		void PreviousTrack(bool isShuffle);

		/// <summary>
		/// Selects the track passed in <paramref name="item"/>.
		/// </summary>
		/// <param name="item">The track to select.</param>
		void SelectTrack(object item);


		/// <summary>
		/// Queues a list of tracks for playback.
		/// </summary>
		/// <param name="items"></param>
		void SelectTracks(IEnumerable<object> items);

		/// <summary>
		/// Gets or sets a value indicating whether this instance is muted.
		/// </summary>
		/// <value>
		///   <c>true</c> if this instance is muted; otherwise, <c>false</c>.
		/// </value>
		bool IsMuted { get; }
	}
}
