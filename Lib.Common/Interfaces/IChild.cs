﻿namespace Lib.Common
{
	public interface IChild
	{
		INode GetParent();
		INode SetParent(INode parent);
	}
}