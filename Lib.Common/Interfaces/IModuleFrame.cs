﻿using MvvmCross.Platforms.Wpf.Views;
using MvvmCross.ViewModels;

namespace Lib.Common
{
	/// <summary>
	/// Provides the skeleton and it's peripherials for a <see cref="IModule"/>
	/// </summary>
	/// <typeparam name="TViewModel"></typeparam>
	public interface IModuleFrame<TViewModel> where TViewModel : IMvxViewModel
	{

		/// <summary>
		/// Property for Binding in XAML
		/// </summary>
		MvxWpfView UserInterface { get; }

		/// <summary>
		/// Deactivates the module.
		/// </summary>
		void Deactivate();

		/// <summary>
		/// Creates the viewthat is content of this frame.
		/// </summary>
		/// <param name="viewModel"></param>
		void CreateView(TViewModel viewModel);
	}


	public interface IModuleFrame
	{

		/// <summary>
		/// The name.
		/// </summary>
		string Name { get; }
	}
}
