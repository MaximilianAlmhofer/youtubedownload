﻿using System;

namespace Lib.Common
{

	public interface IDownloadSource : IModel
	{
		/// <summary>
		/// Liefert die Web-URL samt Path zum Medienelement zurück.<br />
		/// Für Youtube wäre das <b>https://www.youtube.com/watch?v={Id}</b>
		/// </summary>
		/// <returns></returns>
		Uri GetWebpageUri();

		/// <summary>
		/// Liefert die Nummer die das Format identifiziert.
		/// </summary>
		/// <param name="forAudio">Gibt an, ob die ID für das Audioformat gesucht wird, sonst Videoformat.</param>
		/// <returns></returns>
		object GetFormatIdentifier(bool forAudio);

		/// <summary>
		/// Wandelt Objekt mit heruntergeladenen Informationen 
		/// in ein Objekt mit heruntergeladenen + lokalen Informationen um.
		/// </summary>
		/// <param name="saveFilePath"></param>
		/// <returns></returns>
		object ToLocal(string saveFilePath);
	}
}
