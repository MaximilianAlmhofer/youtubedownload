﻿using System;
using System.ComponentModel;

namespace Lib.Common
{
	public interface IMediaItem : INotifyPropertyChanged
	{
		/// <summary>
		/// Gets the backing storage item object.
		/// </summary>
		/// <returns>The storage item object</returns>
		IStorageItem GetStorageItem();

		/// <summary>
		/// ViewModel
		/// </summary>
		object ViewModel { get; set; }

		/// <summary>
		/// Download Viewmodel
		/// </summary>
		object DownloadItem { get; set; }

		/// <summary>
		/// Gets the Uri of the stored file.
		/// </summary>
		Uri Source { get; }

		/// <summary>
		/// Gibt an ob es eine Playlist ist.
		/// </summary>
		bool IsPlayList { get; }
	}
}
