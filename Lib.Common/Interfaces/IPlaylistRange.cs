﻿namespace Lib.Common
{
	public interface IPlaylistRange
	{
		/// <summary>
		/// Startindex der Playlist
		/// (unsigned short > 0)
		/// </summary>
		ushort PlaylistStart { get; }

		/// <summary>
		/// Endindex der Playlist
		/// (unsigned short > 0 > PlaylistStart + 1)
		/// </summary>
		ushort PlaylistEnd { get; }
	}
}
