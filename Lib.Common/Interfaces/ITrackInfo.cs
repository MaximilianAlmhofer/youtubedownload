﻿using System.ComponentModel;

namespace Lib.Common
{
	/// <summary>
	/// Markerinterface bundles (maybe deprecated)
	/// TODO: Use IDownloadSource instead
	/// </summary>
	public interface ITrackInfo : IDownloadSource, INotifyPropertyChanged
	{

	}
}
