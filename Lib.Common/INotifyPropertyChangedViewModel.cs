﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Lib.Common
{
	public interface INotifyPropertyChangedViewModel : INotifyPropertyChanged
	{
		void RaisePropertyChanged([CallerMemberName] string propertyName = null);
	}
}
