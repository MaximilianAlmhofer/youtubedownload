﻿using System;

namespace Lib.Common
{
	public class StateResult : IResult
	{
		public StateResult(object data)
		{
			Value = data;
		}

		public void SetResult(object value)
		{
			Value = value;
		}

		public string GetString()
		{
			return Value?.ToString();
		}

		public object Value
		{
			get;
			protected set;
		}
	}



	public class ErrorResult : StateResult, IErrorResult
	{
		public ErrorResult(string err) : base("")
		{
			SetError(err);
		}

		public ErrorResult(Exception ex = null) : base("")
		{
			if (ex != null)
				SetError(ex);
		}

		public void SetError(object error)
		{
			if (Value is string errMsg && error is string)
			{
				Value = string.Join("\r\n", errMsg, error);
			}
			else
			{
				Value = error;
			}
		}
	}
}