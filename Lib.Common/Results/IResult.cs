﻿namespace Lib.Common
{
	public interface IResult
	{
		/// <summary>
		/// Das Datenobjekt mit dem Ergebnis des Commands
		/// </summary>
		object Value { get; }

		void SetResult(object value);

		string GetString();
	}


	public interface IErrorResult : IResult
	{
		void SetError(object error);
	}
}
