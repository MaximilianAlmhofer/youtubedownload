﻿using System.Collections.Generic;

namespace Lib.Common
{
	public class Result
	{

		public Result()
		{
			Value = new object();
		}

		public Result(object value)
		{
			Value = value ?? new object();
		}

		public Result(Result other)
		{
			if (other != null)
			{
				Value = other.Value;
			}

			if (Value is null)
			{
				Value = new object();
			}
		}

		public object Value { get; set; }

		public List<string> Errors { get; private set; } = new List<string>();

		public bool HasError => Errors.Count > 0;

		public bool Ok => !HasError;

		public bool OkAndNotNull => Ok && Value != null;

		public Result Add(Result other)
		{
			if (other != null)
			{
				Errors.AddRange(other.Errors);
			}
			return this;
		}

		public Result AddError(string error)
		{
			if (error != null)
			{
				Errors.Add(error);
			}
			return this;
		}
	}

	public class Result<T> where T : new()
	{
		public Result(T value)
		{
			Value = value;
			object _Val = Value;
			if (_Val is null)
			{
				Value = new T();
			}
		}

		public Result(Result<T> other)
		{
			if (other != null)
			{
				Value = other.Value;
			}

			object _Val = Value;
			if (_Val is null)
			{
				Value = new T();
			}
		}

		public T Value { get; set; }

		public List<string> Errors { get; private set; } = new List<string>();

		public bool HasError => Errors.Count > 0;

		public bool Ok => !HasError;

		public bool OkAndNotNull => Ok && Value != null;

		public Result<T> Add(Result<T> other)
		{
			if (other != null)
			{
				Errors.AddRange(other.Errors);
			}
			return this;
		}

		public Result<T> Add(Result other)
		{
			if (other != null)
			{
				Errors.AddRange(other.Errors);
			}
			return this;
		}

		public Result<T> AddError(string error)
		{
			if (error != null)
			{
				Errors.Add(error);
			}
			return this;
		}
	}
}
