﻿namespace Lib.Common
{
	public sealed class MetadataResult : IResult
	{
		public void SetResult(object value)
		{
			// Wenn schon ein Wert besteht und beides ein string ist 
			// dann - auch wenn sehr naiv - die Strings zambauen :)
			if (Value is string notNull && value is string)
			{
				Value = string.Concat(notNull, value);
			}
			else
			{
				Value = value;
			}
		}

		public string GetString()
		{
			return Value?.ToString();
		}

		public object Value { get; private set; }
	}
}