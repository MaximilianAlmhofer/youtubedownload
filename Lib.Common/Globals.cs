﻿using Microsoft.Extensions.Configuration;

using System;
using System.Drawing;
using System.IO;
using System.Reflection;

namespace Lib.Common
{
	public static class Globals
	{
		#region Vars
		public static IConfigurationRoot AppSettings { get; set; }

		public static string FriendlyApplicationName => Path.GetFileNameWithoutExtension(AppDomain.CurrentDomain.FriendlyName);
		#endregion

		#region Fonts
		internal static Font ItalicFont => new Font("Microsoft Sans Serif", 10.625F, FontStyle.Italic, GraphicsUnit.World, 0);

		public static Font LightFont => new Font("Microsoft Sans Serif", 10.625F, FontStyle.Regular, GraphicsUnit.World, 0);

		public static Font ButtonFont => new Font("Segoe MDL2 Assets", 14.125F, FontStyle.Regular, GraphicsUnit.World, 0);
		#endregion

		#region BindingFlags
		public static BindingFlags InstancePublicGetSetP => (BindingFlags)12308;

		public static BindingFlags InstanceNonPublicGetSetP => (BindingFlags)12324;

		internal static BindingFlags InstancePublicGetSetF => (BindingFlags)3092;

		public static BindingFlags InstanceNonPublicGetField => (BindingFlags)1060;

		public static BindingFlags InstanceNonPublicGetSetF => (BindingFlags)3108;

		public static BindingFlags InstanceNonPublicMethod => (BindingFlags)292;

		public static BindingFlags InstancePublicMethod => (BindingFlags)276;

		public static BindingFlags StaticNonPublicMethod => (BindingFlags)296;
		#endregion
	}
}
