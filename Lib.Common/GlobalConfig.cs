﻿namespace Lib.Common
{

	public class GlobalConfig
	{

		public AppFolder AppFolder { get; set; }

		public string StdConf { get; set; }

		public string ListConf { get; set; }

		public string ArchiveFile { get; set; }

		public string IndexDat { get; set; }

		public DownloadOptions DownloadOptions { get; set; }

	}
}
