﻿using System;
using System.Drawing;
using System.IO;

namespace Lib.Common
{
	public class AccountPictureConverter
	{
		public static string ConvertToFile(string accountpictureFile, string toDestinationDirectory, int mode)
		{
			if (string.IsNullOrEmpty(accountpictureFile) || !accountpictureFile.EndsWith("accountpicture-ms"))
			{
				Console.WriteLine("Please provide the .accountpicture-ms file path.");
				return string.Empty;
			}
			if (!File.Exists(accountpictureFile))
			{
				Console.WriteLine("File at path {0} does not exist.", accountpictureFile);
				return String.Empty;
			}

			string filename = Path.GetFileNameWithoutExtension(accountpictureFile);

			if (mode == 96)
			{
				Bitmap image96 = GetImage96(accountpictureFile);
				image96.Save(Path.Combine(toDestinationDirectory, filename + $"-{mode}.bmp"));
			}
			if (mode == 448)
			{
				Bitmap image448 = GetImage448(accountpictureFile);
				image448.Save(Path.Combine(toDestinationDirectory, filename + $"-{mode}.bmp"));
			}

			Console.WriteLine("Extracted images successfully:\r\n{0}", Path.Combine(toDestinationDirectory, filename + $"-{mode}.bmp"));
			return Path.Combine(toDestinationDirectory, string.Concat(filename, $"-{mode}.bmp"));
		}

		public static Bitmap GetImage96(string path)
		{
			FileStream fs = new FileStream(path, FileMode.Open);
			long position = Seek(fs, "JFIF", 0);
			byte[] b = new byte[System.Convert.ToInt32(fs.Length)];
			fs.Seek(position - 6, SeekOrigin.Begin);
			fs.Read(b, 0, b.Length);
			fs.Close();
			fs.Dispose();
			return GetBitmapImage(b);
		}

		public static Bitmap GetImage448(string path)
		{
			FileStream fs = new FileStream(path, FileMode.Open);
			long position = Seek(fs, "JFIF", 100);
			byte[] b = new byte[System.Convert.ToInt32(fs.Length)];
			fs.Seek(position - 6, SeekOrigin.Begin);
			fs.Read(b, 0, b.Length);
			fs.Close();
			fs.Dispose();
			return GetBitmapImage(b);
		}

		public static Bitmap GetBitmapImage(byte[] imageBytes)
		{
			//var bitmapImage = new Bitmap();
			//bitmapImage.BeginInit();
			//bitmapImage.StreamSource = new MemoryStream(imageBytes);
			//bitmapImage.EndInit();
			//return bitmapImage;
			var ms = new MemoryStream(imageBytes);
			var bitmapImage = new Bitmap(ms);
			return bitmapImage;
		}

		public static long Seek(FileStream fs, string searchString, int startIndex)
		{
			char[] search = searchString.ToCharArray();
			long result = -1, position = 0, stored = startIndex,
			begin = fs.Position;
			int c;
			while ((c = fs.ReadByte()) != -1)
			{
				if ((char)c == search[position])
				{
					if (stored == -1 && position > 0 && (char)c == search[0])
					{
						stored = fs.Position;
					}
					if (position + 1 == search.Length)
					{
						result = fs.Position - search.Length;
						fs.Position = result;
						break;
					}
					position++;
				}
				else if (stored > -1)
				{
					fs.Position = stored + 1;
					position = 1;
					stored = -1;
				}
				else
				{
					position = 0;
				}
			}

			if (result == -1)
			{
				fs.Position = begin;
			}
			return result;
		}
	}
}
