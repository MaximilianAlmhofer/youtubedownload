﻿
using AudioLib.Visualization;

namespace Lib.Common
{
	public sealed class PlayerParameters : ViewModelParameters<IPlayerViewModel>
	{
		public PlayerParameters(IVisualizationPlugin visualizationPlugin, IModuleFrame<IPlayerViewModel> playbackModule)
		{
			VisualizationPlugin = visualizationPlugin;
			PlaybackModule = playbackModule;
		}

		public IVisualizationPlugin VisualizationPlugin { get; }

		public IModuleFrame<IPlayerViewModel> PlaybackModule { get; }

		public override object[] GetArguments()
		{
			return new object[]
			{
				VisualizationPlugin,
				PlaybackModule
			};
		}
	}
}
