﻿
using System;
using System.Security;

namespace Lib.Common
{
	public sealed class LoginParameters : ViewModelParameters<ILoginViewModel>
	{
		public LoginParameters(string emailAddress, string password)
		{
			LoginTime = DateTime.Now;
			EmailAddress = emailAddress ?? throw new ArgumentNullException(nameof(emailAddress));
			SecurePassword(password);
			this.PassWordPlain = password;
		}

		public string EmailAddress { get; set; }

		public string PassWordPlain { get; set; }

		public SecureString Password { get; set; }

		public HashCode PasswordHash { get; set; }

		public DateTime LoginTime { get; }

		public override object[] GetArguments()
		{
			return new object[]
			{
				EmailAddress,
				Password,
				PasswordHash,
				LoginTime
			};
		}

		public unsafe void SecurePassword(string password)
		{
			fixed (char* ptr = &password.ToCharArray()[0])
			{
				Password = new SecureString(ptr, password.Length);
				PasswordHash = new HashCode();
				int idx = 0;
				while (idx++ < password.Length)
				{
					PasswordHash.Add((*ptr)++);
				}
			}
		}
	}
}
