﻿using System;

namespace Lib.Common
{
	public abstract class ViewModelParameters<TViewModel>
	{
		public Type ViewModelType => typeof(TViewModel);

		public abstract object[] GetArguments();
	}
}
