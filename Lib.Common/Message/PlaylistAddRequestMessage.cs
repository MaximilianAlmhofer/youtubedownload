﻿
using MvvmCross.Plugin.Messenger;

using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Lib.Common.Message
{
	public class PlaylistAddRequestMessage : MvxMessage, IAddPlaylistRequest, INotifyPropertyChanged
	{
		public PlaylistAddRequestMessage(object sender) : base(sender)
		{
		}

		public Action<string> Callback { get; set; }

		private string _title;
		public string Title
		{
			get => _title;
			set
			{
				_title = value;
				RaisePropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
		{
			var handler = PropertyChanged;
			handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
