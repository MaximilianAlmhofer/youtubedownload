﻿
using MvvmCross.Plugin.Messenger;

namespace Lib.Common.Message
{
	public class LogMessage : MvxMessage
	{
		public enum LogTarget
		{
			Console,
			File
		};

		public LogMessage(object sender, string logText, LogTarget logTarget = LogTarget.Console) : base(sender)
		{
			LogText = logText;
			LogTo = logTarget;
		}

		public string LogText { get; }

		public LogTarget LogTo { get; }
	}
}
