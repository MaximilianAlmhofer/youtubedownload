﻿using MvvmCross.Plugin.Messenger;

using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace Lib.Common.Message
{
	public class PropertyChangedMessage<TDeclaringType> : MvxMessage where TDeclaringType : INotifyPropertyChanged
	{
		public PropertyChangedMessage(object sender, Expression<Func<Type, string>> propertyExpression) : base(sender)
		{
			DeclaringType = typeof(TDeclaringType);
			PropertyName = propertyExpression.Compile()(DeclaringType);
		}

		public string PropertyName { get; }

		public Type DeclaringType { get; }
	}
}
