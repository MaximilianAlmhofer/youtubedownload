﻿
using MvvmCross.Plugin.Messenger;

using System.Collections.Generic;

namespace Lib.Common.Message
{
	public class NewListDownloadMessage : MvxMessage
	{
		public NewListDownloadMessage(object sender, IEnumerable<IPlaylistTrackEntry> entries, IPlaylistInfo playlist)
			: base(sender)
		{
			Playlist = playlist;
			Entries = entries;
		}

		public IPlaylistInfo Playlist { get; }

		public IEnumerable<IPlaylistTrackEntry> Entries { get; }
	}
}
