﻿
using MvvmCross.Plugin.Messenger;

using System;

namespace Lib.Common.Message
{
	public class MediaItemChangedMessage : MvxMessage
	{
		public MediaItemChangedMessage(object sender, IMediaItem item) : base(sender)
		{
			MediaItem = item ?? throw new ArgumentNullException(nameof(item));
		}

		public IMediaItem MediaItem { get; }
	}
}
