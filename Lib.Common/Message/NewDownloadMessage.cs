﻿
using MvvmCross.Plugin.Messenger;

namespace Lib.Common.Message
{
	public class NewDownloadMessage : MvxMessage
	{
		public NewDownloadMessage(object sender, IStorageItem storageItem) : base(sender)
		{
			StorageItem = storageItem;
		}

		public IStorageItem StorageItem { get; }
	}
}
