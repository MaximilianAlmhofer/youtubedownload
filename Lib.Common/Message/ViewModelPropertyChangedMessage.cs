﻿
using System;
using System.Linq.Expressions;

namespace Lib.Common.Message
{
	public sealed class ViewModelPropertyChangedMessage : PropertyChangedMessage<INotifyPropertyChangedViewModel>
	{
		public ViewModelPropertyChangedMessage(object sender, Expression<Func<Type, string>> propertyExpression)
			: base(sender, propertyExpression)
		{
		}
	}
}
