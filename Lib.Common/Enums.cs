﻿namespace Lib.Common
{
	public enum FileType : byte
	{
		audio,
		video
	};

	public enum Format : byte
	{
		mp3,
		mp4,
		flac,
		mkv,
		wave,
		webm
	};


	public enum AudioCodecs
	{
		Aac,
		Aiff,
		Ape,
		Asf,
		Flac,
		Mpeg,
		Mpeg4,
		MusePack,
		NonContainer,
		Ogg,
		Riff,
		WavPack
	};
}
