﻿using Newtonsoft.Json;

namespace Lib.Common
{
	[JsonObject]
	public class AppFolder : Folder
	{
		[JsonConstructor]
		public AppFolder(string path) : base(null, path)
		{
		}

		public static bool IsRoot => true;

		private Folder downloadFolder;
		public Folder DownloadFolder => downloadFolder;
		public string DownloadFolderPath
		{
			get => downloadFolder?.FullPath;
			set => SetValue(value, ref downloadFolder);
		}

		private Folder metadataFolder;
		public string MetadataFolderPath
		{
			get => metadataFolder?.FullPath;
			set => SetValue(value, ref metadataFolder);
		}

		private Folder configBackupFolder;
		public string ConfigBackupFolderPath
		{
			get => configBackupFolder?.FullPath;
			set => SetValue(value, ref configBackupFolder);
		}

		private Folder scriptsFolder;
		public string ScriptsFolderPath
		{
			get => scriptsFolder?.FullPath;
			set => SetValue(value, ref scriptsFolder);
		}

		private void SetValue(string value, ref Folder folder)
		{
			if (value != null && folder?.FullPath != value)
			{
				folder = new Folder(this, value);
			}
		}

		public INode GetFolder(string name)
		{
			return GetChild(name);
		}
	}
}
