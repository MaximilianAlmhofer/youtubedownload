﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

using System.Collections.Generic;
using System.IO;

namespace Lib.Common
{

	#region Node
	public abstract class Node : INode, IChild, IParent
	{
		protected readonly List<Node> childFolders = new List<Node>();

		protected Node parent;

		public virtual INode Root
		{
			get
			{
				INode parent = GetParent();
				if (parent != null)
				{
					while (parent.GetParent() != null)
					{
						parent = parent.GetParent();
					}
					return parent;
				}
				return this;
			}
		}

		internal Node this[INode child]
		{
			get
			{
				if (!(child as Node is null))
				{
					int index = childFolders.IndexOf((Node)child);
					if (index > -1 && index < childFolders.Count - 1)
					{
						return childFolders[index];
					}
				}
				return null;
			}
			set
			{
				if (!(child as Node is null))
				{
					int index = childFolders.IndexOf((Node)child);
					if (index > -1 && index < childFolders.Count - 1)
					{
						childFolders[index] = value;
					}
					else
					{
						childFolders.Add(value);
					}
				}
			}
		}
		public abstract string Name { get; }

		#region INode
		public abstract INode GetParent();
		public abstract INode GetChild(string name);
		public abstract INode SetParent(INode parent);
		public abstract INode AddChild(INode child);
		public abstract IEnumerable<INode> GetChildren();
		public virtual string FullPath { get; set; }
		#endregion
	}
	#endregion

	#region DirectoryNode
	public abstract class DirectoryNode : Node
	{
		public abstract INode this[string name] { get; }

		public override string Name { get; }

		public override string FullPath { get; set; }

		protected internal DirectoryNode(Node parentNode)
		{
			parent = parentNode;
		}

		public override IEnumerable<INode> GetChildren()
		{
			return childFolders;
		}

		public override INode GetParent()
		{
			return parent;
		}

		public override INode AddChild(INode child)
		{
			if (child is null)
			{
				throw new System.ArgumentNullException(nameof(child));
			}

			child.SetParent(this);
			return this;
		}

		public override INode SetParent(INode parent)
		{
			if (parent is null)
			{
				throw new System.ArgumentNullException(nameof(parent));
			}

			((Node)parent)[this] = this;
			return this;
		}

		public override INode GetChild(string name)
		{
			return this[name];
		}
	}
	#endregion

	[JsonObject(MemberSerialization = MemberSerialization.OptIn, NamingStrategyType = typeof(CamelCaseNamingStrategy))]
	public class Folder : DirectoryNode, INode
	{

		[JsonConstructor]
		public Folder(Folder parentFolder, string path) : base(parentFolder)
		{
			parent = parentFolder;

			FullPath = ((!string.IsNullOrEmpty(path)) && Directory.Exists(path)) ? path : "";

			if (!string.IsNullOrEmpty(FullPath))
			{
				foreach (var subDir in Directory.EnumerateDirectories(FullPath))
				{
					Folder subFolder = new Folder(this, subDir);
					AddChild(subFolder);
				}
			}
		}

		[JsonIgnore]
		public override INode Root => base.Root as Folder;

		[JsonProperty]
		public override string Name => string.IsNullOrEmpty(FullPath) ? string.Empty : Path.GetFileNameWithoutExtension(FullPath);

		[JsonProperty]
		public override string FullPath { get; set; } = "";

		[JsonIgnore]
		public override INode this[string name]
		{
			get
			{
				INode folder = this;
				if (!string.IsNullOrEmpty(name))
				{
					int index = childFolders.FindIndex(x => x.Name == name);
					if (index < 0 || index > childFolders.Count - 1)
					{
						AddChild(new Folder(this, Path.Combine(FullPath, "pl")));
						index = childFolders.FindIndex(x => x.Name == name);
					}

					folder = childFolders[index];
				}
				return folder as Folder;
			}
		}
	}
}
