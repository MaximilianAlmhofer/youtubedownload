﻿namespace Lib.Common
{
	public class DownloadOptions : ISettings
	{
		private FileType Type = default;

		private Format Format = default;

		private string archiveFile;
		public string ArchiveFile
		{
			get => archiveFile;
			set => archiveFile = value;
		}

		private int audioQuality;
		public int AudioQuality
		{
			get => audioQuality;
			set => audioQuality = value;
		}

		private ushort playlistStart = 1;
		public ushort PlaylistStart
		{
			get => playlistStart;
			set => playlistStart = value;
		}

		private ushort playlistEnd = 15;
		public ushort PlaylistEnd
		{
			get => playlistEnd;
			set => playlistEnd = value;
		}

		public bool Archive => !string.IsNullOrEmpty(ArchiveFile);

		public bool ExtractAudio => Type == FileType.audio;

		private string videoFormat;
		public string VideoFormat
		{
			get => (int)Format % 2 == 0 ? string.Empty : Format.ToString();
			set
			{
				if (!string.IsNullOrEmpty(value))
					this.SetEnumField<Format>("Type", "video");
				this.SetEnumField<Format>("Format", value);
				videoFormat = value;
			}
		}

		private string audioFormat;
		public string AudioFormat
		{
			get => (int)Format % 2 != 0 ? string.Empty : Format.ToString();
			set
			{
				if (!string.IsNullOrEmpty(value))
					this.SetEnumField<Format>("Type", "audio");
				this.SetEnumField<Format>("Format", value);
				audioFormat = value;
			}
		}


		public override string ToString()
		{
			return $"{Type.ToString()}/{Format.ToString()}" +
				   $"Audioqualität:\t{AudioQuality}\r\n" +
				   $"Archiv:\t\t{Archive}";
		}

		#region Equality
		public override bool Equals(object obj)
		{
			if (obj is DownloadOptions settings)
			{
				return settings.Archive == Archive
					&& settings.ArchiveFile == ArchiveFile
					&& settings.AudioQuality == AudioQuality
					&& settings.Format == Format
					&& settings.Type == Type;
			}
			return false;
		}

		public override int GetHashCode()
		{
			return Archive.GetHashCode()
				| Archive.GetHashCode()
				| AudioQuality.GetHashCode()
				| Format.GetHashCode()
				| Type.GetHashCode();
		}

		public static bool operator ==(DownloadOptions left, DownloadOptions right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(DownloadOptions left, DownloadOptions right)
		{
			return !(left == right);
		}
		#endregion
	}
}
