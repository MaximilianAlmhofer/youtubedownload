﻿using Newtonsoft.Json;

using System.Collections.Generic;
using System.Linq;

namespace Lib.Common
{
	public static class FileLoader
	{
		public static IEnumerable<IStorageItem> LoadTracks(string folder)
		{
			return System.IO.Directory.EnumerateFiles(folder, "*.json")
				.Select(x => System.IO.File.ReadAllText(x))
				.Select(x => JsonConvert.DeserializeObject<IStorageItem>(x));
		}
	}
}
