﻿namespace Lib.Common
{
	public static class Strings
	{
		public const string StartDownloadHint = "**** Zum Downloaden Youtube-URL auf die Form ziehen *****";
		public const string IncorrectConfigurationWarning = "Fehlende oder fehlerhafte Konfiguration\r\n\r\n----------------\r\n{0}\r\n----------------\r\n\r\nKonfiguration anpassen?";
		public const string ContentAppSettingsError = "Inhalt der appsettings.json überprüfen und bei \"Configuration\" den Pfad zur initialen Konfigurationsdatei eintragen!\r\n\r\n";

		public const string DownloadTag = "[download]";
		public const string FfmpegTag = "[ffmpeg]";
		public const string YoutubeTag = "[youtube]";
		public const string IsInArchive = "has already been recorded in archive";

		public const string Destination = "Destination:";
		public const string Playlistname = "Downloading playlist:";
		public const string ManifestDownloadFehlgeschlagen = "Keine Information zum gewünschten Titel verfügbar";
		public const string UngueltigeUrl = "Keine gültige URL erkannt.\r\n\r\nInhalt:\r\n---------\r\n\"{0}\"";

		public const string PlaylistNumbers = "[youtube:playlist]";
	}
}
