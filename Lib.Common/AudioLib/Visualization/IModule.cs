﻿namespace AudioLib.Visualization
{
	/// <summary>
	/// Describes the stucture of a module
	/// </summary>
	public interface IModule
	{
		/// <summary>
		/// Name of the module
		/// </summary>
		string Name { get; }

		/// <summary>
		/// Detail content of the module
		/// </summary>
		object Content { get; }
	}
}
