﻿using AudioLib.Visualization;

using NAudio.Dsp;

using System;

namespace AudioLib.Visualization
{
	public interface IVisualizationPlugin : IEquatable<IVisualizationPlugin>, IModule
	{
		void OnMaxCalculated(float min, float max);

		void OnFFTCalculated(Complex[] result);
	}
}
