﻿namespace AudioLib.Visualization
{
	public interface IWaveFormRenderer
	{
		/// <summary>
		/// After the FastFourier-Transform using a HammingWindow transformation
		/// the max and mins of the amplitude are added onto the canvas, 
		/// which builds the wave-visualization.
		/// </summary>
		/// <param name="maxValue"></param>
		/// <param name="minValue"></param>
		void AddValue(float maxValue, float minValue);
	}
}
