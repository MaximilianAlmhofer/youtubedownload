﻿using FlacBox;

using NAudio.Wave;

using System;
using System.Diagnostics;
using System.IO;

namespace AudioLib.Playback
{
	public sealed class FlacPlayback : NAudioEngine
	{
		private FlacReader flacReader;
		private Wave16OverFlacStream waveOutFlacStream;

		public FlacPlayback(string key) : base(key)
		{
		}

		protected override void CreateDevice()
		{
			waveOutDevice = new NAudio.Wave.WaveOut { Volume = 0.5F, DeviceNumber = 0, DesiredLatency = 100 };
			waveOutDevice.PlaybackStopped += WaveOutDevicePlaybackStopped;
		}

		protected override void OpenFile(string fileName)
		{
			base.OpenFile(fileName);

			try
			{
				FileTag = TagLib.File.Create(fileName);
				WaveFormat trueWaveFormat;
				FlacStreaminfo streamInfo;
				using (var srReader = new FlacReader(fileName))
				{
					trueWaveFormat = srReader.GetWaveFormat();
					streamInfo = srReader.Streaminfo;
				}
				flacReader = new FlacReader(File.OpenRead(fileName), streamInfo, true);
				waveOutFlacStream = new Wave16OverFlacStream(flacReader);
				ActiveStream = waveOutFlacStream;
				ChannelLength = WaveOutChannel.TotalTime.TotalSeconds;
				ActiveStream = WaveOutChannel;
				waveOutDevice.Init(WaveOutChannel);
			}
			catch (Exception e)
			{
				Debug.WriteLine(e.Message, "Problem opening file");
				((IAudioPlayback)this).CloseFile();
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (waveOutDevice != null)
				waveOutDevice.PlaybackStopped -= WaveOutDevicePlaybackStopped;

			flacReader.Close();

			waveOutFlacStream.Close();
			waveOutFlacStream.Dispose();
			base.Dispose(disposing);
			flacReader = null;
			waveOutFlacStream = null;
		}
	}
}
