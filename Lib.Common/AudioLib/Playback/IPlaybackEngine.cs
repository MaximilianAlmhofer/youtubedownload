﻿using System.ComponentModel;

using Lib.Common;

namespace AudioLib.Playback
{
	public interface IPlaybackEngine : IPlaybackManager
	{
		NAudioEngine AudioPlayback { get; }

		IMediaItem MediaItem { get; set; }

		void Select(IMediaItem item);

		void SelectRandom();
	}
}
