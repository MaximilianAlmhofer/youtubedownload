﻿
using AudioLib.Sampling;

using NAudio.Wave;

namespace Lib.Common.AudioLib.Playback.v2
{
	public static class PlaybackExtensions
	{
		public static SampleAggregator ToSampleAggregator(this ISampleProvider sampleProvider)
		{
			return sampleProvider as SampleAggregator;
		}
	}
}
