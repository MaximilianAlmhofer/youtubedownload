﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Data;

using AudioLib.Spectrum;

using Lib.Common;
using Lib.Common.AudioLib.Playback.v2;

using MediaCore;

using MvvmCross.ViewModels;

using NAudio.Wave;



namespace AudioLib.Playback.v2
{
	public class NAudioEngine_v2 : MvxInteraction<ISampleProvider>, ISpectrumPlayer, IDisposable, ICacheableKeyEmbedded<string>
	{
		private static NAudioEngine_v2 nAudioEngine;

		private ConcurrentDictionary<string, IAudioPlayback_v2> playbacks = new ConcurrentDictionary<string, IAudioPlayback_v2>();

		protected static readonly object _lockToken = new object();

		protected readonly int _fftDataSize = (int)FFTDataSize.FFT2048;

		protected IWavePlayer _waveOutDevice;

		private IAudioPlayback_v2 _audioPlayback;

		private bool disposed;

		#region Events
		private void AudioPlayback_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (sender == _audioPlayback)
			{
				RaisePropertyChanged(e.PropertyName);
			}
		}

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
		{
			var handler = PropertyChanged;
			handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public event EventHandler<PositionChangedEventArgs> PositionChanged = delegate { };

		protected virtual void OnPositionChanged(PositionChangedEventArgs e)
		{
			var handler = PositionChanged;
			handler?.Invoke(this, e);
		}

		public event EventHandler<FftEventArgs> FftCalculated = delegate { };

		protected virtual void OnFftCalculated(FftEventArgs e)
		{
			var handler = FftCalculated;
			handler?.Invoke(this, e);
		}

		public event EventHandler<AmplitudeEventArgs> MaximumCalculated = delegate { };

		protected virtual void OnMaximumCalculated(AmplitudeEventArgs e)
		{
			var handler = MaximumCalculated;
			handler?.Invoke(this, e);
		}

		public event EventHandler<PlaybackbackStateChangedEventArgs> PlaybackStateChanged = delegate { };

		protected virtual void OnPlaybackStateChanged(PlaybackState oldState, Exception exception = default)
		{
			var handler = PlaybackStateChanged;
			var args = new PlaybackbackStateChangedEventArgs(oldState, this.PlaybackState, exception);
			handler?.Invoke(this, args);
		}

		public EventHandler<PlaybackStoppedEventArgs> PlaybackStopped = delegate { };

		protected virtual void WaveOutDevicePlaybackStopped(object sender, StoppedEventArgs e)
		{
			EndOfStream = true;
			var handler = PlaybackStopped;
			handler?.Invoke(sender, new PlaybackStoppedEventArgs(this, e.Exception));
		}


		protected virtual void PositionTimerTick(object sender, EventArgs e)
		{
			PlaybackBase_v2.inChannelTimerUpdate = true;
			ChannelPosition = (WaveOutChannel.Position / (double)WaveOutChannel.Length) * WaveOutChannel.TotalTime.TotalSeconds;
			var args = new PositionChangedEventArgs(ChannelPosition);
			PlaybackBase_v2.inChannelTimerUpdate = false;
			OnPositionChanged(args);
		}

		#endregion

		public NAudioEngine_v2 SetPlayback(string storageItemId, string ext)
		{

			if (!playbacks.TryGetValue(storageItemId, out var playbackSource))
			{
				if (ext == ".flac")
					playbackSource = new FlacPlayback_v2(storageItemId);
				else if (ext == ".mp3")
					playbackSource = new Mp3Playback_v2(storageItemId);
				else throw new ArgumentException(nameof(ext) + " ist unlesbar.");

				playbacks.TryAdd(storageItemId, playbackSource);
			}
			_audioPlayback = playbackSource;
			_waveOutDevice = _audioPlayback.WavePlayer();
			_audioPlayback.InitTimer(PositionTimerTick);
			this._audioPlayback = playbackSource;
			this._audioPlayback.PropertyChanged += AudioPlayback_PropertyChanged;

			return this;
		}

		public static NAudioEngine_v2 Create(string storageItemId, string extension, EventHandler<PlaybackStoppedEventArgs> stoppedHandler = null)
		{
			if (nAudioEngine is null) nAudioEngine = new NAudioEngine_v2();
			nAudioEngine = nAudioEngine.SetPlayback(storageItemId, extension);
			nAudioEngine.PlaybackStopped += stoppedHandler;
			return nAudioEngine;
		}

		#region ISpectrumPlayer
		public bool GetFFTData(float[] fftDataBuffer)
		{
			_audioPlayback.SampleProvider().ToSampleAggregator().GetFFTResults(fftDataBuffer);
			return _audioPlayback.IsPlaying() ?? false;
		}

		public int GetFFTFrequencyIndex(int frequency)
		{
			double maxFrequency;
			if (ActiveStream != null)
				maxFrequency = ActiveStream.WaveFormat.SampleRate / 2.0d;
			else
				maxFrequency = 22050;
			return (int)((frequency / maxFrequency) * (_fftDataSize / 2));
		}
		#endregion

		#region IWaveformPlayer
		public double GetMaximum()
		{
			return ActiveStream?.TotalTime.TotalSeconds ?? 0;
		}
		#endregion

		#region Properties
		public bool IsPlaying
		{
			get => _audioPlayback.IsPlaying() ?? false;
		}

		public WaveStream ActiveStream
		{
			get => _audioPlayback.ActiveStream();
		}

		public WaveChannel32 WaveOutChannel
		{
			get => _audioPlayback.WaveChannel();
		}


		public double ChannelLength
		{
			get => _audioPlayback.ChannelLength();
		}

		public double ChannelPosition
		{
			get => _audioPlayback.ChannelPosition();
			set
			{
				_audioPlayback.SetChannelPosition(value);
				RaisePropertyChanged();
			}
		}

		public TagLib.File FileTag
		{
			get => _audioPlayback.FileTag();
		}


		public bool CanPlay()
		{
			return PlaybackState == PlaybackState.Stopped || PlaybackState == PlaybackState.Paused
				&& this.PlaybackState == _waveOutDevice.PlaybackState
				&& ActiveStream != null;
		}

		public bool CanPause()
		{
			return PlaybackState == PlaybackState.Playing
				&& this.PlaybackState == _waveOutDevice.PlaybackState;
		}

		public bool CanStop()
		{
			return PlaybackState == PlaybackState.Playing || PlaybackState == PlaybackState.Paused
				&& this.PlaybackState == _waveOutDevice.PlaybackState;
		}

		public bool IsLoaded()
		{
			if (ActiveStream != null)
			{
				return _waveOutDevice != null;
			}
			return false;
		}

		public PlaybackState PlaybackState
		{
			get
			{
				if (_waveOutDevice != null)
				{
					RaisePropertyChanged();
					return _waveOutDevice.PlaybackState;
				}

				return PlaybackState.Stopped;
			}
		}

		public virtual Single Volume
		{
			get => _waveOutDevice?.Volume ?? 1F;
			set
			{
				if (_waveOutDevice != null)
				{
					_waveOutDevice.Volume = value;
					RaisePropertyChanged();
				}
			}
		}

		public virtual TimeSpan CurrentTime
		{
			get => ActiveStream?.CurrentTime ?? TimeSpan.Zero;
			set
			{
				if (ActiveStream != null)
				{

					ActiveStream.CurrentTime = value;
					RaisePropertyChanged();
				}
			}
		}

		public virtual TimeSpan TotalTime
		{
			get
			{
				return ActiveStream?.TotalTime ?? TimeSpan.Zero;
			}
		}

		public string Key
		{
			get;
			private set;
		}

		public bool EndOfStream
		{
			get;
			private set;
		} //ActiveStream != null && ActiveStream.Position == ActiveStream.TotalTime.TotalSeconds;

		#endregion

		public void Load(string fileName)
		{
			if (ActiveStream != null || (bool)_audioPlayback?.IsPlaying())
			{
				// _audioPlayback.Stop();
				// zurück cachen..
			}
			_audioPlayback.OpenFile(fileName);
			InitSampleAggregator((int)FFTDataSize.FFT2048);
		}

		protected virtual void InitSampleAggregator(int fftDataSize)
		{
			_audioPlayback.SampleProvider().FftCalculated += (s, a) => OnFftCalculated(a);
			_audioPlayback.SampleProvider().MaximumCalculated += (s, a) => OnMaximumCalculated(a);
		}

		public virtual double GetCurrentTimeInSeconds()
		{
			return ChannelPosition;
		}

		public virtual void SetCurrentPosition(double value)
		{
			ChannelPosition = value;
		}


		public void UnLoad()
		{
			Stop();
			_audioPlayback.CloseFile();
		}


		#region PlaybackControl
		public void Play()
		{
			if (CanPlay())
			{
				_audioPlayback.Start(ref _waveOutDevice);
				OnPlaybackStateChanged(_waveOutDevice.PlaybackState);
			}
		}

		public void Pause()
		{
			if (_waveOutDevice != null && CanPause())
			{
				_audioPlayback.Pause(ref _waveOutDevice);
				OnPlaybackStateChanged(_waveOutDevice.PlaybackState);
			}
		}

		public void Stop()
		{
			if (CanStop())
			{
				_audioPlayback.Stop(ref _waveOutDevice);
				WaveOutDevicePlaybackStopped(this, new StoppedEventArgs());
			}
		}
		#endregion

		#region IDisposable
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!disposed)
			{
				if (disposing)
				{
					StopAndCloseStream();
				}
				disposed = true;
			}
		}

		protected virtual void StopAndCloseStream()
		{
			if (_waveOutDevice != null)
			{
				_waveOutDevice.Stop();
			}
			if (_waveOutDevice != null)
			{
				_waveOutDevice.Dispose();
				_waveOutDevice = null;
			}
		}
		#endregion
	}
}
