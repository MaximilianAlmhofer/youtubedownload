﻿
using System;

using AudioLib.Playback.v2;

using MediaCore;

using NAudio.Wave;

namespace AudioLib.Playback
{

	public sealed class Mp3Playback_v2 : PlaybackBase_v2, IAudioPlayback_v2
	{

		public Mp3Playback_v2(string storageItemId) : base(storageItemId)
		{
			waveOutDevice = new NAudio.Wave.WaveOut { Volume = 0.5F, DeviceNumber = 0, DesiredLatency = 100 };
		}

		public void OpenFile(string fileName)
		{
			if (System.IO.File.Exists(fileName))
			{
				try
				{
					fileTag = TagLib.File.Create(fileName);
					activeStream = new Mp3FileReader(fileName);
					waveChannel32 = new WaveChannel32(activeStream);
					_ChannelLength = waveChannel32.TotalTime.TotalSeconds;
					waveChannel32.Sample += WaveStreamSampleProcessed;
					waveOutDevice.Init(waveChannel32);
					InitSampleAggregator((int)FFTDataSize.FFT2048);
				}
				catch
				{
					activeStream = null;
				}
			}
		}

		/// <summary>
		/// Objekt disposen !! 
		/// Muss mit OpenFile() 
		/// erneut geladen werden
		/// </summary>
		public void CloseFile()
		{
			if (activeStream != null)
			{
				activeStream.Close();
			}
		}

		protected void WaveStreamSampleProcessed(object sender, SampleEventArgs e)
		{
			sampleAggregator.Add(e.Left, e.Right);
			long repeatStartPosition = (long)((SelectionBegin.TotalSeconds / activeStream.TotalTime.TotalSeconds) * activeStream.Length);
			long repeatStopPosition = (long)((SelectionEnd.TotalSeconds / activeStream.TotalTime.TotalSeconds) * activeStream.Length);
			if (((SelectionEnd - SelectionBegin) >= TimeSpan.FromMilliseconds(REPEAT_THRESHOLD)) && activeStream.Position >= repeatStopPosition)
			{
				sampleAggregator.Clear();
				activeStream.Position = repeatStartPosition;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (waveChannel32 != null)
				waveChannel32.Sample -= WaveStreamSampleProcessed;

			base.Dispose(disposing);
			if (activeStream != null)
				activeStream.Dispose();

			activeStream = null;
			waveChannel32 = null;

		}
	}
}
