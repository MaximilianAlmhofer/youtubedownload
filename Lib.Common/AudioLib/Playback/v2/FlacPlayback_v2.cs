﻿using System;
using System.Diagnostics;
using System.IO;

using FlacBox;

using MediaCore;

using NAudio.Wave;

namespace AudioLib.Playback.v2
{
	public sealed class FlacPlayback_v2 : PlaybackBase_v2, IAudioPlayback_v2
	{
		private FlacReader flacReader;
		private Wave16OverFlacStream waveOutFlacStream;

		public FlacPlayback_v2(string storageItemId) : base(storageItemId)
		{
			waveOutDevice = new NAudio.Wave.WaveOut { Volume = 0.5F, DeviceNumber = 0, DesiredLatency = 100 };
		}

		public void OpenFile(string fileName)
		{
			try
			{
				fileTag = TagLib.File.Create(fileName);
				WaveFormat trueWaveFormat;
				FlacStreaminfo streamInfo;
				using (var srReader = new FlacReader(fileName))
				{
					trueWaveFormat = srReader.GetWaveFormat();
					streamInfo = srReader.Streaminfo;
				}
				flacReader = new FlacReader(File.OpenRead(fileName), streamInfo, true);
				waveOutFlacStream = new Wave16OverFlacStream(flacReader);
				activeStream = waveOutFlacStream;
				waveChannel32 = new WaveChannel32(activeStream);
				_ChannelLength = waveChannel32.TotalTime.TotalSeconds;
				activeStream = waveChannel32;
				waveOutDevice.Init(waveChannel32);
			}
			catch (Exception e)
			{
				Debug.WriteLine(e.Message, "Problem opening file");
				((IAudioPlayback_v2)this).CloseFile();
			}
		}

		/// <summary>
		/// Objekt disposen !! 
		/// Muss mit OpenFile() 
		/// erneut geladen werden
		/// </summary>
		public void CloseFile()
		{

			if (waveOutFlacStream != null)
			{
				try
				{
					if (activeStream != null)
						activeStream.Close();
					flacReader.Close();
					waveOutFlacStream.Close();
					waveOutFlacStream.Dispose();
					InitSampleAggregator((int)FFTDataSize.FFT2048);
				}
				catch (Exception)
				{
					Dispose(true);
				}
			}
		}

	
		protected override void Dispose(bool disposing)
		{
			if (waveChannel32 != null)
				waveChannel32.Sample -= WaveStreamSampleProcessed;

			flacReader.Close();
			waveOutFlacStream.Close();
			waveOutFlacStream.Dispose();
			base.Dispose(disposing);
			flacReader = null;
			waveOutFlacStream = null;
		}
	}
}
