﻿
using System;
using System.ComponentModel;

using AudioLib.Sampling;

using NAudio.Wave;

namespace AudioLib.Playback.v2
{

	public interface IServerController
	{
		void Pause(ref IWavePlayer waveOutDevice);

		void Start(ref IWavePlayer waveOutDevice);

		void Stop(ref IWavePlayer waveOutDevice);
	}

	public interface IAudioPlayback_v2 : IServerController, INotifyPropertyChanged
	{

		/// <summary>
		/// Load media source and initialize playback
		/// </summary>
		/// <param name="fileName">Media source file</param>
		void OpenFile(string fileName);

		/// <summary>
		/// Schließt die gerade geöffnete Datei.
		/// </summary>
		void CloseFile();

		/// <summary>
		/// Returnsa the output device.
		/// </summary>
		/// <returns></returns>
		IWavePlayer WavePlayer();

		/// <summary>
		/// Returns the Wavestream underlying.
		/// </summary>
		/// <returns></returns>
		WaveStream ActiveStream();

		/// <summary>
		/// Returns a channel for stereo wave.synthesis
		/// </summary>
		/// <returns></returns>
		WaveChannel32 WaveChannel();

		/// <summary>
		/// Provides a fast-fourier-transformed view on the 
		/// audio wave's timing-discrete signal.
		/// </summary>
		/// <returns></returns>
		SampleAggregator SampleProvider();

		/// <summary>
		/// Length of the channel of the WaveChannel32
		/// </summary>
		/// <returns></returns>
		double ChannelLength();

		/// <summary>
		/// Current position of the playback in the channel.
		/// </summary>
		/// <returns></returns>
		double ChannelPosition();

		/// <summary>
		/// File Tag Object with metadata detailed 
		/// about audio codec standards
		/// </summary>
		/// <returns></returns>
		TagLib.File FileTag();

		/// <summary>
		/// Returns wether the audioplayback is currently playing.
		/// </summary>
		bool? IsPlaying();

		/// <summary>
		/// Sets the position in the channel.
		/// For repositioning  (will not work with flac)
		/// </summary>
		/// <param name="value"></param>
		void SetChannelPosition(double value);

		/// <summary>
		/// Inits the position callback
		/// </summary>
		/// <param name="eventHandlerRef"></param>
		void InitTimer(EventHandler eventHandlerRef);
	}
}
