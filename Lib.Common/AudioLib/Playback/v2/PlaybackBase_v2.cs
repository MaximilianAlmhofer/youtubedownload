﻿
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Threading;

using AudioLib.Sampling;

using MediaCore;

using NAudio.Wave;

namespace AudioLib.Playback
{
	public class PlaybackBase_v2 : INotifyPropertyChanged, IDisposable
	{
		internal const int WAVEFORM_COMPRESSEDPOINT_COUNT = 2000;
		internal const int REPEAT_THRESHOLD = 200;

		private bool disposedValue = false;

		protected DispatcherTimer _positionTimer = new DispatcherTimer(DispatcherPriority.ApplicationIdle);

		protected internal NAudio.Wave.WaveOut waveOutDevice;
		protected internal SampleAggregator sampleAggregator;
		protected internal WaveChannel32 waveChannel32;
		protected internal WaveStream activeStream;
		protected internal TagLib.File fileTag;

		protected double _ChannelLength;
		protected double _ChannelPosition;
		protected bool InChannelSet;

		internal static bool inChannelTimerUpdate;

		protected TimeSpan RepeatStart;
		protected TimeSpan RepeatStop;
		protected bool InRepeatSet;

		internal readonly string storageItemId;

		protected PlaybackBase_v2(string storageItemId)
		{
			this.storageItemId = storageItemId;
		}

		public bool? IsPlaying()
		{
			return waveOutDevice?.PlaybackState == PlaybackState.Playing;
		}

		public TimeSpan SelectionBegin
		{
			get => RepeatStart;
			set
			{
				if (!InRepeatSet)
				{
					InRepeatSet = true;
					TimeSpan oldValue = RepeatStart;
					RepeatStart = value;
					if (oldValue != RepeatStart)
						OnPropertyChanged();
					InRepeatSet = false;
				}
			}
		}

		public TimeSpan SelectionEnd
		{
			get => RepeatStop;
			set
			{
				if (!InChannelSet)
				{
					InRepeatSet = true;
					TimeSpan oldValue = RepeatStop;
					RepeatStop = value;
					if (oldValue != RepeatStop)
						OnPropertyChanged();
					InRepeatSet = false;
				}
			}
		}

		public IWavePlayer WavePlayer()
		{
			return waveOutDevice;
		}

		public WaveStream ActiveStream()
		{
			return activeStream;
		}

		public WaveChannel32 WaveChannel()
		{
			return waveChannel32;
		}

		public SampleAggregator SampleProvider()
		{
			return sampleAggregator;
		}

		public double ChannelLength()
		{
			return _ChannelLength;
		}

		public double ChannelPosition()
		{
			return _ChannelPosition;
		}

		public TagLib.File FileTag()
		{
			return fileTag;
		}

		public void SetChannelPosition(double value)
		{
			if (!InChannelSet)
			{
				InChannelSet = true;
				double oldValue = _ChannelPosition;
				double position = Math.Max(0, Math.Min(value, _ChannelLength));
				if (!inChannelTimerUpdate && activeStream != null)
				{
					activeStream.Position = (long)(position / activeStream.TotalTime.TotalSeconds * activeStream.Length);
				}
				_ChannelPosition = position;
				if (oldValue != _ChannelPosition)
					OnPropertyChanged();
				InChannelSet = false;
			}
		}

		protected virtual void InitSampleAggregator(int fftDataSize)
		{
			sampleAggregator = new SampleAggregator(waveChannel32, fftDataSize)
			{
				NotificationCount = waveChannel32.WaveFormat.SampleRate / 100,
				PerformFFT = true
			};
		}

		//protected virtual void OnMaximumCalculated(AmplitudeEventArgs a)
		//{

		//}

		//protected virtual void OnFftCalculated(FftEventArgs a)
		//{

		//}

		protected virtual void WaveStreamSampleProcessed(object sender, SampleEventArgs e)
		{

		}

		public void InitTimer(EventHandler eventHandlerRef)
		{
			_positionTimer.Tick -= eventHandlerRef;
			_positionTimer.Tick += eventHandlerRef;
			_positionTimer.Interval = TimeSpan.FromMilliseconds(1000);
			_positionTimer.IsEnabled = false;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					(waveOutDevice as IDisposable)?.Dispose();
					(waveChannel32 as IDisposable)?.Dispose();
					(fileTag as IDisposable)?.Dispose();
					(activeStream as IDisposable)?.Dispose();
					waveOutDevice = null;
					waveChannel32 = null;
					fileTag = null;
					activeStream = null;
				}
				disposedValue = true;
			}
		}

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			var handler = PropertyChanged;
			handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public void Pause(ref IWavePlayer waveOutDevice)
		{
			waveOutDevice = this.waveOutDevice;
			waveOutDevice.Pause();
			_positionTimer.Stop();
		}

		public void Start(ref IWavePlayer waveOutDevice)
		{
			waveOutDevice = this.waveOutDevice;
			waveOutDevice.Play();
			_positionTimer.Start();
		}

		public void Stop(ref IWavePlayer waveOutDevice)
		{
			waveOutDevice = this.waveOutDevice;
			waveOutDevice.Stop();
			_positionTimer.Stop();
			_positionTimer.IsEnabled = false;
		}
	}
}
