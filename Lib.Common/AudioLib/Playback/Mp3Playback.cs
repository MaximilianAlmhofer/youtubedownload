﻿
using NAudio.Wave;

using System;

namespace AudioLib.Playback
{
	public sealed class Mp3Playback : NAudioEngine
	{
		public Mp3Playback(string key) : base(key)
		{
		}


		public override double PixelWidth
		{
			get => pixelWidth;
			set
			{
				pixelWidth = value;
				if (sampleAggregator != null)
				{
					sampleAggregator.SecondsPerPixel = (int)((ActiveStream as Mp3FileReader)?.TotalSeconds() / Math.Max(1, value));
				}
			}
		}

		protected override void CreateDevice()
		{
			waveOutDevice = new NAudio.Wave.WaveOut { Volume = 0.5F, DeviceNumber = 0, DesiredLatency = 100 };
			waveOutDevice.PlaybackStopped += WaveOutDevicePlaybackStopped;
		}

		protected override void OpenFile(string fileName)
		{
			base.OpenFile(fileName);

			if (System.IO.File.Exists(fileName))
			{
				try
				{
					FileTag = TagLib.File.Create(fileName);
					ActiveStream = new Mp3FileReader(fileName);
					InitSampleAggregator();
					ChannelLength = WaveOutChannel.TotalTime.TotalSeconds;
					WaveOutChannel.Sample += WaveStreamSampleProcessed;
					waveOutDevice.Init(WaveOutChannel);
				}
				catch
				{
					ActiveStream = null;
				}
			}
		}


		protected override void WaveStreamSampleProcessed(object sender, SampleEventArgs e)
		{
			sampleAggregator.Add(e.Left, e.Right);
			long repeatStartPosition = (long)((SelectionBegin.TotalSeconds / ActiveStream.TotalTime.TotalSeconds) * ActiveStream.Length);
			long repeatStopPosition = (long)((SelectionEnd.TotalSeconds / ActiveStream.TotalTime.TotalSeconds) * ActiveStream.Length);
			if (((SelectionEnd - SelectionBegin) >= TimeSpan.FromMilliseconds(repeatThreshold)) && ActiveStream.Position >= repeatStopPosition)
			{
				sampleAggregator.Clear();
				ActiveStream.Position = repeatStartPosition;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (waveOutDevice != null)
				waveOutDevice.PlaybackStopped -= WaveOutDevicePlaybackStopped;
			base.Dispose(disposing);
		}
	}
}
