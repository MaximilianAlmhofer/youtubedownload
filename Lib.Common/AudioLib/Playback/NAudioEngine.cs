﻿using AudioLib.Sampling;
using AudioLib.Spectrum;

using Lib.Common;

using MediaCore;

using MvvmCross.Base;
using MvvmCross.ViewModels;

using NAudio.Wave;

using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Threading;



namespace AudioLib.Playback
{
	public abstract class NAudioEngine : MvxInteraction<ISampleProvider>, ISpectrumPlayer, IAudioPlayback, IDisposable, ICacheableKeyEmbedded<string>
	{
		protected static readonly object lockToken = new object();
		private DispatcherTimer positionTimer = new DispatcherTimer(DispatcherPriority.ApplicationIdle);
		protected readonly int fftDataSize = (int)FFTDataSize.FFT2048;
		private bool disposed;
		private bool isPlaying;
		private bool inChannelTimerUpdate;
		private double channelLength;
		private double channelPosition;
		private bool inChannelSet;

		protected IWavePlayer waveOutDevice;
		protected WaveStream activeStream;
		private WaveChannel32 waveChannel32;
		protected SampleAggregator sampleAggregator;

		private TagLib.File fileTag;
		private TimeSpan repeatStart;
		protected TimeSpan repeatStop;
		private bool inRepeatSet;

		#region Constants
		protected const int waveformCompressedPointCount = 2000;
		protected const int repeatThreshold = 200;
		#endregion

		#region Events

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
		{
			var handler = PropertyChanged;
			handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public event EventHandler<PositionChangedEventArgs> PositionChanged = delegate { };

		protected virtual void OnPositionChanged(PositionChangedEventArgs e)
		{
			var handler = PositionChanged;
			handler?.Invoke(this, e);
		}

		public event EventHandler<FftEventArgs> FftCalculated = delegate { };

		protected virtual void OnFftCalculated(FftEventArgs e)
		{
			var handler = FftCalculated;
			handler?.Invoke(this, e);
		}

		public event EventHandler<AmplitudeEventArgs> MaximumCalculated = delegate { };

		protected virtual void OnMaximumCalculated(AmplitudeEventArgs e)
		{
			var handler = MaximumCalculated;
			handler?.Invoke(this, e);
		}

		public event EventHandler<PlaybackbackStateChangedEventArgs> PlaybackStateChanged = delegate { };

		protected virtual void OnPlaybackStateChanged(PlaybackState oldState, Exception exception = default)
		{
			var handler = PlaybackStateChanged;
			var args = new PlaybackbackStateChangedEventArgs(oldState, this.PlaybackState, exception);
			handler?.Invoke(this, args);
		}

		#endregion

		#region Constructor

		protected NAudioEngine(string key)
		{
			Key = key ?? throw new ArgumentNullException(nameof(key));
			AsyncDispatcher = MvvmCross.Mvx.IoCProvider.GetSingleton<IMvxMainThreadAsyncDispatcher>();
			// CanPlay = true;
		}

		void InitTimer()
		{
			positionTimer.Tick -= PositionTimerTick;
			positionTimer.Tick += PositionTimerTick;
			positionTimer.Interval = TimeSpan.FromMilliseconds(1000);
			positionTimer.IsEnabled = false;
		}

		#endregion

		#region Waveform Generation
		private class WaveformGenerationParams
		{
			public WaveformGenerationParams(int points, string path)
			{
				Points = points;
				Path = path;
			}

			public int Points { get; protected set; }
			public string Path { get; protected set; }
		}


		public void Skip(int seconds)
		{
			ActiveStream.Skip(seconds);
		}
		#endregion

		#region ISpectrumPlayer
		protected double pixelWidth;
		public virtual double PixelWidth
		{
			get;
			set;
		}

		public bool GetFFTData(float[] fftDataBuffer)
		{
			sampleAggregator.GetFFTResults(fftDataBuffer);
			return isPlaying;
		}


		public int GetFFTFrequencyIndex(int frequency)
		{
			double maxFrequency;
			if (ActiveStream != null)
				maxFrequency = ActiveStream.WaveFormat.SampleRate / 2.0d;
			else
				maxFrequency = 22050; // Assume a default 44.1 kHz sample rate.
			return (int)((frequency / maxFrequency) * (fftDataSize / 2));
		}
		#endregion

		#region IWaveformPlayer
		public double GetMaximum()
		{
			return ActiveStream?.TotalTime.TotalSeconds ?? 0;
		}

		public TimeSpan SelectionBegin
		{
			get => repeatStart;
			set
			{
				if (!inRepeatSet)
				{
					inRepeatSet = true;
					TimeSpan oldValue = repeatStart;
					repeatStart = value;
					if (oldValue != repeatStart)
						RaisePropertyChanged();
					inRepeatSet = false;
				}
			}
		}

		public TimeSpan SelectionEnd
		{
			get => repeatStop;
			set
			{
				if (!inChannelSet)
				{
					inRepeatSet = true;
					TimeSpan oldValue = repeatStop;
					repeatStop = value;
					if (oldValue != repeatStop)
						RaisePropertyChanged();
					inRepeatSet = false;
				}
			}
		}

		public bool IsPlaying
		{
			get => isPlaying;
			protected set
			{
				bool oldValue = isPlaying;
				isPlaying = value;
				if (oldValue != isPlaying)
					RaisePropertyChanged();
				positionTimer.IsEnabled = value;
			}
		}

		#endregion

		#region Properties
		/// <summary>
		/// Inputstream from the audio source e.g. file, mixer line, etc..
		/// </summary>
		public WaveStream ActiveStream
		{
			get => activeStream;
			protected set
			{
				WaveStream oldValue = activeStream;
				activeStream = value;
				if (oldValue is null)
				{
					InitTimer();
					WaveOutChannel = new WaveChannel32(value);
				}

				if (oldValue != activeStream)
					RaisePropertyChanged();
			}
		}


		/// <summary>
		/// Output channel the output device is initialized with
		/// </summary>
		public WaveChannel32 WaveOutChannel
		{
			get => waveChannel32;
			protected set
			{
				if (waveChannel32 != value)
				{
					waveChannel32 = value;
					RaisePropertyChanged();
				}
			}
		}


		public double ChannelLength
		{
			get => channelLength;
			protected set
			{
				double oldValue = channelLength;
				channelLength = value;
				if (oldValue != channelLength)
					RaisePropertyChanged();
			}
		}

		public double ChannelPosition
		{
			get => channelPosition;
			set
			{
				if (!inChannelSet)
				{
					inChannelSet = true; // Avoid recursion
					double oldValue = channelPosition;
					double position = Math.Max(0, Math.Min(value, ChannelLength));
					if (!inChannelTimerUpdate && ActiveStream != null)
					{
						ActiveStream.Position = (long)(position / ActiveStream.TotalTime.TotalSeconds * ActiveStream.Length);
					}

					channelPosition = position;
					if (oldValue != channelPosition)
						RaisePropertyChanged();
					inChannelSet = false;
				}
			}
		}

		public TagLib.File FileTag
		{
			get => fileTag;
			set
			{
				TagLib.File oldValue = fileTag;
				fileTag = value;
				if (oldValue != fileTag)
					RaisePropertyChanged();
			}
		}


		public bool CanPlay
		{
			get => PlaybackState == PlaybackState.Stopped || PlaybackState == PlaybackState.Paused
				&& this.PlaybackState == waveOutDevice.PlaybackState
				&& ActiveStream != null;
			//protected set
			//{
			//	bool oldValue = canPlay;
			//	canPlay = value;
			//	if (oldValue != canPlay)
			//		RaisePropertyChanged();
			//}
		}

		public bool CanPause
		{
			get => PlaybackState == PlaybackState.Playing
				&& this.PlaybackState == waveOutDevice.PlaybackState;
			//protected set
			//{
			//	bool oldValue = canPause;
			//	canPause = value;
			//	if (oldValue != canPause)
			//		RaisePropertyChanged();
			//}
		}

		public bool CanStop
		{
			get => PlaybackState == PlaybackState.Playing || PlaybackState == PlaybackState.Paused
				&& this.PlaybackState == waveOutDevice.PlaybackState;
			//protected set
			//{
			//	bool oldValue = canStop;
			//	canStop = value;
			//	if (oldValue != canStop)
			//		RaisePropertyChanged();
			//}
		}
		public bool IsLoaded => ActiveStream != null && waveOutDevice != null;

		public PlaybackState PlaybackState
		{
			get
			{
				if (waveOutDevice != null)
				{
					RaisePropertyChanged();
					return waveOutDevice.PlaybackState;
				}

				return PlaybackState.Stopped;
			}
		}

		public virtual Single Volume
		{
			get => waveOutDevice?.Volume ?? 1F;
			set
			{
				if (waveOutDevice != null)
				{
					waveOutDevice.Volume = value;
					RaisePropertyChanged();
				}
			}
		}

		public virtual TimeSpan CurrentTime
		{
			get => ActiveStream?.CurrentTime ?? TimeSpan.Zero;
			set
			{
				if (ActiveStream != null)
				{

					ActiveStream.CurrentTime = value;
					RaisePropertyChanged();
				}
			}
		}

		public virtual TimeSpan TotalTime => ActiveStream?.TotalTime ?? TimeSpan.Zero;

		public IMvxMainThreadAsyncDispatcher AsyncDispatcher
		{
			get;
		}

		public string Key
		{
			get;
			private set;
		}

		public bool EndOfStream
		{
			get;
			private set;
		} //ActiveStream != null && ActiveStream.Position == ActiveStream.TotalTime.TotalSeconds;

		#endregion

		#region EventHandler

		protected EventHandler<PlaybackStoppedEventArgs> PlaybackStopped = delegate { };

		protected virtual void WaveOutDevicePlaybackStopped(object sender, StoppedEventArgs e)
		{
			EndOfStream = true;

			var handler = PlaybackStopped;
			handler?.Invoke(sender, new PlaybackStoppedEventArgs(this, e.Exception));
		}

		protected virtual void PositionTimerTick(object sender, EventArgs e)
		{
			inChannelTimerUpdate = true;
			ChannelPosition = (WaveOutChannel.Position / (double)WaveOutChannel.Length) * WaveOutChannel.TotalTime.TotalSeconds;
			var args = new PositionChangedEventArgs(ChannelPosition);
			inChannelTimerUpdate = false;
			OnPositionChanged(args);
		}

		protected virtual void WaveStreamSampleProcessed(object sender, SampleEventArgs e)
		{
		}

		#endregion

		public static NAudioEngine Create(IStorageItem storageItem, EventHandler<PlaybackStoppedEventArgs> stoppedHandler = null)
		{
			string extension = storageItem?.GetExtension() ?? throw new ArgumentNullException(nameof(storageItem));
			NAudioEngine audioPlayback;

			if (extension == ".flac")
			{
				audioPlayback = new FlacPlayback(storageItem.Id);
			}
			else
			{
				audioPlayback = new Mp3Playback(storageItem.Id);
			}

			audioPlayback.PlaybackStopped += stoppedHandler;
			return audioPlayback;
		}


		public static NAudioEngine Create(string storageItemId, string extension, EventHandler<PlaybackStoppedEventArgs> stoppedHandler = null)
		{
			NAudioEngine audioPlayback;

			if (extension == ".flac")
			{
				audioPlayback = new FlacPlayback(storageItemId);
			}
			else
			{
				audioPlayback = new Mp3Playback(storageItemId);
			}

			audioPlayback.PlaybackStopped += stoppedHandler;
			return audioPlayback;
		}

		protected virtual void OpenFile(string fileName)
		{
			if (ActiveStream != null)
			{
				SelectionBegin = TimeSpan.Zero;
				SelectionEnd = TimeSpan.Zero;
				ChannelPosition = 0;
			}
		}

		protected abstract void CreateDevice();

		public virtual double GetCurrentTimeInSeconds()
		{
			return ChannelPosition;
		}

		public virtual void SetCurrentPosition(double value)
		{
			ChannelPosition = value;
		}

		protected virtual void InitSampleAggregator()
		{
			sampleAggregator = new SampleAggregator(WaveOutChannel, fftDataSize);
			sampleAggregator.NotificationCount = WaveOutChannel.WaveFormat.SampleRate / 100;
			sampleAggregator.PerformFFT = true;
			sampleAggregator.FftCalculated += (s, a) => OnFftCalculated(a);
			sampleAggregator.MaximumCalculated += (s, a) => OnMaximumCalculated(a);
		}

		public void Load(string fileName)
		{
			((IAudioPlayback)this).EnsureDeviceCreated();
			OpenFile(fileName);
		}

		public void UnLoad()
		{
			Stop();
			((IAudioPlayback)this).CloseFile();
		}

		public void CloseFile()
		{
			if (ActiveStream != null)
			{
				ActiveStream.Dispose();
				ActiveStream = null;
			}
		}

		public void EnsureDeviceCreated()
		{
			if (waveOutDevice == null)
			{
				CreateDevice();
			}
		}

		#region PlaybackControl
		public void Play()
		{
			if (CanPlay)
			{
				waveOutDevice.Play();
				positionTimer.Start();
			}
		}

		public void Pause()
		{
			if (waveOutDevice != null && CanPause)
			{
				positionTimer.Stop();
				waveOutDevice.Pause();
			}
		}

		public void Stop()
		{
			if (CanStop)
			{
				positionTimer?.Stop();
				waveOutDevice?.Stop();
				ActiveStream?.Flush();

				if (ActiveStream != null)
				{
					ActiveStream.Position = 0;
					ActiveStream.CurrentTime = TimeSpan.Zero;
					WaveOutChannel.Position = 0;
					WaveOutChannel.CurrentTime = TimeSpan.Zero;
				}
			}
		}

		#endregion

		#region IDisposable

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!disposed)
			{
				if (disposing)
				{
					StopAndCloseStream();
				}
				disposed = true;
			}
		}



		protected virtual void StopAndCloseStream()
		{
			if (waveOutDevice != null)
			{
				waveOutDevice.Stop();
			}
			if (ActiveStream != null)
			{
				ActiveStream.Close();
				ActiveStream = null;
			}
			if (waveChannel32 != null)
			{
				waveChannel32.Close();
				waveChannel32 = null;
			}
			if (waveOutDevice != null)
			{
				waveOutDevice.Dispose();
				waveOutDevice = null;
			}
		}


		#endregion
	}
}
