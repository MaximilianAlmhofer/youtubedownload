﻿using MediaCore;

using NAudio.Dsp;
using NAudio.Wave;

using System;

namespace AudioLib.Sampling
{
	public sealed class SampleAggregator : ISampleProvider
	{

		public class StereoChannelVolumeValues
		{
			public float volumeLeftMaxValue;
			public float volumeLeftMinValue;
			public float volumeRightMaxValue;
			public float volumeRightMinValue;

			internal void SetLeftValues(float leftValue)
			{
				SetMaxValue(volumeLeftMaxValue, leftValue);
				SetMinValue(volumeLeftMinValue, leftValue);
			}

			internal void SetRightValues(float rightValue)
			{
				SetMaxValue(volumeRightMaxValue, rightValue);
				SetMinValue(volumeRightMinValue, rightValue);
			}

			private void SetMinValue(float minValue, float value)
			{
				minValue = Math.Min(minValue, value);
			}

			private void SetMaxValue(float maxValue, float value)
			{
				maxValue = Math.Max(maxValue, value);
			}
		}

		public StereoChannelVolumeValues leftRightVolumeValues = new StereoChannelVolumeValues();
		private float maxValue, minValue;
		private int count, fftPos, m;
		private readonly Complex[] fftBuffer;
		private readonly FftEventArgs fftArgs;
		private readonly int fftLength;
		private readonly ISampleProvider source;
		private readonly int channels;
		private int secondsPerPixel;

		private static readonly object lockToken = new object();

		public bool PerformFFT { get; set; }
		public int NotificationCount { get; set; }
		public WaveFormat WaveFormat => source.WaveFormat;

		public int SecondsPerPixel
		{
			get => secondsPerPixel;
			set => secondsPerPixel = value;
		}

		public event EventHandler<AmplitudeEventArgs> MaximumCalculated = delegate { };
		public event EventHandler<FftEventArgs> FftCalculated = delegate { };

		/// <summary>
		/// Constructor for the Mp3FileReader-Playback
		/// </summary>
		/// <param name="source"></param>
		/// <param name="fftLength"></param>
		public SampleAggregator(ISampleProvider source, int fftLength = 1024)
		{
			channels = source.WaveFormat.Channels;
			if (!IsPowerOfTwo(fftLength))
			{
				throw new ArgumentException("FFT Length must be a power of two");
			}
			m = (int)Math.Log(fftLength, 2.0);
			this.fftLength = fftLength;
			fftBuffer = new Complex[fftLength];
			fftArgs = new FftEventArgs(fftBuffer);
			this.source = source;
		}

		/// <summary>
		/// Constructor for the FlacFileReader-Playback
		/// </summary>
		/// <param name="source"></param>
		/// <param name="fftLength"></param>
		public SampleAggregator(IWaveProvider source, int fftLength = 1024)
		{
			channels = source.WaveFormat.Channels;
			if (!IsPowerOfTwo(fftLength))
			{
				throw new ArgumentException("FFT Length must be a power of two");
			}
			m = (int)Math.Log(fftLength, 2.0);
			this.fftLength = fftLength;
			fftBuffer = new Complex[fftLength];
			fftArgs = new FftEventArgs(fftBuffer);
			this.source = source.ToSampleProvider();
		}

		#region Methods
		bool IsPowerOfTwo(int x)
		{
			return (x & (x - 1)) == 0;
		}


		public void Reset()
		{
			count = 0;
			maxValue = minValue = 0;
		}


		private void Add(float value)
		{
			if (PerformFFT && FftCalculated != null)
			{
				fftBuffer[fftPos].X = (float)(value * FastFourierTransform.HammingWindow(fftPos, fftLength));
				fftBuffer[fftPos].Y = 0;
				fftPos++;
				if (fftPos >= fftBuffer.Length)
				{
					fftPos = 0;
					// 1024 = 2^10
					FastFourierTransform.FFT(true, m, fftBuffer);
					FftCalculated(this, fftArgs);
				}
			}

			maxValue = Math.Max(maxValue, value);
			minValue = Math.Min(minValue, value);
			count++;
			if (count >= NotificationCount && NotificationCount > 0)
			{
				if (MaximumCalculated != null)
				{
					MaximumCalculated(this, new AmplitudeEventArgs(minValue, maxValue));
				}
				Reset();
			}
		}


		public void Add(float leftValue, float rightValue)
		{

			if (fftPos == 0)
			{
				leftRightVolumeValues = new StereoChannelVolumeValues();
			}

			Add((leftValue + rightValue) / 2.0f);

			leftRightVolumeValues.SetLeftValues(leftValue);
			leftRightVolumeValues.SetRightValues(rightValue);
		}


		public int Read(float[] buffer, int offset, int count)
		{
			int samplesRead = 0;

			lock (lockToken)
			{
				samplesRead = source.Read(buffer, offset, count);

				for (int n = 0; n < samplesRead; n += channels)
				{
					Add(buffer[n + offset]);
				}
			}

			return samplesRead;
		}

		/// <summary>
		/// Performs an FFT calculation on the channel data upon request.
		/// </summary>
		/// <param name="fftBuffer">A buffer where the FFT data will be stored.</param>
		public void GetFFTResults(float[] buffer)
		{
			Complex[] channelDataClone = new Complex[fftBuffer.Length];
			fftBuffer.CopyTo(channelDataClone, 0);
			FastFourierTransform.FFT(true, m, channelDataClone);
			for (int i = 0; i < channelDataClone.Length / 2; i++)
			{
				/* Berechnen der tatsächlichen Intensitäten für das FFT-Ergebnis. */
				buffer[i] = (float)Math.Sqrt(channelDataClone[i].X * channelDataClone[i].X + channelDataClone[i].Y * channelDataClone[i].Y);
			}
		}

		public void Clear()
		{
			leftRightVolumeValues = new StereoChannelVolumeValues();
			fftPos = 0;
		}
		#endregion
	}

}
