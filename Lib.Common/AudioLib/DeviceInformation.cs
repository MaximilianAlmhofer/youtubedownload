﻿using MvvmCross.Logging;

using NAudio.Wave;

using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace AudioLib
{
	public sealed class DeviceInformation
	{
		private static readonly IMvxLogProvider logProvider;


		static DeviceInformation()
		{
			logProvider = MvvmCross.Mvx.IoCProvider.GetSingleton<IMvxLogProvider>();
		}


		public static IEnumerable<WaveOutCapabilities> GetWaveOutCapabilities()
		{

			WaveOutCapabilities[] caps = new WaveOutCapabilities[WaveOut.DeviceCount];

			for (int i = 0; i < WaveOut.DeviceCount;)
			{
				try
				{
					caps[i] = WaveOut.GetCapabilities(i++);
				}
				catch (Exception ex)
				{
					Debug.WriteLine(string.Format("WaveOut_DeviceCapabilities_Query_Failed on Devive: {0}", WaveOut.DeviceCount), ex);
				}
			}
			return caps;
		}


		public static IEnumerable<WaveInCapabilities> GetWaveInCapabilities()
		{

			WaveInCapabilities[] caps = new WaveInCapabilities[WaveIn.DeviceCount];

			for (int i = 0; i < WaveOut.DeviceCount;)
			{
				try
				{
					caps[i] = WaveIn.GetCapabilities(i++);
				}
				catch (Exception ex)
				{
					Debug.WriteLine(string.Format("WaveIn_DeviceCapabilities_Query_Failed on Device {0}", WaveOut.DeviceCount), ex);
				}
			}
			return caps;
		}
	}
}
