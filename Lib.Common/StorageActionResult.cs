﻿
using System;

namespace Lib.Common
{
	[Flags]
	public enum StorageActionResult : ushort
	{
		Added = 0x3E,
		Moved = 0x3F,
		Renamed = 0x7D,
		Deleted = 0x7E,
		Error = 0x7F
	}
}