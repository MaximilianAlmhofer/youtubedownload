﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;

using static Lib.Common.Globals;

namespace Lib.Common
{
	using static System.Diagnostics.Trace;

	public static class CommonExtensions
	{

		private static void LogObjectt(this object @object)
		{
			try
			{
				var sb = new StringBuilder(@object.GetType().FullName).AppendLine();
				@object.GetType().GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public)
					.Select(x => new[] { x.Name, x.GetValue(@object) })
					.OrderByDescending(x => x[0].ToString().Length).ToList()
					.ForEach(x => sb.AppendJoin("\r\n", string.Join(":\t", x)).AppendLine());
				Debug.Write(sb.ToString());
			}
			catch (Exception ex)
			{
				Debug.WriteLine("EXCEPTION:");
				Debug.WriteLine(@object.GetType().Name);
				Debug.Write(ex.ToLongString());
			}
		}


		public static void SetField<T>(this object @object, string name, T value, bool @private = true)
		{
			try
			{
				@object?.GetType().GetField(name, @private ? InstanceNonPublicGetSetF : InstancePublicGetSetF)?.SetValue(@object, value);
			}
			catch (FieldAccessException ex1)
			{
				ex1.Trace();
			}
			catch (TargetException ex2)
			{
				ex2.Trace();
			}
			catch (Exception ex)
			{
				ex.Trace();
				throw;
			}
		}


		public static void SetProperty<T>(this object @object, string name, T value, bool @private = false)
		{
			try
			{
				@object?.GetType().GetProperty(name, @private ? InstanceNonPublicGetSetP : InstancePublicGetSetP)?.SetValue(@object, value);
			}
			catch (FieldAccessException ex1)
			{
				ex1.Trace();
			}
			catch (TargetException ex2)
			{
				ex2.Trace();
			}
			catch (Exception ex)
			{
				ex.Trace();
				throw;
			}
		}


		public static T GetProperty<T>(this object @object, string name, bool @private = false)
		{
			try
			{
				return (T)@object.GetType().GetProperty(name, @private ? InstanceNonPublicGetSetP : InstancePublicGetSetP)?.GetValue(@object, null);
			}
			catch (Exception ex)
			{
				ex.Trace();
				throw;
			}
		}


		public static void SetEnumField<TEnum>(this object @object, string fieldName, string enumValueName, bool @private = true) where TEnum : struct
		{
			try
			{
				FieldInfo field;
				if ((field = @object?.GetType().GetField(fieldName, @private ? InstanceNonPublicGetSetF : InstancePublicGetSetF)) != null
					&& Enum.TryParse<TEnum>(enumValueName, out var result))
				{
					field.SetValue(@object, result);
				}
			}
			catch (FieldAccessException ex1)
			{
				ex1.Trace();
			}
			catch (TargetException ex2)
			{
				ex2.Trace();
			}
			catch (Exception ex)
			{
				ex.Trace();
				throw;
			}
		}

		public static string ToLongString(this Exception ex, params object[] args)
		{
			string exceptionMessage = string.Concat($"Ausnahme in {ex.Source} bei {ex.TargetSite}:" +
				$"\r\n\r\n{ex.Message}" +
				$"\r\n\r\nSTACKTRACE:" +
				$"\r\n{ex.StackTrace}");

			AddArgs(ref exceptionMessage, ex.GetType(), args);

			Exception inner = ex.InnerException;
			while (inner != null)
			{
				exceptionMessage = string.Join(
					"\r\n\r\n----- STACK-FRAME -----\r\n",
					exceptionMessage, inner.ToLongString());
				inner = inner.InnerException;
			}

			return exceptionMessage;
		}


		public static string ToLongString(this Exception ex)
		{
			string exceptionMessage = string.Concat($"Ausnahme in {ex.Source} bei {ex.TargetSite}:" +
				$"\r\n\r\n{ex.Message}" +
				$"\r\n\r\nSTACKTRACE:" +
				$"\r\n{ex.StackTrace}");

			Exception inner = ex.InnerException;
			while (inner != null)
			{
				exceptionMessage = string.Join(
					"\r\n\r\n----- STACK-FRAME -----\r\n",
					exceptionMessage, inner.ToLongString());
				inner = inner.InnerException;
			}

			return exceptionMessage;
		}


		private static void AddArgs(ref string exceptionString, Type exceptionType, params object[] args)
		{
			if (args?.Length % 2 == 0)
			{
				var sb = new StringBuilder(exceptionString).AppendLine();
				for (int i = 0; i < args.Length - 1;)
				{
					PropertyInfo pInfo = exceptionType.GetType().GetProperty(args[i + 1].ToString(), InstancePublicGetSetP);
					sb.AppendFormat("{0} {1}\r\n", pInfo.Name, args[i]);
					i++;
				}
				exceptionString = sb.ToString();
			}
		}


		public static void Trace(this object @object)
		{
			WriteLine(@object is Exception ? (@object as Exception).ToLongString() : @object);
		}


		public static void Trace(this object @object, params object[] args)
		{
			WriteLine(@object is Exception ? (@object as Exception).ToLongString(args) : @object);
		}


		public static Type GetTypeFromAssembly(string assemblyFullName, string typeName, string interfaceName = "")
		{
			Type searchedType = null;
			var assemblies = AppDomain.CurrentDomain.GetAssemblies();
			Assembly assembly = assemblies?.SingleOrDefault(x => x.GetName().Name == assemblyFullName);
			if (assembly != null)
			{
				Type type = assembly.GetExportedTypes()?.ToList().SingleOrDefault(x => string.Equals(x.Name, typeName));
				if (type != null && !string.IsNullOrEmpty(interfaceName))
				{
					searchedType = type.GetTypeInfo().ImplementedInterfaces?.SingleOrDefault(x => x.Name == interfaceName);
				}
				else
				{
					searchedType = type;
				}
			}
			return searchedType;
		}

#nullable enable
		public static Uri? ToUri(this string source)
		{
			if (!string.IsNullOrEmpty(source) && Uri.IsWellFormedUriString(source, UriKind.RelativeOrAbsolute))
				return new Uri(source);
			return null;
		}

#nullable disable
	}
}