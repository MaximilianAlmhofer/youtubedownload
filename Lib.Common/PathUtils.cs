﻿using System;
using System.IO;
using System.Linq;

namespace Lib.Common
{
	public static class PathUtils
	{
		public static string ToLinux(this string windowsPath)
		{
			string linuxPath = new Uri(windowsPath).ToString().Remove(0, 8);
			string drive = "/mnt/" + linuxPath.Split(Path.VolumeSeparatorChar)[0].ToLower();
			linuxPath = string.Concat(drive, linuxPath.Remove(0, 2));
			return linuxPath;
		}

		public static string Finddir(string start, string seek)
		{
			start = Path.GetDirectoryName(start);
			while (start != null && !Directory.Exists(seek))
			{
				seek = Path.Combine(Path.GetDirectoryName(start), seek);
			}
			return seek;
		}

		public static string GetValidFileSystemName(string filesystemEntry)
		{
			if (string.IsNullOrEmpty(filesystemEntry))
				return string.Empty;

			Path.GetInvalidPathChars().ToList().ForEach(x =>
			{
				filesystemEntry = filesystemEntry.Replace(x.ToString(), string.Empty);
				Path.GetInvalidFileNameChars().ToList().ForEach(y =>
				{
					filesystemEntry = filesystemEntry.Replace(y.ToString(), string.Empty);
				});
			});
			return filesystemEntry;
		}
	}
}
