﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace Lib.Common
{
	public static class MDSerializer
	{
		private static GlobalConfig config;

		private static Dictionary<string, string> storage = new Dictionary<string, string>();

		public static void Init(GlobalConfig _config)
		{
			config = _config;
			if (config != null)
			{
				if (!string.IsNullOrEmpty(config.IndexDat) && File.Exists(config.IndexDat))
				{
					DeserializeB(config.IndexDat);
				}
			}
		}

		public static Task SerializeJ(object value, string path)
		{
			string json = JsonConvert.SerializeObject(value);
			return File.WriteAllTextAsync(path, json);
		}

		public static Task<T> DeserializeJ<T>(string path)
			where T : IStorageItem, IDownloadSource
		{
			string json = File.ReadAllText(path);
			return Task.FromResult(JsonConvert.DeserializeObject<T>(json));
		}

		public static void SerializeB(string indexFile)
		{
			if (storage.Count == 0)
			{
				try
				{
					foreach (var trackinfo in FileLoader.LoadTracks(config.AppFolder.MetadataFolderPath))
					{
						storage[trackinfo.Id] = trackinfo.FileLocation;
					}
				}
				catch (Exception ex)
				{
					ex.Trace();
				}
			}
			using (var fs = File.OpenWrite(indexFile))
			{
				new BinaryFormatter().Serialize(fs, storage);
			}
		}

		public static void DeserializeB(string path)
		{
			try
			{
				FileInfo fi = new FileInfo(path);
				if (fi.Length > 0)
				{
					using (var stream = new FileStream(path, FileMode.Open))
					{
						try
						{
							storage = new BinaryFormatter().Deserialize(stream) as Dictionary<string, string>;
						}
						catch (SerializationException emptyStream)
						{
							stream.Close();
							File.WriteAllText(path, null);
							Debug.Write(emptyStream.Message);
						}
					}
				}
			}
			catch (SerializationException ex) when (File.ReadAllBytes(config.IndexDat).Length < 10)
			{
				ex.Trace();
			}
		}
	}
}