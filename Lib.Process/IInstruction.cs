﻿namespace Lib.Command
{
	public interface IInstruction
	{
		/// <summary>
		/// Commandtext für die youtube-dl Kommandozeile
		/// </summary>
		string Command { get; }
	}
}
