﻿using System.Security;

namespace Lib.Command
{
	public interface IElevatedInstruction
	{
		/// <summary>
		/// Windows Username
		/// </summary>
		string UserName { get; }

		/// <summary>
		/// Secured Windowes Password
		/// </summary>
		SecureString Password { get; }
	}
}
