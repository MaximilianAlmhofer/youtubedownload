﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace Lib.Process
{
	public enum EnMessageLevel
	{
		Notification,
		Error
	}
	public class LogMessages
	{
		private StringBuilder sb = new StringBuilder();

		/// <summary>
		/// Loggen einfache Nachricht.
		/// </summary>
		/// <param name="message"></param>
		/// <param name="level"></param>
		public void Log(string message, EnMessageLevel level)
		{
			sb.AppendFormat("{0}:\t| {1}", level, message);
		}

		/// <summary>
		/// Loggen Fehlermeldung die ev. verschachtelt ist.
		/// </summary>
		/// <param name="exception"></param>
		public void Log(Exception exception, string additionalMessage = "")
		{
			Exception ex = exception.InnerException;

			sb.AppendFormat("\r\n".PadLeft(50, '-'))
				.AppendFormat("{0}:\t| {1}\r\n{2}", EnMessageLevel.Error, exception.GetType().Name, additionalMessage);

			while (ex != null)
			{
				Log(ex);
				ex = ex.InnerException;
			}

			sb.AppendFormat("{0}", exception.ToString());
		}


		public async Task LogAsync(System.Diagnostics.Process proc)
		{
			if (proc.StandardError.BaseStream.CanRead && !proc.StandardError.EndOfStream)
			{
				var logStr = await proc.StandardError.ReadToEndAsync();
				proc.StandardError.DiscardBufferedData();
				if (!string.IsNullOrEmpty(logStr))
				{
					Log(logStr, EnMessageLevel.Error);
				}
			}
		}

		/// <summary>
		/// Ausgeben Meldungen und Stringbuilder zurücksetzen.
		/// </summary>
		/// <returns></returns>
		public string GetCollectedText()
		{
			string messages = sb.ToString();
			sb.Clear();
			return messages;
		}
	}
}
