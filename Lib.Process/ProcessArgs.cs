﻿using Lib.Command;

namespace Lib.Process
{
	public class ProcessArgs : IInstruction
	{

		public string Executable { get; set; }

		public string Command { get; set; }

		public string WorkingDirectory { get; set; }

		public ProcessBehavior ProcessBehavior { get; set; }

		public ProcessArgs(ProcessBehavior processBehavior,
			string exe = null, string args = null, string workingDir = null)
		{
			ProcessBehavior = processBehavior;
			Executable = exe;
			Command = args;
			WorkingDirectory = workingDir;
		}
	}
}
