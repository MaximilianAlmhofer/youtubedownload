﻿using Lib.Command;

using System;
using System.Security;

namespace Lib.Process
{
	public sealed class ElevatedProcessArgs : IElevatedInstruction
	{


		private readonly ProcessArgs processCreateArgs;

		public static explicit operator ProcessArgs(ElevatedProcessArgs _this)
		{
			return _this?.processCreateArgs;
		}

		public string UserName { get; }

		public SecureString Password { get; }



		public ElevatedProcessArgs(ProcessArgs processCreateArgs, string userName, string passwordAsPlainText)
		{

			this.processCreateArgs = processCreateArgs ?? throw new ArgumentNullException(nameof(processCreateArgs));

			UserName = userName ?? throw new ArgumentNullException(nameof(userName));
			Password = CreateSecure(passwordAsPlainText);

			static unsafe SecureString CreateSecure(string plainText)
			{
				plainText = plainText ?? throw new ArgumentNullException(nameof(plainText));

				char[] chars = plainText.ToCharArray();

				fixed (char* pointer = &chars[0])
				{
					return new SecureString(pointer, chars.Length);
				}
			}
		}
	}
}
