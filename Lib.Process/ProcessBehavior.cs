﻿using System.Diagnostics;

namespace Lib.Process
{
	/// <summary>
	///		Minimized erwirkt das was eingentlich Hidden ausdrückt, 
	///		wenn in Kombi mit SkipWindow = true (ProcessStartInfo.CreateNoWindow)
	/// </summary>
	public class ProcessBehavior
	{
		public bool RunInSeperateOSShell { get; set; }

		public bool SkipWindow { get; set; }


		public ProcessWindowStyle WindowStyle { get; set; } = ProcessWindowStyle.Minimized;

		public bool ShowErrorDialog { get; set; }

		public bool RedirectStdOut { get; set; }

		public bool RedirectStdErr { get; set; }

		public bool RedirectStdIn { get; set; }

		public ProcessBehavior(bool useSeperateShell, bool createNoWindow = true,
			bool rStdin = false, bool rStdout = true, bool rStderr = true, bool showErrorDlg = false,
			ProcessWindowStyle? windowStyle = null)
		{
			RunInSeperateOSShell = useSeperateShell;
			if (RunInSeperateOSShell)
			{
				SkipWindow = true;
			}
			SkipWindow = createNoWindow;
			WindowStyle = windowStyle ?? ProcessWindowStyle.Minimized;
			RedirectStdIn = rStdin;
			RedirectStdOut = rStdout;
			RedirectStdErr = rStderr;
			ShowErrorDialog = showErrorDlg;

		}

		public static ProcessBehavior DefaulBehavior => new ProcessBehavior(useSeperateShell: false);
	}
}
