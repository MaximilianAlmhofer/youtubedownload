﻿using Lib.Command;

using System;
using System.Diagnostics;

namespace Lib.Process
{
	public static class ProcessFactory
	{


		public static System.Diagnostics.Process Create(IInstruction instruction)
		{
			var createArgs = instruction as ProcessArgs ??
				throw new ArgumentNullException(nameof(instruction));

			return new System.Diagnostics.Process() { StartInfo = CreateStartInfo(createArgs) };
		}




		public static System.Diagnostics.Process CreateElevated(IElevatedInstruction instruction)
		{
			var createArgs = (ProcessArgs)(ElevatedProcessArgs)instruction ??
				throw new ArgumentNullException(nameof(instruction));

			var psi = CreateStartInfo(createArgs);
			psi.Verb = "runas";

			return new System.Diagnostics.Process() { StartInfo = psi };
		}




		private static ProcessStartInfo CreateStartInfo(ProcessArgs createArgs)
		{
			return new ProcessStartInfo(createArgs.Executable, createArgs.Command)
			{
				UseShellExecute = createArgs.ProcessBehavior.RunInSeperateOSShell,
				CreateNoWindow = createArgs.ProcessBehavior.SkipWindow,
				WindowStyle = createArgs.ProcessBehavior.WindowStyle,
				WorkingDirectory = createArgs.WorkingDirectory,
				ErrorDialog = createArgs.ProcessBehavior.ShowErrorDialog,
				RedirectStandardOutput = createArgs.ProcessBehavior.RedirectStdOut,
				RedirectStandardError = createArgs.ProcessBehavior.RedirectStdErr,
				RedirectStandardInput = createArgs.ProcessBehavior.RedirectStdIn,
			};
		}


	}
}
