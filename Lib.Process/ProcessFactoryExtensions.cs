﻿using Lib.Command;

using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Threading;

namespace Lib.Process
{
	public static class ProcessFactoryExtensions
	{
		private static readonly object lockToken = new LockCookie();


		public static System.Diagnostics.Process WithOutputDataHandler(this System.Diagnostics.Process proc, DataReceivedEventHandler handler)
		{
			proc.EnableRaisingEvents = true;
			proc.OutputDataReceived += handler;
			return proc;
		}


		public static System.Diagnostics.Process WithErrorDataHandler(this System.Diagnostics.Process proc, LogMessages logm)
		{
			proc.EnableRaisingEvents = true;
			proc.Exited += (_sender, _e) =>
			{
				lock (lockToken)
				{
					logm = logm.CompleteLogPhase(proc);
				}
			};
			proc.ErrorDataReceived += (sender, e) =>
			{
				lock (lockToken)
				{
					logm.Log(e.Data, EnMessageLevel.Error);
				}
			};

			return proc;
		}


		public static LogMessages CompleteLogPhase(this LogMessages logm, System.Diagnostics.Process process)
		{
			string rest = process.StandardError.ReadToEnd();
			if (!string.IsNullOrEmpty(rest))
			{
				logm.Log(rest, EnMessageLevel.Error);
				logm.Log($"Der Process endete mit ExitCode {process.ExitCode}.", EnMessageLevel.Notification);
			}
			return logm;
		}


		public static IElevatedInstruction RunAs(this IInstruction primitivePrincipal, string userName, string password)
		{
			return new ElevatedProcessArgs(primitivePrincipal as ProcessArgs, userName, password);
		}


		public static string ToDescription(this System.Enum source)
		{
			return source?.GetType().GetTypeInfo().GetCustomAttribute<DescriptionAttribute>()?.Description;
		}
	}
}
