#!/bin/bash

configuration=$2

# Validate args $1, $2
if [[ $# > 1 ]]; then
  if [[ "$1" != "Debug" && "$1" != "Release" ]]; then
    if [[ "$2" != "Debug" && "$2" != "Release" ]]; then
      echo "Setting Configuration=Debug" &> /dev/null
      configuration="Debug"
    fi
  else
    echo "First argument must be the name of the project."
    exit -1
  fi
else
  echo "Argument count invalid 1=Project name, 2=Configuration"
  exit -1
fi

# Create an emoty custom AssemblyInfo file to the projects obj directory.
assembly_info="/mnt/g/.NET Core/3.0/$1/obj/$configuration/netcoreapp3.1/$1.AssemblyInfo.cs"
echo $assembly_info

if [[ ! -a $assembly_info ]]; then
	touch $assembly_info
	exit 1
else
	exit 0
fi
