﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace xtra_snd_tools.Modules.Search
{
	public delegate void StringSearchRoutedEventHandler(object sender, StringSearchRoutedEventArgs e);

	public class StringSearchRoutedEventArgs : RoutedEventArgs
	{

		private readonly StringSearch search;

		public StringSearchRoutedEventArgs(params string[] keys)
		{
			search = new StringSearch(keys);
		}

		public StringSearchRoutedEventArgs(RoutedEvent routedEvent, params string[] keys) : base(routedEvent)
		{
			search = new StringSearch(keys);
		}

		public StringSearchRoutedEventArgs(RoutedEvent routedEvent, object source, params string[] keys) : base(routedEvent, source)
		{
			search = new StringSearch(keys);
		}

		public Func<string, StringSearchResult> FindFirst => search.FindFirst;

		public Func<string, StringSearchResult[]> FindAll => search.FindAll;

		public Func<string, bool> ContainsAny => search.ContainsAny;

		public StringSearchResult GetFirst() => this.FindFirst(search.Keywords[0]);

		public StringSearchResult[] GetAll()
		{
			var resultList = new List<StringSearchResult>();
			foreach (var key in search.Keywords)
			{
				var _result = this.FindAll(key);
				if (_result != null && _result?.Length > 0)
				{
					resultList.AddRange(_result);
				}
			}
			return resultList.ToArray();
		}
	}
}
