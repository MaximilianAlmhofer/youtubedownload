﻿using Lib.Common;

using MvvmCross.Commands;

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace xtra_snd_tools.Modules.Search
{

	public enum SearchMode
	{
		Bound,
		Instant,
		Delay,
	}

	[TemplatePart(Name = "PART_HintLabel", Type = typeof(Label))]
	public class SearchTextBox : TextBox
	{
		private ScrollViewer scrollViewer;
		private Label hintLabel;
		private Label toolTip;
		private FrameworkElement contentHostTemplateContent;
		private ICommand discardCommand;
		private ICommand searchCommand;
		private Debouncer debouncer;
		private bool inModify;

		public static readonly DependencyProperty HintTextProperty;
		public static readonly DependencyProperty SearchModeProperty;
		public static readonly DependencyProperty HasTextProperty;
		public static readonly DependencyPropertyKey HasTextPropertyKey;
		public static readonly DependencyProperty DelayProperty;
		public static readonly RoutedEvent StringSearchEvent;

		public string HintText
		{
			get => (string)GetValue(HintTextProperty);
			set => SetValue(HintTextProperty, value);
		}

		public SearchMode SearchMode
		{
			get => (SearchMode)GetValue(SearchModeProperty);
			set => SetValue(SearchModeProperty, value);
		}

		public bool HasText
		{
			get => (bool)GetValue(HasTextProperty);
			private set => SetValue(HasTextPropertyKey, value);
		}

		public TimeSpan Delay
		{
			get => (TimeSpan)GetValue(DelayProperty);
			set => SetValue(DelayProperty, value);
		}

		public event StringSearchRoutedEventHandler StringSearch
		{
			add => AddHandler(StringSearchEvent, value);
			remove => RemoveHandler(StringSearchEvent, value);
		}

		[Bindable(false), Browsable(false)]
		public ICommand DiscardCommand
		{
			get
			{
				return discardCommand ??= new MvxCommand(() =>
				{
					this.Clear();
				});
			}
		}

		[Bindable(false), Browsable(false)]
		public ICommand SearchCommand
		{
			get
			{
				return searchCommand ??= new MvxCommand(() =>
				{
					if (SearchMode != SearchMode.Bound)
					{
						string text = this.Text;
						var args = new StringSearchRoutedEventArgs(StringSearchEvent, text);

						if (SearchMode == SearchMode.Instant)
						{
							RaiseEvent(args);
						}
						else if (SearchMode == SearchMode.Delay)
						{
							debouncer.Debounce(Delay, p => RaiseEvent((StringSearchRoutedEventArgs)p), args);
						}
					}
				});
			}
		}

		static SearchTextBox()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(SearchTextBox), new FrameworkPropertyMetadata(typeof(SearchTextBox)));
			TextBox.TextProperty.OverrideMetadata(typeof(SearchTextBox), new FrameworkPropertyMetadata(
				string.Empty, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.Journal,
				new PropertyChangedCallback(Text_PropertyChanged), new CoerceValueCallback(Text_CoerceValue), true, UpdateSourceTrigger.LostFocus));
			StringSearchEvent = EventManager.RegisterRoutedEvent(nameof(StringSearchEvent), RoutingStrategy.Bubble, typeof(StringSearchRoutedEventHandler), typeof(SearchTextBox));
			SearchModeProperty = DependencyProperty.Register(nameof(SearchMode), typeof(SearchMode), typeof(SearchTextBox), new FrameworkPropertyMetadata(
				SearchMode.Bound, FrameworkPropertyMetadataOptions.AffectsRender, new PropertyChangedCallback(SearchMode_PropertyChanged)));
			DelayProperty = DependencyProperty.Register(nameof(Delay), typeof(TimeSpan), typeof(SearchTextBox), new UIPropertyMetadata(TimeSpan.FromSeconds(1)));
			HintTextProperty = DependencyProperty.Register(nameof(HintText), typeof(string), typeof(SearchTextBox), new PropertyMetadata("Type to search..."));
			HasTextPropertyKey = DependencyProperty.RegisterReadOnly(nameof(HasText), typeof(bool), typeof(SearchTextBox), new PropertyMetadata(false, HasText_PropertyChanged));
			HasTextProperty = HasTextPropertyKey.DependencyProperty;
		}

		public SearchTextBox()
		{
			this.debouncer = new Debouncer(System.Windows.Threading.DispatcherPriority.ContextIdle, this.Dispatcher);
		}

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();
			this.scrollViewer = base.GetTemplateChild("PART_ContentHost") as ScrollViewer;
			this.hintLabel = base.GetTemplateChild("PART_HintLabel") as Label;
			this.toolTip = base.GetTemplateChild("searchModeTooltip") as Label;
			Grid templateContent = scrollViewer.Template.LoadContent() as Grid;
			this.contentHostTemplateContent = templateContent;
			this.KeyDown += SearchTextBox_KeyDown;
			this.KeyUp += SearchTextBox_KeyUp;
		}

		private static void SearchMode_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			if (d is SearchTextBox _this)
			{
				_this.ShowTooltipLabel(3010);
			}
		}

		private void ShowTooltipLabel(long milliSeconds)
		{
			if (toolTip != null)
			{
				toolTip.Opacity = 1.0d;
				toolTip.Visibility = Visibility.Visible;
				debouncer.Debounce(TimeSpan.FromMilliseconds(milliSeconds), c => ((Label)c).Visibility = Visibility.Collapsed, toolTip);
			}
		}

		private static void Text_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			if (d is SearchTextBox _this)
			{
				if (_this.inModify)
				{
					string oldValue = (string)e.OldValue;
					string newValue = (string)e.NewValue;
					if (newValue.EndsWith(' ') && newValue.Length - oldValue.Length == 1)
					{
						_this.CoerceValue(e.Property);
					}
				}
			}
		}

		private static object Text_CoerceValue(DependencyObject d, object baseValue)
		{
			baseValue ??= string.Empty;

			if (((SearchTextBox)d).inModify && ((string)baseValue).EndsWith(' '))
			{
				return ((string)baseValue).TrimEnd();
			}
			return baseValue;
		}

		protected override void OnTextChanged(TextChangedEventArgs e)
		{
			HasText = !string.IsNullOrEmpty(Text);
			base.OnTextChanged(e);
		}

		private static void HasText_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			if (d is SearchTextBox _this)
			{
				_this.ShowHideHintLabel();
			}
		}

		private void ShowHideHintLabel()
		{
			if (HasText)
			{
				HideHintText();
			}
			else
			{
				ShowHintText();
			}
		}

		private void ShowHintText()
		{
			hintLabel.Visibility = Visibility.Visible;
		}

		private void HideHintText()
		{
			hintLabel.Visibility = Visibility.Collapsed;
		}

		private void SearchTextBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (SearchMode == SearchMode.Bound)
			{
				ShowTooltipLabel(3010);
				return;
			}
			if (e.Key == Key.LeftCtrl || e.Key == Key.RightCtrl)
			{
				if (e.IsDown)
				{
					inModify = true;
				}
			}
			else if (inModify && e.Key == Key.Space)
			{
				SwitchMode(SearchMode);
				e.Handled = true;
			}
		}

		private void SwitchMode(SearchMode searchMode)
		{
			this.SearchMode = searchMode == SearchMode.Delay ? SearchMode.Instant : SearchMode.Delay;
		}

		private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
		{
			if ((e.Key == Key.LeftCtrl || e.Key == Key.RightCtrl) && e.IsUp)
			{
				inModify = false;
				e.Handled = true;
			}
		}
	}
}
