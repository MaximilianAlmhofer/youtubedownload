﻿
using MaterialDesignThemes.Wpf;

using MvvmCross.Commands;

using System;
using System.Threading.Tasks;
using System.Windows.Input;

using xtra_snd_tools.Common.Contracts.ViewModels;

namespace xtra_snd_tools.Mainshell.Dialogs
{
	public class DialogViewModel : NamedDialogViewModel
	{
		private ICommand runDialogCommand;
		private ICommand runExtendedDialogCommand;

		public DialogViewModel()
		{
			OpenSample4DialogCommand = new MvxCommand<object>(OpenSample4Dialog);
			AcceptSample4DialogCommand = new MvxCommand<object>(AcceptSample4Dialog);
			CancelSample4DialogCommand = new MvxCommand<object>(CancelSample4Dialog);
		}

		#region SAMPLE 3

		public ICommand RunDialogCommand
		{
			get
			{
				return runDialogCommand ??= new MvxCommand<object>(ExecuteRunDialog);
			}
		}

		public ICommand RunExtendedDialogCommand
		{
			get
			{
				return runExtendedDialogCommand ??= new MvxCommand<object>(ExecuteRunExtendedDialog);
			}
		}

		private async void ExecuteRunDialog(object o)
		{
			var view = new SampleDialog
			{
				DataContext = new NamedDialogViewModel()
			};

			var result = await DialogHost.Show(view, "RootDialog", ClosingEventHandler);

			Console.WriteLine("Dialog was closed, the CommandParameter used to close it was: " + (result ?? "NULL"));
		}

		private void ClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
		{
			Console.WriteLine("You can intercept the closing event, and cancel here.");
		}

		private async void ExecuteRunExtendedDialog(object o)
		{
			var view = new SampleDialog
			{
				DataContext = new NamedDialogViewModel()
			};

			var result = await DialogHost.Show(view, "RootDialog", ExtendedOpenedEventHandler, ExtendedClosingEventHandler);

			Console.WriteLine("Dialog was closed, the CommandParameter used to close it was: " + (result ?? "NULL"));
		}

		private void ExtendedOpenedEventHandler(object sender, DialogOpenedEventArgs eventargs)
		{
			Console.WriteLine("You could intercept the open and affect the dialog using eventArgs.Session.");
		}

		private void ExtendedClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
		{
			if ((bool)eventArgs.Parameter == false) return;

			eventArgs.Cancel();

			eventArgs.Session.UpdateContent(new ProgressDialog());

			Task.Delay(TimeSpan.FromSeconds(3))
				.ContinueWith((t, _) => eventArgs.Session.Close(false), null,
					TaskScheduler.FromCurrentSynchronizationContext());
		}

		#endregion

		#region SAMPLE 4

		public ICommand OpenSample4DialogCommand { get; }
		public ICommand AcceptSample4DialogCommand { get; }
		public ICommand CancelSample4DialogCommand { get; }

		private bool isSample4DialogOpen;
		public bool IsSample4DialogOpen
		{
			get => isSample4DialogOpen;
			set
			{
				this.Set(ref isSample4DialogOpen, value);
			}
		}

		private object sample4Content;
		public object Sample4Content
		{
			get => sample4Content;
			set
			{
				this.Set(ref sample4Content, value);
			}
		}

		private void OpenSample4Dialog(object obj)
		{
			Sample4Content = new Sample4Dialog();
			IsSample4DialogOpen = true;
		}

		private void CancelSample4Dialog(object obj)
		{
			IsSample4DialogOpen = false;
		}

		private void AcceptSample4Dialog(object obj)
		{
			Sample4Content = new ProgressDialog();
			Task.Delay(TimeSpan.FromSeconds(3))
				.ContinueWith((t, _) => IsSample4DialogOpen = false, null,
					TaskScheduler.FromCurrentSynchronizationContext());
		}

		#endregion
	}
}
