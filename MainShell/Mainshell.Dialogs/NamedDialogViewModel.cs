using xtra_snd_tools.Common.Contracts.ViewModels;

namespace xtra_snd_tools.Mainshell.Dialogs
{
	public class NamedDialogViewModel : NotifyPropertyChangeViewModel
	{
		private string name;

		public string Name
		{
			get { return name; }
			set
			{
				this.Set(ref name, value);
			}
		}
	}
}