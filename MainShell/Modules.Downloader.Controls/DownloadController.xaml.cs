﻿using Lib.Common;

using MvvmCross.ViewModels;

namespace xtra_snd_tools.Modules.Downloader.Controls
{
	/// <summary>
	/// Interaktionslogik für DownloadController.xaml
	/// </summary>
	[MvxViewFor(typeof(IDownloadViewModel))]
	public partial class DownloadController : MvvmCross.Platforms.Wpf.Views.MvxWpfView
	{
		// Visibility="{Binding IsDownloadInProgress, Mode=OneWay, UpdateSourceTrigger=PropertyChanged, Converter={StaticResource BoolToVisibilityConverter}, FallbackValue=Collapsed}"
		public DownloadController()
		{
			InitializeComponent();
		}
	}
}
