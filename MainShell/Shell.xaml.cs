﻿using EnvDTEAdapter;

using Lib.Common;
using Lib.Utility;

using MaterialDesignThemes.Wpf;

using MvvmCross.Commands;
using MvvmCross.Platforms.Wpf.Presenters.Attributes;
using MvvmCross.Platforms.Wpf.Views;

using Settings;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Animation;

using xtra_snd_tools.Infra;
using xtra_snd_tools.Infra.Services.Interfaces;
using xtra_snd_tools.Mainshell.Controls;
using xtra_snd_tools.Mainshell.Dialogs;
using xtra_snd_tools.Mainshell.Settings;
using xtra_snd_tools.Mainshell.ViewModels;
using xtra_snd_tools.Mainshell.Views;
using xtra_snd_tools.Modules.Playback.Views;

namespace xtra_snd_tools
{
	[MvxWindowPresentation(Identifier = "Shell", Modal = false, ViewModelType = typeof(IMainWindowViewModel), ViewType = typeof(HomeView))]
	public partial class Shell : MvvmCross.Platforms.Wpf.Views.MvxWindow, IWin32Window
	{
		private Frame FrameContent;
		private readonly MainWindowViewModel viewModel;


		private static void DoLogin()
		{
			using (var login = new LoginModal())
			{
				bool? success;
				do
				{
					if (login.Canceled)
					{
						break;
					}

					success = login.ShowDialog();
					if (login.Canceled)
					{
						break;
					}
				} while (!success.GetValueOrDefault());

				if (login.Canceled)
				{
					App.Current.Shutdown();
				}
			}
		}

		public Shell()
		{
			DoLogin();
			InitializeComponent();
			FrameContent = this.ContentRoot.FindVisualChild<Frame>(typeof(Menu), typeof(PlaybackControlPanel));
			DataContextChanged += MainWindow_DataContextChanged;
			Loaded += MainWindow_Loaded;
			viewModel = new MainWindowViewModel();
			ViewModel = viewModel;
			DataContext = viewModel;
		}

		#region IWin32Window
		public IntPtr Handle
		{
			get
			{
				IntPtr handle = IntPtr.Zero;
				HwndSource hwnd;
				if ((hwnd = (HwndSource)HwndSource.FromVisual(this)) != null)
				{
					handle = hwnd.Handle;
				}

				return handle;
			}
		}
		#endregion

		#region Themes 
		private void LoadExpressionDarkTheme()
		{
			ExpressionDarkMenuItem.IsChecked = true;
			ExpressionDarkMenuItem.IsEnabled = false;
			ExpressionLightMenuItem.IsChecked = false;
			ExpressionLightMenuItem.IsEnabled = true;

			var themes = (ResourceDictionary)Application.LoadComponent(new Uri("\\Themes\\Generic.xaml", UriKind.Relative));
			var dark = (ResourceDictionary)Application.LoadComponent(new Uri("\\Themes\\Controls.xaml", UriKind.Relative));

			themes.MergedDictionaries.Clear();
			themes.MergedDictionaries.Add(dark);
			Resources.MergedDictionaries.Clear();
			Resources.MergedDictionaries.Add(themes);
		}

		private void LoadExpressionLightTheme()
		{
			ExpressionDarkMenuItem.IsChecked = false;
			ExpressionDarkMenuItem.IsEnabled = true;
			ExpressionLightMenuItem.IsChecked = true;
			ExpressionLightMenuItem.IsEnabled = false;

			var themes = (ResourceDictionary)Application.LoadComponent(new Uri("\\Themes\\Generic.xaml", UriKind.Relative));
			var light = (ResourceDictionary)Application.LoadComponent(new Uri("\\Themes\\ControlsLight.xaml", UriKind.Relative));
			
			themes.MergedDictionaries.Clear();
			themes.MergedDictionaries.Add(light);
			Resources.MergedDictionaries.Clear();
			Resources.MergedDictionaries.Add(themes);
		}
		#endregion

		#region Methods
		private static readonly Dictionary<string, MvxWpfView> cachedViews = new Dictionary<string, MvxWpfView>();
		public static MvxWpfView OnViewNavigating(Type viewType, object dataContext)
		{
			if (viewType is null)
				viewType = typeof(HomeView);
			string viewTypeName = viewType.FullName;

			if (!cachedViews.ContainsKey(viewTypeName))
			{
				var view = (MvxWpfView)Activator.CreateInstance(viewType);
				view.DataContext = dataContext;
				cachedViews.Add(viewTypeName, view);
			}

			return cachedViews[viewTypeName];
		}
		#endregion


		#region Handlers
		private void MainWindow_Loaded(object sender, RoutedEventArgs e)
		{
			FrameContent.Navigate(OnViewNavigating(typeof(HomeView), viewModel.HomeViewModel));
		}

		private void MainWindow_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
		{
			if (BindingContext.DataContext is null)
			{
				BindingContext.Init(e.NewValue, DataContextProperty, "DataContext");
			}
		}

		#region Menu
		private void ExpressionDarkMenuItem_Checked(object sender, RoutedEventArgs e)
		{
			LoadExpressionDarkTheme();
		}

		private void ExpressionLightMenuItem_Checked(object sender, RoutedEventArgs e)
		{
			LoadExpressionLightTheme();
		}

		private void LogoutMenuItem_Click(object sender, RoutedEventArgs e)
		{
			var auth = MvvmCross.Mvx.IoCProvider.Resolve<IAuthenticationService>();
			if (auth != null)
			{
				auth.Logout(AppSetup.Instance.User.GetName());
			}
			DoLogin();
		}

		private void ShutdownMenuItem_Click(object sender, RoutedEventArgs e)
		{
			App.Current.Shutdown();
		}


		private void OptionsMenuItem_Click(object sender, RoutedEventArgs e)
		{
			FormSettings.ModalDialog(App.Current.Config, App.Current.AppSettings);
		}


		private void HomeViewMenuItem_Clicked(object sender, RoutedEventArgs e)
		{
			FrameContent.Navigate(OnViewNavigating(typeof(HomeView), viewModel.PlayerViewModel));
		}


		private void MediaListViewMenuItem_Clicked(object sender, RoutedEventArgs e)
		{
			FrameContent.Navigate(OnViewNavigating(typeof(MediaPlayerView), viewModel.TrackListViewModel));
		}


		private void MediaplayerViewMenuItem_Clicked(object sender, RoutedEventArgs e)
		{
			FrameContent.Navigate(OnViewNavigating(typeof(MiniPlayerView), viewModel.PlayerViewModel));
		}


		//private void PlayerVisibleMenuItem_Checked(object sender, RoutedEventArgs e)
		//{
		//	if (PlaybackControlPanel != null)
		//		ShowPlaybackControl();
		//}


		//private void PlayerVisibleMenuItem_Unchecked(object sender, RoutedEventArgs e)
		//{
		//	if (PlaybackControlPanel != null)
		//		HidePlaybackControl();
		//}


		bool settingsDisplayed = false;
		private void AudioSettingsSubMenuItem_Click(object sender, RoutedEventArgs e)
		{
			if (settingsDisplayed)
			{
				var settingsPage = FrameContainer.Children[1] as AudioSettingsControl;
				DoubleAnimationUsingKeyFrames da = new DoubleAnimationUsingKeyFrames();
				for (var i = settingsPage.Width; i >= 0; i -= 80)
				{
					var k = new DiscreteDoubleKeyFrame(i, KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds((settingsPage.Width - i) + 80)));
					da.KeyFrames.Add(k);
				}
				da.AccelerationRatio = 0.8;
				da.Completed += (ss, ee) =>
				{
					FrameContainer.Children.RemoveAt(1);
					settingsDisplayed = false;
				};
				settingsPage.BeginAnimation(WidthProperty, da);
			}
			else
			{
				var settingsPage = new AudioSettingsControl();
				DoubleAnimationUsingKeyFrames da = new DoubleAnimationUsingKeyFrames();
				for (int i = 0; i <= 400; i += 80)
				{
					var k = new DiscreteDoubleKeyFrame(i, KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(i)));
					da.KeyFrames.Add(k);
				}
				da.AccelerationRatio = 0.9;
				settingsPage.BeginAnimation(WidthProperty, da);
				FrameContainer.Children.Add(settingsPage);
				Grid.SetColumn(settingsPage, 1);
				settingsDisplayed = true;
			}
		}

		#endregion // Menu

		#endregion // Handlers
		private void RestartMainProcess_Click(object sender, RoutedEventArgs e)
		{
			
		}

		private void CurrentDomain_ProcessExit(object sender, EventArgs e)
		{
			System.Diagnostics.Process p = (System.Diagnostics.Process)AppDomain.CurrentDomain.GetData("Process");
			p.Start();
		}

		private void UIElement_PreviewMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			var dependencyObject = Mouse.Captured as DependencyObject;
			while (dependencyObject != null)
			{
				if (dependencyObject is ScrollBar) return;
				dependencyObject = VisualTreeHelper.GetParent(dependencyObject);
			}

			MenuToggleButton.IsChecked = false;
		}

		private async void MenuPopupButton_OnClick(object sender, RoutedEventArgs e)
		{
			var sampleMessageDialog = new MessageDialog
			{
				//Message = { Text = ((ButtonBase)sender).Content.ToString() }
			};

			await DialogHost.Show(sampleMessageDialog, "RootDialog");
		}

		private void OnCopy(object sender, ExecutedRoutedEventArgs e)
		{
			if (e.Parameter is string stringValue)
			{
				try
				{
					Clipboard.SetDataObject(stringValue);
				}
				catch (Exception ex)
				{
					Trace.WriteLine(ex.ToString());
				}
			}
		}
	}
}
