﻿using Lib.Common;

using System.ComponentModel;

using xtra_snd_tools.Modules.AudioVisualization.Controls;

namespace xtra_snd_tools
{
	internal static class PlayerControlHelper
	{
		internal static void CreateWaveFormVisualizationElement(IPlayerViewModel viewModel, string caption, PropertyChangedEventHandler propertyChangedHandler)
		{
			PolylineWaveFormVisualization visualization = new PolylineWaveFormVisualization(new PolylineWaveFormControl(), caption);
			var parameters = new PlayerParameters(visualization, new AudioPlaybackPlugin(visualization));
			if (viewModel != null)
			{
				viewModel.Prepare(parameters);
				viewModel.PropertyChanged += propertyChangedHandler;
			}
		}
	}
}
