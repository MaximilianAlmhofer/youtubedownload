﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.IO;
using System.Reflection.Metadata.Ecma335;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Animation;

using AudioLib.Playback;
using AudioLib.Visualization;

using grpc.client.lib;
using grpc.client.lib.ApiHandlers;

using Lib.Common;
using Lib.Common.Interfaces;
using Lib.Utility.Threading;

using Microsoft.AspNetCore.Server.Kestrel.Transport.Sockets;

using MvvmCross.Commands;
using MvvmCross.Core;

using NAudio.Wave;

using xtra_snd_tools.Common.Contracts.ViewModels;
using xtra_snd_tools.Infra;
using xtra_snd_tools.Mainshell.Markup;
using xtra_snd_tools.Modules.AudioVisualization;
using xtra_snd_tools.Modules.Playback.ViewModels;

namespace xtra_snd_tools.Mainshell.ViewModels
{
	public class PlaybackControlPanelViewModel : NotifyPropertyChangeViewModel, IPlaybackControlPanelViewModel
	{
		private IPlayerViewModel playerViewModelBackingField;

		public PlaybackControlPanelViewModel(IMainWindowViewModel mainWindowViewModel)
		{
			this.playerViewModelBackingField = mainWindowViewModel.PlayerViewModel;
		}

		private bool panelVisible;
		public bool PanelVisible
		{
			get => panelVisible;
			set
			{
				this.Set(ref panelVisible, value);
				if (MuteWhenHidden && !playerViewModelBackingField.Manager.IsMuted)
				{
					(playerViewModelBackingField as PlayerViewModel).Mute();
				}
			}
		}

		private bool muteWhenHidden;
		public bool MuteWhenHidden
		{
			get => muteWhenHidden;
			set => this.Set(ref muteWhenHidden, value);
		}


		#region IPlayerViewmodel Impl
		object IPlayerViewModel.LockToken => playerViewModelBackingField.LockToken;

		public IPlaybackManager Manager
		{
			get
			{
				lock (playerViewModelBackingField.LockToken)
				{
					return playerViewModelBackingField.Manager;
				}
			}
		}

		public IMediaItem MediaItem 
		{
			get
			{
				lock (playerViewModelBackingField.LockToken)
				{
					return playerViewModelBackingField.MediaItem;
				}
			}
		}

		public PlaybackState PlaybackState
		{
			get
			{
				lock (playerViewModelBackingField.LockToken)
				{
					return playerViewModelBackingField.PlaybackState;
				}
			}
		}

		public TimeSpan CurrentTime 
		{
			get
			{
				lock (playerViewModelBackingField.LockToken)
				{
					return playerViewModelBackingField.CurrentTime;
				}
			}
		}

		public TimeSpan NaturalDuration 
		{
			get
			{
				lock (playerViewModelBackingField.LockToken)
				{
					return playerViewModelBackingField.NaturalDuration;
				}
			}
		}

		public float Volume
		{
			get
			{
				lock (playerViewModelBackingField.LockToken)
				{
					return playerViewModelBackingField.Volume;
				}
			}
			set
			{
				lock (playerViewModelBackingField.LockToken)
				{
					playerViewModelBackingField.Volume = value;
				}
				RaisePropertyChanged();
			}
		}

		public float SpeedRatio 
		{
			get
			{
				lock (playerViewModelBackingField.LockToken)
				{
					return playerViewModelBackingField.SpeedRatio;
				}
			}
			set
			{
				lock (playerViewModelBackingField.LockToken)
				{
					playerViewModelBackingField.SpeedRatio = value;
				}
				RaisePropertyChanged();
			}
		}

		public double Balance 
		{
			get
			{
				lock (playerViewModelBackingField.LockToken)
				{
					return playerViewModelBackingField.Balance;
				}
			}
			set
			{
				lock (playerViewModelBackingField.LockToken)
				{
					playerViewModelBackingField.Balance = value;
				}
				RaisePropertyChanged();
			}
		}

		public double Position
		{
			get
			{
				lock (playerViewModelBackingField.LockToken)
				{
					return playerViewModelBackingField.Position;
				}
			}
			set
			{
				lock (playerViewModelBackingField.LockToken)
				{
					playerViewModelBackingField.Position = value;
				}
				RaisePropertyChanged();
			}
		}

		public double LengthInSeconds
		{
			get
			{
				lock (playerViewModelBackingField.LockToken)
				{
					return playerViewModelBackingField.LengthInSeconds;
				}
			}
			set
			{
				lock (playerViewModelBackingField.LockToken)
				{
					playerViewModelBackingField.LengthInSeconds = value;
				}
				RaisePropertyChanged();
			}
		}

		public Uri PlayPauseImageSource
		{
			get
			{
				lock (playerViewModelBackingField.LockToken)
				{
					return playerViewModelBackingField.PlayPauseImageSource;
				}
			}
		}

		public string Title
		{
			get
			{
				lock (playerViewModelBackingField.LockToken)
				{
					return playerViewModelBackingField.Title;
				}
			}
		}

		public object Visualization
		{
			get
			{
				return ((PlayerViewModel)playerViewModelBackingField).Visualization;
			}
		}

		public void Mute()
		{
			playerViewModelBackingField.Mute();
		}

		public void Prepare(PlayerParameters parameter)
		{
			playerViewModelBackingField.Prepare(parameter);	
		}


		public async Task RequestMediaItemAsync()
		{
			using (var receiver = grpc.client.lib.Receivers.SampleComplexReceiver.Create())
			{
				receiver.ReceiveStreamPart += Receiver_ReceiveStreamPart;
				var storageItem = MediaItem.GetStorageItem();

				Lib.Utility.Abstract.Ensure.NotNullOrEmpty(storageItem.FileLocation);
				Lib.Utility.Abstract.Ensure.NotNullOrEmpty(storageItem.Id);
				Lib.Utility.Abstract.Ensure.IsTrue(Path.GetExtension(storageItem.GetExtension()) != string.Empty);

				using (var grpcHandler = new PlaybackRequestHandler("https://localhost:55443"))
				{
					await foreach (var task in grpcHandler.StreamSamplesAsync(receiver, new grpc.protobuf.Protos.SampleAggregation.MediaItemInfoRequest
					{
						FilePath = storageItem.FileLocation,
						StorageItemId = storageItem.Id,
						Extension = storageItem.GetExtension()
					}, App.Current.CancellationHandle)) await task;
				}
			}
		}

		private async void Receiver_ReceiveStreamPart(object sender, grpc.client.lib.Receivers.StreamResponseEventArgs<NAudio.Dsp.Complex> e)
		{
			await AsyncDispatcher.ExecuteOnMainThreadAsync(() =>
			{
				if (playerViewModelBackingField is IAudioVisualization visualization)
				{
					visualization.SelectedVisualization.OnMaxCalculated(e.Result.Value.X * 2, e.Result.Value.Y * 2);
				}
			}, false).ConfigureAwait(false);
		}
		#endregion

		public ICommand PlayPauseCommand => new MvxAsyncCommand(InitiatePlaybackStreamRequestAsync, CanExecuteRequest, false);

		private bool CanExecuteRequest()
		{
			CommandManager.InvalidateRequerySuggested();
			return true;
		}

		private Task InitiatePlaybackStreamRequestAsync(CancellationToken token)
		{
			return RequestMediaItemAsync().WithCancellation(token);
		}


		public ICommand StopCommand => new MvxCommand(StopPlaybackStreamRequest, CanStopRequest);

		private bool CanStopRequest()
		{
			CommandManager.InvalidateRequerySuggested();
			return true;
		}

		private void StopPlaybackStreamRequest()
		{
			if ((this.Manager as IPlaybackEngine).AudioPlayback.IsPlaying)
			{
				this.playerViewModelBackingField.Manager.Stop();
			}
		}
	}
}
