﻿using EnvDTE;

using EnvDTEAdapter;

using Lib.Common;
using Lib.Common.Interfaces;

using MvvmCross.Commands;
using System;
using System.Windows.Input;

using xtra_snd_tools.Common.Contracts.ViewModels;

namespace xtra_snd_tools.Mainshell.ViewModels
{
	public class MainWindowViewModel : NotifyPropertyChangeViewModel, IMainWindowViewModel
	{

		private ICommand attachDetachDebuggerCommand;
		private ICommand restartProcessPrepareCommand;
		private IHomeViewModel homeViewModel;
		private IPlayerViewModel playerViewModel;
		private ITrackListViewModel trackListViewModel;
		private IPlaylistEditorViewModel editorViewModel;
		private IDownloadViewModel downloadViewModel;
		private IPlaybackControlPanelViewModel playbackControlPanelViewModel;
		private IEnvDTEAdapter adapter = AdapterFactory.CreateWrapper();

		public IHomeViewModel HomeViewModel
		{
			get
			{
				return homeViewModel ??= MvvmCross.Mvx.IoCProvider.Resolve<IHomeViewModel>();
			}
		}

		public ITrackListViewModel TrackListViewModel
		{
			get
			{
				return trackListViewModel ??= MvvmCross.Mvx.IoCProvider.Resolve<ITrackListViewModel>();
			}
		}

		public IPlaylistEditorViewModel PlaylistEditorViewModel
		{
			get
			{
				return editorViewModel ??= MvvmCross.Mvx.IoCProvider.Resolve<IPlaylistEditorViewModel>();
			}
		}

		public IDownloadViewModel DownloadViewModel
		{
			get
			{
				return downloadViewModel ??= MvvmCross.Mvx.IoCProvider.Resolve<IDownloadViewModel>();
			}
		}

		public IPlaybackControlPanelViewModel PlaybackControlPanelViewModel
		{
			get
			{
				return playbackControlPanelViewModel ??= MvvmCross.Mvx.IoCProvider.Resolve<IPlaybackControlPanelViewModel>();
			}
		}

		public IPlayerViewModel PlayerViewModel
		{
			get
			{
				return playerViewModel ??= MvvmCross.Mvx.IoCProvider.Resolve<IPlayerViewModel>();
			}
		}


		public bool DebuggerIsAttached { get => System.Diagnostics.Debugger.IsAttached; }

		public ICommand AttachDetachDebuggerCommand
		{
			get
			{
				return attachDetachDebuggerCommand ??= new MvxCommand(() =>
				{
					if (System.Diagnostics.Debugger.IsAttached)
					{
						adapter.DetachDebugger("VisualStudio.DTE.16.0");
					}
					else
					{
						adapter.AttachDebugger("VisualStudio.DTE.16.0");
					}
					RaisePropertyChanged(nameof(DebuggerIsAttached));
				});
			}
		}

		public ICommand RestartProcessPrepareCommand
		{
			get
			{
				return restartProcessPrepareCommand ??= new MvxCommand(() =>
				{
					var behaviour = new Lib.Process.ProcessBehavior(true, false, false, false, false, true, System.Diagnostics.ProcessWindowStyle.Normal);
					var p = Lib.Process.ProcessFactory.Create(new Lib.Process.ProcessArgs(behaviour, "xtra_snd_tools.exe", "/debug", AppDomain.CurrentDomain.BaseDirectory));
					AppDomain.CurrentDomain.ProcessExit += (sender, e) =>
					{
						System.Diagnostics.Process p = (System.Diagnostics.Process)AppDomain.CurrentDomain.GetData("Process");
						p.Start();
					};
					AppDomain.CurrentDomain.SetData("Process", p);
					App.Current.Shutdown();
				});
			}
		}
	}
}