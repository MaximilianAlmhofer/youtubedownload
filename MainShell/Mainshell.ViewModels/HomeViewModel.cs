﻿using Lib.Common;

using xtra_snd_tools.Common.Contracts.ViewModels;

namespace xtra_snd_tools.Mainshell.ViewModels
{

	public class HomeViewModel : NotifyPropertyChangeViewModel, IHomeViewModel
	{
		private readonly IMainWindowViewModel mainWindowViewModel;

		public HomeViewModel(IMainWindowViewModel mainWindowViewModel)
		{
			this.mainWindowViewModel = mainWindowViewModel;
		}

		public ITrackListViewModel TrackListViewModel
		{
			get => mainWindowViewModel.TrackListViewModel;
		}

		public IPlaylistEditorViewModel PlaylistEditorViewModel
		{
			get => mainWindowViewModel.PlaylistEditorViewModel;
		}
	}
}
