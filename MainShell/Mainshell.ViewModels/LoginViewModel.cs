﻿using Lib.Common;
using Lib.Utility;
using Lib.WindowsLogin;

using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using xtra_snd_tools.Common.Contracts.ViewModels;
using xtra_snd_tools.Infra;
using xtra_snd_tools.Infra.Services;
using xtra_snd_tools.Infra.Services.Interfaces;

namespace xtra_snd_tools.Mainshell.ViewModels
{
	public class LoginViewModel : NotifyPropertyChangeViewModel<LoginParameters>, ILoginViewModel
	{

		private readonly IAuthenticationService authService;

		private UserCredentials credentials;
		private LoginParameters loginArgs;

		private string password;
		private string emailAddress;


		public LoginViewModel(IAuthenticationService authService)
		{
			PropertyChanged += LoginViewModel_PropertyChanged;
			this.authService = authService ??= new AuthenticationService();
			this.userDelegation = true;
			RaisePropertyChanged(nameof(UserDelegation));
		}

		private void LoginViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == nameof(EmailAddress))
			{
				UserDelegation = true;
				this.isGuestLogin = false;

				string currentUser = Environment.UserName.ToLower();
				if (!string.Equals(EmailAddress.ToLower(), currentUser) && !(EmailAddress.Split('@')?[0].ToLower()).Contains(currentUser))
				{
					UserDelegation = false;
					this.isGuestLogin = true;
				}
				RaisePropertyChanged(nameof(IsGuestLogin));
			}
		}

		public override void Prepare(LoginParameters parameter)
		{
			this.loginArgs = parameter;

			Lib.Utility.StringFormatter.Format(
				"{0}\r\n---\r\n[ViewModel:LoginViewModel]\tUsername/Email:\t[{1}]\r\n[ViewModel:LoginViewModel]\tPassword:\t[{2}]\r\n",
				nameof(LoginViewModel.Prepare), EmailAddress, Password).Trace();
		}


		public Task<bool> LoginAsync()
		{
			Lib.Utility.StringFormatter.Format("{0}\r\n---\r\n[ViewModel:LoginViewModel]\tUsername/Email:\t[{1}]\r\n[ViewModel:LoginViewModel]\tPassword:\t[{2}]\r\n" +
				"[FieldInfo]: name='password', type='System.string', value='{3}'", nameof(LoginViewModel.LoginAsync), EmailAddress, Password, password).Trace();

			var loginTask = authService.AuthenticateAsync(EmailAddress, Password, out credentials);
			AppSetup.Instance.User = credentials;
			return loginTask;
		}


		public UserCredentials GetCredentials(string userName)
		{
			if (credentials is null)
			{
				LoginAsync();
			}

			return credentials;
		}


		public string EmailAddress
		{
			get
			{
				if (loginArgs != null)
				{
					return loginArgs?.EmailAddress;
				}
				else
				{
					return emailAddress;
				}
			}
			set
			{
				if (loginArgs != null)
				{
					loginArgs.EmailAddress = value;
				}
				else
				{
					emailAddress = value;
				}
				RaisePropertyChanged();
			}
		}


		public string Password
		{
			get
			{
				if (loginArgs != null)
				{
					return loginArgs.PassWordPlain;
				}
				else
				{
					return password;
				}
			}
			set
			{
				if (loginArgs != null)
				{
					loginArgs.PassWordPlain = value;
					loginArgs.SecurePassword(value);
				}
				else
				{
					password = value;
				}
				RaisePropertyChanged();
			}
		}


		private bool isGuestLogin;
		[Bindable(true)]
		public bool IsGuestLogin
		{
			get
			{
				return isGuestLogin;
			}
			private set
			{
				if (isGuestLogin != value)
				{
					isGuestLogin = value;
					RaisePropertyChanged();
				}
			}
		}


		private bool userDelegation;
		[Bindable(true)]
		public bool UserDelegation
		{
			get
			{
				return userDelegation;
			}
			private set
			{
				if (userDelegation != value)
				{
					userDelegation = value;
					RaisePropertyChanged();
				}
			}
		}


		private Uri windowsUserProfilePicture;
		[Bindable(true)]
		public Uri WindowsUserProfilePicture
		{
			get
			{
				if (windowsUserProfilePicture != GetUserProfilePictureOrDefault())
				{
					windowsUserProfilePicture = GetUserProfilePictureOrDefault();
					RaisePropertyChanged();
				}
				return windowsUserProfilePicture;
			}
		}

		private static Uri GetUserProfilePictureOrDefault()
		{
			string userCacheDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "user");
			if (Directory.Exists(userCacheDir) && File.Exists(Path.Combine(userCacheDir, $"{Environment.UserName.ToLower()}.bmp")))
				return new Uri(Path.Combine(userCacheDir, $"{Environment.UserName.ToLower()}.bmp"), UriKind.RelativeOrAbsolute);

			var profilePicSysFileInfo = new DirectoryInfo(Path.Combine(StringFormatter.Format("{0}", Environment.GetEnvironmentVariable("AppData")),
				@"Microsoft\Windows\AccountPictures"))?.GetFileSystemInfos("*-ms")?.FirstOrDefault();
			if (profilePicSysFileInfo is null)
				return new Uri(@"C:\ProgramData\Microsoft\User Account Pictures\user.png", UriKind.Absolute);

			if (!Directory.Exists(userCacheDir))
				Directory.CreateDirectory(userCacheDir).Create();
			string convertedPictureFile = AccountPictureConverter.ConvertToFile(profilePicSysFileInfo.FullName, userCacheDir, 96);
			var files = Directory.EnumerateFileSystemEntries(userCacheDir, "*-96.bmp");
			string file = files.SingleOrDefault(x => files.Max(y => new FileInfo(y).CreationTime) == new FileInfo(x).CreationTime);
			File.Move(file, Path.Combine(userCacheDir, $"{Environment.UserName.ToLower()}.bmp"));
			return new Uri(Path.Combine(userCacheDir, $"{Environment.UserName.ToLower()}.bmp"), UriKind.RelativeOrAbsolute);
		}


		public DateTime LoginTime
		{
			get => loginArgs?.LoginTime ?? DateTime.Now;
		}
	}
}
