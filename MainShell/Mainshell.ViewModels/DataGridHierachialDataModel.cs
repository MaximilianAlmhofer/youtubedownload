﻿using Lib.Common;

using System;
using System.Collections.Generic;
using System.Linq;

using xtra_snd_tools.Common.Contracts.ViewModels;

namespace xtra_snd_tools.Mainshell.ViewModels
{
	public class DataGridHierarchialDataModel : NotifyPropertyChangeViewModel, IHierarchic<IMediaItem>
	{
		private readonly WeakReference<Func<IMediaItem, IMediaItem>> selectItemWeakReference;
		private readonly WeakReference<IHomeViewModel> wfViewModelContainer;
		private readonly List<IHierarchic<IMediaItem>> children;
		private readonly DataGridHierarchialData dataModels;
		private object data;

		public DataGridHierarchialDataModel(DataGridHierarchialData data, WeakReference<IHomeViewModel> containerRef)
		{
			if (!containerRef.TryGetTarget(out var container))
				throw new ArgumentException(nameof(containerRef));

			this.wfViewModelContainer = containerRef;
			this.selectItemWeakReference = new WeakReference<Func<IMediaItem, IMediaItem>>(e => container.TrackListViewModel.Tracks.SelectItem(e));
			this.dataModels = data;
			this.children = new List<IHierarchic<IMediaItem>>();
		}

		public DataGridHierarchialDataModel(DataGridHierarchialData data)
		{
			this.children = new List<IHierarchic<IMediaItem>>();
			this.dataModels = data;
		}

		#region DataItems
		public IHierarchic Parent { get; set; }

		IMediaItem IHierarchic<IMediaItem>.Data
		{
			get => data as IMediaItem;
			set
			{
				data = value;
				RaisePropertyChanged();
				RaisePropertyChanged("Data");
			}
		}

		public object Data
		{
			get
			{
				if (data is IMediaItem)
				{
					wfViewModelContainer.TryGetTarget(out var container);
					((IMediaItem)data).ViewModel = container.PlaylistEditorViewModel;
				}
				return data;
			}
			set
			{
				data = value;
				RaisePropertyChanged();
			}
		}

		public object SelectedData
		{
			get => data;
			set
			{
				if (data != value)
				{
					if (value is IMediaItem _value)
					{
						if (selectItemWeakReference.TryGetTarget(out var selectItemFunc))
						{
							data = selectItemFunc.Invoke(_value);
						}
					}
					else
					{
						data = value;
					}
					RaisePropertyChanged();
				}
			}
		}
		#endregion

		#region Level / Expand-Collapse Properties
		private int _level = -1;
		public int Level
		{
			get
			{
				if (_level == -1)
				{
					_level = (Parent != null) ? Parent.Level + 1 : 0;
				}
				return _level;
			}
		}


		private bool _expanded = false;
		public bool IsExpanded
		{
			get { return _expanded; }
			set
			{
				if (_expanded != value)
				{
					_expanded = value;
					if (_expanded == true)
						Expand();
					else
						Collapse();
				}
			}
		}


		private bool _visible = false;
		public bool IsVisible
		{
			get { return _visible; }
			set
			{
				if (_visible != value)
				{
					_visible = value;
					if (_visible)
						ShowChildren();
					else
						HideChildren();
				}
			}
		}
		#endregion

		#region Expand-Collapse Methods
		private void Collapse()
		{
			dataModels.RemoveChildrenFromItems(this);
			foreach (IHierarchic d in children)
				d.IsVisible = false;
		}

		private void Expand()
		{
			dataModels.AddChildrenToItems(this);
			foreach (IHierarchic d in children)
				d.IsVisible = true;
		}

		private void ShowChildren()
		{
			if (IsExpanded)
			{
				// Following Order is Critical
				dataModels.AddChildrenToItems(this);
				foreach (IHierarchic d in children)
					d.IsVisible = true;
			}
		}

		private void HideChildren()
		{
			if (IsExpanded)
			{
				// Order is Critical
				dataModels.RemoveChildrenFromItems(this);
				foreach (IHierarchic d in children)
					d.IsVisible = false;
			}
		}

		public void AddChild(IHierarchic<IMediaItem> t)
		{
			if (t != null)
			{
				t.Parent = this;
				children.Add(t);
			}
		}
		#endregion

		#region Children
		public bool HasChildren
		{
			get
			{
				return children.Count > 0;
			}
		}

		public IHierarchic FirstChild
		{
			get
			{
				return children.FirstOrDefault();
			}
		}

		public IEnumerable<IHierarchic> GetChildren()
		{
			return children;
		}

		public IEnumerable<IHierarchic> GetVisibleDescendants()
		{
			return children
					.Where(x => x.IsVisible)
					.SelectMany(x => (new[] { x }).Concat(x.GetVisibleDescendants()));
		}

		public IHierarchic DeepClone(object item)
		{
			IHierarchic clone = (IHierarchic)((ICloneable)this).Clone();
			clone.Data = item;
			clone.Parent = this;
			return clone;
		}

		object ICloneable.Clone()
		{
			var _childs = new IHierarchic<IMediaItem>[this.children.Count];
			this.children.CopyTo(_childs);
			try
			{
				DataGridHierarchialDataModel clone = (IHierarchic)this.MemberwiseClone() as DataGridHierarchialDataModel;
				clone.children.Clear();
				return clone;
			}
			finally
			{
				if (this.children.Count == 0 && _childs.Length > 0)
					_childs.ToList().ForEach(c => this.children.Add(c));
			}
		}
		#endregion
	}
}
