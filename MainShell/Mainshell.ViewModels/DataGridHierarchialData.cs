﻿using Lib.Common;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using xtra_snd_tools.Modules.Playback.ViewModels;

namespace xtra_snd_tools.Mainshell.ViewModels
{
	/// <summary>
	/// Model für's Grid View
	/// </summary>
	public class DataGridHierarchialData : ObservableCollection<IHierarchic>, IHierarchicData
	{

		private readonly System.Windows.Threading.Dispatcher dispatcher;
		private readonly WeakReference<MvvmCross.ViewModels.IMvxViewModel> wrViewModelsContainer;
		private IHierarchicData This => this;

		public DataGridHierarchialData(System.Windows.Threading.Dispatcher dispatcher, WeakReference<MvvmCross.ViewModels.IMvxViewModel> wrViewModelsContainer) : base()
		{
			this.dispatcher = dispatcher;
			this.wrViewModelsContainer = wrViewModelsContainer;
			Initialize();
		}

		public IList<IHierarchic> RawData
		{
			get
			{
				return base.Items;
			}
		}

		private void Initialize()
		{
			wrViewModelsContainer.TryGetTarget(out var container);
			if ((container as IHomeViewModel) is null)
				throw new ArgumentException("ViewModelsContainerWeakReference_Target_InvalidType");

			var storageSource = (container as IHomeViewModel).TrackListViewModel;
			var list = new List<IHierarchic>();

			for (int i = 0; i < storageSource.Tracks.Count; i++)
			{
				var parentItem = new DataGridHierarchialDataModel(this)
				{
					Data = ((IStorageItem)((IMediaItem)storageSource.Tracks[i]).GetStorageItem()).Title,
					IsVisible = true
				};
				list.Add(parentItem);
				parentItem.AddChild(new DataGridHierarchialDataModel(this, new WeakReference<IHomeViewModel>(container as IHomeViewModel))
				{
					SelectedData = storageSource.Tracks[i],
					IsVisible = false,
					IsExpanded = true
				});
			}

			Clear();
			foreach (IHierarchic h in list.Where(c => c.IsVisible).SelectMany(x => new[] { x }.Concat((x as IHierarchic<IMediaItem>).GetVisibleDescendants())))
			{
				this.Add(h);
			}
		}

		public void AddChildrenToItems(IHierarchic parent)
		{
			if (!this.Contains(parent))
				return;
			int parentIndex = this.IndexOf(parent);
			foreach (IHierarchic c in (parent as IHierarchic<IMediaItem>).GetChildren())
			{
				//1) Nur unterbei einfügen, wenn [this] eine Playlist ist.
				if (c.Data is MediaItems p)
				{
					//2) Beginnend jew. b. ParentIndex
					int childIndex = parentIndex;
					foreach (var item in p.Entries)
					{
						//3) Kinder einfügen, mitzählen
						childIndex += 1;
						IHierarchic clone = c.DeepClone(item);
						this.Insert(childIndex, clone);
					}
					//4) ParentIndex um Anz. Kinder erhöhen.
					parentIndex += childIndex;
				}
			}
		}

		public void RemoveChildrenFromItems(IHierarchic d)
		{
			foreach (IHierarchic c in (d as IHierarchic<IMediaItem>).GetChildren())
			{
				if (this.Contains(c))
					this.Remove(c);
			}
		}
	}
}
