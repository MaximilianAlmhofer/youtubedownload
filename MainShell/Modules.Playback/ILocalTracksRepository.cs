﻿using Lib.Common;

using System.Threading.Tasks;

using xtra_snd_tools.Modules.Playback.ViewModels;

namespace Modules.Playback
{
	public interface ILocalTracksRepository
	{
		/// <summary>
		/// Initialize
		/// </summary>
		void LoadLazy(object owner);

		/// <summary>
		/// Schließen
		/// </summary>
		/// <returns></returns>
		void Close();

		/// <summary>
		/// Historyliste.
		/// </summary>

		PlaybackItemList Items { get; }

		/// <summary>
		/// Löschen asynchron mit callback
		/// </summary>
		/// <param name="item"></param>
		/// <param name="keepManifest"></param>
		/// <returns></returns>
		Task<StorageActionResult> DeleteAsync(bool keepManifest = false, params IStorageItem[] itemsToDelete);
	}
}