﻿using Lib.Common;
using Lib.Common.Message;
using Lib.Utility;

using Microsoft.Extensions.Options;

using MvvmCross.Binding.Extensions;
using MvvmCross.Plugin.Messenger;

using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Annotations;
using System.Windows.Annotations.Storage;
using System.Xml;

using xtra_snd_tools.Common.Contracts;
using xtra_snd_tools.Infra;
using xtra_snd_tools.Modules.Playback.ViewModels;

namespace Modules.Playback
{

	public sealed class LocalTracksRepository : ILocalTracksRepository, IMessagePublisher<ViewModelPropertyChangedMessage>
	{
		#region AsyncStorageHandler (internal)
		interface IAsyncStorageHandler
		{

			/// <summary>
			/// Wenn die GUI neu geladen werden soll.
			/// </summary>
			event EventHandler<RefreshEventArgs> RefreshNeeded;

			/// <summary>
			/// Immer wenn die UI komplett neu geladen werden soll.
			/// </summary>
			event EventHandler<AsyncCompletedEventArgs> LoadCompleted;

			/// <summary>
			/// E/A Fehler auf Dateisystemebene passiert.
			/// </summary>
			event EventHandler<ErrorEventArgs> Error;

			/// <summary>
			/// Die Verzeichnisstruktur und/oder Anzahl der Dateien hat sich geändert.
			/// </summary>
			event EventHandler<StoreContentChangedEventArgs> StoreContentChanged;

			/// <summary>
			/// Die Tracks
			/// </summary>
			IAsyncEnumerable<StorageItem> LoadTracksAsync(CancellationToken cancellation = default);

			/// <summary>
			/// Neu laden!
			/// </summary>
			IAsyncEnumerable<StorageItem> ReloadAsync(CancellationToken cancellation = default);

			/// <summary>
			/// 
			/// </summary>
			/// <param name="tracks"></param>
			/// <returns></returns>
			Task SaveChangesAsync(IEnumerable<StorageItem> tracks);

			/// <summary>
			/// Registriert Tasks zum Löschen der Datei.
			/// </summary>
			/// <param name="keepManifest">Zum behalten des Manifests.</param>
			/// <param name="storageItem">Die Datei die gelöscht werden soll.</param>
			/// <returns></returns>
			Task<StorageActionResult> DeleteAsync(bool keepManifest = false, CancellationToken cancellation = default, params IStorageItem[] storageItem);
		}

		class AsyncStorageHandler : IAsyncStorageHandler
		{
			internal readonly List<StorageItem> searchList = new List<StorageItem>();

			#region Eventhandlers
			public event EventHandler<AsyncCompletedEventArgs> LoadCompleted = delegate { };
			protected virtual void OnLoadLompleted(AsyncCompletedEventArgs e)
			{
				var handler = LoadCompleted;
				handler?.Invoke(this, e);
			}

			public event EventHandler<ErrorEventArgs> Error = delegate { };
			protected virtual void OnError(ErrorEventArgs e)
			{
				var handler = Error;
				handler?.Invoke(this, e);
			}

			public event EventHandler<StoreContentChangedEventArgs> StoreContentChanged = delegate { };
			protected virtual void OnStoreContentChanged(CancellationToken cancellation = default, params Tuple<StoreContentAction, FileInfo>[] changedItems)
			{
				var options = new ParallelOptions
				{
					MaxDegreeOfParallelism = Math.Max(2, (int)changedItems.Length / Math.Max(2, (int)(changedItems.Length % 3))),
					TaskScheduler = TaskScheduler.Default,
					CancellationToken = cancellation
				};
				Parallel.ForEach(changedItems, item =>
				{
					string name = StringFormatter.Format("{0} - {1}", item.Item1, item.Item2.Name);
					var typeName = new XmlQualifiedName(name, "xtra_snd_tools.Modules.Playback.ViewModels.Contract");
					var annotation = new Annotation(typeName,
						Guid.NewGuid(),
						item.Item2.CreationTime, item.Item2.LastWriteTime);
					var handler = StoreContentChanged;
					handler?.Invoke(this, new StoreContentChangedEventArgs(item.Item1, annotation));
				});
			}
			public event EventHandler<RefreshEventArgs> RefreshNeeded = delegate { };
			protected virtual void OnRefreshNeeded()
			{
				var handler = RefreshNeeded;
				handler?.Invoke(this, new RefreshEventArgs(this));
			}
			#endregion

			#region Operational methods
			public async IAsyncEnumerable<StorageItem> LoadTracksAsync([EnumeratorCancellation] CancellationToken cancellation = default)
			{
				Exception ex = null;
				await foreach (var track in __LoadAsyncStorageItems().WithCancellation(cancellation))
				{
					if (!searchList.Contains(track))
						searchList.Add(track);

					if (cancellation.IsCancellationRequested)
					{
						OnLoadLompleted(new AsyncCompletedEventArgs(null, true, searchList));
						yield break;
					}

					yield return track;
				}
				OnLoadLompleted(new AsyncCompletedEventArgs(ex, false, searchList));

				async IAsyncEnumerable<StorageItem> __LoadAsyncStorageItems()
				{
					foreach (var text in System.IO.Directory.EnumerateFiles(
						MvvmCross.Mvx.IoCProvider.Resolve<IOptions<GlobalConfig>>().Value.AppFolder.MetadataFolderPath, "*.json"))
					{
						StorageItem item = default;
						try
						{
							item = await MDSerializer.DeserializeJ<StorageItem>(text);
						}
						catch (Exception ex)
						{
							OnError(new ErrorEventArgs(ex));
							yield break;
						}
						yield return item;
					}
				}
			}

			public async IAsyncEnumerable<StorageItem> ReloadAsync([EnumeratorCancellation] CancellationToken cancellation = default)
			{
				List<StorageItem> tmp = new List<StorageItem>();

				// 1. Alle neuen finden und in die searchList einfügen.
				await foreach (var item in LoadTracksAsync().WithCancellation(cancellation))
				{
					if (cancellation.IsCancellationRequested)
					{
						OnLoadLompleted(new AsyncCompletedEventArgs(null, true, searchList));
						yield break;
					}
					tmp.Add(item);
				}
				if (cancellation.IsCancellationRequested)
				{
					OnError(new ErrorEventArgs(new OperationCanceledException(cancellation)));
					yield break;
				}

				Tuple<StoreContentAction, FileInfo>[] tuples = null;

				// 2. Elemente dazugekommen
				if (tmp.Count > searchList.Count)
				{
					var toAdd = tmp.Where(x => !searchList.Contains(x));
					searchList.AddRange(toAdd);

					tuples = new Tuple<StoreContentAction, FileInfo>[toAdd.Count()];

					for (int i = 0; i < toAdd.Count(); i++)
					{
						tuples[i] = Tuple.Create<StoreContentAction, FileInfo>(StoreContentAction.Added, new FileInfo(toAdd.ElementAt(i).FileLocation));
					}
				}
				// 3. Elemente entfernt
				else if (tmp.Count > 0 && tmp.Count < searchList.Count)
				{
					var except = searchList.Where(x => !tmp.Contains(x));
					searchList.RemoveAll(x => except.Contains(x));

					tuples = new Tuple<StoreContentAction, FileInfo>[except.Count()];
					for (int i = 0; i < except.Count(); i++)
					{
						tuples[i] = Tuple.Create<StoreContentAction, FileInfo>(StoreContentAction.Deleted, new FileInfo(except.ElementAt(i).FileLocation));
					}
				}
				// 4. Zurück streamen
				foreach (var item in searchList)
				{
					if (cancellation.IsCancellationRequested)
						yield break;
					yield return item;
				}
				OnStoreContentChanged(cancellation, tuples);
			}

			public async Task<StorageActionResult> DeleteAsync(bool keepManifest = false, CancellationToken cancellation = default, params IStorageItem[] storageItemsToDelete)
			{
				if (storageItemsToDelete is null || storageItemsToDelete.Length == 0)
					return StorageActionResult.Deleted;

				var tuples = new List<Tuple<StoreContentAction, FileInfo>>();

				var storageItemList = storageItemsToDelete.ToList();
				storageItemList.ForEach(x => x.IsBackupManifest = keepManifest);

				await DeleteAsync(cancellation, storageItemsToDelete);
				OnStoreContentChanged(cancellation, tuples.ToArray());

				return StorageActionResult.Deleted;
			}


			private Task DeleteAsync(CancellationToken cancellationToken = default, params IStorageItem[] storageItemsToDelete)
			{
				try
				{
					var deleteTasks = new List<Task>();

					storageItemsToDelete.ToList().ForEach(storageItem =>
					{
						deleteTasks.Add(Task.Run(() => System.IO.File.Delete(storageItem.FileLocation)).ContinueWith(_ =>
						{
							if (!storageItem.IsBackupManifest)
							{
								try
								{
									System.IO.File.Delete(storageItem.ManifestFile);
									storageItem = null;
								}
								catch (Exception ex)
								{
									ex.Trace();
								}
							}
						}));
					});
					return Task.WhenAll(deleteTasks);
				}
				finally
				{
					cancellationToken.TimeoutAfter(10000, () => ReloadAsync().WithCancellation(cancellationToken));
				}
			}

			public async Task SaveChangesAsync(IEnumerable<StorageItem> tracks)
			{
				foreach (var track in tracks)
				{
					await MDSerializer.SerializeJ(track, track.ManifestFile);
				}
				OnRefreshNeeded();
			}
			#endregion
		}
		#endregion

		#region Class member declarations
		private static readonly CancellationTokenSource cts = new CancellationTokenSource();
		private IAsyncStorageHandler storage = default;
		private readonly IMvxMessenger messenger;
		private Lazy<Task<PlaybackItemList>> historyLazyTask;
		private PlaybackItemList history = new PlaybackItemList();
		#endregion

		#region Contructor
		public LocalTracksRepository(IMvxMessenger messenger)
		{
			this.messenger = messenger;
			this.storage = new AsyncStorageHandler();
			this.storage.StoreContentChanged += Storage_StoreContentChanged;
			this.storage.RefreshNeeded += Storage_RefreshNeeded;
			this.storage.LoadCompleted += Storage_LoadCompleted;
			this.storage.Error += Storage_Error;
		}
		#endregion

		#region Properties
		public PlaybackItemList Items
		{
			get
			{
				return historyLazyTask?.Value.Result;
			}
		}

		private Lazy<Task<PlaybackItemList>> lazyLoaderTask(object owner)
		{
			var _owner = owner ??= history.FirstOrDefault()?.ViewModel;
			return new Lazy<Task<PlaybackItemList>>(async () =>
			{
				var list = new List<IStorageItem>();
				await foreach (var item in storage.LoadTracksAsync())
				{
					list.Add(item);
				}
				return new PlaybackItemList(list, _owner);
			});
		}

		#endregion

		#region AsyncStorageHandler callbacks
		private void Storage_Error(object sender, ErrorEventArgs e)
		{
			Trace.Write(e.GetException().ToLongString());
		}

		private void Storage_LoadCompleted(object sender, AsyncCompletedEventArgs e)
		{
			if (e.Error != null)
				e.Error.Trace();
		}

		private void Storage_RefreshNeeded(object sender, RefreshEventArgs e)
		{
			LoadLazy(null);
		}

		private void Storage_StoreContentChanged(object sender, StoreContentChangedEventArgs e)
		{

		}
		#endregion

		#region Methods
		public void LoadLazy(object owner)
		{
			if (historyLazyTask is null)
				historyLazyTask = lazyLoaderTask(owner);
		}


		public void Close()
		{
			cts.Dispose();
			storage = null;
			historyLazyTask = null;
		}

		public Task<StorageActionResult> DeleteAsync(bool keepManifest = false, params IStorageItem[] itemsToDelete)
		{
			return storage.DeleteAsync(keepManifest, cts.Token, itemsToDelete);
		}

		public void Publish(ViewModelPropertyChangedMessage message)
		{
			messenger.Publish(message);
		}
		#endregion
	}
}
