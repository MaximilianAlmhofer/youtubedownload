﻿
using Lib.Common;
using Lib.Common.Interfaces;

using MediaCore;

using Modules.Playback;

using MvvmCross.Base;

using NAudio.Wave;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;

using xtra_snd_tools.Common.Contracts.ViewModels;
using xtra_snd_tools.Modules.Playback.ViewModels;

namespace AudioLib.Playback
{
	public class PlaybackEngine : IPlaybackEngine, IDisposable
	{
		private static Action propertyChanged = null;

		private readonly IKeyedStorage<string> audioSources;

		private readonly ILocalTracksRepository repository;

		private static readonly object lockToken = new LockCookie();

		private static readonly Random random = new Random(DateTime.Now.Millisecond);

		private static readonly Debouncer debouncer = new Debouncer(DispatcherPriority.Input, Dispatcher.FromThread(Thread.CurrentThread));

		private readonly ConcurrentDictionary<string, Lazy<NAudioEngine>> enginesLazy = new ConcurrentDictionary<string, Lazy<NAudioEngine>>();

		public event EventHandler<PlaybackbackStateChangedEventArgs> PlaybackStateChanged = delegate { };

		public event EventHandler<PlaybackStoppedEventArgs> PlaybackStopped = delegate { };

		public event EventHandler<AmplitudeEventArgs> PeakAmplitudeCalculated = delegate { };

		public event EventHandler<PositionChangedEventArgs> PositionChanged = delegate { };

		public event EventHandler<PlaybackReadyEventArgs> PlaybackReady = delegate { };


		public PlaybackEngine(ILocalTracksRepository repository, IKeyedStorage<string> audioSources, Action propertyChangedCallback)
		{
			propertyChanged = propertyChangedCallback;
			this.repository = repository;
			this.audioSources = audioSources;
			Init();
		}

		private ICacheableKeyEmbedded<string> audioPlayback;
		public NAudioEngine AudioPlayback
		{
			get
			{
				return GetAudioPlayback();
			}
			private set
			{
				audioPlayback = value;
				propertyChanged();
			}
		}

		private NAudioEngine GetAudioPlayback()
		{
			if (audioPlayback is null)
			{
				InitAudioPlayback();
			}
			return (NAudioEngine)audioPlayback;
		}

		private IMediaItem mediaItem;
		public IMediaItem MediaItem
		{
			get
			{
				return mediaItem;
			}
			set
			{
				mediaItem = value;
				InitAudioPlayback();
				propertyChanged();
			}
		}

		public IPlaybackList Tracks
		{
			get => repository.Items;
		}

		private void InitAudioPlayback()
		{
			if (MediaItem?.Source != null)
			{
				if (enginesLazy.TryGetValue(mediaItem.GetStorageItem().Id, out var cacheable))
				{

					audioPlayback = (NAudioEngine)cacheable.Value;
					AudioPlayback.PositionChanged += (sender, e) => PositionChanged?.Invoke(sender, e);
					AudioPlayback.MaximumCalculated += (sender, e) => PeakAmplitudeCalculated?.Invoke(sender, e);
					AudioPlayback.PlaybackStateChanged += (sender, e) => OnPlaybackStateChanged(sender, e);
					PlaybackStopped += (sender, e) => PlaybackStopped?.Invoke(this, new PlaybackStoppedEventArgs(sender, e.Exception));
					propertyChanged();
				}
				else
				{
					Thread.Sleep(4400);
					InitAudioPlayback();
				}
			}
		}

		private void OnPlaybackStateChanged (object sender, PlaybackbackStateChangedEventArgs e)
		{
			if (e.Exception != null)
			{
				PlaybackStopped?.Invoke(this, new PlaybackStoppedEventArgs(sender, e.Exception));
			}
			else
			{
				switch (e.PreviousState)
				{
					case PlaybackState.Paused:
					case PlaybackState.Playing:
						if (e.State == PlaybackState.Stopped)
							OnPlaybackStopped(sender, e);
						break;
				}
			}
		}

		private void OnPlaybackStopped (object sender, PlaybackbackStateChangedEventArgs e)
		{
			AudioPlayback.PositionChanged -= delegate { };
			AudioPlayback.MaximumCalculated -= delegate { };
			AudioPlayback.PlaybackStateChanged -= delegate { };
			PlaybackStopped?.Invoke(this, new PlaybackStoppedEventArgs(AudioPlayback, null));

		}

		internal void LoadAudioPlayback()
		{
			if (MediaItem.Source != null)
			{
				try
				{
					AudioPlayback.Load(MediaItem.Source.LocalPath);
					PlaybackReady?.Invoke(this, new PlaybackReadyEventArgs(
						(long)AudioPlayback.GetCurrentTimeInSeconds(),
						(long)AudioPlayback.GetMaximum()));
				}
				catch (Exception ex)
				{
					MessageBox.Show("Datei Laden:\r\n" + ex.Message, ex.GetType().Name);
				}
			}
		}

		public void Init()
		{
			Dispatcher.FromThread(Thread.CurrentThread).InvokeAsync(() =>
			{
				(new Thread(new ThreadStart(delegate
				{

					foreach (IMediaItem item in Tracks)
					{
						Lazy<NAudioEngine> engineLazy = new Lazy<NAudioEngine>(() =>
						{
							var engine = NAudioEngine.Create(item.GetStorageItem(), PlaybackStopped);
							try
							{
								engine.Load(item.Source.LocalPath);
							}
							catch (Exception ex)
							{
								MessageBox.Show("Datei Laden:\r\n" + ex.Message, ex.GetType().Name);
							}
							return engine;
						});
						lock (Tracks)
						{
							var key = item.GetStorageItem().Id;
							lock (lockToken)
							{
								enginesLazy.AddOrUpdate(key, engineLazy,
								(_key, _newValue) => enginesLazy[_key] = _newValue);
							}
						}
					}
					mediaItem = Enumerable.FirstOrDefault<IMediaItem>(Tracks.Cast<IMediaItem>());
					if (
					Tracks.Count == 0)
						Tracks.Add(mediaItem);
				}))
				{
					IsBackground = true
				}).Start();
			}, DispatcherPriority.Background);
			mediaItem = Tracks?.Current;
		}

		public void SelectRandom()
		{
			try
			{
				MediaItem = Tracks.MoveToIndex(GetNextShuffleIndex());
			}
			catch (Exception ex)
			{
				ex.Trace();
				throw;
			}
		}

		private int GetNextShuffleIndex()
		{
			int currentIndex = Tracks.IndexOf(MediaItem);
			int nextIndex;
			do
			{
				nextIndex = random.Next(0, this.Tracks.Count);
			}
			while (nextIndex == currentIndex);

			return nextIndex;
		}

		public void Select(IMediaItem track)
		{
			PlaybackState? state = AudioPlayback?.PlaybackState;
			if (state != PlaybackState.Stopped)
			{
				Stop();
			}

			IMediaItem item = track as IMediaItem;

			if (track is IStorageItem)
			{
				item = new MediaItem((IStorageItem)track, this);
			}

			if (!Tracks.Contains(item))
			{
				Tracks.Add(item);
			}
			else
			{
				Tracks.MoveToIndex(Tracks.IndexOf(item));
			}

			SetCurrentItemInternal(Tracks.SelectItem(item));
			Play();
		}

		public void PrepareNextTrack(bool isShuffle)
		{
			if (isShuffle)
			{
				SelectRandom();
			}
			else
			{
				MediaItem = Tracks.MoveNext();
			}
		}

		private void SetCurrentItemInternal(IMediaItem _mediaItem)
		{
			if (_mediaItem is null)
				return;

			if (!audioSources.TryGet(_mediaItem.GetStorageItem().Id, out audioPlayback))
			{
				audioSources.Set(AudioPlayback);
				MediaItem = _mediaItem;
				return;
			}
			mediaItem = _mediaItem;
			propertyChanged();
		}

		#region IPlaybackManager
		public bool IsMuted
		{
			get
			{
				return (AudioPlayback?.Volume == 0) && (AudioPlayback?.IsPlaying ?? false);
			}
		}

		public void Pause()
		{
			((IPlaybackManager)this).Pause();
		}

		public void Play()
		{
			((IPlaybackManager)this).Play();
		}

		public void Stop()
		{
			((IPlaybackManager)this).Stop();
		}

		public void NextTrack(bool isShuffle)
		{
			((IPlaybackManager)this).NextTrack(isShuffle);
		}

		public void PreviousTrack(bool isShuffle)
		{
			((IPlaybackManager)this).PreviousTrack(isShuffle);
		}

		public void SelectTrack(object item)
		{
			((IPlaybackManager)this).SelectTrack(item);
		}

		public void SelectTracks(IEnumerable<object> items)
		{
			((IPlaybackManager)this).SelectTracks(items);
		}

		void IPlaybackManager.Play()
		{
			if (AudioPlayback != null)
			{
				if (!AudioPlayback.IsLoaded)
				{
					LoadAudioPlayback();
				}
				AudioPlayback.Play();
			}
		}

		void IPlaybackManager.Pause()
		{
			AudioPlayback.Pause();
		}

		void IPlaybackManager.Stop()
		{
			if (AudioPlayback.PlaybackState != PlaybackState.Stopped)
			{
				AudioPlayback.Stop();
			}
		}

		void IPlaybackManager.NextTrack(bool isShuffle)
		{
			if (Tracks.Count > 0)
			{
				PrepareNextTrack(isShuffle);

				((IPlaybackManager)this).Stop();

				SetCurrentItemInternal(Tracks.MoveNext());
				propertyChanged();

				((IPlaybackManager)this).Play();
			}
		}

		void IPlaybackManager.PreviousTrack(bool isShuffle)
		{
			if (Tracks.Count > 0)
			{
				if (isShuffle)
					PrepareNextTrack(true);

				((IPlaybackManager)this).Stop();

				SetCurrentItemInternal(Tracks.MovePrevious());
				propertyChanged();

				((IPlaybackManager)this).Play();
			}
		}

		void IPlaybackManager.SelectTrack(object item)
		{
			Select((IMediaItem)item);
		}

		void IPlaybackManager.SelectTracks(IEnumerable<object> items)
		{
			foreach (var item in items.OfType<IMediaItem>())
			{
				((IPlaybackManager)this).SelectTrack(item);
			}
		}

		public void Dispose()
		{
			foreach (var eng in enginesLazy)
			{
				eng.Value.DisposeIfDisposable();
			}
			audioSources.DisposeIfDisposable();
			repository.DisposeIfDisposable();
		}
		#endregion
	}
}
