﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Windows;
using System.Windows.Resources;
using System.Xml;

using EnvDTE;

using Lib.Common;
using Lib.Utility.Abstract;

using MvvmCross.Base;
using MvvmCross.Core;

namespace xtra_snd_tools.ServiceHost
{
	internal sealed class DotnetServiceHost
	{
		const uint NORMAL_PRIORITY_CLASS = 0x0020;

		internal struct RunningProcessHandle : ICacheableKeyEmbedded<string>, IDisposable
		{
			
			public string Key => Assembly?.FullName;

			public Assembly Assembly { get; private set; }

			public STARTUPINFO pStartup { get; private set; }

			public PROCESS_INFORMATION procInfo { get; private set; }

			public SECURITY_ATTRIBUTES pSecAttr { get; private set; }

			public SECURITY_ATTRIBUTES tSecAttr { get; private set; }

			public uint uPriority { get; }

			public bool Invalid()
			{
				return procInfo.hProcess == IntPtr.Zero
					|| procInfo.hProcess != IntPtr.Zero && (ReferenceEquals(pSecAttr, default(SECURITY_ATTRIBUTES)) || ReferenceEquals(tSecAttr, default(SECURITY_ATTRIBUTES)));
			}

			internal RunningProcessHandle(Assembly assembly, SECURITY_ATTRIBUTES pSecAttr = default, SECURITY_ATTRIBUTES tSecAttr = default, STARTUPINFO pStartup = default, PROCESS_INFORMATION procInfo = default, uint priority = NORMAL_PRIORITY_CLASS)
			{
				Ensure.NotNull(assembly);
				Ensure.IsTrue(priority % 2 == 0);
				Assembly = assembly;
				this.uPriority = priority;
				this.pSecAttr = pSecAttr;
				this.tSecAttr = tSecAttr;
				this.pStartup = pStartup;
				this.procInfo = procInfo;
			}

			public static RunningProcessHandle InvalidHandle => new RunningProcessHandle();

			public void Dispose()
			{
				pStartup = default;
				procInfo = default;
				pSecAttr = default;
				tSecAttr = default;
				Assembly = null;
			}
		}

		private static string executable => Path.Combine(Environment.GetEnvironmentVariable("programfiles"), @"dotnet\dotnet.exe");
		private static readonly IDictionary<string, RunningProcessHandle> instances = new Dictionary<string, RunningProcessHandle>();
		private static DotnetServiceHost instance;

		private DotnetServiceHost()
		{
		}

		public static DotnetServiceHost Instance => instance ??= new DotnetServiceHost();


		public void Run(Assembly assembly)
		{
			Ensure.NotNull(assembly);
			if (!IsLoaded(assembly))
			{
				var runningProcess = CreateProcessEx(assembly);
				instances.Add(runningProcess.Key, runningProcess);
			}
		}

		public void Terminate(Assembly assembly)
		{
			Ensure.NotNull(assembly);
			if (IsLoaded(assembly))
			{
				var runningProcess = instances[assembly.FullName];
				if (!TerminateProcess(runningProcess.procInfo.hProcess, 0))
				{
					ThrowCannotTerminateProcess(runningProcess);
				}
				instances.Remove(assembly.FullName);
			}
		}


		private InvalidProgramException ThrowCannotTerminateProcess(RunningProcessHandle runningProcess)
		{
			StringBuilder sb = new StringBuilder($"Cannot terminate process {runningProcess.Assembly.FullName}")
						.AppendFormat("ProcessID: [{0}]", runningProcess.procInfo.dwProcessId)
						.AppendFormat("Process handle: [{0}]", runningProcess.procInfo.hProcess)
						.AppendFormat("ThreadID: [{0}]", runningProcess.procInfo.dwThreadId)
						.AppendFormat("Thread handle: [{0}]", runningProcess.procInfo.hThread)
						.AppendFormat("Priority: [{0}]", runningProcess.uPriority);
			throw new InvalidProgramException(sb.ToString());
		}

		public void FailFast(uint uExitCode)
		{
			for (int i = 0; i < instances.Count; i++)
			{
				TerminateProcess(
					instances.ElementAt(i).Value.procInfo.hProcess, 
					uExitCode);
			}
			instances.Clear();
		}

		private bool IsLoaded(Assembly assembly)
		{
			return instances.ContainsKey(assembly.FullName);
		}


		private RunningProcessHandle CreateProcessEx(Assembly assembly, uint priority = NORMAL_PRIORITY_CLASS)
		{
			RunningProcessHandle pHandle = RunningProcessHandle.InvalidHandle;
			var pSecAttr = new SECURITY_ATTRIBUTES() { bInheritHandle = 1 };
			pSecAttr.nLength = Marshal.SizeOf(pSecAttr);
			var tSecAttr = new SECURITY_ATTRIBUTES() { bInheritHandle = 0 };
			tSecAttr.nLength = Marshal.SizeOf(tSecAttr);
			STARTUPINFO pStartup = new STARTUPINFO() { wShowWindow = 1, dwX = 10, dwY = 10, dwXSize = 100, dwYSize = 50, lpTitle = "dotnet - server-x" };
			PROCESS_INFORMATION procInfo;
			if (CreateProcess(assembly.Location.Replace("dll", "exe"), null,
				ref pSecAttr, ref tSecAttr, true, priority, IntPtr.Zero, System.IO.Path.GetDirectoryName(assembly.Location),
				ref pStartup, out procInfo))
			{
				pHandle = new RunningProcessHandle(assembly, pSecAttr, tSecAttr, pStartup, procInfo, priority);
			}
			return pHandle;
		}

		private bool TerminateProcessEx(IntPtr hProcess, uint uExitCode)
		{
			return TerminateProcess(hProcess, uExitCode);
		}


		[DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool CreateProcess(
		   string lpApplicationName,
		   string lpCommandLine,
		   ref SECURITY_ATTRIBUTES lpProcessAttributes,
		   ref SECURITY_ATTRIBUTES lpThreadAttributes,
		   bool bInheritHandles,
		   uint dwCreationFlags,
		   IntPtr lpEnvironment,
		   string lpCurrentDirectory,
		   [In] ref STARTUPINFO lpStartupInfo,
		   out PROCESS_INFORMATION lpProcessInformation);

		[DllImport("kernel32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool TerminateProcess(IntPtr hProcess, uint uExitCode);
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct SECURITY_ATTRIBUTES
	{
		public int nLength;
		public IntPtr lpSecurityDescriptor;
		public int bInheritHandle;
	}

	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	internal struct STARTUPINFO
	{
		public Int32 cb;
		public string lpReserved;
		public string lpDesktop;
		public string lpTitle;
		public Int32 dwX;
		public Int32 dwY;
		public Int32 dwXSize;
		public Int32 dwYSize;
		public Int32 dwXCountChars;
		public Int32 dwYCountChars;
		public Int32 dwFillAttribute;
		public Int32 dwFlags;
		public Int16 wShowWindow;
		public Int16 cbReserved2;
		public IntPtr lpReserved2;
		public IntPtr hStdInput;
		public IntPtr hStdOutput;
		public IntPtr hStdError;
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct PROCESS_INFORMATION
	{
		public IntPtr hProcess;
		public IntPtr hThread;
		public int dwProcessId;
		public int dwThreadId;
	}
}
