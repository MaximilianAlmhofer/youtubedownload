﻿using System;
using System.Runtime.InteropServices;

namespace xtra_snd_tools
{
	public interface IWin32Window : System.Windows.Interop.IWin32Window, System.Windows.Forms.IWin32Window
	{
	}

	public interface IWin32Component
	{
		IWin32Window Owner { get; }

		IntPtr Handle { get; }
	}


	[ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("6d5140c1-7436-11ce-8034-00aa006009fa")]
	internal interface IServiceProvider
	{
		[return: MarshalAs(UnmanagedType.IUnknown)]
		object QueryService(ref Guid guidService, ref Guid riid);
	}
}
