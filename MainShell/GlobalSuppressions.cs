﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1303:Literale nicht als lokalisierte Parameter übergeben", Justification = "None", Scope = "member", Target = "~M:xtra_snd_tools.App.OnStartup(System.Windows.StartupEventArgs)")]
[assembly: SuppressMessage("Reliability", "CA2007:Aufruf von \"ConfigureAwait\" für erwarteten Task erwägen", Justification = "None", Scope = "member", Target = "~M:xtra_snd_tools.Views.LoginPrompt.btnLogin_Click(System.Object,System.Windows.RoutedEventArgs)")]
