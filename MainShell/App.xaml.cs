﻿using AudioLib.Playback;

using Lib.Common;
using Lib.Common.Interfaces;
using Lib.Utility;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

using Modules.Playback;
using MvvmCross;
using MvvmCross.Core;
using MvvmCross.IoC;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.Platforms.Wpf.Core;
using MvvmCross.Platforms.Wpf.Presenters;
using MvvmCross.Plugin.Messenger;
using MvvmCross.ViewModels;

using Settings;

using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Interop;

using xtra_snd_tools.Infra.Serialization;
using xtra_snd_tools.Infra.Services;
using xtra_snd_tools.Infra.Services.Interfaces;
using xtra_snd_tools.Mainshell.Settings.ViewModels;
using xtra_snd_tools.Mainshell.ViewModels;
using xtra_snd_tools.Modules.Downloader.ViewModels;
using xtra_snd_tools.Modules.Playback.ViewModels;
using xtra_snd_tools.Modules.PlaylistEditor.ViewModels;

using MaterialDesignThemes.Wpf;
using MaterialDesignColors;

using MvxApplication = MvvmCross.Platforms.Wpf.Views.MvxApplication;
using System.Windows.Media;
using xtra_snd_tools.Mainshell.Controls;
using ShowMeTheXAML;
using xtra_snd_tools.Mainshell.Dialogs;
using xtra_snd_tools.ExceptionHandling;
using System.Windows.Markup;
using Lib.Utility.Extensions;
using ICSharpCode.AvalonEdit.Editing;
using Lib.Utility.Logging;
using Lib.Utility.Logging.Common;
using Lib.Utility.Logging.Configuration;
using Lib.Utility.Threading;
using ConfigurationBuilder = Lib.Utility.Logging.Configuration.LoggerConfigurationBuilder;
using IoCC = MvvmCross.Mvx;
using grpc.client.lib.ApiHandlers;
using xtra_snd_tools.ServiceHost;
using System.Reflection;

namespace xtra_snd_tools
{
	/// <summary>
	/// Interaktionslogik für "App.xaml"
	/// </summary>
	public partial class App : MvxApplication
	{
		private DialogViewModel dialog = new DialogViewModel();
		private CancellationHandle cancellationHandle = new CancellationHandle();
		private ILoggerConfiguration _loggerConfiguration;
		private GlobalConfig config;
		
		protected override void OnStartup(StartupEventArgs e)
		{
			//DotnetServiceHost.Instance.Run(Assembly.LoadFile(Path.GetFullPath(@"W:\.NET Core\3.0\web\server-x\bin\debug\netcoreapp3.1\server-x.dll")));
			IoC.Bootstrap();
			XamlDisplay.Init();
			UnhandledExceptionService.Setup();
			
			base.OnStartup(e);
#if !DEBUG

			if (!ShellFunctions.ValidateFeaturesEnabled(out var disabledFeatures))
			{
				StringBuilder sb = new StringBuilder();

				foreach (var feature in disabledFeatures)
				{
					sb.AppendFormat(CultureInfo.InvariantCulture, "{0}", feature.ToString());
				}

				MessageBox.Show(sb.ToString(), "Features manuell installieren!");
				
				//Process.Start("pwsh.exe", Environment.UserName,  Environment.UserDomainName)
			}

			if (Debugger.IsAttached)
			{
				if (!ShellFunctions.IsElevated())
				{
					MessageBox.Show("Erhöhte Rechte erfprderlich!", "Setup");
					Shutdown();
				}

				if (!ShellFunctions.EnsureRequiredFeaturesPs())
				{
					MessageBox.Show("Features manuell installieren!", "Setup");
					Shutdown();
				}

			#region Check Youtube-dl
				try
				{
					if (!ShellFunctions.RunShellCommand(CommandBuilder.GetYoutubeDlVersionCommand()))
					{
						if (!ShellFunctions.RunShellCommand(CommandBuilder.GetInstallYoutubeDlCommand()))
						{
							MessageBox.Show("/usr/bin/youtube-dl muss manuell installiert werden.");
							Shutdown();
						}
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show($"/usr/bin/youtube-dl muss manuell installiert werden.\r\n{ex.Message}");
					Shutdown();
				}
			#endregion
			}
#endif

			var file = new FileInfo(@"M:\Sound\Die_rzte_Junge.mp3");
			var result = file.Open(FileAccess.Read);
			if (result.Success)
			{
				Lib.Utility.Abstract.Result<FileStream> result1 = (Lib.Utility.Abstract.Result<FileStream>)result;
				using (result1.Value)
				{
					result1.Value.Close();
				}
			}

			if (Config.AppFolder is null || string.IsNullOrEmpty(Config.AppFolder.MetadataFolderPath))
			{
				FormSettings.ModalDialog(Config, App.Current.AppSettings);
			}
		}

		protected override void OnExit(ExitEventArgs e)
		{
			try
			{
				App.ConfigurationLoader.SaveLocationChanges();
			}
			finally
			{
				base.OnExit(e);
			}
		}


		protected override void RegisterSetup()
		{
			this.RegisterSetupType<MvxWpfSetup<xtra_snd_tools.Infra.AppSetup.App>>();
		}


		internal static new App Current => (App)Application.Current;

		public static IWin32Window Win32Window => (Shell)App.MainWindow;

		public CancellationHandle CancellationHandle => cancellationHandle ??= new CancellationHandle();

		public static new Shell MainWindow
		{
			get
			{
				App.Current.Dispatcher.VerifyAccess();
				return (Shell)Application.Current.MainWindow;
			}
			set
			{
				App.Current.Dispatcher.VerifyAccess();
				if (value != App.MainWindow)
				{
					App.MainWindow = value;
				}
			}
		}


		internal IConfigurationRoot AppSettings
		{
			get => Globals.AppSettings;
			private set => Globals.AppSettings = value;
		}


		internal GlobalConfig Config
		{
			get
			{
				if (config is null)
				{
					config = MvvmCross.Mvx.IoCProvider.Resolve<IOptions<GlobalConfig>>().Value;
				}
				return config;
			}
		}

		internal ILoggerConfiguration LoggerConfiguration
		{
			get
			{
				return _loggerConfiguration ??= MvvmCross.Mvx.IoCProvider.Resolve<ILoggerConfiguration>();
			}
		}


		#region ConfigurationLoader : IOptionsFactory<TOptions>
		private sealed class ConfigurationLoader : IOptionsFactory<GlobalConfig>
		{
			public ConfigurationLoader()
			{
				Initialize();
				Load();
			}


			private static int recursionDepth = 0;


			private GlobalConfig Config { get; set; }


			private FileConfigurationProvider FileConfigProvider => App.Current.AppSettings.Providers.OfType<FileConfigurationProvider>().FirstOrDefault();


			private void Initialize()
			{
				var builder = new Microsoft.Extensions.Configuration.ConfigurationBuilder()
					.SetBasePath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Properties"))
					.AddJsonFile($"appsettings.json", optional: false, reloadOnChange: true);
				App.Current.AppSettings = builder.Build();
				FileConfigProvider.Source.OnLoadException = ctx =>
				{
					ctx.Ignore = false;
					ctx.Exception = new FileLoadException("Configuration Load Error!");
					ctx.Provider = FileConfigProvider;
				};
			}


			private void Load()
			{
				#region Config Load Or Shutdown
				try
				{
					LoadInitialConfigRaw();
				}
				catch (Exception ex)
				{
					MessageBox.Show(Strings.ContentAppSettingsError + ex.Message,
						$"{nameof(Load)} fehlgeschlagen!");

					Application.Current.Shutdown();
				}
				#endregion
				#region Config Deserialisieren
				try
				{
					MDSerializer.Init(Config);
				}
				catch (Exception ex)
				{
					if (MessageBox.Show(ex.Message, ex.GetType().Name, MessageBoxButton.OKCancel, MessageBoxImage.Information) == MessageBoxResult.Cancel)
					{
						Application.Current.Shutdown();
					}
				}
				#endregion
			}


			private void LoadInitialConfigRaw()
			{
				try
				{

					FileInfo fi = new FileInfo(App.Current.AppSettings.GetSection("Configuration").Value);
					if (!fi.Exists)
					{
						using (File.Create(fi.FullName))
						{
							;
						}
					}
					try
					{
						var content = File.ReadAllText(fi.FullName);
						if (!string.IsNullOrEmpty(content))
						{
							Config = MvxJsonConvert.DeserializeObject<GlobalConfig>(content);
						}
					}
					catch (Exception ex)
					{
						MessageBox.Show($"bei {nameof(ConfigurationLoader.LoadInitialConfigRaw)}:\r\n" + ex.Message, ex.GetType().Name);
						App.Current.Shutdown();
					}
				}
				catch (Exception ex)
				{

					if (MessageBox.Show(string.Format(Strings.IncorrectConfigurationWarning, ex.Message), "Fehler",
						MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
					{
#if DEBUG
						if (!Debugger.IsAttached)
						{
							Debugger.Launch();
						}
#endif
						if (recursionDepth < 3)
						{
							HandleChangeConfigurationFile();
						}
					}
					if (recursionDepth == 3)
					{
						Application.Current.Shutdown();
					}

					recursionDepth = 0;
				}
			}


			private void HandleChangeConfigurationFile()
			{
				recursionDepth++;
				Debug.Assert(File.Exists(FileConfigProvider.Source.Path), "ASSERT: ConfigurationSource.Path exists");
				FileConfigProvider.GetReloadToken().RegisterChangeCallback(delegate (object state)
				{
					LoadInitialConfigRaw();

				}, App.Current.AppSettings);
				Process.Start(@"C:\WINDOWS\System32\notepad.exe", FileConfigProvider.Source.Path).WaitForExit();
				App.Current.AppSettings.Reload();
			}


			public GlobalConfig Create(string name)
			{
				return Config;
			}


			public static void SaveLocationChanges()
			{
				if (!string.IsNullOrEmpty(Current.Config.IndexDat))
				{
					MDSerializer.SerializeB(Current.Config.IndexDat);
				}
			}
		}
		#endregion

		#region Bootstrapper
		class IoC
		{
			
			internal static void Bootstrap()
			{
				MvxIoCProvider.Initialize();

				Mvx.IoCProvider.RegisterType<IMvxWpfViewPresenter>(() => new MvxWpfViewPresenter(MainWindow));
				Mvx.IoCProvider.RegisterType(() => Options.Create(new OptionsManager<GlobalConfig>(new ConfigurationLoader()).Value));
				Mvx.IoCProvider.ConstructAndRegisterSingleton<IAuthenticationService, AuthenticationService>();
				Mvx.IoCProvider.ConstructAndRegisterSingleton<IMvxNavigationCache, MvxNavigationCache>();
				Mvx.IoCProvider.RegisterSingleton<IMvxNavigationService>(() =>
						new MvxNavigationService(Mvx.IoCProvider.Resolve<IMvxNavigationCache>(),
						new MvxViewModelLoader(Infra.AppSetup.Instance.CoreApp)));

				Mvx.IoCProvider.RegisterSingleton(HttpClientSingleton.Instance(App.Current.cancellationHandle.Token));
				Mvx.IoCProvider.RegisterSingleton<ILoggerConfiguration>(() => 
						App.Current._loggerConfiguration = new LoggerConfigurationBuilder()
														.WithPrefix(() => "xtra_")
														.WithDirectory(() => AppDomain.CurrentDomain.BaseDirectory)
														.Build());

				Mvx.IoCProvider.RegisterType<ILoggerFactory, ILoggerConfiguration>(config => new NLogLoggerFactory(config));
				Mvx.IoCProvider.RegisterSingleton<ILocalTracksRepository>(
														() => new LocalTracksRepository(Mvx.IoCProvider.Resolve<IMvxMessenger>()));

				Mvx.IoCProvider.RegisterType<ITrackListViewModel, IMvxMessenger, ILocalTracksRepository>(
														(msg, rep) => new Modules.Playback.ViewModels.TrackListViewModel(msg, rep));

				Mvx.IoCProvider.RegisterType<IKeyedStorage<string>, ILoggerFactory>(factory => new SelfContainedKeyStorage<string, NAudioEngine>(factory));

				Mvx.IoCProvider.RegisterType<IAudioSettingsViewModel, AudioSettingsViewModel>();

				Mvx.IoCProvider.RegisterType<IPlayerViewModel, IMvxMessenger, ILocalTracksRepository, ILoggerFactory>((msg, rep, factory) => new PlayerViewModel(msg, rep, factory));

				Mvx.IoCProvider.RegisterType<IDownloadViewModel, IDownloadItem, IMvxMessenger, ILoggerFactory>(
					(dLitem, msg, log) => new DownloadViewModel(dLitem, msg, log));

				Mvx.IoCProvider.RegisterType<ILoginViewModel, LoginViewModel>();

				Mvx.IoCProvider.RegisterSingleton<IMainWindowViewModel>(new MainWindowViewModel());

				Mvx.IoCProvider.RegisterType<IHomeViewModel, IMainWindowViewModel>(mvm => new HomeViewModel(mvm));

				Mvx.IoCProvider.RegisterType<IHierarchic, DataGridHierarchialDataModel>();

				Mvx.IoCProvider.RegisterType<IHierarchicData, DataGridHierarchialData>();

				Mvx.IoCProvider.RegisterType<IPlaybackControlPanelViewModel, IMainWindowViewModel>(mvm => new PlaybackControlPanelViewModel(mvm));

				Mvx.IoCProvider.RegisterType<IPlaylistEditorViewModel, IMvxMessenger, IMainWindowViewModel>(
						(msg, mvm) => new PlaylistEditorViewModel(msg, mvm, Mvx.IoCProvider.Resolve<IOptions<GlobalConfig>>()));
			}
		}
		#endregion
	}
}
