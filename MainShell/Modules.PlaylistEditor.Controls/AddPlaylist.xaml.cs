﻿using MvvmCross.ViewModels;

using System.Windows;

namespace xtra_snd_tools.Modules.PlaylistEditor.Controls
{
	public partial class AddPlaylist : MvvmCross.Platforms.Wpf.Views.MvxWpfView
	{
		public AddPlaylist() : base()
		{
			InitializeComponent();
		}

		public static readonly DependencyProperty ViewModelBindingProperty = DependencyProperty.RegisterAttached("ViewModelBinding", typeof(IMvxViewModel), typeof(AddPlaylist));
		public IMvxViewModel ViewModelBinding
		{
			get => this.ViewModel;
			set => this.ViewModel = value;
		}
		public static void SetViewModelBinding(UIElement element, IMvxViewModel value)
		{
			element.SetValue(ViewModelBindingProperty, value);
		}

		public static IMvxViewModel GetViewModelBinding(UIElement element)
		{
			return (IMvxViewModel)element.GetValue(ViewModelBindingProperty);
		}
	}
}
