﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace xtra_snd_tools.Modules.PlaylistEditor.Controls
{
	[TemplateVisualState(GroupName = VisualStateGroupName, Name = NormalState)]
	[TemplateVisualState(GroupName = VisualStateGroupName, Name = EditingState)]
	[TemplatePart(Name = EditPresenterPartname, Type = typeof(ContentPresenter))]
	public class PlaylistEdit : ContentControl
	{
		public const string EditPresenterPartname = "PART_CPEdit";
		public const string VisualStateGroupName = "PlaylistEditVisualStates";
		public const string NormalState = "Normal";
		public const string EditingState = "Editing";

		private Label editLabel;
		private ContentPresenter cpEdit;

		static PlaylistEdit()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(PlaylistEdit), new FrameworkPropertyMetadata(typeof(PlaylistEdit)));
			ContentSourcePropertyKey = DependencyProperty.RegisterReadOnly(nameof(ContentSource), typeof(string), typeof(PlaylistEdit), new PropertyMetadata());
			ContentSourceProperty = ContentSourcePropertyKey.DependencyProperty;
		}

		public static readonly DependencyProperty ContentSourceProperty;
		public static readonly DependencyPropertyKey ContentSourcePropertyKey;

		public string ContentSource
		{
			get => (string)GetValue(ContentSourcePropertyKey.DependencyProperty);
			private set => SetValue(ContentSourcePropertyKey, value);
		}

		public override void OnApplyTemplate()
		{
			if ((cpEdit = base.GetTemplateChild(EditPresenterPartname) as ContentPresenter) != null)
			{
				DependencyPropertyDescriptor.FromProperty(ContentPresenter.ContentSourceProperty, typeof(ContentPresenter))
					.AddValueChanged(cpEdit, EditPresenterOnValueChanged);
				this.editLabel = cpEdit.ContentTemplate.LoadContent() as Label;
			}
			base.OnApplyTemplate();
		}

		private void EditPresenterOnValueChanged(object sender, EventArgs e)
		{

		}
	}
}
