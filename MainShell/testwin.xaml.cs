﻿using MvvmCross.Platforms.Wpf.Views;

using System;
using System.Windows;
using System.Windows.Controls;

namespace xtra_snd_tools
{
	/// <summary>
	/// Interaktionslogik für testwin.xaml
	/// </summary>
	public partial class testwin : MvvmCross.Platforms.Wpf.Views.MvxWindow
	{
		public testwin()
		{
			InitializeComponent();
			DataContext = this;
		}
		public FileFormats ImageEnum { get; set; }

		private void OnImageMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{

		}
	}

	/// <summary>
	/// Audio encoding formats
	/// </summary>
	public enum FileFormats
	{
		Aac,
		Aiff,
		Ape,
		Asf,
		Flac,
		Mpeg,
		Mpeg4,
		MusePack,
		NonContainer,
		Ogg,
		Riff,
		WavPack
	};
}
