﻿using Newtonsoft.Json;

namespace xtra_snd_tools.Modules.Downloader.ViewModels
{
	[JsonObject("downloader_options")]
	public class DownloaderOptions
	{
		[JsonProperty("http_chunk_size")]
		public long? HttpChunkSize { get; set; }
	}
}
