﻿using Newtonsoft.Json;

using System;

namespace xtra_snd_tools.Modules.Downloader.ViewModels
{
	[JsonObject]
	public class Thumbnail
	{
		public string Id { get; set; }

		public Uri Url { get; set; }
	}
}
