﻿using Newtonsoft.Json;

namespace xtra_snd_tools.Modules.Downloader.ViewModels
{
	[JsonObject("http_headers")]
	public class HttpHeaders
	{
		[JsonProperty("Accept-Charset")]
		public string AcceptCharset { get; set; }

		[JsonProperty("Accept-Language")]
		public string AcceptLang { get; set; }

		[JsonProperty("Accept-Encoding")]
		public string AcceptEnc { get; set; }

		[JsonProperty("Accept")]
		public string Accept { get; set; }

		[JsonProperty("User-Agent")]
		public string UserAgent { get; set; }
	}
}
