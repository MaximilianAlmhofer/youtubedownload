﻿using Lib.Common;

using Newtonsoft.Json;

using System;
using System.Globalization;
using System.Linq;

namespace xtra_snd_tools.Modules.Downloader.ViewModels
{
	[JsonObject("fragments")]
	public class Fragment
	{

		[JsonConstructor]
		public Fragment()
		{
			SetRange();
		}

		[JsonProperty("duration", NullValueHandling = NullValueHandling.Ignore)]
		public float? Duration { get; set; }

		[JsonProperty("path")]
		public string Path { get; set; }

		[JsonIgnore]
		public int Start { get; private set; }

		[JsonIgnore]
		public int End { get; private set; }

		private void SetRange()
		{
			if (!string.IsNullOrEmpty(Path))
			{
				try
				{
					int[] range = Path.Split('/')[1].Split('-').Select(x => Convert.ToInt32(x, NumberFormatInfo.InvariantInfo)).ToArray();
					Start = range[0];
					End = range[1];
				}
				catch (Exception ex)
				{
					ex.Trace();
				}
			}
		}
	}
}
