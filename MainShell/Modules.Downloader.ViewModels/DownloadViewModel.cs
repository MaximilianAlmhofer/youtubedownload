﻿using ICSharpCode.AvalonEdit.Editing;

using Lib.Common;
using Lib.Common.Message;
using Lib.Utility;
using Lib.Utility.Logging;

using Modules.Downloader;

using MvvmCross.Base;
using MvvmCross.Commands;
using MvvmCross.Logging;
using MvvmCross.Plugin.Messenger;
using MvvmCross.ViewModels;

using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

using Lib.Utility.Abstract;
using xtra_snd_tools.Common.Contracts;
using xtra_snd_tools.Common.Contracts.ViewModels;
using Lib.Utility.Threading;
using MvvmCross.Core;
using MvvmCross.Logging.LogProviders;

namespace xtra_snd_tools.Modules.Downloader.ViewModels
{
	public class DownloadViewModel : ViewModelBase<IAddSelf>, IDownloadViewModel
	{
		#region Class member declarations
		private bool wait = false;
		private IStorageItem _storageItem;
		private ILoggerFactory _loggerFactory;

		private Uri downloadSource;
		private DownloadTarget downloadTarget;
		internal DownloadEngine downloader;
		#endregion

		public DownloadViewModel(IDownloadItem owner, IMvxMessenger messenger, ILoggerFactory loggerFactory) : base(messenger)
		{
			Ensure.NotNull<ILoggerFactory>(loggerFactory);
			Ensure.NotNull<IDownloadItem>(owner);

			_loggerFactory = loggerFactory;
			Owner = owner;
			Owner.ActiveDownloads.Add(this);
			CancellationToken = App.Current.CancellationHandle.Token;
		}

		#region Overrides
		/// <summary>
		/// Startet den konfigurierten Download.
		/// </summary>
		public override void Start()
		{
			downloadTarget.Start(downloadSource);
		}

		/// <summary>
		/// Konfiguriert den Download mit dem PlayerViewModel.
		/// </summary>
		/// <param name="parameter"></param>
		public override void Prepare(IAddSelf parameter)
		{
		}

		#endregion

		#region Commands
		IMvxCommand progressCommand;
		/// <summary>
		/// Gets the progress command.
		/// </summary>
		/// <value>The progress command.</value>
		public IMvxCommand ProgressCommand
		{
			get
			{
				if (progressCommand is null)
				{
					progressCommand = new MvxCommand(() =>
					{
						if (!wait)
						{
							SuspendDownload();
							wait = true;
						}
						else
						{
							ContinueDownload();
							wait = false;
						}
					});
				}
				return progressCommand;
			}
		}
		#endregion

		#region Properties
		/// <summary>
		/// Download cancellation token.
		/// </summary>
		public CancellationToken CancellationToken { get; }

		/// <summary>
		/// This is actually a ViewModel bound to the UIElement that owns 
		/// the UIElement to which this ViewModel is bound to.
		/// <br/>MAYBE OBSOLETE.
		/// </summary>
		public IDownloadItem Owner { get; }

		/// <summary>
		/// StorageItem object
		/// </summary>
		public IStorageItem StorageItem
		{
			get => _storageItem;
			set
			{
				if (_storageItem != value)
				{
					_storageItem = value;
					RaisePropertyChanged();
				}
			}
		}

		/// <summary>
		/// URI of the storage file path
		/// </summary>
		public Uri StorageFilePath
		{
			get
			{
				Uri uri = null;
				if (_storageItem is StorageItem trackInfo)
				{
					string fileLocation = trackInfo.FileLocation;
					if (!File.Exists(fileLocation))
					{
						fileLocation = System.IO.Path.ChangeExtension(fileLocation, ".mp4");
					}
					if (!File.Exists(fileLocation))
					{
						Debug.WriteLine(fileLocation + "\r\nKANN NICHT GEFUNDEN WERDEN");
					}
					else
					{
						uri = new Uri(fileLocation, UriKind.Absolute);
					}
				}
				return uri;
			}
		}

		#region Download operation
		/// <summary>
		/// Download in Arbeit
		/// </summary>
		public bool IsDownloadInProgress
		{
			get
			{
				return Progress > 0 && (Progress < 100 || !CompletedSuccessFully);
			}
		}

		/// <summary>
		/// Erfolgreich abgeschlossen
		/// </summary>
		public bool CompletedSuccessFully { get; set; }


		private double progress = 0;
		/// <summary>
		/// Numerischer Fortschrittswert
		/// </summary>
		public int Progress
		{
			get
			{
				int val = 0;
				if ((int)progress > 0)
				{
					val = (int)progress;
				}
				return val;
			}
			set
			{
				progress = value;
				RaisePropertyChanged();
				RaisePropertyChanged(nameof(IsDownloadInProgress));
			}
		}

		private string percentProgress;
		/// <summary>
		/// PRozentueller Fortschrittstext
		/// </summary>
		public string PercentProgress
		{
			get => percentProgress;
			set
			{
				percentProgress = value;
				RaisePropertyChanged();
			}
		}

		private string dataRate;
		/// <summary>
		/// Datenratentext
		/// </summary>
		public string DataRate
		{
			get => dataRate;
			set
			{
				dataRate = value;
				RaisePropertyChanged();
			}
		}

		private string downloadSpeed;
		/// <summary>
		/// Downloadgeschwindigkeitstext
		/// </summary>
		public string DownloadSpeed
		{
			get => downloadSpeed;
			set
			{
				downloadSpeed = value;
				RaisePropertyChanged();
			}
		}

		private string downloadTime;
		/// <summary>
		/// Downloadzeittext
		/// </summary>
		public string DownloadTime
		{
			get => downloadTime;
			set
			{
				downloadTime = value;
				RaisePropertyChanged();
			}
		}

		#endregion

		#endregion

		#region Methods		
		/// <summary>
		/// Adds the downloaded item to the list of DownloadViewModels.
		/// </summary>
		/// <param name="selectedItem">The selected item.</param>
		/// <param name="raisePropertyChanged">if set to <c>true</c> [raise property changed].</param>
		/// <returns></returns>
		public string AddSelf(object selectedItem, bool raisePropertyChanged)
		{
			string title = "";
			if (selectedItem is StorageItem localTrackInfo)
			{
				title = localTrackInfo.Title;
				if (raisePropertyChanged)
					StorageItem = localTrackInfo;
				else
					_storageItem = localTrackInfo;
			}
			return title;
		}

		/// <summary>
		/// Start through DownloadTarget
		/// </summary>
		/// <param name="uri"></param>
		public void Start(Uri uri)
		{
			downloadSource = uri;
			downloadTarget = new DownloadTarget(this, _loggerFactory.Logger());
			Start();
		}

		/// <summary>
		/// Suspends the download.
		/// </summary>
		public void SuspendDownload()
		{
			downloader?.SuspendDownload();
		}

		/// <summary>
		/// Continues the download.
		/// </summary>
		public void ContinueDownload()
		{
			downloader?.ContinueDownload();
		}

		/// <summary>
		/// Publish a <see cref="NewDownloadMessage"/> message
		/// </summary>
		public void Publish(NewDownloadMessage message)
		{
			messenger.Publish(message);
		}

		/// <summary>
		/// Publish a <see cref="NewListDownloadMessage"/> message
		/// </summary>
		public void Publish(NewListDownloadMessage message)
		{
			messenger.Publish(message);
		}

		/// <summary>
		/// Publish a <see cref="LogMessage"/> message
		/// </summary>
		public void Publish(string logText, LogMessage.LogTarget logTarget)
		{
			var message = new LogMessage(this, logText, logTarget);
			messenger.Publish(message);
		}


		#region Download operation		
		/// <summary>
		/// Encapsulates the download target to abtract the thread call from the viewmodel instance.
		/// </summary>
		/// <seealso cref="MvxViewModel" />
		/// <seealso cref="ViewModels.Interfaces.IAddSelf" />
		internal class DownloadTarget : MvxViewModel, IAddSelf
		{
			private Uri downloadSource;
			private readonly ILogger _logger;
			private readonly StringBuilder sb = new StringBuilder();
			private readonly IAddSelf _caller;
			private CancellationHandle _cancellationHandle;
			private readonly DownloadViewModel _viewModel;

			/// <summary>
			/// Initializes a new instance of the <see cref="DownloadTarget"/> class.
			/// </summary>
			/// <param name="caller">The caller.</param>
			public DownloadTarget(IAddSelf caller, ILogger logger)
			{
				_caller = caller;
				_logger = logger;
				_viewModel = (DownloadViewModel)caller;
				_cancellationHandle = CancellationHandle.CreateLinkedTokenSource(App.Current.CancellationHandle, caller.CancellationToken);
			}


			public void Start(Uri uri)
			{
				downloadSource = uri;
				Start();
			}

			/// <summary>
			/// Starts the download operation on a worker thread.
			/// </summary>
			public override void Start()
			{

				base.Start();
				ThreadPool.QueueUserWorkItem(async delegate
				{
					if (downloadSource is null)
					{
						MessageBox.Show(StringFormatter.Format("Ungültige Url: {0}", downloadSource), "Drap-Drop-Fehler");
						return;
					}


					await AsyncDispatcher.ExecuteOnMainThreadAsync(async () =>
					{
						try
						{
							_viewModel.downloader = new DownloadEngine(
								MvvmCross.Mvx.IoCProvider.Resolve<IMvxMainThreadAsyncDispatcher>(), async 
								logLine =>
								{
									await Task.Run(() => WriteLine(logLine)).ContinueWith(async _ =>
									await RaisePropertyChanged(nameof(_viewModel.Owner)).ConfigureAwait(false),
									TaskScheduler.FromCurrentSynchronizationContext()).Unwrap();
								}, 
								PersistiereMetadatenAsync);
							await _viewModel.downloader.ExecuteAsync(downloadSource.OriginalString, (value, percent, data, speed, time) =>
							{
								ReportProgress(value, percent, data, speed, time);
							},
							currentItem =>
							{
								if (currentItem is IPlaylistInfo playlist)
									_viewModel.Publish(new NewListDownloadMessage(_viewModel, ((PlaylistInfo)playlist).Entries.Cast<IPlaylistTrackEntry>(), playlist));
								else
									_viewModel.Publish(new NewDownloadMessage(_viewModel, (IStorageItem)currentItem));

								RaisePropertyChanged(nameof(_viewModel.Owner));

							}, 
							() => _viewModel.CompletedSuccessFully = true, _cancellationHandle).ConfigureAwait(false);

						}
						finally
						{
							await RaiseAllPropertiesChanged().ConfigureAwait(false);
						}
					}).ConfigureAwait(false);
				});
			}


			string IAddSelf.AddSelf(object selectedItem, bool raisePropertyChanged)
			{
				string title = "";
				if (selectedItem is StorageItem localTrackInfo)
				{
					title = localTrackInfo.Title;
					if (raisePropertyChanged)
						_viewModel.StorageItem = localTrackInfo;
					else
						_viewModel._storageItem = localTrackInfo;
				}
				return title;
			}

			/// <summary>
			/// Gets the cancellation token.
			/// </summary>
			/// <value>
			/// The cancellation token.
			/// </value>
			public CancellationToken CancellationToken { get => _cancellationHandle.Token; }


			private void ReportProgress(int value, string perc, string data, string speed, string time)
			{
				_logger.Logged(() => SafeSetValues(value, perc, data, speed, time),
							   () => StringFormatter.Format($"progress: {value}\tpercentage: {perc}\r\ndata: {data}\r\ntime: {time}\tspeed: {speed}"));
			}


			private void SafeSetValues(int progress, string perc, string data, string speed, string time)
			{
				_viewModel.Progress = progress;
				_viewModel.PercentProgress = perc;
				if (string.IsNullOrEmpty(time))
				{
					time = speed;
					speed = "";
				}
				_viewModel.DataRate = data;
				_viewModel.DownloadTime = time;
				_viewModel.DownloadSpeed = speed;
			}


			private async Task PersistiereMetadatenAsync(Delegate handler, params object[] args)
			{
				await _logger.Logged<Task>(async () =>
				{
					string saveFilePath = (string)handler.DynamicInvoke(args);
					await RichtextBoxWriteLine(saveFilePath).ConfigureAwait(true);
					bool isList = (bool)args[5];
					string metadata;
					if (isList)
						metadata = ((PlaylistInfo)args[3]).ManifestFile;
					else
						metadata = ((StorageItem)args[3]).ManifestFile;

					await MDSerializer.SerializeJ(args[3], metadata).ConfigureAwait(true);
				}, $"Executing {nameof(PersistiereMetadatenAsync)}");
			}


			private Task RichtextBoxWriteLine(string line)
			{
				return Task.Run(() => WriteLine(line)).ContinueWith(_ => Task.Delay(150));
			}

			/// <summary>
			/// Writes the line.
			/// </summary>
			/// <param name="input">The input.</param>
			public void WriteLine(string input)
			{
				_logger.Logged(() =>
				{
					try
					{
						if (_logger.IsTraceEnabled())
							_logger.Trace(input);
						sb.AppendLine(input);
					}
					catch (Exception ex)
					{
						_logger.Error(ex);
					}
				}, $"Executing {nameof(WriteLine)}");
				
			}
		}
		#endregion
		#endregion
	}
}
