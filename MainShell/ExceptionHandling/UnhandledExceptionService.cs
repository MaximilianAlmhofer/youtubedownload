﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

using NLog.LayoutRenderers.Wrappers;

using xtra_snd_tools.Mainshell.Dialogs;

namespace xtra_snd_tools.ExceptionHandling
{
	public class UnhandledExceptionService
	{
        private static bool isSetup;
        private static UnhandledExceptionService instance;

		static UnhandledExceptionService()
		{
            instance = new UnhandledExceptionService();
		}

        public static void Setup()
        {                    
           if (!isSetup)
			{
                // Catch exceptions from all threads in the AppDomain.
                AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
                       instance.ShowUnhandledException(args.ExceptionObject as Exception, "AppDomain.CurrentDomain.UnhandledException", true);

                // Catch exceptions from each AppDomain that uses a task scheduler for async operations.
                TaskScheduler.UnobservedTaskException += (sender, args) =>
                    instance.ShowUnhandledException(args.Exception, "TaskScheduler.UnobservedTaskException", true);

                // Catch exceptions from a single specific UI dispatcher thread.
                Dispatcher.FromThread(Thread.CurrentThread).UnhandledException += (sender, args) =>
                {
                    if (!Debugger.IsAttached)
                    {
                        args.Handled = true;
                        instance.ShowUnhandledException(args.Exception, "Dispatcher.UnhandledException", true);
                    }
                };
                isSetup = true;
            }
        }


		private void ShowUnhandledException(Exception exception, string unhandledExceptionType, bool promptUserForShutdown)
		{
            var messageBoxTitle = $"Unexpected Error Occurred: {unhandledExceptionType}";
            var messageBoxMessage = $"The following exception occurred:\n\n{exception}";
            var messageBoxButtons = MessageBoxButton.OK;

            if (promptUserForShutdown)
            {
                messageBoxMessage += "\n\nNormally the app would die now. Should we let it die?";
                messageBoxButtons = MessageBoxButton.YesNo;
            }

            // Let the user decide if the app should die or not (if applicable).
            if (MessageBox.Show(messageBoxMessage, messageBoxTitle, messageBoxButtons) == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }
	}
}
