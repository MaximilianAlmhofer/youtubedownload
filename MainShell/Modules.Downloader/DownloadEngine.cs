﻿
using Lib.Command;
using Lib.Common;
using Lib.Utility;
using Lib.Utility.Threading;

using MaterialDesignColors.Recommended;

using MaterialDesignThemes.Wpf;

using Microsoft.Extensions.Options;

using MvvmCross.Base;

using Newtonsoft.Json;

using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using xtra_snd_tools.Common.Contracts;
using xtra_snd_tools.Mainshell.Dialogs;

namespace Modules.Downloader
{

	public delegate void ProgressReporter(
		int progress,
		string percentage,
		string dataMB,
		string rateKBs,
		string remainingTime);

	internal class DownloadEngine
	{
		private delegate string CreateMediaFileName(
			GlobalConfig config,
			string destFileName,
			INode downloadDest,
			ref object local,
			ref IDownloadSource model,
			bool isPlayList);

		private Func<string, Task> consolelog_Delegate = async delegate { await Task.CompletedTask; };
		private Func<Delegate, object[], Task> storetodisk_Delegate = async delegate { await Task.CompletedTask; lock (new object()) { Task.Delay(0); } };
		private ManualResetEventSlim mre;
		private readonly GlobalConfig config;
		private ManagedProcessCommand cmdObj;
		private IMvxMainThreadAsyncDispatcher dispatcher;

		public DownloadEngine(
			IMvxMainThreadAsyncDispatcher dispatcher,
			Func<string, Task> consolelog_callback,
			Func<Delegate, object[], Task> storetodisk_callback)
		{
			mre = new ManualResetEventSlim(false);
			this.dispatcher = dispatcher;
			config = MvvmCross.Mvx.IoCProvider.Resolve<IOptions<GlobalConfig>>().Value;
			consolelog_Delegate = consolelog_callback;
			storetodisk_Delegate = storetodisk_callback;
		}

		/// <summary>
		/// Suspends the download.
		/// </summary>
		public void SuspendDownload()
		{
			mre.Set();
		}

		/// <summary>
		/// Continues the download.
		/// </summary>
		public void ContinueDownload()
		{
			if (mre.IsSet)
			{
				mre.Wait();
			}
			mre.Reset();
		}


		/// <summary>
		/// Executes the download thread asynchronously.
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <param name="reportProgress_Delegate">The report progress delegate.</param>
		/// <param name="resultAvailable">The bind current.</param>
		/// <param name="completedCallback">The completion callback.</param>
		/// <param name="cancellation">The cancellation.</param>
		public async Task ExecuteAsync(string url, ProgressReporter reportProgress_Delegate, Action<object> resultAvailable, Action completedCallback, CancellationHandle cancellationHandle)
		{
			//CancellationTokenSource cts = new CancellationTokenSource(40000);
			Task delayedTask = Task.Delay(150000, cancellationHandle.Token);

			#region Download - IPC Aktion
			Uri droppedSource = default;
			CommandBuilder builder = default;
			IInstruction processCmd;

			try
			{
				droppedSource = new Uri(url);
				builder = CommandBuilder.CreateFromUrl(droppedSource, config);
				processCmd = builder.GetMetadataCommand(config.AppFolder.DownloadFolderPath);
			}
			catch (Exception ex)
			{
				ex.Trace();
				MessageBox.Show(ex.ToLongString(), ex.GetType().Name);
				return;
			}

			IPCRunner.Instance.ExecuteThread(new ManagedProcessCommand(processCmd, mre),
			ipcResponse =>
			{
				bool playlistStored = false;
				dynamic local = null;
				string json = ipcResponse?.Value?.ToString();
				if (string.IsNullOrEmpty(json))
				{
					dispatcher.ExecuteOnMainThreadAsync(async () =>
					await consolelog_Delegate(Strings.ManifestDownloadFehlgeschlagen +
					"\r\nFolgender Command: \r\n\r\n" + processCmd.Command), true);
					return;
				}
				if (json != null)
				{
					var dlFolder = config.AppFolder;
					var remote = CreateModel(json, dlFolder, builder is PlaylistCommandBuilder);
					cmdObj = new ManagedProcessCommand(builder.Build(remote), mre);

					IPCRunner.Instance.ExecuteThread(cmdObj,
					async state =>
					{

						#region Update GUI								
						if (state is IResult res && res.Value != null)
						{
							#region Download Zeilen
							if (res.Value.ToString().StartsWith(Strings.DownloadTag))
							{
								string downloadInfoText = res.Value.ToString().Replace(Strings.DownloadTag, "").TrimStart().TrimEnd();
								string perc, data, speed, time;
								double value;

								#region FileName aussafischen
								bool isList = downloadInfoText.StartsWith(Strings.Playlistname);
								if (downloadInfoText.StartsWith(Strings.Destination) || isList)
								{
									var @delegate = new CreateMediaFileName(ErzeugeDateiname);
									object[] args = new object[]
									{
										config,
										downloadInfoText.Replace(isList ? Strings.Playlistname : Strings.Destination, "").TrimStart().TrimEnd(),
										dlFolder.DownloadFolder,
										(object)local,
										remote,
										isList
									};

									if (!playlistStored)
									{
										if (remote is PlaylistInfo)
										{
											await storetodisk_Delegate(@delegate, args);
										}
										playlistStored = true;
									}
									if (!isList && !playlistStored)
									{
										await storetodisk_Delegate(@delegate, args);
									}

									if (args[3] is IStorageItem item)
									{
										local = (IStorageItem)args[3];
										await dispatcher.ExecuteOnMainThreadAsync(() => resultAvailable((IStorageItem)local));
									}
									else
									if (args[3] is IPlaylistInfo info)
									{
										local = (IPlaylistInfo)args[3];
										await dispatcher.ExecuteOnMainThreadAsync(() => resultAvailable((IPlaylistInfo)local));
									}

								}
								#endregion
								#region Fortschritt auswerten
								else if (char.IsDigit(downloadInfoText[0]))
								{
									var numericParts = downloadInfoText
										.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
										.Where(p => char.IsDigit(p[0]));

									// Für den Fall das das 1. Zeichen im Dateinamen numerisch ist
									// wird das Array numericParts nur 1 Element enthalten...
									if (numericParts.Count() > 1)
									{
										perc = numericParts.ElementAtOrDefault(0);
										data = numericParts.ElementAtOrDefault(1);
										speed = numericParts.ElementAtOrDefault(2);
										time = numericParts.ElementAtOrDefault(3);

										value = double.Parse(perc.TrimEnd(new char[] { '%' }),
												NumberStyles.Float, new NumberFormatInfo());

										reportProgress_Delegate((int)value, perc, data, speed, time);

										#region Return verzögern und Cancellation prüfen
										if (delayedTask.Status == TaskStatus.RanToCompletion)
										{
											await await delayedTask.ContinueWith(task =>
											{
												if (task.IsCanceled || cancellationHandle.Token.IsCancellationRequested)
												{
													task.ConfigureAwait(true);
													return Task.FromCanceled(cancellationHandle.Token);
												}
												return Task.Delay(1000, cancellationHandle.Token);
											}, cancellationHandle.Token);
										}
										#endregion
									}
								}
								#endregion
								#region File schon vorhanden
								else if (string.Equals(Strings.IsInArchive, downloadInfoText))
								{
									await dispatcher.ExecuteOnMainThreadAsync(() => 
									new DialogViewModel().RunExtendedDialogCommand.Execute("Der Track wurde schon heruntergeladen."));
									return;
								}
								#endregion
								else if (res is IErrorResult)
								{
									StringBuilder error = new StringBuilder(res.Value.ToString())
										.AppendLine(config.DownloadOptions.ToString());

									await consolelog_Delegate(error.ToString());
								}
							}
							#endregion
							#region Restlicher Output
							else
							{
								await consolelog_Delegate(res.Value.ToString());
							}
							#endregion
						}
						#endregion
					},
					async result =>
					{
						#region Reset GUI
						try
						{
							reportProgress_Delegate(0, "0.0 %", "00.0", "0.0 MiB", "00:00:00.000");
							await consolelog_Delegate(result.Value?.ToString());
						}
						catch (Exception ex)
						{
							ex.Trace();
							await consolelog_Delegate(ex.ToLongString());
						}
						finally
						{

							if (local is StorageItem storageItem)
							{
								Uri file = new Uri(storageItem.FileLocation);

								await consolelog_Delegate(string.Format("{0}\r\n\r\n{1}",
								Strings.StartDownloadHint, file?.AbsoluteUri));

								completedCallback();
								cancellationHandle.Token.ThrowIfCancellationRequested();
							}

							mre.Reset();
							mre.Dispose();
						}
						#endregion
					}, cancellationHandle.Token);
				}
			}, cancellationHandle.Token);

			#endregion

			await delayedTask;
		}


		private IDownloadSource CreateModel(string json, AppFolder appFolder, bool isPlaylist)
		{
			IDownloadSource model = CreateModelFromJson(json, ref appFolder, isPlaylist);
			if (model is null)
			{
				throw new InvalidManifestException(json, isPlaylist);
			}
			return model;
		}


		/// <summary>
		/// -- Delegate Target --
		/// Erzeugt abhängig vom Ty des Doiwnloads den Dateinamen des Downloadziels.
		/// Die Zusammensetzung wird vom Skript youtube-dl durchgeführt, was jedoch in den Einstellungen konfiguriertbar ist.
		/// Abhängig davon, pb es isch um eine Einzeldatei oder eine Playlist handelt, wird entweder der Dateiname der Einzeldatei im Zielordern,
		/// oder der Dateiname der Playlistdatei im zuvor angelegten Order für die Playlist zusammengestellt und in das Model zurückgeschrieben,
		/// damit die Datei später anhand des Models auffindbar ist.
		/// TOOD: 
		/// Ungelöst ist bisher der Fall, dass der Benutzer die Datei manuell veschiebt.
		/// Sauberer Weise müsste die Manifestdatei verworfen werden, wenn für diese keine Mediadatei gefunden werden kann, 
		/// in der bloßen Annahme, dass die Datei nicht mehr existiert...
		/// </summary>
		/// <param name="config">Das Konfigurationsobjekt (global zugreifbar).</param>
		/// <param name="destFileName">Der Dateiname (ohne Pfad) der zu angelegten Datei.</param>
		/// <param name="downloadDest">Das <see cref="Folder"/>-Objekt in dem sich die Datei befindet.</param>
		/// <param name="local">Das zu Zielobjekt <see cref="StorageItem"/>.Objekt (by reference übergeben um garantiert die Änderung außerhalb zu erhalten).</param>
		/// <param name="remote">Das Sourceobjekt <see cref="TrackInfo"/>-Objekt aus dem das Zielobjekt erzeugt wird (by reference übergeben um Änderungen garantiert außerhalb zu erhalten).</param>
		/// <param name="isPlaylist">Gibt an, ob es sich um eine Playlist oder eine Einzeldatei handelt. TODO: (Vorläufige SchnellSchnellLösung)</param>
		/// <returns></returns>
		private static string ErzeugeDateiname(GlobalConfig config, string destFileName, INode downloadDest, ref object local, ref IDownloadSource remote, bool isPlaylist)
		{
			string saveFilePath;

			if (isPlaylist)
			{
				PlaylistInfo info = (PlaylistInfo)remote;

				destFileName = destFileName.Replace(" ", "_");
				Path.GetInvalidPathChars().ToList().ForEach(x =>
				{
					destFileName = destFileName.Replace(x.ToString(), string.Empty);
					Path.GetInvalidFileNameChars().ToList().ForEach(y =>
					{
						destFileName = destFileName.Replace(y.ToString(), string.Empty);
					});
				});
				var dir = ((Folder)downloadDest).GetChild("pl").FullPath;
				saveFilePath = Path.Combine(dir, destFileName);
				local = (PlaylistInfo)info.ToLocal(saveFilePath);
			}
			else
			{
				TrackInfo info = default;
				if (IsPlaylistEntryTrack(destFileName, (PlaylistInfo)remote, out ITrackInfo entry))
				{
					info = (TrackInfo)entry;
				}
				else
				{
					info = (TrackInfo)remote;
				}
				if (string.IsNullOrEmpty(config.DownloadOptions.AudioFormat))
				{
					destFileName = Path.ChangeExtension(destFileName, info.Ext);
				}
				else
				{
					destFileName = Path.ChangeExtension(destFileName, config.DownloadOptions.AudioFormat);
				}
				if (entry != null)
				{
					var parts = destFileName.Split("/");
					saveFilePath = Path.Combine(downloadDest.FullPath, "pl", parts[0]);
					saveFilePath = Path.Combine(saveFilePath, parts[1]);
					local = (IPlaylistTrackEntry)entry;
				}
				else
				{
					saveFilePath = Path.Combine(downloadDest.FullPath, destFileName);
					local = (IStorageItem)remote.ToLocal(saveFilePath);
				}
			}

			return saveFilePath;
		}

		private static bool IsPlaylistEntryTrack(string destFileName, PlaylistInfo playlistInfo, out ITrackInfo entry)
		{
			entry = null;
			if (!string.IsNullOrEmpty(destFileName) && char.IsDigit(destFileName.Split("/").Last()[0]))
			{
				string hint = destFileName[..10].Replace("_", " ").Replace("Mix - ", string.Empty);
				int index = playlistInfo.Entries.FindIndex(x => x.Title.Contains(hint));
				entry = playlistInfo.GetTrack(index);
			}
			return entry != null;
		}

		/// <summary>
		/// Erzeugt das Manifest (serialisiertes Objekt)
		/// </summary>
		/// <param name="json">Das Objekt in Json</param>
		/// <param name="downloadDest">Der Zielordner</param>
		/// <param name="isPlaylist">True, wenn Objekt eine Playlist ist, sonst False</param>
		/// <returns></returns>
		private static IDownloadSource CreateModelFromJson(string json, ref AppFolder appFolder, bool isPlaylist)
		{
			IDownloadSource model;
			if (isPlaylist)
			{
				PlaylistInfo info = JsonConvert.DeserializeObject<PlaylistInfo>(json);
				model = info;
			}
			else
			{
				TrackInfo info = JsonConvert.DeserializeObject<TrackInfo>(json);
				model = info;
			}
			return model;
		}
	}
}
