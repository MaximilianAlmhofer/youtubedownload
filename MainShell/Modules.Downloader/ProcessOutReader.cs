﻿using Lib.Common;

using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;

namespace Modules.Downloader
{

	public interface IProcessPipe
	{
		StreamReader StandardOutput { get; }

		StreamWriter StandardInput { get; }

		bool IsDone { get; }
	}

	public class ProcessPipe : IProcessPipe
	{
		public StreamReader StandardOutput { get; }
		public StreamWriter StandardInput { get; }
		public bool IsDone
		{
			get
			{
				if (StandardOutput != null)
				{
					return StandardOutput.EndOfStream;
				}
				else if (StandardInput != null)
				{
					return StandardInput.BaseStream.Position == StandardInput.BaseStream.Length - 1;
				}
				else return false;
			}
		}
		public ProcessPipe(System.Diagnostics.Process process)
		{
			if (process is null)
			{
				throw new ArgumentNullException(nameof(process));
			}

			if (process.StartInfo.RedirectStandardInput)
				StandardInput = process.StandardInput;
			if (process.StartInfo.RedirectStandardOutput)
				StandardOutput = process.StandardOutput;
		}
	}

	internal sealed class ProcessOutHandler
	{
		private readonly ConcurrentQueue<IResult> statements = new ConcurrentQueue<Lib.Common.IResult>();
		private readonly IProcessPipe process;

		public ProcessOutHandler(System.Diagnostics.Process process)
		{
			this.process = new ProcessPipe(process);
			this.statements = new ConcurrentQueue<IResult>();
		}

		internal bool IsDone => process.IsDone;
		internal int Count => statements.Count;

		internal bool ReceiveData(ManualResetEventSlim mre, out IResult result)
		{
			if (mre is null)
			{
				throw new ArgumentNullException(nameof(mre));
			}

			result = null;

			lock (statements)
			{
				if (!statements.TryDequeue(out result))
				{
					// Dem anderen Thread  einen Vorsprung geben..
					mre.Wait(3000);
				}
			}

			return result != null;
		}

		internal bool ReadStdout()
		{
			try
			{
				string data;
				if (!process.StandardOutput.EndOfStream)
				{
					data = process.StandardOutput.ReadLine();
				}
				else
				{
					data = process.StandardOutput.ReadToEnd();
				}
				if (!string.IsNullOrEmpty(data))
				{
					statements.Enqueue(new StateResult(data));
				}
			}
			catch (Exception ex)
			{
				statements.Enqueue(new ErrorResult(ex));
			}
			return !process.StandardOutput.EndOfStream;
		}

		internal void Finish()
		{
			statements.Enqueue(new StateResult("ENDE"));
		}
	}
}
