﻿using Lib.Common;

using System.Collections.Concurrent;
using System.Threading;

namespace Modules.Downloader
{
	/// <summary>
	/// Dispatched Daten von der Warteschlange.
	/// </summary>
	public class QueueDispatcher
	{
		public ConcurrentQueue<IResult> SourceQueue { get; } = new ConcurrentQueue<IResult>();

		public QueueDispatcher(ref ConcurrentQueue<IResult> statements)
		{
			SourceQueue = statements;
		}

		public bool TryDispatch(ManualResetEventSlim mre, out IResult result)
		{
			result = null;
			lock (SourceQueue)
			{
				SourceQueue.TryDequeue(out result);
			}
			if (result is null)
			{
				mre.Wait(3000);
			}
			return result != null;
		}
	}
}
