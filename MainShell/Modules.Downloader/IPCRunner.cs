﻿using Lib.Command;
using Lib.Common;
using Lib.Process;

using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Modules.Downloader
{
	public delegate void DownloadCompletedHandler(IResult result);

	public class IPCRunner
	{
		#region Singleton logic
		private IPCRunner()
		{
			mutex0 = new Mutex(false, "PROCRUNNER_GLOBAL_MUTEX");
			mutex1 = new Mutex(false, "PROCRUNNER_LOGGER_MUTEX");
			mutex2 = new Mutex(false, "PROCRUNNER_DOWNLOAD_MUTEX");
			mutex3 = new Mutex(false, "PROCRUNNER_WRITE_MUTEX");
			mutex4 = new Mutex(false, "EXCLUSIVE_QUEUE_ACCESS");
		}

		private static class Holder
		{
			internal static IPCRunner instance;

			static Holder() { instance = new IPCRunner(); }
		}

		public static IPCRunner Instance => Holder.instance;
		#endregion

		#region Class variables
		private static Mutex mutex0 = null;
		private static Mutex mutex1 = null;
		private static Mutex mutex2 = null;
		private static Mutex mutex3 = null;
		private static Mutex mutex4 = null;
		private string metadataJson = string.Empty;
		private static LogMessages logger = new LogMessages();
		#endregion


		/// <summary>
		/// Mediadownload - Thread Action
		/// </summary>
		/// <param name="commandObject"></param>
		/// <param name="callback"></param>
		/// <param name="completedHandler"></param>
		/// <param name="cancellation"></param>
		public void ExecuteThread(ManagedProcessCommand commandObject,
								ContextCallback callback,
								Action<IResult> completedHandler,
								CancellationToken cancellation)
		{
			new Thread(new ParameterizedThreadStart(delegate
			{
				var threadResetEvent = new ManualResetEvent(true);
				bool lockTaken = false;
				try
				{
					if (IPCRunner.EnsureLock(mutex2, "PROCRUNNER_DOWNLOAD_MUTEX", out lockTaken))
					{
						ExecuteProcess(Tuple.Create(commandObject, callback),
													threadResetEvent,
													cancellation);
						threadResetEvent.WaitOne();
						mutex2.WaitOne();
					}
				}
				catch (Exception ex)
				{
					ex.Trace();
					IErrorResult err = new ErrorResult();
					err.SetError(ex.ToLongString());
					callback(err);
				}
				finally
				{
					if (lockTaken)
					{
						mutex2.ReleaseMutex();
					}
					if (threadResetEvent?.SafeWaitHandle?.IsClosed == false)
					{
						threadResetEvent.Reset();
						threadResetEvent.Close();
					}
					completedHandler(commandObject.Result);
				}
			})).Start();
		}


		/// <summary>
		/// Metadatadownload - Thread Action
		/// </summary>
		/// <param name="commandObject"></param>
		/// <param name="callback"></param>
		public void ExecuteThread(ManagedProcessCommand commandObject, Action<IResult> callback, CancellationToken cancellation = default)
		{
			if (commandObject is null)
			{
				throw new ArgumentNullException(nameof(commandObject));
			}

			bool lockTaken = false;

			commandObject.Result = new MetadataResult();

			try
			{
				new Thread(new ParameterizedThreadStart(async delegate
				{
					try
					{
						IPCRunner.EnsureLock(mutex3, "PROCRUNNER_WRITE_MUTEX", out lockTaken);
						var commandHolder = commandObject.Instruction;
						try
						{
							await DownloadMetadata(commandHolder, cancellation).ConfigureAwait(false);
						}
						catch (TaskCanceledException)
						{
							throw;
						}

						commandObject.Result.SetResult(metadataJson);
						metadataJson = null;
						callback.Invoke(commandObject.Result);
					}
					catch
					{
						throw;
					}
					finally
					{
						mutex3.WaitOne();

						if (lockTaken)
						{
							mutex3.ReleaseMutex();
						}
					}
				})).Start();
			}
			catch (TaskCanceledException ex)
			{
				ex.Trace();
			}
		}



		/// <summary>
		/// MediafileDownload
		/// </summary>
		/// <param name="parameter"></param>
		/// <param name="threadResetEvent"></param>
		/// <param name="cancellation"></param>
		/// <returns></returns>
		private static void ExecuteProcess(Tuple<ManagedProcessCommand, ContextCallback> parameter, ManualResetEvent threadResetEvent, CancellationToken cancellation)
		{
			if (parameter is null)
			{
				throw new ArgumentNullException(nameof(parameter));
			}

			if (threadResetEvent is null)
			{
				throw new ArgumentNullException(nameof(threadResetEvent));
			}

			ManagedProcessCommand commandObject = parameter.Item1;

			using (var proc = ProcessFactory.Create(commandObject.Instruction))
			{
				if (proc.Start())
				{
					var callback = parameter.Item2;
					var p = new ProcessOutHandler(proc);
					var mre = new ManualResetEventSlim(true);

					if (!proc.StandardOutput.EndOfStream)
					{
						ThreadpoolThread_ReadStandardOutput_Start(proc, commandObject, callback, p, mre, threadResetEvent, cancellation);
						CurrentThread_ProcessStatements(proc, commandObject, callback, p, mre);
					}
					proc.WaitForExit();
				}
			}
		}


		private static void CurrentThread_ProcessStatements(Process proc, ManagedProcessCommand commandObject, ContextCallback callback, ProcessOutHandler p, ManualResetEventSlim mre)
		{
			try
			{
				do
				{
					while (p.ReadStdout())
					{
						if (commandObject.SetEvent.IsSet)
						{
							SpinWait.SpinUntil(() => !commandObject.SetEvent.IsSet);
						}

						mre.Wait(100);
					}
				}
				while (!p.IsDone);
			}
			finally
			{
				while (!proc.StandardError.EndOfStream)
				{
					callback.Invoke(new ErrorResult(proc.StandardError.ReadLine()));
				}
				p.Finish();
			}
		}


		private static void ThreadpoolThread_ReadStandardOutput_Start(Process proc, ManagedProcessCommand commandObject, ContextCallback callback, ProcessOutHandler p, ManualResetEventSlim mre, ManualResetEvent threadResetEvent, CancellationToken cancellation)
		{

			ThreadPool.QueueUserWorkItem(delegate (object state)
			{
				try
				{
					Func<string, bool> isArchived = value => value.Contains(Strings.IsInArchive);

					SpinWait.SpinUntil(() => p.Count > 15 && !cancellation.IsCancellationRequested && threadResetEvent.Set());

					while (true)
					{
						if (commandObject.SetEvent.IsSet)
						{
							try
							{
								while (commandObject.SetEvent.IsSet)
								{
									mre.Wait();
								}
							}
							finally
							{
								mre.Set();
								commandObject.ResetEvent.Reset();
							}
						}
						try
						{
							if (cancellation.IsCancellationRequested)
							{
								callback(new StateResult("ABBRUCH"));
								break;
							}
							if (threadResetEvent.Set())
							{
								threadResetEvent.Reset();

								if (p.ReceiveData(mre, out var result))
								{
									Thread.Sleep(500);

									if (isArchived(result.GetString()))
									{
										callback(result);
										break;
									}
									if (result.Value?.ToString() == "ENDE" || (result.GetString()?.Contains("ffmpeg") ?? false))
									{
										break;
									}
									callback(result);
								}
								if (mre.IsSet)
								{
									mre.Wait(3000);
								}
								else
								{
									mre.Set();
								}
							}
						}
						catch (Exception ex)
						{
							var error = new ErrorResult();
							error.SetError(ex.ToLongString());
							callback(error);
							if (string.Equals(error.Value, "SIGINT"))
							{
								proc.Kill();
							}
							break;
						}
					}
				}
				catch (ObjectDisposedException ex)
				{
					var error = new ErrorResult();
					error.SetError(ex.ToLongString());
					callback(error);
				}
				finally
				{
					if (!threadResetEvent.SafeWaitHandle.IsClosed)
						threadResetEvent.Set();
					cancellation.ThrowIfCancellationRequested();
				}
			}, cancellation);
		}



		/// <summary>
		/// Metadatadownload
		/// </summary>
		/// <param name="instruction"></param>
		/// <returns></returns>
		private Task DownloadMetadata(IInstruction instruction, CancellationToken cancellation)
		{
			Task result = Task.CompletedTask;
			using (var proc = ProcessFactory.Create(instruction).WithOutputDataHandler(OnOutputDataReceived).WithErrorDataHandler(logger))
			{
				if (proc.Start())
				{

					proc.BeginOutputReadLine();
					mutex0.WaitOne();

					proc.WaitForExit(50000);
					if (cancellation.CanBeCanceled)
					{
						if (metadataJson is null)
							proc.WaitForExit();
					}
				}
			}

			if (result.IsCompleted && result.Status == TaskStatus.RanToCompletion)
			{
				mutex0.ReleaseMutex();
			}
			return result;
		}

		private void OnOutputDataReceived(object sender, DataReceivedEventArgs e)
		{
			if (string.IsNullOrEmpty(metadataJson))
			{
				metadataJson = e.Data;
			}
			else if (!metadataJson.EndsWith("}", StringComparison.OrdinalIgnoreCase))
			{
				metadataJson += e.Data;
			}
		}

		#region Waithandles Mutex
		private static bool EnsureLock(Mutex mutex, string mIdentifier, out bool lockTaken)
		{
			lockTaken = false;
			bool lockTaken2 = false;
			SpinWait.SpinUntil(() =>
			{
				bool innerLockTaken = Mutex.TryOpenExisting(mIdentifier, out mutex);
				lockTaken2 = innerLockTaken;
				return lockTaken2;
			});
			lockTaken = lockTaken2;
			return lockTaken2;
		}
		#endregion
	}
}
