﻿using NAudio.CoreAudioApi;

using System.Linq;

namespace xtra_snd_tools.Mainshell.Settings.ViewModels
{
	internal static class Devices
	{
		private static MMDeviceEnumerator deviceEnumerator = new MMDeviceEnumerator();

		public static MMDeviceCollection EnumerateDevices()
		{
			return deviceEnumerator.EnumerateAudioEndPoints(DataFlow.Render, DeviceState.All);
		}

		public static MMDevice GetById(string id)
		{
			return EnumerateDevices().SingleOrDefault(x => x.ID == id);
		}
	}
}
