﻿
using NAudio.CoreAudioApi;

using System;
using System.Threading;
using System.Windows.Threading;
using xtra_snd_tools.Common.Contracts.ViewModels;

namespace xtra_snd_tools.Mainshell.Settings.ViewModels
{
	public class MMDeviceModel : NotifyPropertyChangeViewModel
	{
		private string deviceId;

		public MMDeviceModel(MMDevice device)
		{
			deviceId = device.ID;
			UpdateCurrentDevice();
		}

		public string DeviceName { get; set; }

		public string EndpointName { get; set; }

		public string DeviceState { get; set; }

		public Connector Connector { get; set; }

		private void UpdateFromId(string id)
		{
			this.deviceId = id;
			this.UpdateCurrentDevice();
		}

		private void UpdateCurrentDevice()
		{
			MMDevice device = Devices.GetById(this.deviceId);
			this.DeviceName = device.DeviceFriendlyName;
			this.EndpointName = device.FriendlyName;
			this.DeviceState = Enum.GetName(typeof(DeviceState), device.State);
			this.Connector = device.DeviceTopology.GetConnector(device.DeviceTopology.ConnectorCount - 1);
			Dispatcher.FromThread(Thread.CurrentThread).Invoke(async () => await RaiseAllPropertiesChanged());
		}
	}
}
