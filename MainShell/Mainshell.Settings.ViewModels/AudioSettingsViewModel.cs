﻿using Lib.Common;

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Automation;

using xtra_snd_tools.Common.Contracts.ViewModels;

namespace xtra_snd_tools.Mainshell.Settings.ViewModels
{

	public class AudioSettingsViewModel : NotifyPropertyChangeViewModel, IAudioSettingsViewModel
	{

		public AudioSettingsViewModel()
		{
			AudioEndpoints = new ObservableCollection<MMDeviceModel>(Devices.EnumerateDevices().Select(x => new MMDeviceModel(x)));
		}

		public ObservableCollection<MMDeviceModel> AudioEndpoints { get; }


		private MMDeviceModel selectedDevice;

		public event EventHandler<AutomationEventArgs> DeviceAutomation = delegate { };

		protected virtual void OnDeviceAutomation(AutomationEventArgs e)
		{
			var handler = DeviceAutomation;
			handler?.Invoke(this, e);
		}

		public MMDeviceModel SelectedDevice
		{
			get => selectedDevice;
			set
			{
				selectedDevice = value;
				RaisePropertyChanged();
			}
		}
	}
}
