﻿
using AudioLib.Playback;

using Lib.Common;
using Lib.Utility;

using System;
using System.Windows;
using System.Windows.Controls;

namespace xtra_snd_tools
{
	internal static class PlaybackEvents
	{

		internal static void SeekbarControl_PreviewMouseLeftButtonDown(IPlaybackEngine playbackManager)
		{

			if (playbackManager.AudioPlayback.PlaybackState == NAudio.Wave.PlaybackState.Playing)
			{
				playbackManager?.Pause();
			}
		}


		internal static void SeekbarControl_PreviewMouseLeftButtonUp(IPlaybackManager playbackManager, double value, System.Windows.Input.MouseButtonEventArgs e)
		{
			if (playbackManager != null)
			{
				(playbackManager as IPlaybackEngine).AudioPlayback?.SetCurrentPosition(value);
				playbackManager.Play();
			}
		}


		internal static void SeekbarControl_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{

			Slider slider = (sender as Slider);
			var position = e.GetPosition(slider);

			if (slider.DataContext is IPlaybackManager playbackManager && (playbackManager as IPlaybackEngine).AudioPlayback != null)
			{
				var newValue = slider.ActualWidth / (playbackManager as IPlaybackEngine).AudioPlayback.ActiveStream.TotalSeconds() * position.X;

				playbackManager.Stop();
				(playbackManager as IPlaybackEngine).AudioPlayback.SetCurrentPosition(Math.Max(0, Math.Min(newValue, slider.Maximum)));
				playbackManager.Play();

				slider.UpdateBinding(Slider.ValueProperty);
			}
		}

		internal static void HandleVolumeChanged(IPlaybackManager playbackManager, RoutedPropertyChangedEventArgs<double> e)
		{
			if ((playbackManager as IPlaybackEngine)?.AudioPlayback != null)
			{
				(playbackManager as IPlaybackEngine).AudioPlayback.Volume = (float)e.NewValue;
			}
		}
	}
}
