﻿using Lib.Common;
using Lib.Common.Message;

using Microsoft.Extensions.Options;

using MvvmCross.Commands;
using MvvmCross.Plugin.Messenger;
using MvvmCross.ViewModels;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

using xtra_snd_tools.Common.Contracts.ViewModels;
using xtra_snd_tools.Mainshell.ViewModels;

namespace xtra_snd_tools.Modules.PlaylistEditor.ViewModels
{
	public class PlaylistEditorViewModel : ViewModelBase, IPlaylistEditorViewModel
	{
		private readonly string[][] formatListArray = new string[2][]
		{
			new[] { "mp3", "flac", "wav" },
			new[] { "mp4", "webm", "flv" }
		};

		private readonly List<IMediaItem> editingList = new List<IMediaItem>();
		private readonly IPlayerViewModel playerViewModel;
		private readonly IOptions<GlobalConfig> config;
		private DataGridHierarchialData dataGridSource;

		private MvxInteraction<PlaylistAddRequestMessage> _interaction = new MvxInteraction<PlaylistAddRequestMessage>();
		public IMvxInteraction<PlaylistAddRequestMessage> Interaction => _interaction;

		public PlaylistEditorViewModel(IMvxMessenger messenger, IMainWindowViewModel viewModelRepository, IOptions<GlobalConfig> config) : base(messenger)
		{
			this.config = config;
			playerViewModel = viewModelRepository.PlayerViewModel ?? throw new ArgumentNullException(nameof(viewModelRepository));
			this.SubscribeToPlaylistAddRequestMessage(HandlePlaylistAddRequestMessage);
		}

		public override Task Initialize()
		{
			return base.Initialize();
		}

		public IHierarchicData DataGridSource
		{
			get
			{
				return dataGridSource;
			}
			set
			{
				dataGridSource = value as DataGridHierarchialData;
				RaisePropertyChanged();
			}
		}

		private ILocalPlaylist playlist;
		public ILocalPlaylist Playlist
		{
			get => playlist;
			private set
			{
				playlist = value;
				RaisePropertyChanged();
			}
		}

		private IMvxCommand createPlaylistCommand;
		public IMvxCommand CreatePlaylistCommand => createPlaylistCommand ??= new MvxCommand(() =>
																  {
																	  Playlist = new LocalPlaylist(config);
																	  messenger.Publish(new ShowTitleInputMessage(this));
																	  var request = new PlaylistAddRequestMessage(this)
																	  {
																		  Callback = value =>
																		  {
																			  if (value != null)
																				  Title = value;
																		  },
																	  };
																	  _interaction.Raise(request);
																  });

		private IMvxCommand clearListCommand;
		public IMvxCommand ClearListCommand
		{
			get
			{
				if (clearListCommand is null)
				{
					clearListCommand = new MvxCommand(() =>
					{
						editingList.Clear();
						Playlist = null;
					});
				}
				return clearListCommand;
			}
		}

		private IMvxCommand addToPlaylistCommand;
		public IMvxCommand AddToPlaylistCommand
		{
			get
			{
				if (addToPlaylistCommand is null)
				{
					addToPlaylistCommand = new MvxCommand(() =>
					{
						playerViewModel.Manager.SelectTracks(editingList);
						editingList.Clear();
					});
				}
				return addToPlaylistCommand;
			}
		}

		private IMvxCommand<IMediaItem> removeFromPlaylistCommand;
		public IMvxCommand<IMediaItem> RemoveFromPlaylistCommand
		{
			get
			{
				if (removeFromPlaylistCommand is null)
				{
					removeFromPlaylistCommand = new MvxCommand<IMediaItem>(item =>
					{
						((ITrackListViewModel)playerViewModel).Tracks.Remove(item);
						editingList.Clear();
					});
				}
				return removeFromPlaylistCommand;
			}
		}

		public ObservableCollection<string> GetFormatTypeItemsSource()
		{
			return new ObservableCollection<string>(formatListArray[0].Concat(formatListArray[1]));
		}

		public string Title
		{
			get => playlist?.Title;
			set
			{
				playlist.Title = value;
				RaisePropertyChanged();
			}
		}

		public void AddItem(object item)
		{
			if (item is IMediaItem mediaItem)
				editingList.Add(mediaItem);
		}

		private void HandlePlaylistAddRequestMessage(PlaylistAddRequestMessage message)
		{
			Title = message.Title;
			messenger.Publish(new ShowTitleInputMessage(this));
		}
	}
}
