﻿using Lib.Common;

using Microsoft.Extensions.Options;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

using xtra_snd_tools.Common.Contracts;

namespace xtra_snd_tools.Modules.PlaylistEditor.ViewModels
{
	public sealed class LocalPlaylist : IObservable<PlaylistEntry>, ILocalPlaylist, IDisposable
	{
		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		private IList<PlaylistEntry> _entries = new List<PlaylistEntry>();

		private IOptions<GlobalConfig> _config;

		private string storageFolder, manifestFile, title;

		private Uri thumbnail;

		public LocalPlaylist(IOptions<GlobalConfig> config)
		{
			_config = config;
		}

		public EventHandler<PropertyChangedEventArgs> PropertyChangedHandler
		{
			get
			{
				return new EventHandler<PropertyChangedEventArgs>((sender, e) =>
				{
					OnPropertyChanged(e.PropertyName);
				});
			}
		}

		public string ManifestFile
		{
			get => manifestFile;
			set
			{
				if (!string.Equals(manifestFile, value, StringComparison.OrdinalIgnoreCase))
				{
					if (ValidateStoragePath(manifestFile, true))
					{
						manifestFile = value;
						OnPropertyChanged();
					}
				}
			}
		}

		public string StorageFolder
		{
			get => storageFolder;
			set
			{
				if (!string.Equals(storageFolder, value, StringComparison.OrdinalIgnoreCase))
				{
					if (ValidateStoragePath(storageFolder, false))
					{
						storageFolder = value;
						OnPropertyChanged();
					}
				}
			}
		}

		public Uri Thumbnail
		{
			get => thumbnail;
			set
			{
				if (thumbnail != value)
				{
					thumbnail = value;
					OnPropertyChanged();
				}
			}
		}

		public string Title
		{
			get => title;
			set
			{
				if (string.IsNullOrEmpty(title))
				{
					CreateManifestFile(value);
					CreateStorageFolder(value);
				}
				else if (!string.Equals(title, value, StringComparison.OrdinalIgnoreCase))
				{
					UpdateManifestFile(value);
					UpdateStorageFolder(value);
				}
				title = value;
				OnPropertyChanged();
			}
		}

		private void UpdateStorageFolder(string value)
		{
			value = PathUtils.GetValidFileSystemName(value);
			try
			{
				DirectoryInfo info = new DirectoryInfo(value);
				Directory.Move(StorageFolder, info.FullName);
				info.Refresh();
				StorageFolder = info.FullName;
			}
			catch (Exception ex)
			{
				ex.Trace();
			}
		}

		private void UpdateManifestFile(string value)
		{
			value = PathUtils.GetValidFileSystemName(value);
			try
			{
				FileInfo info = new FileInfo(value);
				File.Move(ManifestFile, info.FullName);
				info.Refresh();
				ManifestFile = info.FullName;
			}
			catch (Exception ex)
			{
				ex.Trace();
			}
		}

		private void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			var handler = PropertyChanged;
			handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		private bool ValidateStoragePath(string storagePath, bool checkFile)
		{
			return checkFile ? File.Exists(storagePath) : Directory.Exists(storagePath);
		}

		private void CreateManifestFile(string title)
		{
			title = PathUtils.GetValidFileSystemName(title);
			string manifest = Path.ChangeExtension(title, ".json");
			ManifestFile = Path.Combine(_config.Value.AppFolder.MetadataFolderPath, "pl", manifest);
		}

		private void CreateStorageFolder(string title)
		{
			title = PathUtils.GetValidFileSystemName(title);
			string _storageFolder = Path.Combine(_config.Value.AppFolder.DownloadFolder.GetChild("pl").FullPath, title);
			if (Directory.Exists(_storageFolder))
			{
				_storageFolder = string.Join("_", title, DateTime.Now.ToShortDateString().Replace(".", "_"));
			}
			StorageFolder = _storageFolder;
		}


		public IEnumerable<IPlaylistTrackEntry> GetEntries()
		{
			return _entries;
		}

		public ITrackInfo GetTrack(int index)
		{
			return _entries?.ElementAtOrDefault(index);
		}

		public IDisposable Subscribe(IObserver<PlaylistEntry> observer)
		{
			foreach (var entry in _entries)
			{
				try
				{
					observer.OnNext(entry);
				}
				catch (Exception ex)
				{
					observer.OnError(ex);
				}
				finally
				{
					observer.OnCompleted();
				}
			}
			return this;
		}

		public void Dispose()
		{
			MDSerializer.SerializeJ(ManifestFile, JsonConvert.SerializeObject((PlaylistInfo)(IPlaylistInfo)(IPlaylist)this));
			_entries = null;
		}
	}
}
