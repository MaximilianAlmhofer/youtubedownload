﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace xtra_snd_tools.Mainshell.Markup
{
	public class DurationTimeSpanConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				TimeSpan ts = (TimeSpan)value;
				return new Duration(ts);
			}
			catch (Exception)
			{
				return new Duration();
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				Duration duration = (Duration)value;
				return duration.HasTimeSpan ? duration.TimeSpan : TimeSpan.Zero;
			}
			catch (Exception)
			{
				return TimeSpan.Zero;
			}
		}
	}
}
