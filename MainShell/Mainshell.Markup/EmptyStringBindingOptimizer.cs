﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace xtra_snd_tools.Mainshell.Markup
{
	public class EmptyStringBindingOptimizer : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			string value = values.ElementAtOrDefault(0)?.ToString();
			if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
			{
				value = "Unbekannt";
			}
			return value;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
