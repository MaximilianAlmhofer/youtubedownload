﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace xtra_snd_tools.Mainshell.Markup
{
	public sealed class RadialAnimation
	{
		private DispatcherTimer timer = null;
		private LineSegment LineSegment2 = null;
		PathFigure PathFigure1 = null;
		private bool ISIncrementdirectionX = true;
		private bool IsIncrementX = true;
		private bool IsIncrementY = true;
		private LineSegment LineSegmentFirstcorner = null;
		private LineSegment LineSegmentseconcorner = null;
		private LineSegment LineSegmentThirdcorner = null;
		private LineSegment LineSegmentFourthcorner = null;
		private readonly double offsetOfAnimation = 1;
		private double height = 0;
		private double width = 0;

		public void MakeRadiaAnimation(FrameworkElement animatedElement, double width, double height, TimeSpan timeSpan)
		{
			this.height = height;
			this.width = width;
			double steps = 2 * (this.height + this.width);
			double tickTime = timeSpan.TotalSeconds / steps;
			string topCentre = Convert.ToString(this.width / 2) + ",0";
			string centre = Convert.ToString(this.width / 2) + "," + Convert.ToString(this.height / 2);
			PathGeometry PathGeometry1 = new PathGeometry();
			PathFigure1 = new PathFigure
			{
				StartPoint =
		((Point)new PointConverter().ConvertFromString(centre))
			};
			PathGeometry1.Figures.Add(PathFigure1);
			LineSegment LineSegmentdummy = new LineSegment
			{
				Point =
		((Point)new PointConverter().ConvertFromString(topCentre))
			};
			PathFigure1.Segments.Add(LineSegmentdummy);
			LineSegmentseconcorner = new LineSegment
			{
				Point =
		((Point)new PointConverter().ConvertFromString(topCentre))
			};
			PathFigure1.Segments.Add(LineSegmentseconcorner);
			LineSegmentThirdcorner = new LineSegment
			{
				Point =
		((Point)new PointConverter().ConvertFromString(topCentre))
			};
			PathFigure1.Segments.Add(LineSegmentThirdcorner);
			LineSegmentFourthcorner = new LineSegment
			{
				Point =
		((Point)new PointConverter().ConvertFromString(topCentre))
			};
			PathFigure1.Segments.Add(LineSegmentFourthcorner);
			LineSegmentFirstcorner = new LineSegment
			{
				Point =
		((Point)new PointConverter().ConvertFromString(topCentre))
			};
			PathFigure1.Segments.Add(LineSegmentFirstcorner);
			LineSegment2 = new LineSegment();
			LineSegment2.Point =
		((Point)new PointConverter().ConvertFromString(topCentre));
			PathFigure1.Segments.Add(LineSegment2);
			animatedElement.Clip = PathGeometry1;
			PointAnimationUsingKeyFrames pointAnimationUsingKeyFrames = new PointAnimationUsingKeyFrames();
			DiscretePointKeyFrame discretePointKeyFrame = new DiscretePointKeyFrame
			{
				Value = new Point(0, 0)
			};
			TimeSpan keyTime = new TimeSpan(0, 0, 1);
			discretePointKeyFrame.KeyTime = keyTime;
			timer = new DispatcherTimer
			{
				Interval = TimeSpan.FromSeconds(tickTime)
			};
			timer.Tick += new EventHandler(Timer_Tick);
			timer.IsEnabled = true;
		}

		private void Timer_Tick(object sender, EventArgs e)
		{
			if ((LineSegment2.Point.X <= 0) && (LineSegment2.Point.Y <= 0))
			{
				LineSegmentFirstcorner.Point =
					new Point(LineSegment2.Point.X, LineSegment2.Point.Y);
				ISIncrementdirectionX = true;
				IsIncrementX = true;
			}
			else if (((LineSegment2.Point.X >= width)
				&& (LineSegment2.Point.Y <= 0)))
			{
				LineSegmentseconcorner.Point =
					new Point(LineSegment2.Point.X, LineSegment2.Point.Y);
				LineSegmentThirdcorner.Point =
					new Point(LineSegment2.Point.X, LineSegment2.Point.Y);
				LineSegmentFourthcorner.Point =
					new Point(LineSegment2.Point.X, LineSegment2.Point.Y);
				LineSegmentFirstcorner.Point =
					new Point(LineSegment2.Point.X, LineSegment2.Point.Y);
				ISIncrementdirectionX = false;
				IsIncrementY = true;
			}

			else if ((LineSegment2.Point.X >= width)
				&& (LineSegment2.Point.Y >= height))
			{
				LineSegmentThirdcorner.Point =
					new Point(LineSegment2.Point.X, LineSegment2.Point.Y);
				LineSegmentFourthcorner.Point =
					new Point(LineSegment2.Point.X, LineSegment2.Point.Y);
				LineSegmentFirstcorner.Point =
					new Point(LineSegment2.Point.X, LineSegment2.Point.Y);
				ISIncrementdirectionX = true;
				IsIncrementX = false;
			}

			else if ((LineSegment2.Point.X <= 0)
				&& (LineSegment2.Point.Y >= height))
			{
				LineSegmentFourthcorner.Point =
					new Point(LineSegment2.Point.X, LineSegment2.Point.Y);
				LineSegmentFirstcorner.Point =
					new Point(LineSegment2.Point.X, LineSegment2.Point.Y);
				ISIncrementdirectionX = false;
				IsIncrementY = false;
			}

			double x, y;

			if (ISIncrementdirectionX == true)
			{
				if (IsIncrementX)
				{
					x = LineSegment2.Point.X + offsetOfAnimation;
					y = LineSegment2.Point.Y;
				}
				else
				{
					x = LineSegment2.Point.X - offsetOfAnimation;
					y = LineSegment2.Point.Y;
				}
			}
			else
			{
				if (IsIncrementY)
				{
					x = LineSegment2.Point.X;
					y = LineSegment2.Point.Y + offsetOfAnimation;
				}
				else
				{
					x = LineSegment2.Point.X;
					y = LineSegment2.Point.Y - offsetOfAnimation;
				}
			}
			LineSegment2.Point = new Point(x, y);
			if ((LineSegment2.Point.X == width / 2)
				&& (LineSegment2.Point.Y == 0))
			{
				timer.IsEnabled = false;
			}
		}
	}
}
