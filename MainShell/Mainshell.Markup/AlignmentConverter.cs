﻿
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace xtra_snd_tools.Mainshell.Markup
{
	public class AlignmentConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!Enum.TryParse<HorizontalAlignment>(value.ToString(), out var alignmentOfOtherElement))
			{
				return HorizontalAlignment.Left;
			}

			switch (alignmentOfOtherElement)
			{
				case HorizontalAlignment.Right:
					return HorizontalAlignment.Left;

				case HorizontalAlignment.Left:
				default:
					return HorizontalAlignment.Right;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}
