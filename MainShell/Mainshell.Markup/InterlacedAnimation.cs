﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace xtra_snd_tools.Mainshell.Markup
{
	public sealed class InterlacedAnimation
	{
		private DispatcherTimer timer = null;
		private PathGeometry pathGeometry = null;
		private readonly double rectangleSize = 3;
		private double startingCoordinate = 0;
		private double endcoordinate = 0;
		private FrameworkElement animatedElement = null;
		private double width = 0;
		private double height = 0;
		private double offset = 0.5;
		private bool IsTimerShouldBeStoppedForUp = false;
		private bool IsTimerShouldBeStoppedForDown = false;

		public void MakeInterlacedAnimation(FrameworkElement animatedElement, double width, double height, TimeSpan timeSpan)
		{
			double steps = (height / (rectangleSize + offset));
			double tickTime = timeSpan.TotalSeconds / steps;
			this.animatedElement = animatedElement;
			this.width = width;
			this.height = height;
			endcoordinate = height;
			pathGeometry = new PathGeometry();
			animatedElement.Clip = pathGeometry;
			timer = new DispatcherTimer
			{
				Interval = TimeSpan.FromSeconds(tickTime)
			};
			timer.Tick += new EventHandler(Timer_Tick);
			timer.IsEnabled = true;
		}

		private void Timer_Tick(object sender, EventArgs e)
		{
			if (IsTimerShouldBeStoppedForUp == true && IsTimerShouldBeStoppedForDown == true)
			{
				timer.IsEnabled = false;
			}
			if (startingCoordinate == endcoordinate)
			{
				offset = 0;
			}
			if (startingCoordinate >= (height / 2 - 5))
			{
				offset = 0;
			}
			if (endcoordinate <= (height / 2 - 5))
			{
				offset = 0;
			}
			if (startingCoordinate < height)
			{
				RectangleGeometry myRectGeometry2 = new RectangleGeometry
				{
					Rect = new Rect(0, startingCoordinate, width, rectangleSize)
				};
				pathGeometry = Geometry.Combine(pathGeometry, myRectGeometry2, GeometryCombineMode.Union, null);
			}
			else
			{
				IsTimerShouldBeStoppedForUp = true;
			}
			if (endcoordinate > 0)
			{
				RectangleGeometry myRectGeometry3 = new RectangleGeometry
				{
					Rect = new Rect(0, endcoordinate, width, rectangleSize)
				};
				pathGeometry = Geometry.Combine(pathGeometry, myRectGeometry3, GeometryCombineMode.Union, null);
			}
			else
			{
				IsTimerShouldBeStoppedForDown = true;
			}
			startingCoordinate = startingCoordinate + rectangleSize + offset;
			endcoordinate = endcoordinate - rectangleSize - offset;
			animatedElement.Clip = pathGeometry;
		}
	}
}
