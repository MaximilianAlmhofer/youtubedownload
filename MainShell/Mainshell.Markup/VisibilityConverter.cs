﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace xtra_snd_tools.Mainshell.Markup
{
	public class VisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is bool && parameter != null)
			{
				if (System.Convert.ToBoolean(parameter))
				{
					value = !(bool)value;
				}
			}
			// If the item has children, then show the checkbox, otherwise hide it
			return ((bool)value ? Visibility.Visible : Visibility.Collapsed);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}

}
