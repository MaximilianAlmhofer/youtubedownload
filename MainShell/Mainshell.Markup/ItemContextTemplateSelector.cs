﻿
using System.Windows;
using System.Windows.Controls;

using xtra_snd_tools.Modules.Playback.ViewModels;

namespace xtra_snd_tools.Mainshell.Markup
{
	public class ItemContextTemplateSelector : DataTemplateSelector
	{
		public DataTemplate DownloadItemTemplate { get; set; }
		public DataTemplate PlaylistItemTemplate { get; set; }

		public override DataTemplate SelectTemplate(object item, DependencyObject container)
		{
			DataTemplate template = base.SelectTemplate(item, container);

			if (item is MediaItems)
			{
				template = PlaylistItemTemplate;
			}
			else
			{
				template = DownloadItemTemplate;
			}
			return template;
		}
	}
}
