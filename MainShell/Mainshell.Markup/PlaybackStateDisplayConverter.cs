﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace xtra_snd_tools.Mainshell.Markup
{
	public class PlaybackStateDisplayConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (values[0] == System.Windows.DependencyProperty.UnsetValue)
				return "";
			if (values[1] == System.Windows.DependencyProperty.UnsetValue)
				return "";
			if (values[2] == System.Windows.DependencyProperty.UnsetValue)
				return "";
			TimeSpan position = TimeSpan.FromSeconds((double)values[0]);
			string title = (string)values[1];
			string totalDuration = (string)values[2];

			return string.Format("{0}\r\n{1} -      ({2})", title, position.ToString("hh\\:mm\\:ss"), totalDuration);

		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
