﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace xtra_snd_tools.Mainshell.Markup
{
	public class ContentMarginConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			double relWidth = System.Convert.ToDouble(values[0]);
			double totalWidth = System.Convert.ToDouble(values[1]);

			return new Thickness(10, 5, totalWidth - relWidth, 5);
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}
