﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace xtra_snd_tools.Mainshell.Markup
{
	public class MasterExpandDetailsConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			return values.Cast<bool>().All(x => true);
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			return (from v in targetTypes where v.Name == typeof(bool).Name select (bool)value).Cast<object>().ToArray();
		}
	}
}
