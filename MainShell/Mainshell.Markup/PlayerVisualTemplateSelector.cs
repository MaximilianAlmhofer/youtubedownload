﻿using System.Windows;
using System.Windows.Controls;

namespace xtra_snd_tools.Mainshell.Markup
{
	public class PlayerVisualTemplateSelector : DataTemplateSelector
	{

		public DataTemplate PlayListTemplate { get; set; }

		public DataTemplate SampleWaveTemplate { get; set; }

		public override DataTemplate SelectTemplate(object item, DependencyObject container)
		{
			DataTemplate template = base.SelectTemplate(item, container);

			return template;
		}
	}
}
