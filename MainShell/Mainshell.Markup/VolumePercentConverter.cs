﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace xtra_snd_tools.Mainshell.Markup
{
	public class VolumePercentConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			double volume = System.Convert.ToDouble(value);
			volume = volume * 100;

			return (int)volume;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
