﻿using System.Windows;
using System.Windows.Controls;
using xtra_snd_tools.Mainshell.Controls;

namespace xtra_snd_tools.Mainshell.Markup
{
	public sealed class EditLabelToggleButtonTemplateSelector : DataTemplateSelector
	{
		public DataTemplate EditTemplate { get; set; }

		public DataTemplate AppliedTemplate { get; set; }

		public override DataTemplate SelectTemplate(object item, DependencyObject container)
		{
			DataTemplate template = EditTemplate;
			var editLabel = FindEditLabel(container);
			if (editLabel != null)
			{
				template = editLabel.IsEditing ? AppliedTemplate : EditTemplate;
			}
			return template;
		}

		private static EditLabel FindEditLabel(DependencyObject container)
		{
			if (container is ContentPresenter cp)
			{
				FrameworkElement parentUi = (FrameworkElement)cp.Parent;
				while (parentUi?.TemplatedParent != null)
				{
					if ((parentUi.TemplatedParent as EditLabel) is null)
					{
						parentUi = (FrameworkElement)parentUi.TemplatedParent;
					}
					else return (EditLabel)parentUi.TemplatedParent;
				}
			}
			return null;
		}
	}
}
