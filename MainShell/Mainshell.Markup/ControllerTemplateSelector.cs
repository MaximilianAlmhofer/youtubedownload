﻿
using System.Windows;
using System.Windows.Controls;

using xtra_snd_tools.Modules.AudioVisualization;

namespace xtra_snd_tools.Mainshell.Markup
{
	public sealed class ControllerTemplateSelector : DataTemplateSelector
	{
		public DataTemplate DefaultControllerTemplate { get; set; }

		public DataTemplate AdvancedControllerTemplate { get; set; }

		public override DataTemplate SelectTemplate(object item, DependencyObject container)
		{
			DataTemplate selectTemplate = DefaultControllerTemplate;

			if (item is IAudioVisualization viewModel)
			{
				selectTemplate = viewModel.Visualizations.Count > 0
					? viewModel.SelectedVisualization != null
					? AdvancedControllerTemplate : DefaultControllerTemplate
					: DefaultControllerTemplate;
			}
			return selectTemplate;
		}
	}
}
