﻿using Lib.Common;

using System;
using System.Globalization;
using System.Windows.Data;

namespace xtra_snd_tools.Mainshell.Markup
{
	public class FileHeaderEnumImageConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{

			var myEnum = (AudioCodecs)Enum.Parse(typeof(AudioCodecs), value.ToString());

			switch (myEnum)
			{

				case AudioCodecs.Aac:
				case AudioCodecs.Aiff:
				case AudioCodecs.Ape:
				case AudioCodecs.Asf:
					return "/resources/images/taglib/advanced_lossless_formats_32px.png";
				case AudioCodecs.Flac:
					return "/resources/images/taglib/free_lossless_formats_32px.png";
				case AudioCodecs.Mpeg:
				case AudioCodecs.Mpeg4:
					return "/resources/images/taglib/lossy_formats_32px.png";
				default:
					return "/resources/images/taglib/other_formats_32px.png";
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
