﻿
using Lib.Common;

using System.Windows;
using System.Windows.Controls;

namespace xtra_snd_tools.Mainshell.Markup
{
	public class ItemContentTemplateSelector : DataTemplateSelector
	{
		public DataTemplate DefaultContentTemplate { get; set; }

		public DataTemplate SelectedItemContentTemplate { get; set; }

		public override DataTemplate SelectTemplate(object item, DependencyObject container)
		{
			DataTemplate selectTemplate;

			if (item is IDownloadViewModel)
			{
				selectTemplate = SelectedItemContentTemplate;
			}
			else
			{
				selectTemplate = base.SelectTemplate(item, container);
			}

			return selectTemplate;
		}
	}
}
