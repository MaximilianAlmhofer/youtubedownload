﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace xtra_snd_tools.Mainshell.Markup
{
	public class RelativeTimedPositionConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				double totalWidth = System.Convert.ToDouble(values[0]);

				TimeSpan position = (TimeSpan)values[1];
				TimeSpan totalDuration = (TimeSpan)values[2];

				double ratio = position / totalDuration;
				double rval = totalWidth * ratio;
				return new Thickness(rval, 0, 0, 0);
			}
			catch
			{
				return default(Thickness);
			}
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
