﻿
using System;
using System.Globalization;
using System.Windows.Data;

using xtra_snd_tools.Modules.Playback.ViewModels;

namespace xtra_snd_tools.Mainshell.Markup
{
	public class SliderConditionBindingConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (values is null)
			{
				return 0;
			}
			if (values[0] == System.Windows.DependencyProperty.UnsetValue || values[1] == System.Windows.DependencyProperty.UnsetValue)
			{
				return 0;
			}
			if (Enum.TryParse<NAudio.Wave.PlaybackState>(values[0].ToString(), out var state) && state == NAudio.Wave.PlaybackState.Stopped)
			{
				return 0;
			}
			return ((PlayerViewModel)values[1]).Position;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			return new object[] { value };
		}
	}
}
