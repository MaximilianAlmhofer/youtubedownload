﻿using System.Windows;
using System.Windows.Controls;

namespace xtra_snd_tools.Mainshell.Markup
{
	public class MediaButtonTemplateSelector : DataTemplateSelector
	{

		public DataTemplate PlayTemplate { get; set; }

		public DataTemplate PauseTemplate { get; set; }

		public override DataTemplate SelectTemplate(object item, DependencyObject container)
		{
			var template = base.SelectTemplate(item, container);

			if (item is Viewbox vb)
			{
				template = /*(MediaState)vb.Tag == MediaState.Play ?*/ PlayTemplate /*: PauseTemplate*/;
			}
			return template;
		}
	}
}
