﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace xtra_snd_tools.Mainshell.Markup
{
	public class DurationConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			TimeSpan t = new TimeSpan(0, 0, 0);

			if (value != null)
			{
				double val = System.Convert.ToDouble(value);

				if (val != double.NaN && TimeSpan.MaxValue.Seconds < val)
					t = TimeSpan.FromSeconds(val);

			}

			return t;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return TimeSpan.Zero;
		}
	}
}
