﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace xtra_snd_tools.Mainshell.Markup
{
	public sealed class TabIndexConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			TabItem tabItem = value as TabItem;
			List<TabItem> list = (from TabItem x in ItemsControl.ItemsControlFromItemContainer(tabItem).ItemContainerGenerator.Items
								  where x.Visibility == Visibility.Visible
								  select x).ToList();
			int count = list.Count;
			int index = list.IndexOf(tabItem);
			if (index == 0)
			{
				return "First";
			}
			if (count - 1 == index)
			{
				return "Last";
			}
			return "";
		}
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return DependencyProperty.UnsetValue;
		}
	}
}
