﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace xtra_snd_tools.Mainshell.Markup
{
	public class ElapsedTimeConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				TimeSpan elapsed = (TimeSpan)values[0];
				TimeSpan total = (TimeSpan)values[1];
				return total.Subtract(elapsed).ToString("hh\\:mm\\:ss");
			}
			catch (Exception)
			{

				return null;
			}
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
