﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace xtra_snd_tools.Mainshell.Markup
{
	public class LevelToIndentConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				int level = (int)value;
				return new Thickness(level * 10, 0, 0, 0);
			}
			catch (Exception)
			{
				return new Thickness(0);
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
