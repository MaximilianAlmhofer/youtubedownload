﻿using Lib.Common;
using Lib.Common.Message;
using Lib.Utility;
using Lib.Utility.Logging;

using MvvmCross.Logging;
using MvvmCross.Plugin.Messenger;

using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;

using xtra_snd_tools.Modules.Downloader.ViewModels;

namespace xtra_snd_tools.Modules.Playback.Views
{
	/// <summary>
	/// Interaktionslogik für PlayerMiniaturView.xaml
	/// </summary>
	public partial class MediaPlayerView : MvvmCross.Platforms.Wpf.Views.MvxWpfView
	{
		MvxSubscriptionToken logSubscription;

		public MediaPlayerView()
		{
			InitializeComponent();
			Loaded += MediaPlayerView_Loaded;

			var messenger = MvvmCross.Mvx.IoCProvider.Resolve<IMvxMessenger>();
			logSubscription = messenger.SubscribeOnMainThread<LogMessage>(message =>
			{
				if (message.LogTo == LogMessage.LogTarget.Console)
				{
					message.LogText.Trace();
				}
				else
				{
					File.WriteAllText($"MediaPlayerView.xaml.log", message.LogText);
				}
			});
		}


		private void Hyperlink_Click(object sender, RoutedEventArgs e)
		{
			if (sender is Hyperlink link)
			{
				if (link.Name == "linkRemote")
				{
					try
					{
						var pinfo = new ProcessStartInfo(@"C:\Program Files\Mozilla Firefox\firefox.exe",
							link.NavigateUri.OriginalString.Replace("http", "https", StringComparison.OrdinalIgnoreCase));
						Process.Start(pinfo);
					}
					catch (Exception ex)
					{
						MessageBox.Show(ex.ToLongString(), ex.GetType().Name);
					}
				}
				else
				{
					try
					{
						Process.Start("explorer.exe", System.IO.Path.GetDirectoryName(link.NavigateUri.LocalPath));
					}
					catch (Exception ex)
					{
						MessageBox.Show(ex.ToLongString(), ex.GetType().Name);
					}
				}
			}
		}


		private void MediaPlayerView_Loaded(object sender, RoutedEventArgs e)
		{
			items.SelectedIndex = 0;
		}


		private void ItemsControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			((IMessagePublisher<MediaItemChangedMessage>)DataContext).Publish(new MediaItemChangedMessage(DataContext, items.SelectedItem as IMediaItem));
		}


		private void Grid_Drop(object sender, DragEventArgs e)
		{
			if (e.Data.GetValidUniformResourceLocator(out Uri downloadSource) == false)
			{
				downloadSource.OriginalString.Trace();
				MessageBox.Show(StringFormatter.Format(Strings.UngueltigeUrl, downloadSource.OriginalString), "Drag Drop", MessageBoxButton.OK, MessageBoxImage.Exclamation);
				return;
			}

			if (DataContext is IDownloadItem viewModel)
			{
				var download = new DownloadViewModel(viewModel, MvvmCross.Mvx.IoCProvider.Resolve<IMvxMessenger>(), MvvmCross.Mvx.IoCProvider.Resolve<ILoggerFactory>());
				
				download.Start(downloadSource);
			}
		}
	}
}
