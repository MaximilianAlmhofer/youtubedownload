﻿using AudioLib.Playback;

using Lib.Common;

using MvvmCross.ViewModels;

using System.Windows;
using System.Windows.Controls;

namespace xtra_snd_tools.Modules.Playback.Views
{
	[MvxViewFor(typeof(IPlayerViewModel))]
	public partial class MiniPlayerView : MvvmCross.Platforms.Wpf.Views.MvxWpfView
	{
		public MiniPlayerView()
		{
			InitializeComponent();
		}

		private void VolumeControl_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			PlaybackEvents.HandleVolumeChanged(DataContext as IPlaybackManager, e);
		}


		private void PositionSlider_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			PlaybackEvents.SeekbarControl_PreviewMouseLeftButtonDown(DataContext as IPlaybackEngine);
		}


		private void PositionSlider_PreviewMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			Slider slider = sender as Slider;
			PlaybackEvents.SeekbarControl_PreviewMouseLeftButtonUp(DataContext as IPlaybackManager, slider.Value, e);
		}


		private void PositionSlider_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			PlaybackEvents.SeekbarControl_MouseDoubleClick(sender, e);
		}
	}
}