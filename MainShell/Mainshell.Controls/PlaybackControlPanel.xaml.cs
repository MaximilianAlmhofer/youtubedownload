﻿using AudioLib.Playback;

using Lib.Common;
using Lib.Common.Interfaces;
using Lib.Utility;

using MvvmCross.ViewModels;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using xtra_snd_tools.Mainshell.ViewModels;

namespace xtra_snd_tools.Mainshell.Controls
{
	[MvxViewFor(typeof(IPlaybackControlPanelViewModel))]
	public partial class PlaybackControlPanel : MvvmCross.Platforms.Wpf.Views.MvxWpfView
	{
		public PlaybackControlPanel()
		{
			InitializeComponent();
			Loaded += PlaybackControlPanel_Loaded;
		}

		private IPlaybackControlPanelViewModel datacontext
		{
			get => DataContext as IPlaybackControlPanelViewModel;
		}

		private void PlaybackControlPanel_Loaded(object sender, RoutedEventArgs e)
		{
			DataContextChanged += PlaybackControlPanel_DataContextChanged;
		}

		private void PlaybackControlPanel_DataContextChanged (object sender, DependencyPropertyChangedEventArgs e)
		{
			if (e.OldValue is null)
			{
				PlayerControlHelper.CreateWaveFormVisualizationElement(datacontext,
				StringFormatter.Format("Polyline WaveForm Visualization"),
				PlayerControl_PropertyChanged);

				SeekbarControl.AddHandler(MouseDoubleClickEvent, (MouseButtonEventHandler)SeekbarControl_MouseDoubleClick);
				SeekbarControl.AddHandler(PreviewMouseLeftButtonDownEvent, (MouseButtonEventHandler)SeekbarControl_PreviewMouseLeftButtonDown);
				SeekbarControl.AddHandler(PreviewMouseLeftButtonUpEvent, (MouseButtonEventHandler)SeekbarControl_PreviewMouseLeftButtonUp);
				VolumeControl.AddHandler(Slider.ValueChangedEvent, (RoutedPropertyChangedEventHandler<double>)VolumeControl_ValueChanged);
			}
		}

		private void PlayerControl_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{

		}


		private void SeekbarControl_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if (Visibility == Visibility.Visible)
			{
				PlaybackEvents.SeekbarControl_MouseDoubleClick(sender, e);
			}
		}


		private void SeekbarControl_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if (Visibility == Visibility.Visible)
			{
				PlaybackEvents.SeekbarControl_PreviewMouseLeftButtonDown(datacontext.Manager as IPlaybackEngine);
			}
		}


		private void SeekbarControl_PreviewMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if (Visibility == Visibility.Visible)
			{
				PlaybackEvents.SeekbarControl_PreviewMouseLeftButtonUp(datacontext.Manager, (sender as Slider).Value, e);
			}
		}


		private void VolumeControl_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			if (Visibility == Visibility.Visible)
			{
				PlaybackEvents.HandleVolumeChanged(datacontext.Manager, e);
			}
		}
	}
}
