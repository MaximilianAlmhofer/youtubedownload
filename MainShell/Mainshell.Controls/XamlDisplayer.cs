﻿using ShowMeTheXAML;

using System.Windows;
using System.Windows.Controls;

namespace xtra_snd_tools.Mainshell.Controls
{
	public sealed class XamlDisplayer : ShowMeTheXAML.XamlDisplay
	{
		public static readonly DependencyProperty ButtonDockProperty;

		static XamlDisplayer()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(XamlDisplayer), new FrameworkPropertyMetadata(typeof(XamlDisplayer)));
			ButtonDockProperty = DependencyProperty.Register(nameof(ButtonDock), typeof(Dock), typeof(XamlDisplayer), new PropertyMetadata(default(Dock)));
		}

		public Dock ButtonDock
		{
			get => (Dock)GetValue(ButtonDockProperty);
			set => SetValue(ButtonDockProperty, value);
		}
	}
}
