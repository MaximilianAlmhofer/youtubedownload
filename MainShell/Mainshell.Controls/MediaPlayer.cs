﻿using Lib.Common;

using MvvmCross.Commands;
using MvvmCross.Platforms.Wpf.Presenters.Attributes;
using MvvmCross.ViewModels;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using Lib.Logging;
using xtra_snd_tools.Modules.Playback.Views;
using xtra_snd_tools.Modules.Playback.ViewModels;

namespace xtra_snd_tools.Mainshell.Controls
{

	[TemplatePart(Name = VOLUMEBUTTON_PARTNAME, Type = typeof(Slider))]
	[TemplatePart(Name = PLAYBUTTON_PARTNAME, Type = typeof(Button))]
	[MvxContentPresentation(WindowIdentifier = nameof(MiniPlayerView), StackNavigation = true)]
	public sealed class MediaPlayer : MvvmCross.Platforms.Wpf.Views.MvxWpfView
	{
		const string PLAYBUTTON_PARTNAME = "PART_PlayButton";
		const string VOLUMEBUTTON_PARTNAME = "PART_VolumeButton";

		private Button playButton;
		private Slider volumeSlider;

		private PlayerViewModel viewModel;

		private static readonly object syncLock = new object();

		private readonly CommandBindingCollection mediaCommandBindings = new CommandBindingCollection();

		static MediaPlayer()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(MediaPlayer), new FrameworkPropertyMetadata(typeof(MediaPlayer)));
		}

		public MediaPlayer()
		{
			SetupMediaCommands();
		}

		private void SetupMediaCommands()
		{

			CommandBinding bindingPlay = new CommandBinding(MediaCommands.Play, new ExecutedRoutedEventHandler(MediaCommands_Executed), new CanExecuteRoutedEventHandler(CanExecute_MediaCommand));
			CommandBinding bindingPause = new CommandBinding(MediaCommands.Pause, new ExecutedRoutedEventHandler(MediaCommands_Executed), new CanExecuteRoutedEventHandler(CanExecute_MediaCommand));
			CommandBinding bindingSkipFwd = new CommandBinding(MediaCommands.NextTrack, new ExecutedRoutedEventHandler(MediaCommands_Executed), new CanExecuteRoutedEventHandler(CanExecute_MediaCommand));
			CommandBinding bindingSkipPrev = new CommandBinding(MediaCommands.PreviousTrack, new ExecutedRoutedEventHandler(MediaCommands_Executed), new CanExecuteRoutedEventHandler(CanExecute_MediaCommand));
			CommandBinding bindingMute = new CommandBinding(MediaCommands.MuteVolume, new ExecutedRoutedEventHandler(MediaCommands_Executed), new CanExecuteRoutedEventHandler(CanExecute_MediaCommand));
			mediaCommandBindings.AddRange(new[] { bindingPlay, bindingPause, bindingSkipFwd, bindingSkipPrev, bindingMute });
			CommandBindings.AddRange(mediaCommandBindings);
		}

		private static void MediaCommands_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			if (sender is MediaPlayer player)
			{
				player.HandleCommand(e.Command);
			}
		}

		private void HandleCommand(ICommand command)
		{
			if (viewModel is IPlaybackEngine)
			{
				if (command == MediaCommands.Play)
				{
					playback.Play();
				}
				else
				if (command == MediaCommands.Pause)
				{
					playback.Pause();
				}
				else
				if (command == MediaCommands.NextTrack)
				{
					playback.NextTrack();
				}
				else
				if (command == MediaCommands.PreviousTrack)
				{
					playback.PreviousTrack();
				}
				else
				if (command == MediaCommands.MuteVolume)
				{
					playback.Mute();
				}
			}
		}

		private static void CanExecute_MediaCommand(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = true;
		}

		private IMvxCommand playPauseCommand;
		public IMvxCommand PlayPauseCommand => playPauseCommand ?? (playPauseCommand = new MvxCommand(() =>
														 {
															 ICommand command = (DataContext as IPlayerViewModel).PlaybackState == NAudio.Wave.PlaybackState.Playing ? mediaCommandBindings[1].Command : mediaCommandBindings[0].Command;
															 command.Execute(null);
															 playButton.ContentTemplate = playButton.ContentTemplateSelector?.SelectTemplate(playButton.Content, playButton.FindVisualChild<ContentPresenter>());
														 }));

		public override async void OnApplyTemplate()
		{
			base.OnApplyTemplate();

			playButton = (Button)GetTemplateChild(PLAYBUTTON_PARTNAME);
			volumeSlider = (Slider)GetTemplateChild(VOLUMEBUTTON_PARTNAME);

			if (DataContext is MvxViewModel<PlayerParameters> _viewModel)
			{
				viewModel = _viewModel as PlayerViewModel;
				PolylineWaveFormVisualization visualization = new PolylineWaveFormVisualization(new PolylineWaveFormControl(), "Polyline WaveForm Visualization");
				viewModel.Prepare(new PlayerParameters(visualization, new AudioPlaybackPlugin(visualization)));
				await viewModel.Initialize();
			}
		}
	}
}
