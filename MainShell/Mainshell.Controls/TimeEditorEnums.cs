﻿namespace xtra_snd_tools.Mainshell.Controls
{
	/// <summary>
	/// An action performed on a TimeEditor.
	/// </summary>
	public enum TimeEditorAction
	{
		/// <summary>
		/// No action performed
		/// </summary>
		None,

		/// <summary>
		/// Up button pressed.
		/// </summary>
		UpPressed,

		/// <summary>
		/// Down button pressed
		/// </summary>
		DownPressed,

		/// <summary>
		/// A field value was entered.
		/// </summary>
		ValueEntered
	}

	/// <summary>
	/// Fields of a TimeEditor
	/// </summary>
	public enum TimeEditorField
	{
		/// <summary>
		/// No field.
		/// </summary>
		None,

		/// <summary>
		/// The hours field.
		/// </summary>
		Hours,

		/// <summary>
		/// The minutes field.
		/// </summary>
		Minutes,

		/// <summary>
		/// The seconds field.
		/// </summary>
		Seconds
	}
}
