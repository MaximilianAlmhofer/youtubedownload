﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace xtra_snd_tools.Mainshell.Controls
{

	[TemplatePart(Name = "PART_Content", Type = typeof(FrameworkElement))]
	public class Minimap : Slider
	{
		private FrameworkElement contentP;

		static Minimap()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(Minimap), new FrameworkPropertyMetadata(typeof(Minimap)));

			ContentProperty = DependencyProperty.Register(nameof(Content), typeof(object), typeof(Minimap),
				new PropertyMetadata(null, new PropertyChangedCallback(ContentChanged)));

			ContentHeightProperty = DependencyProperty.Register(nameof(ContentHeight), typeof(double), typeof(Minimap),
				new PropertyMetadata(0D, new PropertyChangedCallback(ContentHeightChanged)));

			ContentWidthProperty = DependencyProperty.Register(nameof(ContentWidth), typeof(double), typeof(Minimap),
				new PropertyMetadata(0D, new PropertyChangedCallback(ContentWidthChanged)));
		}


		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();

			contentP = (FrameworkElement)GetTemplateChild("PART_Content");
		}

		private static void ContentWidthChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{

		}

		private static void ContentHeightChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{

		}

		private static void ContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{

		}

		public static readonly DependencyProperty ContentProperty;

		public static readonly DependencyProperty ContentHeightProperty;

		public static readonly DependencyProperty ContentWidthProperty;

		public double ContentHeight
		{
			get => (double)GetValue(ContentHeightProperty);
			set => SetValue(ContentHeightProperty, value);
		}

		public double ContentWidth
		{
			get => (double)GetValue(ContentWidthProperty);
			set => SetValue(ContentWidthProperty, value);
		}

		public object Content
		{
			get => GetValue(ContentProperty);
			set => SetValue(ContentProperty, value);
		}
	}
}
