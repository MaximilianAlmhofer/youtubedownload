﻿
using Lib.Common;
using Lib.Utility;

using MvvmCross.ViewModels;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

using xtra_snd_tools.Modules.AudioVisualization;
using xtra_snd_tools.Modules.AudioVisualization.Controls;

namespace xtra_snd_tools.Mainshell.Controls
{
	[TemplatePart(Name = "PART_PositionControllerArea", Type = typeof(Grid))]
	[TemplatePart(Name = "PART_VolumeControllerArea", Type = typeof(Grid))]
	public class TimelineController : Control
	{
		private IInputElement positionControllerInputArea;
		private Grid volumeControllerGrid;
		private ToggleButton templateToggler;
		private ContentControl positionControllerContent, volumeControllerContent;

		public static readonly DependencyProperty PositionControllerProperty;

		public static readonly DependencyProperty VolumeControllerProperty;

		public static readonly DependencyProperty PositionControllerTemplateProperty;

		public static readonly DependencyProperty VolumeControllerTemplateProperty;

		public static readonly DependencyProperty PositionControllerTemplateSelectorProperty;

		public static readonly DependencyProperty VolumeControllerTemplateSelectorProperty;

		public object PositionController
		{
			get => GetValue(PositionControllerProperty);
			set => SetValue(PositionControllerProperty, value);
		}

		public object VolumeController
		{
			get => GetValue(VolumeControllerProperty);
			set => SetValue(VolumeControllerProperty, value);
		}

		public DataTemplate PositionControllerTemplate
		{
			get => (DataTemplate)GetValue(PositionControllerTemplateProperty);
			set => SetValue(PositionControllerTemplateProperty, value);
		}

		public DataTemplate VolumeControllerTemplate
		{
			get => (DataTemplate)GetValue(VolumeControllerTemplateProperty);
			set => SetValue(VolumeControllerTemplateProperty, value);
		}

		public DataTemplateSelector PositionControllerTemplateSelector
		{
			get => (DataTemplateSelector)GetValue(PositionControllerTemplateSelectorProperty);
			set => SetValue(PositionControllerTemplateSelectorProperty, value);
		}

		public DataTemplateSelector VolumeControllerTemplateSelector
		{
			get => (DataTemplateSelector)GetValue(VolumeControllerTemplateSelectorProperty);
			set => SetValue(VolumeControllerTemplateSelectorProperty, value);
		}

		static TimelineController()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(TimelineController), new FrameworkPropertyMetadata(typeof(TimelineController)));

			PositionControllerTemplateSelectorProperty = DependencyProperty.Register(nameof(PositionControllerTemplateSelector), typeof(DataTemplateSelector), typeof(TimelineController));
			VolumeControllerTemplateSelectorProperty = DependencyProperty.Register(nameof(VolumeControllerTemplateSelector), typeof(DataTemplateSelector), typeof(TimelineController));

			PositionControllerTemplateProperty = DependencyProperty.Register(nameof(PositionControllerTemplate), typeof(DataTemplate), typeof(TimelineController),
				new UIPropertyMetadata(null, new PropertyChangedCallback(PositionControllerTemplateChanged)));
			VolumeControllerTemplateProperty = DependencyProperty.Register(nameof(VolumeControllerTemplate), typeof(DataTemplate), typeof(TimelineController),
				new UIPropertyMetadata(null, new PropertyChangedCallback(VolumeControllerTemplateChanged)));

			PositionControllerProperty = DependencyProperty.Register(nameof(PositionController), typeof(object), typeof(TimelineController),
				new UIPropertyMetadata(null, new PropertyChangedCallback(PositionControllerChanged)));
			VolumeControllerProperty = DependencyProperty.Register(nameof(VolumeController), typeof(object), typeof(TimelineController),
				new UIPropertyMetadata(null, new PropertyChangedCallback(VolumeControllerChanged)));
		}



		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();

			volumeControllerGrid = GetTemplateChild("PART_VolumeControllerArea") as Grid;
			positionControllerInputArea = GetTemplateChild("PART_PositionControllerArea") as Grid;
			if (positionControllerInputArea is null)
			{
				positionControllerInputArea = LogicalTreeHelper.FindLogicalNode(this.FindNextVisualChild<Grid>(), "PART_PositionControllerArea") as Grid;
			}

			positionControllerContent = (positionControllerInputArea as Grid)?.FindNextVisualChild<ContentControl>();
			templateToggler = ((positionControllerContent as HeaderedContentControl)?.Header as WrapPanel)?.FindNextVisualChild<ToggleButton>();
			if (templateToggler != null)
			{
				templateToggler.Click += TemplateToggler_Checked;
			}
			volumeControllerContent = volumeControllerGrid.FindNextVisualChild<ContentControl>();
		}



		private static void VolumeControllerChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			if (d is TimelineController t)
			{
			}
		}


		private static void PositionControllerChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			if (d is TimelineController t)
			{
			}
		}


		private void PlaybackView_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "Position" && (DataContext as IPlayerViewModel).CurrentTime.Seconds == 0)
			{
				(PositionController as RangeBase)?.SetValue(RangeBase.ValueProperty, 0);
			}
		}

		private static void VolumeControllerTemplateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var t = d as TimelineController;
			t.volumeControllerContent.SetValue(ContentControl.ContentTemplateProperty, t.VolumeControllerTemplate);
			d.SetValue(PositionControllerProperty, ((DataTemplate)e.NewValue).LoadContent());
		}


		private static void PositionControllerTemplateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var t = d as TimelineController;
			t.positionControllerContent.SetValue(ContentControl.ContentTemplateProperty, t.PositionControllerTemplate);
			d.SetValue(PositionControllerProperty, ((DataTemplate)e.NewValue).LoadContent());
		}


		private bool IsInputOverPositionSlider(Point hitPoint)
		{
			if (positionControllerInputArea is Grid positionController)
				return positionController.ActualWidth >= hitPoint.X;
			return false;
		}



		private void TemplateToggler_Checked(object sender, RoutedEventArgs e)
		{
			if (!templateToggler.IsChecked.GetValueOrDefault())
			{
				(DataContext as IAudioVisualization)?.Visualizations.Clear();
			}
			else if ((DataContext as IAudioVisualization)?.Visualizations.Count == 0)
			{
				ContentPresenter cp = positionControllerContent.Content as ContentPresenter;
				PolylineWaveFormVisualization visualization = new PolylineWaveFormVisualization(new PolylineWaveFormControl(), "Polyline WaveForm Visualization");
				var parameters = new PlayerParameters(visualization, new AudioPlaybackPlugin(visualization));
				(DataContext as IPlayerViewModel)?.Prepare(parameters);
				(DataContext as IMvxNotifyPropertyChanged).PropertyChanged += PlaybackView_PropertyChanged;
			}
			PositionControllerTemplate = PositionControllerTemplateSelector.SelectTemplate(DataContext, positionControllerContent.Content as ContentPresenter);
		}
	}
}
