﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace xtra_snd_tools.Mainshell.Controls
{

	[TemplatePart(Name = "PART_ToggleEditModeSwitch", Type = typeof(ToggleButton))]
	[TemplateVisualState(GroupName = "EditState")]
	public class EditLabel : Label
	{
		private ToggleButton toggleModeSwitch;

		public static readonly DependencyProperty ApplyButtonTemplateProperty;

		public static readonly DependencyProperty EditButtonTemplateProperty;

		public static readonly DependencyProperty ReadOnlyContentTemplateProperty;

		public static readonly DependencyProperty ReadWriteContentTemplateProperty;

		public static readonly DependencyProperty InlineButtonStyleProperty;

		public static readonly DependencyProperty IsEditingProperty;

		public static readonly DependencyPropertyKey IsEditingKey;

		public static readonly RoutedEvent EditEvent;

		public static readonly RoutedEvent CommitEvent;

		public DataTemplate ReadOnlyContentTemplate
		{
			get => (DataTemplate)GetValue(ReadOnlyContentTemplateProperty);
			set => SetValue(ReadOnlyContentTemplateProperty, value);
		}

		public DataTemplate ReadWriteContentTemplate
		{
			get => (DataTemplate)GetValue(ReadWriteContentTemplateProperty);
			set => SetValue(ReadWriteContentTemplateProperty, value);
		}

		public DataTemplate ApplyButtonTemplate
		{
			get => (DataTemplate)GetValue(ApplyButtonTemplateProperty);
			set => SetValue(ApplyButtonTemplateProperty, value);
		}

		public DataTemplate EditButtonTemplate
		{
			get => (DataTemplate)GetValue(EditButtonTemplateProperty);
			set => SetValue(EditButtonTemplateProperty, value);
		}

		public Style InlineButtonStyle
		{
			get => (Style)GetValue(InlineButtonStyleProperty);
			set => SetValue(InlineButtonStyleProperty, value);
		}

		public bool IsEditing
		{
			get => (bool)GetValue(IsEditingProperty);
			private set => SetValue(IsEditingKey, value);
		}

		public event RoutedEventHandler Commit
		{
			add => AddHandler(CommitEvent, value);
			remove => RemoveHandler(CommitEvent, value);
		}

		public event RoutedEventHandler Edit
		{
			add => AddHandler(EditEvent, value);
			remove => RemoveHandler(EditEvent, value);
		}

		static EditLabel()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(EditLabel), new FrameworkPropertyMetadata(typeof(EditLabel)));
			InlineButtonStyleProperty = DependencyProperty.Register(nameof(InlineButtonStyle), typeof(Style), typeof(EditLabel), new PropertyMetadata());
			ReadOnlyContentTemplateProperty = DependencyProperty.Register(nameof(ReadOnlyContentTemplate), typeof(DataTemplate), typeof(EditLabel), new UIPropertyMetadata());
			ReadWriteContentTemplateProperty = DependencyProperty.Register(nameof(ReadWriteContentTemplate), typeof(DataTemplate), typeof(EditLabel), new UIPropertyMetadata());
			ApplyButtonTemplateProperty = DependencyProperty.Register(nameof(ApplyButtonTemplate), typeof(DataTemplate), typeof(EditLabel), new UIPropertyMetadata());
			EditButtonTemplateProperty = DependencyProperty.Register(nameof(EditButtonTemplate), typeof(DataTemplate), typeof(EditLabel), new UIPropertyMetadata());
			EditEvent = EventManager.RegisterRoutedEvent(nameof(Edit), RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(EditLabel));
			CommitEvent = EventManager.RegisterRoutedEvent(nameof(Commit), RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(EditLabel));
			IsEditingKey = DependencyProperty.RegisterReadOnly(nameof(IsEditing), typeof(bool), typeof(EditLabel), new UIPropertyMetadata());
			IsEditingProperty = IsEditingKey.DependencyProperty;
		}

		public EditLabel() : base() { }

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();
			toggleModeSwitch = GetTemplateChild("PART_ToggleEditModeSwitch") as ToggleButton;
			toggleModeSwitch.Checked += ToggleModeSwitch_Checked;
			toggleModeSwitch.Unchecked += ToggleModeSwitch_Unchecked;
		}

		private void ToggleModeSwitch_Unchecked(object sender, RoutedEventArgs e)
		{
			ToggleButton_OnCheckStateChanged(sender, e);
		}

		private void ToggleModeSwitch_Checked(object sender, RoutedEventArgs e)
		{
			ToggleButton_OnCheckStateChanged(sender, e);
		}

		private static void ToggleButton_OnCheckStateChanged(object sender, RoutedEventArgs e)
		{
			if ((sender as ToggleButton)?.TemplatedParent is EditLabel d)
			{
				switch (e.RoutedEvent.Name)
				{
					case "Checked":
						d.IsEditing = true;
						e.RoutedEvent = EditLabel.EditEvent;
						break;

					case "Unchecked":
						d.IsEditing = false;
						e.RoutedEvent = EditLabel.CommitEvent;
						break;
				}

				e.Source = d;
				d.RaiseEvent(e);
			}
		}
	}
}
