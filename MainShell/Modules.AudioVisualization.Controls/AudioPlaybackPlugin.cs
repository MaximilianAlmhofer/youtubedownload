﻿
using AudioLib.Visualization;

using Lib.Common;

using MvvmCross.Base;
using MvvmCross.Platforms.Wpf.Views;

using System.Composition;
using xtra_snd_tools.Mainshell.Views;

namespace xtra_snd_tools.Modules.AudioVisualization.Controls
{
	[Export(typeof(IModuleFrame<IPlayerViewModel>))]
	class AudioPlaybackPlugin : IModuleFrame<IPlayerViewModel>
	{
		private AudioPlaybackView view;
		private IPlayerViewModel viewModel;
		private IVisualizationPlugin visualization;

		[ImportingConstructor]
		public AudioPlaybackPlugin([Import("VisualLib.IVisualizationPlugin")] IVisualizationPlugin visualization)
		{
			this.visualization = visualization;
		}

		public string Name => "Audio Playback";

		public MvxWpfView UserInterface => view;

		public void CreateView(IPlayerViewModel viewmodel)
		{
			view = new AudioPlaybackView();
			viewModel = viewmodel;
			view.DataContext = viewModel;
		}

		public void Deactivate()
		{
			viewModel.DisposeIfDisposable();
			view = null;
		}
	}
}
