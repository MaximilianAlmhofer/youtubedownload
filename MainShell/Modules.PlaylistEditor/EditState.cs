﻿namespace xtra_snd_tools.Modules.PlaylistEditor
{
	public enum EditState
	{
		Empty,
		EditValue,
		DisplayValue
	}
}
