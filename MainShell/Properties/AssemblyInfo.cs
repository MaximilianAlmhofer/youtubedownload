using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Markup;

[assembly: AssemblyProduct("xtra_snd_tools")]
[assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)]
[assembly: AssemblyCopyright("Copyright © Max Almhofer 2019 - 2020 | some parts are written by other developers and reused. They'r legals should be found in the directory outter Solution level and someone other than you could")]
[assembly: AssemblyVersion("2.0.0.1")]
[assembly: AssemblyInformationalVersion("2.0.1")]
[assembly: AssemblyFileVersion("200.2000.0.1")]
[assembly: ComVisible(true)]
#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif