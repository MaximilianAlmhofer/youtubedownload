﻿using Lib.Common;
using Lib.WindowsLogin;

using MvvmCross.Platforms.Wpf.Presenters.Attributes;

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;

namespace xtra_snd_tools.Mainshell.Views
{
	/// <summary>
	/// Interaktionslogik für LoginPrompt.xaml
	/// </summary>
	[MvxWindowPresentation(Identifier = "LoginPrompt", Modal = true, ViewModelType = typeof(ILoginViewModel), ViewType = typeof(LoginModal))]
	public partial class LoginModal : Window, IDisposable
	{
		public LoginModal()
		{
			InitializeComponent();
			WindowStartupLocation = WindowStartupLocation.CenterScreen;
			DataContext = MvvmCross.Mvx.IoCProvider.Resolve<ILoginViewModel>();
			txtMail.Focus();

#if DEBUG
			if (DataContext is ILoginViewModel viewModel)
			{
				viewModel.EmailAddress = "almho";
				viewModel.Password = "iOcNQkHwqK12345";
				txtMail.Text = viewModel.EmailAddress;
				passBox.Password = viewModel.Password;
			}
			passBox.SelectAll();
			btnLogin.Focus();
#endif
		}

		public bool LoginSuccess { get; private set; }

		public bool Canceled { get; private set; }


		private async void BtnLogin_Click(object sender, RoutedEventArgs e)
		{
#if DEBUG
			Lib.Utility.StringFormatter.Format("{0}\r\n---\r\nUser-Textbox-Text: {1}\r\nPasswordBox-Password: {2}", nameof(BtnLogin_Click), txtMail.Text, passBox.Password).Trace();
#endif
			if (DataContext is ILoginViewModel vm)
			{
				if (!string.IsNullOrEmpty(vm.EmailAddress))
				{
					try
					{
						await LoginAsync().ConfigureAwait(false);
					}
					catch (WindowsLogonException ex)
					{
						MessageBox.Show(ex.Message, Debugger.IsAttached ? ex.GetType().Name : "Login fehlgeschlagen",
							MessageBoxButton.OK, MessageBoxImage.Error);
					}

					if (LoginSuccess)
					{
						DialogResult = LoginSuccess;
						Close();
					}
					else
					{
						passBox.Clear();
						txtMail.SelectAll();
					}
				}
			}
		}


		private void BtnCancel_Click(object sender, RoutedEventArgs e)
		{
			LoginSuccess = false;
			Canceled = true;
			Close();
		}


		protected override void OnClosing(CancelEventArgs e)
		{
			base.OnClosing(e);
			Canceled = !LoginSuccess;
		}


		private void TxtMail_LostFocus(object sender, RoutedEventArgs e)
		{
			if (DataContext is ILoginViewModel vm)
				vm.EmailAddress = txtMail.Text;
		}


		private void PassBox_LostFocus(object sender, RoutedEventArgs e)
		{
			if (DataContext is ILoginViewModel vm)
				vm.Password = passBox.Password;
		}


		internal async Task LoginAsync()
		{
			if (DataContext is ILoginViewModel vm)
				LoginSuccess = await vm.LoginAsync().ConfigureAwait(false);
		}


		#region IDisposable Support
		private bool disposedValue = false;


		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
				}
				disposedValue = true;
			}
		}


		// Dieser Code wird hinzugefügt, um das Dispose-Muster richtig zu implementieren.
		public void Dispose()
		{
			// Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(bool disposing) weiter oben ein.
			Dispose(true);
			// TODO: Auskommentierung der folgenden Zeile aufheben, wenn der Finalizer weiter oben überschrieben wird.
			// GC.SuppressFinalize(this);
		}


		#endregion
	}
}
