﻿using Lib.Common;

using MvvmCross.ViewModels;

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

using xtra_snd_tools.Mainshell.ViewModels;

namespace xtra_snd_tools.Mainshell.Views
{
	[MvxViewFor(typeof(IHomeViewModel))]
	public partial class HomeView : MvvmCross.Platforms.Wpf.Views.MvxWpfView
	{
		private IHomeViewModel homeViewModel;
		private IPlaylistEditorViewModel editorViewModel;

		public HomeView() : base()
		{
			InitializeComponent();
			Loaded += HomeView_Loaded;
		}

		public new IMvxViewModel ViewModel
		{
			get
			{
				if ((base.ViewModel as IPlaylistEditorViewModel) != null)
				{
					editorViewModel = (IPlaylistEditorViewModel)base.ViewModel;
				}

				return editorViewModel;
			}
			set
			{
				if (value is IPlaylistEditorViewModel editorModel)
				{
					base.ViewModel = editorViewModel = editorModel;
				}
			}
		}

		public new object DataContext
		{
			get
			{
				if (base.DataContext is IHomeViewModel _homeViewModel)
				{
					homeViewModel = _homeViewModel;
				}

				return homeViewModel;
			}
			set
			{
				if (value is IHomeViewModel _homeViewModel)
				{
					base.DataContext = homeViewModel = _homeViewModel;
				}
			}
		}

		#region Attached Property
		public static readonly DependencyProperty IsExpandedProperty =
			DependencyProperty.RegisterAttached("IsExpanded",
				typeof(bool),
				typeof(HomeView),
				new FrameworkPropertyMetadata(false));

		public static bool GetIsExpanded(UIElement element)
		{
			element.SetValue(IsExpandedProperty, (element.GetValue(DataGrid.SelectedItemProperty) as DataGridHierarchialDataModel).IsExpanded);
			return (bool)element.GetValue(IsExpandedProperty);
		}

		public static void SetIsExpanded(UIElement element, bool value)
		{
			var dc = element.GetValue(DataGrid.SelectedItemProperty) as DataGridHierarchialDataModel;
			dc.IsExpanded = value;
			element.SetValue(IsExpandedProperty, dc.IsExpanded);
		}

		public bool IsExpanded
		{
			get => GetIsExpanded(dataGrid);
			set
			{
				SetIsExpanded(dataGrid, value);
			}
		}
		#endregion

		#region EventHandlers
		private async void HomeView_Loaded(object sender, RoutedEventArgs e)
		{
			homeViewModel = DataContext as IHomeViewModel;
			ViewModel = homeViewModel.PlaylistEditorViewModel;
			BindingContext.DataContext = homeViewModel;
			if (ViewModel.InitializeTask is null)
			{
				ViewModel.InitializeTask = MvxNotifyTask.Create(Task.Run(() =>
				{
					editorViewModel.DataGridSource = new DataGridHierarchialData(Dispatcher, new WeakReference<IMvxViewModel>(homeViewModel, true));
				}), ex =>
				{
					MessageBox.Show(ex.Message);
				});
			}
			else
			{
				await editorViewModel.InitializeTask.Task;
			}
		}

		private void Expander_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if (((DataGridHierarchialData)homeViewModel.PlaylistEditorViewModel.DataGridSource).Any(x => x.Level == 1))
			{
				((DataGridHierarchialData)homeViewModel.PlaylistEditorViewModel.DataGridSource).Where(x => x.Level == 1).ToList().ForEach(x =>
				{
					x.IsVisible = false;
					homeViewModel.PlaylistEditorViewModel.DataGridSource.RemoveChildrenFromItems(x.Parent);
				});
			}
		}

		private void Expander_Unchecked(object sender, RoutedEventArgs e)
		{
			if (dataGrid.CurrentItem is IHierarchic h)
			{
				if (h.HasChildren && h.FirstChild.Level == 1)
				{
					homeViewModel.PlaylistEditorViewModel.DataGridSource.RemoveChildrenFromItems(h);
				}
			}
		}
		#endregion
	}
}
