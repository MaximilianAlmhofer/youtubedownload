﻿using Lib.Common;
using Lib.Common.Message;

using Modules.Playback;

using MvvmCross.Plugin.Messenger;

using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

using xtra_snd_tools.Common.Contracts.ViewModels;

namespace xtra_snd_tools.Modules.Playback.ViewModels
{
	public class TrackListViewModel : ViewModelBase, ITrackListViewModel, IDownloadItem
	{
		private readonly ILocalTracksRepository repository;
		private ObservableCollection<IDownloadViewModel> activeDownloads;

		public TrackListViewModel(IMvxMessenger messenger, ILocalTracksRepository repository) : base(messenger)
		{
			this.repository = repository;
			this.repository.LoadLazy(this);
			this.SubscribeToNewDownloadMessage(HandleNewDownloadMessage);
			this.SubscribeToNewListDownloadMessage(HandleNewListDownloadMessage);
		}


		public ObservableCollection<IDownloadViewModel> ActiveDownloads
		{
			get
			{
				if (activeDownloads is null)
				{
					activeDownloads = new ObservableCollection<IDownloadViewModel>();
					activeDownloads.CollectionChanged += (sender, e) =>
					{
						RaisePropertyChanged(nameof(ActiveDownloads));
					};
				}
				return activeDownloads;
			}
		}


		public IPlaybackList Tracks
		{
			get
			{
				return (IPlaybackList)this.repository.Items;
			}
		}


		public IMediaItem Current
		{
			get
			{
				return Tracks.Current;
			}
		}


		private string _consoleText = "";
		public string ConsoleText
		{
			get => _consoleText;
			set
			{
				_consoleText = string.Join("\r\n", _consoleText, value);
				RaisePropertyChanged();
			}
		}



		public void Publish(MediaItemChangedMessage message = null)
		{
			if (message is null)
				message = new MediaItemChangedMessage(this, Current);
			messenger.Publish<MediaItemChangedMessage>(message);
		}

		private void HandleNewDownloadMessage(NewDownloadMessage obj)
		{
			Tracks.Insert(0, new MediaItem(obj.StorageItem, this, obj.Sender));
			RaisePropertyChanged(nameof(Tracks));
		}

		private void HandleNewListDownloadMessage(NewListDownloadMessage obj)
		{
			Tracks.Insert(0, new MediaItems(obj.Playlist, this, obj.Sender));
			RaisePropertyChanged(nameof(Tracks));
		}

		public async Task<StorageActionResult> RequestDeleteAsync(bool keepManifest = false, params IMediaItem[] itemsToDelete)
		{
			foreach (var item in itemsToDelete)
				Tracks.Remove(item);

			var returnTask = repository.DeleteAsync(keepManifest, itemsToDelete.Select(x => x.GetStorageItem()).ToArray());
			await returnTask.ContinueWith(async task => await RaiseAllPropertiesChanged());

			return await returnTask;
		}
	}
}
