﻿using AudioLib.Playback;
using AudioLib.Visualization;

using Lib.Common;
using Lib.Common.Message;
using Lib.Utility.Logging;

using MediaCore;

using Modules.Playback;

using MvvmCross.Base;
using MvvmCross.Commands;
using MvvmCross.Plugin.Messenger;

using NAudio.Wave;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using xtra_snd_tools.Common.Contracts.ViewModels;
using xtra_snd_tools.Infra.Services;
using xtra_snd_tools.Modules.AudioVisualization;

namespace xtra_snd_tools.Modules.Playback.ViewModels
{

	public sealed class PlayerViewModel : ViewModelBase<PlayerParameters>, IPlayerViewModel, IAudioController, IAudioVisualization
	{

		private IVisualizationPlugin selectedVisualization;
		private List<IVisualizationPlugin> visualizations;

		private Single tempVolume = 0;
		private PlaybackEngine engine;

		private static readonly object lockToken = new LockCookie();

		private Uri playPauseImageSource = new Uri(xtra_snd_tools.Properties.Resources.Pause_Dark, UriKind.Relative);

		public PlayerViewModel(IMvxMessenger messenger, ILocalTracksRepository repository, ILoggerFactory loggerFactory) : base(messenger)
		{
			//repository.LoadLazy(this);
			this.engine = new PlaybackEngine(repository, new SelfContainedKeyStorage<string, NAudioEngine>(loggerFactory), PropertyChangedCallback);
			this.visualizations = new List<IVisualizationPlugin>();
			this.SubscribeToMediaItemChangedMessage(HandleMediaItemChangedMessage);
		}

		#region Overrides, initialization methods + core logic entrypoint

		public override void Prepare(PlayerParameters parameter)
		{
			if (parameter != null && !visualizations.Contains(parameter.VisualizationPlugin))
			{
				parameter.PlaybackModule.CreateView(this);
				visualizations.Add(parameter.VisualizationPlugin);
				this.SelectedVisualization = visualizations.Last();
			}
		}
		#endregion

		#region Properties
		public object LockToken 
		{
			get => lockToken;
		}

		public string Title 
		{
			get => MediaItem?.GetStorageItem()?.Title; 
		}
		public PlaybackState PlaybackState
		{
			get
			{
				return engine.AudioPlayback?.PlaybackState ?? PlaybackState.Stopped;
			}
		}

		public IPlaybackManager Manager => engine;

		public Uri PlayPauseImageSource
		{
			get => playPauseImageSource;
			set
			{
				playPauseImageSource = value;
				RaisePropertyChanged();
			}
		}

		public Single Volume
		{
			get
			{
				return engine.AudioPlayback?.Volume ?? 0.5F;
			}
			set
			{
				engine.AudioPlayback.Volume = value;
				RaisePropertyChanged();
			}
		}

		private double balance;
		public double Balance
		{
			get
			{
				return balance;
			}

			set
			{
				if (balance != value)
				{
					balance = value;
				}
				RaisePropertyChanged();
			}
		}

		public TimeSpan CurrentTime
		{
			get
			{
				return engine.AudioPlayback?.CurrentTime ?? TimeSpan.Zero;
			}
		}

		public TimeSpan NaturalDuration
		{
			get
			{
				return engine.AudioPlayback?.TotalTime ?? TimeSpan.Zero;
			}
		}

		private bool isMuted;
		public bool IsMuted
		{
			get
			{
				return isMuted;
			}

			private set
			{
				if (isMuted != value)
				{
					isMuted = value;
					RaisePropertyChanged();
				}
			}
		}

		private float speedRatio = 1F;
		public float SpeedRatio
		{
			get
			{
				return speedRatio;
			}

			set
			{
				if (speedRatio != value)
				{
					speedRatio = Math.Min(Math.Max(0, value), 1);
					RaisePropertyChanged();
				}
			}
		}

		private double lengthInSeconds;
		public double LengthInSeconds
		{
			get
			{
				return lengthInSeconds;
			}
			set
			{
				if (lengthInSeconds != value)
				{
					lengthInSeconds = value;
					RaisePropertyChanged();
				}
			}
		}

		private double position;
		public double Position
		{
			get
			{
				return position;
			}
			set
			{
				position = value;
				RaisePropertyChanged();
			}
		}

		private bool isShuffle;
		public bool IsShuffle
		{
			get => isShuffle;
			set
			{
				if (isShuffle != value)
				{
					isShuffle = value;
					RaisePropertyChanged();
				}
			}
		}
		#endregion

		#region Messagehandlers
		private void PropertyChangedCallback()
		{
			this.AsyncDispatcher.ExecuteOnMainThreadAsync(async () => await RaiseAllPropertiesChanged(), true);
		}
		private void HandleMediaItemChangedMessage(MediaItemChangedMessage obj)
		{
			if (PlaybackState != NAudio.Wave.PlaybackState.Playing)
			{
				Manager.SelectTrack(obj.MediaItem);
			}
		}
		#endregion

		#region AudioController-Commands
		private IMvxCommand playPauseCommand;
		/// <summary>
		/// Command to start the playback.
		/// </summary>
		public IMvxCommand PlayPauseCommand
		{
			get
			{
				return playPauseCommand ??= new MvxCommand(() =>
				{
					try
					{
						if (engine.AudioPlayback.PlaybackState == PlaybackState.Playing)
						{
							engine?.Pause();
						}
						else
						{
							engine.Play();
						}
					}
					catch (Exception ex)
					{
						throw ex;
					}
					finally
					{
						PlayPauseImageSource = new Uri(PlaybackState == PlaybackState.Playing ?
							Properties.Resources.Pause_Dark :
							Properties.Resources.Play_Dark,
							UriKind.Relative);
						StopCommand.RaiseCanExecuteChanged();
						SkipBackCommand.RaiseCanExecuteChanged();
						SkipNextCommand.RaiseCanExecuteChanged();
						RaiseAllPropertiesChanged().ConfigureAwait(true);
					}
				}, () => engine.AudioPlayback != null && (engine.AudioPlayback.CanPlay || engine.AudioPlayback.CanPause));
			}
		}

		private IMvxCommand stopCommand;
		/// <summary>
		/// Command to stop the playback.
		/// </summary>
		public IMvxCommand StopCommand
		{
			get
			{
				return stopCommand ??= new MvxCommand(() =>
				{
					try
					{
						engine?.Stop();
					}
					finally
					{
						PlayPauseImageSource = new Uri(Properties.Resources.Play_Dark, UriKind.Relative);
						SkipBackCommand.RaiseCanExecuteChanged();
						SkipNextCommand.RaiseCanExecuteChanged();
					}
				},
				() => engine.AudioPlayback?.CanStop ?? false);
			}
		}

		private IMvxCommand skipBackCommand;
		/// <summary>
		/// Command to skip to the previous item.
		/// </summary>
		public IMvxCommand SkipBackCommand
		{
			get
			{
				return skipBackCommand ??= new MvxCommand(() =>
					engine?.PreviousTrack(IsShuffle),
				() => engine.AudioPlayback != null);
			}
		}

		private IMvxCommand skipNextCommand;
		/// <summary>
		/// Command to skip to the next item.
		/// </summary>
		public IMvxCommand SkipNextCommand
		{
			get
			{
				return skipNextCommand ??= new MvxCommand(() =>
					engine?.NextTrack(IsShuffle),
				() => engine.AudioPlayback != null);
			}
		}

		private IMvxCommand muteCommand;
		/// <summary>
		/// Command to mute the volume.
		/// </summary>
		public IMvxCommand MuteCommand
		{
			get
			{
				return muteCommand ??= new MvxCommand(() => this.Mute());
			}
		}

		private IMvxCommand<double> pitchCommand;
		/// <summary>
		/// Command to start pitch shifting;
		/// </summary>
		public IMvxCommand<double> PitchCommand
		{
			get
			{
				return pitchCommand ??= new MvxCommand<double>(_ => { });
			}
		}
		#endregion

		#region Methods
		public void Mute()
		{
			if (Volume > 0)
			{
				tempVolume = Volume;
				Volume = 0;
				IsMuted = true;
			}
			else
			{
				Volume = tempVolume;
				if (Volume == 0)
				{
					Volume = 10;
					IsMuted = false;
				}
			}
		}
		#endregion

		#region AudioPlayback-Visualization-Callbacks
		public IList<IVisualizationPlugin> Visualizations
		{
			get
			{
				return this.visualizations;
			}
		}

		public IVisualizationPlugin SelectedVisualization
		{
			get
			{
				return this.selectedVisualization;
			}
			set
			{
				if (this.selectedVisualization != value)
				{
					this.selectedVisualization = value;
					RaisePropertyChanged(nameof(SelectedVisualization));
					RaisePropertyChanged(nameof(Visualization));
				}
			}
		}

		public object Visualization
		{
			get
			{
				return this.selectedVisualization?.Content;
			}
		}

		public IMediaItem MediaItem => engine.MediaItem;

		private async void AudioGraph_MaximumCalculated(object sender, AmplitudeEventArgs e)
		{
			await AsyncDispatcher.ExecuteOnMainThreadAsync(() =>
			{
				if (SelectedVisualization != null)
				{
					SelectedVisualization.OnMaxCalculated(e.AmplitudeThroughValue * 2, e.AmplitudePeakValue * 2);
				}
			}).ConfigureAwait(false);
		}

		private void AudioGraph_PlaybackStopped(object sender, PlaybackStoppedEventArgs e)
		{
			IAudioPlayback aPlayback = (IAudioPlayback)e.State;
			if (aPlayback.EndOfStream)
			{
				engine?.NextTrack(IsShuffle);
			}
		}

		private void AudioPlayback_PositionChanged(object sender, PositionChangedEventArgs e)
		{
			if (LengthInSeconds == 0 && engine.AudioPlayback != null)
				LengthInSeconds = engine.AudioPlayback.GetMaximum();
			Position = e.ChannelPosition;
		}
		#endregion

		#region IDisposable		
		/// <summary>
		/// Releases unmanaged and - optionally - managed resources.
		/// </summary>
		public void Dispose()
		{
			messenger.RequestPurge(typeof(MediaItemChangedMessage));
			messenger.DisposeIfDisposable();
			engine.Dispose();
		}

		#endregion
	}
}
