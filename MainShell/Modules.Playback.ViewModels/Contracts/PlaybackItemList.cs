﻿using Lib.Common;
using Lib.Utility;

using MvvmCross.Logging;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace xtra_snd_tools.Modules.Playback.ViewModels
{

	public class PlaybackItemList : ObservableCollection<IMediaItem>, IOrderedEnumerable<IMediaItem>, IPlaybackList
	{
		protected int currentIndex = 0;
		private IEnumerator<IMediaItem> _enumerator;

		private List<IMediaItem> list => base.Items.ToList();

		public PlaybackItemList()
		{
			_enumerator = GetEnumerator();
		}


		public PlaybackItemList(IEnumerable<IStorageItem> tracks, object parent) : this()
		{
			foreach (var track in tracks.Select(x => new MediaItem(x, parent)))
			{
				base.Items.Add(track);
			}
		}

		public bool IsReadOnly => false;

		public IMediaItem Current
		{
			get
			{
				if (Count > 0)
				{
					return base.Items[currentIndex];
				}

				return null;
			}
		}

		public int CurrentIndex => currentIndex;

		public IMediaItem MoveNext()
		{
			currentIndex++;

			if (currentIndex == base.Items.Count)
				currentIndex = 0;

			return base.Items[currentIndex];
		}

		public IMediaItem MovePrevious()
		{
			currentIndex--;

			if (currentIndex == -1)
				currentIndex += base.Items.Count;

			if (currentIndex > -1)
			{
				return base.Items[currentIndex];
			}
			return null;
		}

		public IMediaItem MoveToIndex(int index)
		{
			if (index >= 0 && index < base.Items.Count)
			{
				currentIndex = index;
				return base.Items[currentIndex];
			}
			return null;
		}

		public IMediaItem SelectItem(IMediaItem item)
		{
			return MoveToIndex(list.FindIndex(x => x.Source == item.Source));
		}


		public IOrderedEnumerable<IMediaItem> CreateOrderedEnumerable<TKey>(Func<IMediaItem, TKey> keySelector, IComparer<TKey> comparer, bool descending)
		{
			return descending ? base.Items.OrderByDescending(keySelector, comparer) : base.Items.OrderBy(keySelector, comparer);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return base.Items.GetEnumerator();
		}
	}
}