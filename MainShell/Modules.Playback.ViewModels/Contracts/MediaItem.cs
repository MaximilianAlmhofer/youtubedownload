﻿using Lib.Common;

using System;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;

using xtra_snd_tools.Common.Contracts;

namespace xtra_snd_tools.Modules.Playback.ViewModels
{
	public class MediaItem : IMediaItem
	{
		private object viewModel;
		private bool isLoading;
		private object downloadItem;

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		public MediaItem(IStorageItem storageItem, object viewModel, object downloadItem = null)
		{
			StorageItem = storageItem as StorageItem ?? throw new ArgumentNullException(nameof(storageItem));
			ViewModel = viewModel;
			DownloadItem = downloadItem;
		}

		protected virtual void RaisPropertyChanged([CallerMemberName] string propertyName = null)
		{
			var handler = PropertyChanged;
			handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public IStorageItem GetStorageItem()
		{
			return StorageItem;
		}

		public IStorageItem StorageItem
		{
			get;
		}

		public string DurationString
		{
			get
			{
				string durationString;

				if (((IMediaItem)this).IsPlayList)
				{
					durationString = ((PlaylistInfo)StorageItem).Duration.ToString("hh\\:mm\\:ss", DateTimeFormatInfo.InvariantInfo);
				}
				else
				{
					durationString = TimeSpan.FromSeconds(((StorageItem)StorageItem).Duration).ToString("hh\\:mm\\:ss", DateTimeFormatInfo.InvariantInfo);
				}
				if (!isLoading)
				{
					isLoading = true;
					RaisPropertyChanged();
					isLoading = false;
				}

				return durationString;
			}
		}

		public object ViewModel
		{
			get => viewModel;
			set
			{
				if (viewModel != value)
				{
					viewModel = value;
					RaisPropertyChanged();
				}
			}
		}

		public object DownloadItem
		{
			get => downloadItem;
			set
			{
				if (downloadItem != value)
				{
					downloadItem = value;
					RaisPropertyChanged();
				}
			}
		}

		public string MediaFormat
		{
			get
			{
				string mediaFormat;

				if (StorageItem != null)
				{
					mediaFormat = StorageItem.GetExtension()?.Trim('.').ToUpper(CultureInfo.InvariantCulture);
				}
				else
				{
					mediaFormat = "playlist";
				}
				return mediaFormat;
			}
		}

		public bool IsPlayList
		{
			get => false;
		}

		public Uri Source
		{
			get
			{
				return new Uri(StorageItem.FileLocation);
			}
		}
	}
}
