﻿using Lib.Common;

using System.Collections.Generic;
using System.Linq;

using xtra_snd_tools.Common.Contracts;

namespace xtra_snd_tools.Modules.Playback.ViewModels
{
	public class MediaPlaylist
	{

		public MediaPlaylist(IPlaylistInfo playlist)
		{
			PlaylistInfo pl = (PlaylistInfo)playlist;
			Entries = pl.Entries?.Select(x => new MediaItem(new StorageItem(x), null, null))?.Cast<IMediaItem>()?.ToList();
		}

		public List<IMediaItem> Entries { get; }
	}
}
