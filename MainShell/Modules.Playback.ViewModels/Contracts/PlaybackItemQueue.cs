﻿using Lib.Common;
using Lib.Utility;
using Lib.Utility.Logging;

using MvvmCross.Logging;

using System.Collections.Generic;

namespace xtra_snd_tools.Modules.Playback.ViewModel
{
	public class PlaybackItemQueue : Queue<IMediaItem>
	{
		private IMediaItem currentItem;
		private Stack<IMediaItem> backWard = new Stack<IMediaItem>();

		public PlaybackItemQueue() : base()
		{
		}

		public PlaybackItemQueue(IEnumerable<IMediaItem> tracks) : base(tracks)
		{
		}


		public new IMediaItem Dequeue()
		{
			if (TryPeek(out IMediaItem item))
			{
				backWard.Push(base.Dequeue());
				currentItem = item;
				return item;
			}
			return null;
		}
	}
}
