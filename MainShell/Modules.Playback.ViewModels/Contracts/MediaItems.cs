﻿using Lib.Common;

using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

using xtra_snd_tools.Common.Contracts;
using xtra_snd_tools.Common.Contracts.ViewModels;

namespace xtra_snd_tools.Modules.Playback.ViewModels
{
	public class MediaItems : NotifyPropertyChangeViewModel, IMediaItem
	{
		private PlaylistInfo playlist;
		public MediaItems(IPlaylistInfo playlist, object viewModel, object downloadItem = null)
		{
			this.playlist = (PlaylistInfo)playlist;
			var __entries = playlist.GetEntries()?.ToList();
			Entries = new PlaybackItemList(__entries, viewModel);
			ViewModel = viewModel;
			DownloadItem = downloadItem;
		}

		public Uri Thumbnail
		{
			get
			{
				return playlist.Thumbnail;
			}
		}


		public string Title
		{
			get
			{
				return playlist.Title;
			}
		}


		public string Uploader
		{
			get
			{
				return playlist.GetWebpageUri().OriginalString;
			}
		}


		private IPlaybackList _entries;
		public IPlaybackList Entries
		{
			get => _entries;
			private set => this.Set(ref _entries, value);
		}


		public IPlaylistTrackEntry CurrentItem
		{
			get
			{
				return (IPlaylistTrackEntry)((IMediaItem)this).GetStorageItem();
			}
		}

		private object _viewModel;
		public object ViewModel
		{
			get => _viewModel;
			set => this.Set(ref _viewModel, value);
		}

		private object _downloadItem;
		public object DownloadItem
		{
			get => _downloadItem;
			set => this.Set(ref _downloadItem, value);
		}

		public Uri Source
		{
			get
			{
				return CurrentItem.FileLocation?.ToUri();
			}
		}

		public bool IsPlayList
		{
			get
			{
				return true;
			}
		}

		IStorageItem IMediaItem.GetStorageItem()
		{
			return Entries.Current.GetStorageItem();
		}
	}
}
