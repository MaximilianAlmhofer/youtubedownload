﻿using MvvmCross.ViewModels;

using xtra_snd_tools.Mainshell.Settings.ViewModels;

namespace xtra_snd_tools.Mainshell.Settings
{
	[MvxViewFor(typeof(AudioSettingsViewModel))]
	public partial class AudioSettingsControl : MvvmCross.Platforms.Wpf.Views.MvxWpfView
	{
		public AudioSettingsControl()
		{
			InitializeComponent();

			//DataContext = new AudioSettingsViewModel();
		}
	}
}
