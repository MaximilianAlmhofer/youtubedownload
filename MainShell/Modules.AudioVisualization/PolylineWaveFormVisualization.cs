﻿using AudioLib.Visualization;

using System.Composition;
using System.Diagnostics.CodeAnalysis;
using System.Windows.Controls;

namespace xtra_snd_tools.Modules.AudioVisualization.Controls
{
	[Export(typeof(IVisualizationPlugin))]
	public class PolylineWaveFormVisualization : IVisualizationPlugin
	{
		private IWaveFormRenderer polylineWaveFormControl;

		public PolylineWaveFormVisualization(UserControl control, string name)
		{
			Name = name;
			polylineWaveFormControl = control as IWaveFormRenderer;
		}

		public string Name { get; private set; }

		public object Content => polylineWaveFormControl;

		public void OnMaxCalculated(float min, float max)
		{
			polylineWaveFormControl.AddValue(max, min);
		}

		public void OnFFTCalculated(NAudio.Dsp.Complex[] result)
		{
			// nothing to do
		}

		public bool Equals([AllowNull] IVisualizationPlugin other)
		{
			return other?.Name == Name;
		}
	}
}
