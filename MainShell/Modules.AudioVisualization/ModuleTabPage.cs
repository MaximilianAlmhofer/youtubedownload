﻿using Lib.Common;

using MvvmCross.Platforms.Wpf.Views;

using xtra_snd_tools.Mainshell.ViewModels;
using xtra_snd_tools.Mainshell.Views;

namespace xtra_snd_tools.Modules.AudioVisualization
{
	public class ModuleTabPage : IModuleFrame<HomeViewModel>, IModuleFrame
	{


		private HomeView view;


		private HomeViewModel viewModel;


		public string Name { get; set; }


		public MvxWpfView UserInterface => view;


		public void CreateView(HomeViewModel viewModel)
		{
			view = new HomeView();
			this.viewModel = viewModel;
			view.DataContext = viewModel;
		}


		public void Deactivate()
		{
			view.Dispose();
			viewModel = null;
		}
	}
}
