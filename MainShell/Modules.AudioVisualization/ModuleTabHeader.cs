﻿using Lib.Common;

namespace xtra_snd_tools.Modules.AudioVisualization
{
	public class ModuleTabHeader : IModuleFrame
	{
		public string Name { get; }

		public ModuleTabHeader(string name)
		{
			Name = name;
		}
	}
}
