﻿using AudioLib.Visualization;

using MvvmCross.ViewModels;

using System.Collections.Generic;

namespace xtra_snd_tools.Modules.AudioVisualization
{
	public interface IAudioVisualization : IMvxNotifyPropertyChanged
	{
		IVisualizationPlugin SelectedVisualization { get; set; }

		IList<IVisualizationPlugin> Visualizations { get; }

		object Visualization { get; }
	}
}