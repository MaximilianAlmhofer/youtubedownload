﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;

using xtra_snd_tools.Modules.Downloader.ViewModels;

namespace xtra_snd_tools.Common.Contracts
{
	[JsonObject("requested_formats")]
	public class FormatInfo
	{
		/// <summary>
		/// Nummer identifiziert Format: 251 DASH Audio, 136 DASH Mp4
		/// </summary>
		[JsonProperty("format_id")]
		public string FormatId { get; set; }

		/// <summary>
		/// Format name
		/// </summary>
		[JsonProperty("format")]
		public string FormatName { get; set; }

		/// <summary>
		/// Beschreibung format
		/// </summary>
		[JsonProperty("format_note")]
		public string FormatNote { get; set; }

		/// <summary>
		/// Durchschnittliche Bitrate
		/// </summary>
		[JsonProperty("abr")]
		public float? AverageBitrate { get; set; }

		/// <summary>
		/// Durchschnittliche Samplingrate
		/// </summary>
		[JsonProperty("asr")]
		public float? AverageSamplingRate { get; set; }

		/// <summary>
		/// Videocodec
		/// </summary>
		[JsonProperty("vcodec")]
		public string Vcodec { get; set; }

		/// <summary>
		/// Audiocodec
		/// </summary>
		[JsonProperty("acodec")]
		public string Acodec { get; set; }

		/// <summary>
		/// Videobreite
		/// </summary>
		[JsonProperty("width")]
		public float? Width { get; set; }

		/// <summary>
		/// Videohöhe
		/// </summary>
		[JsonProperty("height")]
		public float? Height { get; set; }

		/// <summary>
		/// Dateigröße
		/// </summary>
		[JsonProperty("filesize")]
		public long? FileSize { get; set; }

		/// <summary>
		/// Dateiendung
		/// </summary>
		[JsonProperty("ext")]
		public string Ext { get; set; }

		/// <summary>
		/// Frames pro Sekunde (video)
		/// </summary>
		[JsonProperty("fps")]
		public int? FramesPerSecond { get; set; }

		/// <summary>
		/// tbr
		/// </summary>
		[JsonProperty("tbr")]
		public float? Tbr { get; set; }

		/// <summary>
		/// Downloadprotokoll
		/// </summary>
		[JsonProperty("protocol")]
		public string Protocol { get; set; }

		/// <summary>
		/// Formatcontainer
		/// </summary>
		[JsonProperty("container")]
		public string Container { get; set; }

		/// <summary>
		/// Url
		/// </summary>
		[JsonProperty("url", NullValueHandling = NullValueHandling.Ignore)]
		public Uri Url { get; set; }

		/// <summary>
		/// Obsolet Url des Players
		/// </summary>
		[JsonProperty("player_url", NullValueHandling = NullValueHandling.Ignore)]
		public Uri PlayerUrl { get; set; }

		/// <summary>
		/// Manifesturl
		/// </summary>
		[JsonProperty("manifest_url", NullValueHandling = NullValueHandling.Ignore)]
		public Uri ManifestUrl { get; set; }

		/// <summary>
		/// Frames url
		/// </summary>
		[JsonProperty("fragment_base_url", NullValueHandling = NullValueHandling.Ignore)]
		public Uri FragmentBaseUrl { get; set; }

		/// <summary>
		/// Fragmente
		/// </summary>
		[JsonProperty("fragments", NullValueHandling = NullValueHandling.Ignore)]
		public IList<Fragment> Fragments { get; set; }

		/// <summary>
		/// Request Http Header
		/// </summary>
		[JsonProperty("http_headers")]
		public HttpHeaders HttpHeaders { get; set; }

#if DEFINED_GRPC
		public grpc.protobuf.Protos.Messages.FormatInfo ToProtoBuf()
		{
			return new grpc.protobuf.Protos.Messages.FormatInfo
			{
				FormatId = FormatId,
				Abr = AverageBitrate ?? 0,
				Tbr = Tbr ?? 0,
				Acodec = Acodec,
				Vcodec = Vcodec,
				Ext = Ext,
				FormatName = FormatName,
				Filesize = (int?)FileSize ?? 0,
				FormatNote = FormatNote,
				Fps = (uint?)FramesPerSecond ?? 0,
				Url = Url.OriginalString,
				Protocol = Protocol
			};
		}
#endif
	}
}
