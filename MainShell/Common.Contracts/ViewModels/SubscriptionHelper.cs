﻿using Lib.Common.Message;

using MvvmCross.Plugin.Messenger;

using System;

namespace xtra_snd_tools.Common.Contracts.ViewModels
{
	public static class SubscriptionHelper
	{
		#region Nongeneric
		internal static void SubscribeTo<T>(this ViewModelBase target, Action<T> deliveryAction) where T : MvxMessage
		{
			string key = nameof(T).Replace("Message", string.Empty);
			target.AddSubscription<T>(key, deliveryAction);
		}

		public static void SubscribeToViewModelPRopertyChanged(this ViewModelBase target, Action<ViewModelPropertyChangedMessage> deliveryAction)
		{
			target.SubscribeTo<ViewModelPropertyChangedMessage>(deliveryAction);
		}

		public static void SubscribeToNewDownloadMessage(this ViewModelBase target, Action<NewDownloadMessage> deliveryAction)
		{
			target.SubscribeTo<NewDownloadMessage>(deliveryAction);
		}

		public static void SubscribeToNewListDownloadMessage(this ViewModelBase target, Action<NewListDownloadMessage> deliveryAction)
		{
			target.SubscribeTo<NewListDownloadMessage>(deliveryAction);
		}

		public static void SubscribeToMediaItemChangedMessage(this ViewModelBase target, Action<MediaItemChangedMessage> deliveryAction)
		{
			target.SubscribeTo<MediaItemChangedMessage>(deliveryAction);
		}

		public static void SubscribeToPlaylistAddRequestMessage(this ViewModelBase target, Action<PlaylistAddRequestMessage> deliveryAction)
		{
			target.SubscribeTo<PlaylistAddRequestMessage>(deliveryAction);
		}
		#endregion

		#region Generic TypArgument
		internal static void SubscribeTo<T, TMessage>(this ViewModelBase<T> target, Action<TMessage> deliveryAction) where T : class where TMessage : MvxMessage
		{
			string key = nameof(T).Replace("Message", string.Empty);
			target.AddSubscription<TMessage>(key, deliveryAction);
		}

		public static void SubscribeToViewModelPropertyChanged<T>(this ViewModelBase<T> target, Action<ViewModelPropertyChangedMessage> deliveryAction)
			where T : class
		{
			target.SubscribeTo<T, ViewModelPropertyChangedMessage>(deliveryAction);
		}

		public static void SubscribeToNewDownloadMessage<T>(this ViewModelBase<T> target, Action<NewDownloadMessage> deliveryAction)
			where T : class
		{
			target.SubscribeTo<T, NewDownloadMessage>(deliveryAction);
		}

		public static void SubscribeToNewListDownloadMessage<T>(this ViewModelBase<T> target, Action<NewListDownloadMessage> deliveryAction)
			where T : class
		{
			target.SubscribeTo<T, NewListDownloadMessage>(deliveryAction);
		}

		public static void SubscribeToMediaItemChangedMessage<T>(this ViewModelBase<T> target, Action<MediaItemChangedMessage> deliveryAction)
			where T : class
		{
			target.SubscribeTo<T, MediaItemChangedMessage>(deliveryAction);
		}

		public static void SubscribeToPlaylistAddRequestMessage<T>(this ViewModelBase<T> target, Action<PlaylistAddRequestMessage> deliveryAction)
			where T : class
		{
			target.SubscribeTo<T, PlaylistAddRequestMessage>(deliveryAction);
		}
		#endregion
	}
}
