﻿using MvvmCross.Plugin.Messenger;

using System;
using System.Collections.Generic;

namespace xtra_snd_tools.Common.Contracts.ViewModels
{
	public class ViewModelBase : NotifyPropertyChangeViewModel
	{
		protected readonly IDictionary<string, MvxSubscriptionToken> subscriptions;
		protected readonly IMvxMessenger messenger;

		protected ViewModelBase(IMvxMessenger messenger) : base()
		{
			this.messenger = messenger;
			subscriptions = new Dictionary<string, MvxSubscriptionToken>();
		}

		protected internal void AddSubscription<TMessage>(string name, Action<TMessage> deliveryAction) where TMessage : MvxMessage
		{
			subscriptions[name] = messenger.Subscribe<TMessage>(deliveryAction);
		}

		protected internal void AddSubscriptionOnMainThread<TMessage>(string name, Action<TMessage> deliveryAction) where TMessage : MvxMessage
		{
			subscriptions[name] = messenger.SubscribeOnMainThread<TMessage>(deliveryAction);
		}

		protected internal void AddSubscriptionOnThreadPoolThread<TMessage>(string name, Action<TMessage> deliveryAction) where TMessage : MvxMessage
		{
			subscriptions[name] = messenger.SubscribeOnThreadPoolThread<TMessage>(deliveryAction);
		}
	}

	public abstract class ViewModelBase<T> : NotifyPropertyChangeViewModel<T>
	{
		protected readonly IDictionary<string, MvxSubscriptionToken> subscriptions;
		protected readonly IMvxMessenger messenger;

		protected ViewModelBase(IMvxMessenger messenger) : base()
		{
			this.messenger = messenger;
			subscriptions = new Dictionary<string, MvxSubscriptionToken>();
		}

		protected internal void AddSubscription<TMessage>(string name, Action<TMessage> deliveryAction) where TMessage : MvxMessage
		{
			subscriptions[name] = messenger.Subscribe<TMessage>(deliveryAction);
		}

		protected internal void AddSubscriptionOnMainThread<TMessage>(string name, Action<TMessage> deliveryAction) where TMessage : MvxMessage
		{
			subscriptions[name] = messenger.SubscribeOnMainThread<TMessage>(deliveryAction);
		}

		protected internal void AddSubscriptionOnThreadPoolThread<TMessage>(string name, Action<TMessage> deliveryAction) where TMessage : MvxMessage
		{
			subscriptions[name] = messenger.SubscribeOnThreadPoolThread<TMessage>(deliveryAction);
		}
	}
}
