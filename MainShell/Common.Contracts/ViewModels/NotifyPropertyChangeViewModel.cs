﻿using Lib.Common;

using MvvmCross.ViewModels;

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace xtra_snd_tools.Common.Contracts.ViewModels
{
	public abstract class NotifyPropertyChangeViewModel : MvxViewModel, INotifyPropertyChangedViewModel
	{
		public new void RaisePropertyChanged([CallerMemberName] string propertyName = null)
		{
			Func<Task> taskFunc = async () =>
			{
				await base.RaisePropertyChanged(propertyName).ConfigureAwait(false);
			};

			taskFunc();
		}
	}

	public abstract class NotifyPropertyChangeViewModel<T> : MvxViewModel<T>, INotifyPropertyChangedViewModel
	{
		public new void RaisePropertyChanged([CallerMemberName] string propertyName = null)
		{
			Func<Task> taskFunc = async () =>
			{
				await base.RaisePropertyChanged(propertyName).ConfigureAwait(false);
			};

			taskFunc();
		}
	}


	public static class NotifyPropertyChangeViewModelExtensions
	{
		public static void Set<TField>(this INotifyPropertyChangedViewModel instance, ref TField field, TField newValue, [CallerMemberName] string propertyName = null)
		{
			if (EqualityComparer<TField>.Default.Equals(field, newValue)) return;
			field = newValue;
			instance.RaisePropertyChanged(propertyName);
		}
	}
}
