﻿using Lib.Common;

using Newtonsoft.Json;

using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;

using xtra_snd_tools.Modules.Playback.ViewModels;

namespace xtra_snd_tools.Common.Contracts
{
	public enum FileState
	{
		Normal,
		SoftDeleted,
		HardDeleted,
		CorruptFile
	}
	public class StorageItem : TrackInfo, IStorageItem
	{
		[JsonConstructor]
		public StorageItem() : base()
		{
		}


		public StorageItem(object derived) : base()
		{
			var sw = Stopwatch.StartNew();
			foreach (var p in derived?.GetType().GetProperties(
				BindingFlags.Instance | BindingFlags.Public |
				BindingFlags.GetProperty))
			{
				if (p.GetSetMethod() != null)
				{
					GetType().GetProperty(p.Name,
					BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic |
					BindingFlags.SetProperty).SetValue(this, p.GetValue(derived));
				}
			}
			sw.Stop();
			if (Debugger.IsAttached)
			{
				Debugger.Log(0, "Information", ".ctor execution time: " + sw.Elapsed);

			}
		}


		public bool IsBackupManifest
		{
			get;
			set;
		}


		#region Properties + Fields
		private string fileLocation;
		/// <summary>
		/// Pfad der Media Datei
		/// </summary>
		[JsonProperty]
		public string FileLocation
		{
			get
			{

				if (string.IsNullOrEmpty(fileLocation))
				{
					string path = PathUtils.Finddir(ManifestFile, "dl");
					string ext = Path.ChangeExtension(Path.GetFileName(ManifestFile), Ext);
					if (!string.IsNullOrEmpty(path) && !string.IsNullOrEmpty(ext))
					{
						fileLocation = Path.Combine(path, ext);
						OnPropertyChanged();
					}
				}

				return fileLocation;
			}
			set
			{
				fileLocation = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// Dateieigenschaften json
		/// </summary>
		[JsonProperty("manifest_file")]
		public string ManifestFile { get; set; }

		/// <summary>
		/// TagLib File object liefert detaillierte Eigenschaften
		/// </summary>
		[JsonIgnore]
		public TagLib.File.IFileAbstraction File { get; }

		private IMediaItem mediaItem;
		/// <summary>
		/// Mediadateiobjekt
		/// </summary>
		[JsonIgnore]
		public IMediaItem MediaItem
		{
			get
			{
				if (mediaItem is null)
				{
					mediaItem = new MediaItem(this, null);
				}
				return mediaItem;
			}
		}
		#endregion

		#region Methods
		public string GetExtension()
		{
			return Ext;
		}


		public TagLib.File GetTagLibFile([CallerMemberName] string fileName = null)
		{
			return Ext?.TrimStart('.')?.ToUpperInvariant() switch
			{
				"MP3" => new TagLib.Mpeg.AudioFile(FileLocation),
				"FLAC" => new TagLib.Flac.File(FileLocation),
				"AAC" => new TagLib.Aac.File(FileLocation),
				"AIFF" => new TagLib.Aiff.File(FileLocation),
				"ASF" => new TagLib.Asf.File(FileLocation),
				"RIFF" => new TagLib.Riff.File(FileLocation),
				"OGG" => new TagLib.Ogg.File(FileLocation),
				"MP4" => new TagLib.Mpeg4.File(FileLocation),
				_ => new TagLib.WavPack.File(new TagLib.NonContainer.File.LocalFileAbstraction(FileLocation)),
			};
		}


		public string GetFilename()
		{
			return Path.Combine(PathUtils.Finddir(ManifestFile, "dl"), Path.GetFileName(fileLocation));
		}


		public void SetFileLocation(string path)
		{
			FileLocation = path;
			OnPropertyChanged(nameof(FileLocation));
		}


		public void SetManifestLocation(string metadataFolder)
		{
			string manifest = Path.ChangeExtension(Path.GetFileName(FileLocation), ".json");
			ManifestFile = Path.Combine(metadataFolder, manifest);
		}


		public bool Equals([AllowNull] IStorageItem other)
		{
			return other?.ManifestFile == this.ManifestFile;
		}
		#endregion
	}
}
