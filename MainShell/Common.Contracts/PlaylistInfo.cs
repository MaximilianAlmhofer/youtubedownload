﻿using Lib.Common;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

namespace xtra_snd_tools.Common.Contracts
{
	[JsonObject]
	public class PlaylistInfo : IPlaylistInfo
	{
		private List<PlaylistEntry> _entries;
		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		[JsonConstructor]
		public PlaylistInfo()
		{
			_entries = new List<PlaylistEntry>();
		}


		#region Properties
		[JsonProperty("id")]
		public string Id
		{
			get;
			set;
		}


		string title;
		[JsonProperty("title")]
		public string Title
		{
			get => title;
			set
			{
				title = value;
				OnPropertyChanged();
			}
		}


		[JsonProperty("_type")]
		public static string Type
		{
			get;
			set;
		}


		[JsonProperty("extractor")]
		public static string Extractor
		{
			get;
			set;
		}


		[JsonProperty("extractor_key")]
		public static string ExtractorKey
		{
			get;
			set;
		}



		[JsonProperty]
		public List<PlaylistEntry> Entries
		{
			get => _entries;
			set
			{
				_entries = value;

				for (int i = 1; i <= _entries.Count; i++)
				{
					if ((_entries[i - 1].Title[0] == '0' && char.IsDigit(_entries[i - 1].Title[1]) || char.IsDigit(_entries[i - 1].Title[1])) && _entries[i - 1].Title[2] == '_')
						continue;

					_entries[i - 1].Title = _entries[i - 1].Title.Insert(0, i < 10 ? $"0{i}_" : $"{i}_");
				}
				OnPropertyChanged();
			}
		}


		string _manifestFile;
		[JsonProperty("manifest_file")]
		public string ManifestFile
		{
			get => _manifestFile;
			set
			{
				_manifestFile = value;
				OnPropertyChanged();
			}
		}


		[JsonIgnore]
		public TimeSpan Duration
		{
			get
			{
				return TimeSpan.FromSeconds(_entries?.Sum(x => x.Duration) ?? 0);
			}
		}

		private string _storageFolder;
		/// <summary>
		/// StorageFolder
		/// </summary>
		public string StorageFolder
		{
			get => _storageFolder;
			set
			{
				_storageFolder = value;
				OnPropertyChanged();
			}
		}

		private Uri _thumbnail;
		public Uri Thumbnail
		{
			get => _thumbnail;
			set
			{
				_thumbnail = value;
				OnPropertyChanged();
			}
		}
		#endregion


		public object ToLocal(string saveFilePath)
		{
			StorageFolder = saveFilePath;
			string baseDir = Path.GetDirectoryName(StorageFolder).Replace("\\dl\\", "\\md\\");
			ManifestFile = Path.Combine(baseDir, Path.ChangeExtension(Path.GetFileName(_storageFolder.Replace("\\dl\\", "\\md\\")), ".json"));
			return this;
		}


		public Uri GetWebpageUri()
		{
			return new Uri(string.Format("https://www.youtube.com/watch?v={0}&list={1}", Entries?.FirstOrDefault()?.Id, Id));
		}


		public ITrackInfo GetTrack(int index)
		{
			return Entries.ElementAtOrDefault(index);
		}


		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			var handler = PropertyChanged;
			handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}


		public bool Equals([AllowNull] IStorageItem other)
		{
			return other?.ManifestFile == ManifestFile;
		}


		public object GetFormatIdentifier(bool forAudio)
		{
			return "best";
		}

		public IEnumerable<IPlaylistTrackEntry> GetEntries()
		{
			return Entries;
		}
	}
}
