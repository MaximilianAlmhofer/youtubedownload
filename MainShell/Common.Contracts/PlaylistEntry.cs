﻿
using Lib.Common;

using Newtonsoft.Json;

namespace xtra_snd_tools.Common.Contracts
{
	public sealed class PlaylistEntry : StorageItem, IPlaylistTrackEntry
	{

		[JsonProperty("playlist_index")]
		public int PlaylistIndex { get; }

		[JsonProperty("playlist")]
		public string Playlist { get; }

		[JsonProperty("n_entries")]
		public int NEntries { get; }

		[JsonProperty("playlist_id")]
		public string PlaylistId { get; }

	}
}
