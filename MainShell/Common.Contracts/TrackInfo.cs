﻿
using Lib.Common;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

using xtra_snd_tools.Modules.Downloader.ViewModels;

namespace xtra_snd_tools.Common.Contracts
{
	[JsonObject]
	public class TrackInfo : ITrackInfo
	{
		public TrackInfo()
		{
			RequestedFormats = new List<FormatInfo>(2);
			Thumbnails = new List<Thumbnail>();
			Categories = new List<string>();
			Tags = new List<string>();
		}

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		#region Properties
		[JsonProperty]
		public string Id { get; set; }

		[JsonProperty]
		public Uri Thumbnail { get; set; }

		[JsonProperty("uploader_url")]
		public Uri UploaderUrl { get; set; }

		[JsonProperty("channel_url")]
		public Uri ChannelUrl { get; set; }

		[JsonProperty("uploader_id")]
		public string UploaderId { get; set; }

		[JsonProperty("channel_id")]
		public string ChannelId { get; set; }

		[JsonProperty]
		public string Ext { get; protected set; }

		[JsonProperty]
		public string Uploader { get; set; }

		[JsonProperty]
		public string Title { get; set; }

		[JsonProperty]
		public string Artist { get; set; }

		[JsonProperty]
		public long Duration { get; set; }

		[JsonProperty]
		public string Description { get; set; }

		[JsonProperty]
		public uint? LikeCount { get; set; }

		[JsonProperty]
		public uint? DislikeCount { get; set; }

		[JsonProperty]
		public float? AverageRating { get; set; }


		/// <summary>
		/// Alternative Thumbnails
		/// </summary>
		[JsonProperty("thumbnails")]
		public IList<Thumbnail> Thumbnails { get; set; }

		/// <summary>
		/// Die wichtigsten
		/// </summary>
		[JsonProperty("requested_formats")]
		public IList<FormatInfo> RequestedFormats { get; }

		/// <summary>
		/// Musik-Kategorien
		/// </summary>
		[JsonProperty("categories")]
		public IList<string> Categories { get; set; }

		/// <summary>
		/// Zum Suchen
		/// </summary>
		[JsonProperty("tags")]
		public IList<string> Tags { get; set; }

		/// <summary>
		/// z.B "137+251"
		/// </summary>
		[JsonProperty("format_id")]
		public string FormatId { get; set; }

		/// <summary>
		/// opus, m4a
		/// </summary>
		[JsonProperty]
		public string Acodec { get; set; }

		/// <summary>
		/// webm (container), mp4
		/// </summary>
		[JsonProperty]
		public string Vcodec { get; set; }

		/// <summary>
		/// 137 - 1920x810 (1080p)+251 - audio only (tiny)
		/// </summary>
		[JsonProperty]
		public string Format { get; set; }
		#endregion

		#region Methods
		public Uri GetWebpageUri()
		{
			return new Uri("https://www.youtube.com/watch?v=" + Id);
		}

		public object GetFormatIdentifier(bool forAudio)
		{
			int[] nums = FormatId.Split('+').Select(x => Convert.ToInt32(x, NumberFormatInfo.InvariantInfo)).ToArray();
			return forAudio ? nums[1] : nums[0];
		}


		public object ToLocal(string saveFilePath)
		{
			if (saveFilePath is null)
			{
				// zum Anzeigen nicht speichern 
				return new StorageItem(this)
				{
					Artist = Artist ?? Uploader,
					Ext = Ext
				};
			}
			var loc = new StorageItem(this)
			{
				FileLocation = saveFilePath
			};
			loc.SetManifestLocation(PathUtils.Finddir(saveFilePath, "md"));

			if (loc.Artist is null)
			{
				loc.Artist = loc.Uploader;
			}

			if (Path.GetExtension(saveFilePath) != loc.Ext)
			{
				loc.Ext = Path.GetExtension(saveFilePath);
			}

			return (object)loc;
		}

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			var handler = PropertyChanged;
			handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

#if DEFINED_GRPC
		public grpc.protobuf.Protos.Messages.TrackInfo ToProtoBuf()
		{
			grpc.protobuf.Protos.Messages.TrackInfo ti = new grpc.protobuf.Protos.Messages.TrackInfo
			{
				Id = Id,
				Title = Title,
				Thumbnail = Thumbnail?.OriginalString,
				Description = Description,
				Acodec = Acodec,
				Vcodec = Vcodec,
				UploaderUrl = UploaderUrl?.OriginalString,
				UploaderId = UploaderId,
				Uploader = Uploader,
				Duration = (uint?)Duration ?? 0,
				Format = Format,
				FormatId = FormatId,
				LikeCount = LikeCount ?? 0,
				DislikeCount = DislikeCount ?? 0,
				AverageRating = AverageRating ?? 0
			};
			foreach (FormatInfo f in RequestedFormats)
			{
				var fmt = f.ToProtoBuf();
				if (Vcodec != "none" && f.Width.GetValueOrDefault() > 0F || f.Height.GetValueOrDefault() > 0F)
				{
					ti.Width = f.Width.GetValueOrDefault();
					ti.Height = f.Height.GetValueOrDefault();
				}
				ti.Formats.Add(fmt);
			}
			if (Tags != null)
			{
				foreach (var tag in Tags)
				{
					ti.Tags.Add(new grpc.protobuf.Protos.Messages.Tag { Name = tag });
				}
			}
			if (Categories != null)
			{
				foreach (var category in Categories)
				{
					ti.Categories.Add(new grpc.protobuf.Protos.Messages.Category { Name = category });
				}
			}
			return ti;
		}
#endif
		#endregion
	}
}
