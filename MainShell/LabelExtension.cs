﻿using System.Windows;

namespace xtra_snd_tools
{
	public static class LabelExtension
	{
		public static bool GetSelectable(DependencyObject obj)
		{
			return (bool)obj.GetValue(SelectableProperty);
		}
		public static void SetSelectable(DependencyObject obj, bool value)
		{
			obj.SetValue(SelectableProperty, value);
		}

		public static readonly DependencyProperty SelectableProperty =
			DependencyProperty.RegisterAttached("Selectable", typeof(bool),
				typeof(LabelExtension), new UIPropertyMetadata(false));
	}
}
