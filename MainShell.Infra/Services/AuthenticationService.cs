﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;

using xtra_snd_tools.Infra.Services.Interfaces;
using Lib.WindowsLogin;

namespace xtra_snd_tools.Infra.Services
{
	public class AuthenticationService : IAuthenticationService
	{
		private static Dictionary<string, UserCredentials> activeLogins = new Dictionary<string, UserCredentials>();

		public AuthenticationService() : base()
		{

		}

		public Task<bool> AuthenticateAsync(string userName)
		{
			var credentials = activeLogins.FirstOrDefault();

			if (!activeLogins.ContainsKey(userName))
			{
				return Task.FromResult(false);
			}

			bool authenticated = Impersonation.RunAsUser<bool>(credentials.Value, LogonType.Interactive, handle =>
			{
				return new WindowsIdentity(handle.DangerousGetHandle()).IsAuthenticated;
			});

			return Task.FromResult(authenticated);
		}

		/// <summary>
		/// Login &amp; re-Login without Password
		/// </summary>
		/// <param name="userName">UserName required</param>
		/// <param name="passWord">Password (for relogin == null)</param>
		/// <param name="credentials">Credentials-object</param>
		/// <returns>True on success, else false</returns>
		public Task<bool> AuthenticateAsync(string userName, string passWord, out UserCredentials credentials)
		{
			if (activeLogins.ContainsKey(userName))
			{
				credentials = activeLogins[userName];
				return Task.FromResult(true);
			}

			credentials = new UserCredentials(Environment.UserDomainName, userName, passWord);

			bool success = Impersonation.RunAsUser(credentials, LogonType.Interactive, handle =>
			{
				return new WindowsIdentity(handle.DangerousGetHandle()).IsAuthenticated;
			});

			if (success)
			{
				activeLogins[userName] = credentials;
			}

			return Task.FromResult(success);
		}


		/// <summary>
		/// Removed the Username's credentials-object from the active logins.
		/// </summary>
		/// <param name="userName">The username to remove</param>
		public void Logout(string userName)
		{
			if (activeLogins.ContainsKey(userName))
			{
				activeLogins.Remove(userName);
			}
		}
	}
}
