﻿using Lib.Common;
using Lib.Utility;
using Lib.Utility.Logging;

using MvvmCross.Base;
using MvvmCross.Logging.LogProviders;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace xtra_snd_tools.Infra.Services
{
	internal class KeyValueCache<T, V> : KeyedCollection<T, V>, IEquatable<KeyValueCache<T, V>> where V : ICacheableKeyEmbedded<T>
	{
		private ILogger _logger;

		internal KeyValueCache(ILoggerFactory loggerFactory) : this(loggerFactory, (IEqualityComparer<T>)null)
		{
		}

		internal KeyValueCache(ILoggerFactory loggerFactory, IEqualityComparer<T> comparer)
			: this(loggerFactory, comparer, 0)
		{
		}

		internal KeyValueCache(ILoggerFactory loggerFactory, IEqualityComparer<T> comparer, int dictionaryCreationThreshold)
			: base(comparer, dictionaryCreationThreshold)
		{
			_logger = loggerFactory.Logger();
		}

		protected override T GetKeyForItem(V item)
		{
			return item is null ? default : item.Key;
		}

		internal void SetOrAdd(V item)
		{
			if (!Contains(GetKeyForItem(item)))
			{
				_logger.Logged(() => InsertItem(0, item), () => LogOnInsert(item));
			}
			else
			{
				_logger.Logged(() => SetItem(IndexOf(item), item), () => LogOnSet(item));
			}
			
		}

		protected override void ClearItems()
		{
			int count = Count;
			foreach (var item in Items)
				item.DisposeIfDisposable();

			base.ClearItems();
			_logger.Info($"Items cleared: {count}");
		}

		public static bool operator ==(KeyValueCache<T, V> left, KeyValueCache<T, V> right)
		{
			return EqualityComparer<KeyValueCache<T, V>>.Default.Equals(left, right);
		}

		public static bool operator !=(KeyValueCache<T, V> left, KeyValueCache<T, V> right)
		{
			return !(left == right);
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(Count, Items, Comparer, Dictionary);
		}

		public override bool Equals(object obj)
		{
			return Equals(obj as KeyValueCache<T, V>);
		}

		public bool Equals(KeyValueCache<T, V> other)
		{
			return other != null &&
				   Count == other.Count &&
				   EqualityComparer<IList<V>>.Default.Equals(Items, other.Items) &&
				   EqualityComparer<IEqualityComparer<T>>.Default.Equals(Comparer, other.Comparer) &&
				   EqualityComparer<IDictionary<T, V>>.Default.Equals(Dictionary, other.Dictionary);
		}

		private string LogOnSet(V item)
		{
			return new StringBuilder(nameof(SetOrAdd) + " -> " + nameof(SetItem))
				.AppendLine(StringFormatter.Format("Item existed in cache. Setting {0} at index {1}", item.ToString(), IndexOf(item)))
				.AppendLine(StringFormatter.Format("Currently cached items: {0}", Count)).ToString();
		}

		private string LogOnInsert(V item)
		{
			return new StringBuilder(nameof(SetOrAdd) + " -> " + nameof(InsertItem))
				.AppendLine(StringFormatter.Format("Item new inserted into cache: {0} {1}", item.ToString(), item.Key))
				.AppendLine(StringFormatter.Format("Currently cached items: {0}", Count)).ToString();
		}
	}
}
