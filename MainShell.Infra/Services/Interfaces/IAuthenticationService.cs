﻿using System.Threading.Tasks;

using Lib.WindowsLogin;

namespace xtra_snd_tools.Infra.Services.Interfaces
{
	public interface IAuthenticationService
	{
		Task<bool> AuthenticateAsync(string userName);

		Task<bool> AuthenticateAsync(string userName, string passWord, out UserCredentials credentials);

		void Logout(string userName);
	}
}
