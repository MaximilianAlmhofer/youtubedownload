﻿using Lib.Common;
using Lib.Common.Interfaces;
using Lib.Utility.Logging;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace xtra_snd_tools.Infra.Services
{

	/// <summary>
	/// Provides access to keyed sotrage for objects that hold the cache key on their own.
	/// </summary>
	/// <typeparam name="TKey"></typeparam>
	/// <typeparam name="TValue"></typeparam>
	public class SelfContainedKeyStorage<TKey, TValue> : IKeyedStorage<TKey> where TValue : ICacheableKeyEmbedded<TKey>
	{
		private readonly KeyValueCache<TKey, TValue> _cache;

		/// <summary>
		/// Initialized a new instance of <see cref="SelfContainedKeyStorage{TKey, TValue}"/> 
		/// </summary>
		public SelfContainedKeyStorage(ILoggerFactory loggerFactory)
		{
			_cache = new KeyValueCache<TKey, TValue>(loggerFactory, EqualityComparer<TKey>.Default);
		}

		public bool TryGet(TKey key, out ICacheableKeyEmbedded<TKey> value)
		{
			value = default;
			if (_cache.TryGetValue(key, out var item))
				value = item;
			return value != default;
		}

		public void Set(ICacheableKeyEmbedded<TKey> value)
		{
			_cache.SetOrAdd((TValue)value);
		}
	}

}
