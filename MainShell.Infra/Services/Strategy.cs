﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Threading;

using yCraft.lib.Core;

namespace Syn3X.Mvx.Logic
{
	public abstract class SearchStrategy<TStrategy, TParameter>
	{
		protected Debouncer debouncer;
		public abstract void Execute(TParameter parameter);

		public SearchStrategy(DispatcherPriority priority, Dispatcher dispatcher)
		{
			debouncer = new Debouncer(priority, dispatcher);
		}
	}

	public class StartsWithSearchStrategy : SearchStrategy<ITitledEntity, string>
	{
		private readonly List<ITitledEntity> source = new List<ITitledEntity>();

		public List<ITitledEntity> Matches
		{
			get;
			private set;
		} = new List<ITitledEntity>();

		public StartsWithSearchStrategy(IEnumerable source, DispatcherPriority priority, Dispatcher dispatcher)
			: base(priority, dispatcher)
		{
			foreach (var item in source)
			{
				this.source.Add(new TitledEntity((string)item));
			}
		}

		public override void Execute(string parameter)
		{
			Matches = source.Where(x => x.Title.StartsWith(parameter)).ToList();
		}
	}


	public interface ITitledEntity
	{
		string Title { get; set; }
	}

	public class TitledEntity : ITitledEntity
	{
		public string Title { get; set; }

		public TitledEntity(string title)
		{
			Title = title;
		}
	}
}
