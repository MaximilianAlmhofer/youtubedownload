﻿using System;
using System.Collections.Generic;
using System.Text;

[assembly: System.Reflection.AssemblyCompany("xtra_snd_tools.Infra")]
[assembly: System.Reflection.AssemblyFileVersion("200.2000.0.1")]
[assembly: System.Reflection.AssemblyInformationalVersion("2.0.1")]
[assembly: System.Reflection.AssemblyProduct("xtra_snd_tools.Infra")]
[assembly: System.Reflection.AssemblyTitle("xtra_snd_tools.Infra")]
[assembly: System.Reflection.AssemblyVersion("2.0.0.1")]
#if DEBUG
[assembly: System.Reflection.AssemblyConfiguration("Debug")]
#else
[assembly: System.Reflection.AssemblyConfiguration("Release")]
#endif