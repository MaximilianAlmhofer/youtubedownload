﻿using Lib.Common;

using MvvmCross.Exceptions;
using MvvmCross.IoC;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.Plugin.Messenger;
using MvvmCross.Presenters;
using MvvmCross.ViewModels;

using System.Threading.Tasks;

using xtra_snd_tools.Infra.Services.Interfaces;
using Lib.Utility;

namespace xtra_snd_tools.Infra
{
	public class CoreAppStart : MvxAppStart<ILoginViewModel, LoginParameters>
	{
		private readonly IAuthenticationService authenticationService;

		public CoreAppStart(IMvxApplication application, IMvxNavigationService navigationService, IAuthenticationService authenticationService)
			: base(application, navigationService)
		{
			this.authenticationService = authenticationService;
		}

		protected override Task<object> ApplicationStartup(object hint = null)
		{
			return base.ApplicationStartup(hint);
		}

		protected override Task NavigateToFirstViewModel(object hint = null)
		{
			try
			{
				var tcs = new TaskCompletionSource<bool>();

				Task.Run(async () =>
				{
					try
					{
						tcs.SetResult(await authenticationService.AuthenticateAsync(AppSetup.Instance.User.GetName()).ConfigureAwait(false));
					}
					catch (System.Exception ex)
					{

						tcs.SetException(ex);
					}
				});

				var isAuthenticated = tcs.Task.Result;

				if (!isAuthenticated)
				{
					NavigationService.Navigate<IHomeViewModel>().GetAwaiter().GetResult();
				}

				return tcs.Task;
			}
			catch (System.Exception exception)
			{
				throw exception.MvxWrap("Problem navigating to ViewModel {0}", typeof(ILoginViewModel).Name);
			}
		}
	}
}
