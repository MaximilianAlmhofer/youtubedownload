﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace xtra_snd_tools.Infra
{
	public static class ReflectionExtensions
	{
        public static string PrintAssemblyContents(string assemblyFile)
        {
            var sb = new StringBuilder();
            var assembly = Assembly.LoadFile(Path.GetFullPath(assemblyFile));
            sb.AppendLine(assembly.GetName().FullName);
            sb.AppendLine();
            assembly.ExportedTypes.Select(x => x.Namespace).Except(new[] { "TagLib", "TagLib.Id3v1", "TagLib.Id3v2" })?.ToList().ForEach(ns =>
            {
                sb.AppendLine();
                sb.AppendLine();
                sb.AppendLine(ns);
                sb.AppendLine("----------------------");
                sb.AppendLine("-");
                int i = 0;
                assembly.ExportedTypes.Where(t => t.Namespace.StartsWith(ns))?.ToList().ForEach(x =>
                {
                    sb.AppendLine($"{++i}. {x.Name}\t({x.Namespace})");
                });
                sb.AppendFormat("{0} types in namespace {1}", i, ns);
                sb.AppendLine();
            });
            return sb.ToString();
        }
    }


    public static class TaskExtensions
	{
        public static Task WithCancellation(this Task task, CancellationToken token)
        {
            return task.ContinueWith(t => t.GetAwaiter().GetResult(), token, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);
        }

        public static void TimeoutAfter(this CancellationToken cToken, long msTimeout, Action action)
		{
            using (cToken.CancelAfter(msTimeout))
			{
                action();
			}
		}

        public static Task TimeoutAfter(this CancellationToken cToken, long msTimeout, Func<Task> func)
        {
            using (cToken.CancelAfter(msTimeout))
            {
                return func();
            }
        }

        private static CancellationTokenSource CancelAfter (this CancellationToken cToken, long msTimeout)
        {
            var cts = CancellationTokenSource.CreateLinkedTokenSource(cToken);
            cts.CancelAfter(TimeSpan.FromMilliseconds(msTimeout));
            return cts;
        }
    }
}
