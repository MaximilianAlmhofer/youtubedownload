﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1030:Nach Möglichkeit Ereignisse verwenden", Justification = "Silent", Scope = "member", Target = "~M:xtra_snd_tools.Infra.ViewModels.NotifyPropertyChangeViewModel.RaisePropertyChanged(System.String)")]
[assembly: SuppressMessage("Design", "CA1030:Nach Möglichkeit Ereignisse verwenden", Justification = "Silent", Scope = "member", Target = "~M:xtra_snd_tools.Infra.ViewModels.NotifyPropertyChangeViewModel`1.RaisePropertyChanged(System.String)")]
[assembly: SuppressMessage("Usage", "CA2227:Sammlungseigenschaften müssen schreibgeschützt sein", Justification = "Silent", Scope = "member", Target = "~P:xtra_snd_tools.Infra.ViewModels.DataGridHierarchialDataModel.DataManager")]
[assembly: SuppressMessage("Design", "CA1062:Argumente von öffentlichen Methoden validieren", Justification = "Silent", Scope = "member", Target = "~M:xtra_snd_tools.Infra.Common.LoginParameters.#ctor(System.String,System.String)")]
[assembly: SuppressMessage("Reliability", "CA2000:Objekte verwerfen, bevor Bereich verloren geht", Justification = "None", Scope = "member", Target = "~M:xtra_snd_tools.Infra.WindowsLogin.UserCredentials.Impersonate(xtra_snd_tools.Infra.WindowsLogin.LogonType)~Microsoft.Win32.SafeHandles.SafeAccessTokenHandle")]
	