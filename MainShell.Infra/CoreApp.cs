﻿using MvvmCross.Core;
using MvvmCross.IoC;
using MvvmCross.Logging;
using MvvmCross.ViewModels;
using MvvmCross.Views;

using NLog;

using System.Threading;
using System.Windows.Threading;

using Lib.WindowsLogin;
using Lib.Common;

namespace xtra_snd_tools.Infra
{
	/// <summary>
	/// App-Wrapper stellt sicher, dass nur 1 Instanz existiert.
	/// </summary>
	public class AppSetup : MvxSetup
	{ 
		public class App : MvxApplication
		{
			internal AppSetup app;

			public override void Initialize()
			{
				app = AppSetup.Instance;

				app.CoreApp = this;

				CreatableTypes()
					.EndingWith("Service")
					.AsInterfaces()
					.RegisterAsDynamic();
				CreatableTypes()
					.EndingWith("ViewModel")
					.AsInterfaces()
					.RegisterAsDynamic();

				RegisterAppStart<IAppStartMarker>();
				RegisterCustomAppStart<CoreAppStart>();
			}
		}

		private static AppSetup instance;

		private AppSetup() { }

		public UserCredentials User { get; set; }

		public static new AppSetup Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new AppSetup();
				}
				return instance;
			}
		}

		public App CoreApp { get; private set; }


		public void ConnectCore(MvvmCross.Platforms.Wpf.Views.MvxApplication realApp)
		{

		}

		public override MvxLogProviderType GetDefaultLogProviderType()
		{
			return MvxLogProviderType.Console;
		}

		protected override IMvxApplication CreateApp()
		{
			return CoreApp;
		}

		protected override IMvxViewsContainer CreateViewsContainer()
		{
			return new MvvmCross.Platforms.Wpf.Views.MvxWpfViewsContainer();
		}

		protected override IMvxViewDispatcher CreateViewDispatcher()
		{
			return new MvvmCross.Platforms.Wpf.Views.MvxWpfViewDispatcher(Dispatcher.FromThread(Thread.CurrentThread),
				MvvmCross.Mvx.IoCProvider.Resolve<MvvmCross.Platforms.Wpf.Presenters.IMvxWpfViewPresenter>());
		}

		protected override IMvxNameMapping CreateViewToViewModelNaming()
		{
			return new MvxViewToViewModelNameMapping();
		}
	}
}