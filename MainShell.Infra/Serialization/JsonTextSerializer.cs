﻿using Newtonsoft.Json;

using System;

namespace xtra_snd_tools.Infra.Serialization
{
	internal sealed class JsonTextSerializer : MvvmCross.Base.IMvxTextSerializer
	{
		private readonly JsonSerializer serializer = new JsonSerializer();

		public object DeserializeObject(Type type, string inputText)
		{
			return JsonConvert.DeserializeObject(inputText, type, new JsonSerializerSettings { Formatting = Formatting.Indented });
		}

		public T DeserializeObject<T>(string inputText)
		{
			return (T)DeserializeObject(typeof(T), inputText);
		}

		public string SerializeObject(object toSerialise)
		{
			return JsonConvert.SerializeObject(toSerialise);
		}
	}
}
