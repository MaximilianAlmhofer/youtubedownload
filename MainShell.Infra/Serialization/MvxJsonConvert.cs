﻿using Lib.Common;

using System;
using System.Collections.Generic;
using System.Reflection;

namespace xtra_snd_tools.Infra.Serialization
{
	public static class MvxJsonConvert
	{
		public static T DeserializeObject<T>(string inputText)
		{
			return new JsonTextSerializer().DeserializeObject<T>(inputText);
		}

		public static object DeserializeObject(Type inputType, string inputText)
		{
			return new JsonTextSerializer().DeserializeObject(inputType, inputText);
		}

		public static string Serialize(object toSerialize)
		{
			return new JsonTextSerializer().SerializeObject(toSerialize);
		}

		public static IDictionary<string, string> SerializeToDictionary<T>(T value)
		{
			Dictionary<string, string> dict = new Dictionary<string, string>();

			foreach (var prop in typeof(T).GetTypeInfo().GetProperties(Globals.InstancePublicGetSetP))
			{
				object val = prop.GetValue(value, null);
				if (val != null)
				{
					dict[prop.Name] = Serialize(val);
				}
			}
			foreach (var prop in typeof(T).GetTypeInfo().GetProperties(Globals.InstanceNonPublicGetSetP))
			{
				object val = prop.GetValue(value, null);
				if (val != null)
				{
					if (!dict.ContainsKey(prop.Name))
					{
						dict[prop.Name] = Serialize(val);
					}
				}
			}
			return dict;
		}
	}
}
