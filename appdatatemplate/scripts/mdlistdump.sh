#!/bin/bash
youtube-dl --skip-download --playlist-start "$1" --playlist-end "$2" --yes-playlist -J "$3"
