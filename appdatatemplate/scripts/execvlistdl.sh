#!/bin/bash
youtube-dl -o $LISTOUTTEMPL --yes-playlist --playlist-start $1 --playlist-end $2 -x --format $3,best --audio-quality $4 $5 --restrict-filenames --add-metadata
