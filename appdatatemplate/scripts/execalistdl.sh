#!/bin/bash
youtube-dl -o $LISTOUTTEMPL --yes-playlist --playlist-start $1 --playlist-end $2 -x --audio-format $3 --format $4 --audio-quality $5 $6 --restrict-filenames --add-metadata
