$args.count -eq 2 || exit -1

Write-Host "Args count: " + $args.count

param([string]$feature = $args[1])

Write-Host "Argument: " + $feature

Get-WindowsOptionalFeature -Online -FeatureName $feature

Write-Host "Done"